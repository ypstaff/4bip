package il.ac.technion.cs.ssdl.cs234311.yp09.KeyboardsTest;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.FourButtonsFragment;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.SettingsActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.TypingActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.KeyboardFragment;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.KeyboardFragmentType;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.EditText;

import com.robotium.solo.Solo;

/**
 * @author Owais Musa
 * 
 */
public class AlphabeticalKeyboardTest extends
    ActivityInstrumentationTestCase2<TypingActivity> {

  private Solo solo;

  EditText messageEditText;

  /**
   * Default constructor needs this
   */
  public AlphabeticalKeyboardTest() {
    super(TypingActivity.class);
  }

  @Override
  public void setUp() {
    FourButtonsFragment.debugMode = true;
    KeyboardFragment.debugMode = true;
    solo = new Solo(getInstrumentation(), getActivity());

    TypingActivity.setKeyboardFragmentType(KeyboardFragmentType.ALPHABETICAL);
    solo.clickOnButton("5");
    solo.waitForActivity(SettingsActivity.class);
    solo.clickOnButton("8");
    solo.waitForActivity(TypingActivity.class);
  }

  @Override
  public void tearDown() throws Exception {
    TypingActivity.setKeyboardFragmentType(KeyboardFragmentType.DEFAULT);
    solo.clickOnButton("5");
    solo.waitForActivity(SettingsActivity.class);
    solo.clickOnButton("8");
    solo.waitForActivity(TypingActivity.class);
    solo.finishOpenedActivities();
  }

  private void preTest() {
    solo.assertCurrentActivity("wrong activity", TypingActivity.class);

    messageEditText = (EditText) solo.getView(R.id.message_edit);

    assertTrue("message_edit should appear in this activity!",
        messageEditText != null);
  }

  private void clickOnButton(String button) {
    if (button.equals("0"))
      solo.clickOnButton("1");
    if (button.equals("1"))
      solo.clickOnButton("9");
    if (button.equals("2"))
      solo.clickOnButton("2");
    if (button.equals("3"))
      solo.clickOnButton("a");
    if (button.equals("4"))
      solo.clickOnButton("3");
    if (button.equals("5"))
      solo.clickOnButton("b");
    if (button.equals("6"))
      solo.clickOnButton("4");
    if (button.equals("s"))
      solo.clickOnButton("c");
  }

  private void clickSeveralTimes(String button, int times) {
    assert times > 0;
    for (int i = 0; i < times; i++)
      clickOnButton(button);
  }

  /**
   * Simple test, choose some letters from main table row
   */
  public void testSimpleCase() {
    preTest();

    // Choose column 0 (blue)
    clickOnButton("0");

    solo.waitForText("A");

    // First character should be in upper case
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("A"));

    // Choose column 1 (blue + orange)
    clickOnButton("1");

    solo.waitForText("Ae");

    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Ae"));
  }

  /**
   * Test double clicks on the columns
   */
  public void testDoubleClick() {
    preTest();

    clickOnButton("0");
    solo.waitForText("A");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("A"));

    clickOnButton("1");
    solo.waitForText("Ae");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Ae"));

    clickSeveralTimes("2", 2);
    solo.waitForText("Aei");

    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Aei"));

    clickSeveralTimes("3", 4);
    solo.waitForText("Aeio");

    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Aeio"));

    clickSeveralTimes("6", 4);
    solo.waitForText("Aeioz");

    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Aeioz"));

    clickSeveralTimes("5", 4);

    // Wait for adding w to the text, when added, throw exception because it is
    // a bug!
    solo.waitForText("Aeiozv");

    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Aeioz"));

    clickSeveralTimes("4", 7);
    solo.waitForText("Aeiozq");

    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Aeiozq"));
  }

}
