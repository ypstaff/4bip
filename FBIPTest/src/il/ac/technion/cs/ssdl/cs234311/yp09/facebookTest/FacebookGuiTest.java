package il.ac.technion.cs.ssdl.cs234311.yp09.facebookTest;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import il.ac.technion.cs.ssdl.cs234311.yp09.facebookFeed.FacebookManagerStub;
import il.ac.technion.cs.ssdl.cs234311.yp09.facebookFeedGui.PostsActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.FourButtonsFragment;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.TypingActivity;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.TextView;

import com.robotium.solo.Solo;

/**
 * Test for PostsActivity
 * 
 * @author Owais Musa
 * 
 */
public class FacebookGuiTest extends
    ActivityInstrumentationTestCase2<PostsActivity> {

  private Solo solo;

  /**
   * Default constructor
   */
  public FacebookGuiTest() {
    super(PostsActivity.class);
  }

  private TextView mAuthorName;
  private TextView mText;
  private TextView mLikes;
  private TextView mComments;
  private TextView mShares;

  private void getViews() {
    mAuthorName = (TextView) solo.getView(R.id.postAuthorNameTextView);
    mText = (TextView) solo.getView(R.id.postTextTextView);
    mLikes = (TextView) solo.getView(R.id.postLikesNumTextView);
    mComments = (TextView) solo.getView(R.id.postCommentsNumTextView);
    mShares = (TextView) solo.getView(R.id.postSharesNumTextView);
  }

  @Override
  public void setUp() {
    FacebookManagerStub.debugMode = true;
    FourButtonsFragment.debugMode = true;
    solo = new Solo(getInstrumentation(), getActivity());
  }

  @Override
  public void tearDown() throws Exception {
    solo.finishOpenedActivities();
    FacebookManagerStub.debugMode = false;
  }

  /**
   * Basic text in post
   */
  public void testBasicFacebookGui() {
    solo.assertCurrentActivity("wrong activity", PostsActivity.class);

    FacebookManagerStub.resetPosts();
    FacebookManagerStub.addPost(null, "we have aa nice app");

    solo.clickOnButton("6");
    solo.waitForActivity(PostsActivity.class);
    solo.waitForText("we have aa nice app");

    getViews();

    assertTrue("we have aa nice app".equals(mText.getText().toString()));
    assertTrue("Dummy".equals(mAuthorName.getText().toString()));
    assertTrue("0".equals(mLikes.getText().toString()));
    assertTrue("0".equals(mComments.getText().toString()));
    assertTrue("0".equals(mShares.getText().toString()));
  }

  private static final short ORANGE_MASK = 4, GREEN_MASK = 8;

  /**
   * 
   */
  public void testLike() {
    solo.assertCurrentActivity("wrong activity", PostsActivity.class);

    FacebookManagerStub.resetPosts();
    FacebookManagerStub.addPost(null, "post to be liked");

    solo.clickOnButton("6");
    solo.waitForActivity(PostsActivity.class);
    solo.waitForText("post to be liked");

    getActivity().runOnUiThread(new Runnable() {

      @Override
      public void run() {
        getActivity().onOperation(GREEN_MASK | ORANGE_MASK);
      }
    });

    solo.clickOnButton("1");

    getViews();
    solo.waitForText("1");
    assertTrue("1".equals(mLikes.getText().toString()));
  }

  /**
   *
   */
  public void testShare() {
    solo.assertCurrentActivity("wrong activity", PostsActivity.class);

    FacebookManagerStub.resetPosts();
    FacebookManagerStub.addPost(null, "post to be liked");

    solo.clickOnButton("6");
    solo.waitForActivity(PostsActivity.class);
    solo.waitForText("post to be liked");

    getActivity().runOnUiThread(new Runnable() {

      @Override
      public void run() {
        getActivity().onOperation(GREEN_MASK | ORANGE_MASK);
      }
    });

    getViews();
    assertTrue("0".equals(mShares.getText().toString()));
    solo.clickOnButton("4");

    solo.clickOnButton("6");
    solo.waitForActivity(PostsActivity.class);
    solo.waitForText("post to be liked");

    getViews();
    solo.waitForText("1");
    String shares = mShares.getText().toString();
    assertTrue("1".equals(shares));
  }

  /**
  *
  */
  public void testComment() {
    solo.assertCurrentActivity("wrong activity", PostsActivity.class);

    FacebookManagerStub.resetPosts();
    FacebookManagerStub.addPost(null, "I love 4BIP");

    solo.clickOnButton("6");
    solo.waitForActivity(PostsActivity.class);
    solo.waitForText("I love 4BIP");

    getActivity().runOnUiThread(new Runnable() {

      @Override
      public void run() {
        getActivity().onOperation(GREEN_MASK | ORANGE_MASK);
      }
    });

    getViews();
    assertTrue("0".equals(mComments.getText().toString()));
    solo.clickOnButton("2");

    solo.waitForActivity(TypingActivity.class);
    solo.assertCurrentActivity("wrong activity", TypingActivity.class);
    solo.clickOnButton("1");
    solo.waitForText("A");
    solo.clickOnButton("8");
    solo.waitForActivity(PostsActivity.class);
    solo.assertCurrentActivity("wrong activity", PostsActivity.class);

    solo.clickOnButton("6");
    solo.waitForActivity(PostsActivity.class);
    solo.waitForText("I love 4BIP");

    getViews();
    solo.waitForText("1");
    String comments = mComments.getText().toString();
    assertTrue("1".equals(comments));
  }

  /**
  *
  */
  public void testPost() {
    solo.assertCurrentActivity("wrong activity", PostsActivity.class);

    FacebookManagerStub.resetPosts();
    FacebookManagerStub.addPost(null, "I love 4BIP");

    solo.clickOnButton("6");
    solo.waitForActivity(PostsActivity.class);
    solo.waitForText("I love 4BIP");

    solo.clickOnButton("7");

    solo.waitForActivity(TypingActivity.class);
    solo.assertCurrentActivity("wrong activity", TypingActivity.class);
    solo.clickOnButton("1");
    solo.waitForText("A");
    solo.clickOnButton("8");
    solo.waitForActivity(PostsActivity.class);
    solo.assertCurrentActivity("wrong activity", PostsActivity.class);

    solo.clickOnButton("6");
    solo.waitForActivity(PostsActivity.class);
    solo.waitForText("A");

    assertTrue(null != FacebookManagerStub.getPosts());
    assertTrue(2 == FacebookManagerStub.getPosts().size());
  }
}
