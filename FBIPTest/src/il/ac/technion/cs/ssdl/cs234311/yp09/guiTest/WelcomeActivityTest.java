package il.ac.technion.cs.ssdl.cs234311.yp09.guiTest;

import il.ac.technion.cs.ssdl.cs234311.yp09.gui.FourButtonsFragment;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.MainMenuActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.TypingActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.WelcomeActivity;
import android.test.ActivityInstrumentationTestCase2;

import com.robotium.solo.Solo;

/**
 * @author Owais
 * 
 */
public class WelcomeActivityTest extends
    ActivityInstrumentationTestCase2<WelcomeActivity> {

  private Solo solo;

  /**
   * Default constructor
   */
  public WelcomeActivityTest() {
    super(WelcomeActivity.class);
  }

  @Override
  public void setUp() {
    FourButtonsFragment.debugMode = true;
    solo = new Solo(getInstrumentation(), getActivity());
  }

  @Override
  public void tearDown() throws Exception {
    solo.finishOpenedActivities();
  }

  /**
   * Basic test
   */
  public void testActivityBase() {
    solo.assertCurrentActivity("wrong activity", WelcomeActivity.class);
    solo.clickOnButton("4");
    solo.waitForActivity(MainMenuActivity.class);
    solo.assertCurrentActivity("wrong activity", MainMenuActivity.class);
    solo.clickOnButton("1");
    solo.waitForActivity(TypingActivity.class);
    solo.assertCurrentActivity("wrong activity", TypingActivity.class);
  }
}
