package il.ac.technion.cs.ssdl.cs234311.yp09.guiTest;

import il.ac.technion.cs.ssdl.cs234311.yp09.gui.FourButtonsFragment;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.ServiceActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.TypingActivity;
import android.test.ActivityInstrumentationTestCase2;

import com.robotium.solo.Solo;

/**
 * @author Owais
 * 
 */
public class ServiceActivityTest extends
    ActivityInstrumentationTestCase2<TypingActivity> {

  private Solo solo;

  /**
   * Default constructor
   */
  public ServiceActivityTest() {
    super(TypingActivity.class);
  }

  @Override
  public void setUp() {
    FourButtonsFragment.debugMode = true;
    solo = new Solo(getInstrumentation(), getActivity());
  }

  @Override
  public void tearDown() throws Exception {
    solo.finishOpenedActivities();
  }

  /**
   * Basic test
   */
  public void testActivityBase() {
    solo.assertCurrentActivity("wrong activity", TypingActivity.class);
    solo.clickOnButton("8");
    solo.assertCurrentActivity("wrong activity", TypingActivity.class);

    solo.clickOnButton("1");
    solo.clickOnButton("8");
    solo.waitForActivity(ServiceActivity.class);
    solo.assertCurrentActivity("wrong activity", ServiceActivity.class);
  }

}
