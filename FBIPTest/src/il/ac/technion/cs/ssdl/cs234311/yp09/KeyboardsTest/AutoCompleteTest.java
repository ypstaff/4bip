package il.ac.technion.cs.ssdl.cs234311.yp09.KeyboardsTest;

import il.ac.technion.cs.ssdl.cs234311.yp09.gui.FourButtonsFragment;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.TypingActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.utilities.AutoComplete;

import java.util.List;

import android.test.ActivityInstrumentationTestCase2;

import com.robotium.solo.Solo;

/**
 * @author Husam Masalha tests the autoComplete class
 * 
 */
public class AutoCompleteTest extends
    ActivityInstrumentationTestCase2<TypingActivity> {

  private Solo solo;

  private AutoComplete autoComplete;

  /**
   * Default constructor needs this
   */
  public AutoCompleteTest() {
    super(TypingActivity.class);
  }

  @Override
  public void setUp() {
    solo = new Solo(getInstrumentation(), getActivity());
  }

  @Override
  public void tearDown() throws Exception {
    solo.finishOpenedActivities();
  }

  private void preTest() {
    solo.assertCurrentActivity("wrong activity", TypingActivity.class);
    FourButtonsFragment.debugMode = false;
    autoComplete = getActivity().autoComplete;

    assertTrue("this activity should include xml parser!", autoComplete != null);
  }

  private static void sleepFor30miliSec() {
    try {
      Thread.sleep(10);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  /**
   * Test basic scenario
   */
  public void testWordLen1() {
    // An example for testing auto complete

    // Initialize xmlParser!
    preTest();

    List<String> res = null;
    sleepFor30miliSec();
    res = autoComplete.getWordCompletion("a");

    assertTrue("result should not be null!", res != null);
    if (res == null)
      return;
    assertTrue("result should be with three options!", res.size() == 3);
    assertTrue("the first word should be and", res.get(0).equals("and"));
    assertTrue("the secound word should be at", res.get(1).equals("at"));
    assertTrue("the therd word should be as", res.get(2).equals("as"));

    sleepFor30miliSec();
    res = autoComplete.getWordCompletion("m");

    assertTrue("result should not be null!", res != null);
    if (res == null)
      return;
    assertTrue("result should be with three options!", res.size() == 3);
    assertTrue("the first word should be my", res.get(0).equals("my"));
    assertTrue("the secound word should be make", res.get(1).equals("make"));
    assertTrue("the therd word should be me", res.get(2).equals("me"));

    sleepFor30miliSec();
    res = autoComplete.getWordCompletion("x");

    assertTrue("result should not be null!", res != null);
    if (res == null)
      return;
    assertTrue("no words start with x!", res.size() == 0);

  }

  /**
   * tests auto complete capital letter
   */
  public void testWordLen1WithCL() {
    // An example for testing auto complete

    // Initialize xmlParser!
    preTest();

    List<String> res = null;

    sleepFor30miliSec();
    res = autoComplete.getWordCompletion("A");

    assertTrue("result should not be null!", res != null);
    if (res == null)
      return;
    assertTrue("result should be with three options!", res.size() == 3);
    assertTrue("the first word should be And", res.get(0).equals("And"));
    assertTrue("the secound word should be At", res.get(1).equals("At"));
    assertTrue("the therd word should be As", res.get(2).equals("As"));

    sleepFor30miliSec();
    res = autoComplete.getWordCompletion("M");

    assertTrue("result should not be null!", res != null);
    if (res == null)
      return;
    assertTrue("result should be with three options!", res.size() == 3);
    assertTrue("the first word should be My", res.get(0).equals("My"));
    assertTrue("the secound word should be Make", res.get(1).equals("Make"));
    assertTrue("the therd word should be Me", res.get(2).equals("Me"));

    sleepFor30miliSec();
    res = autoComplete.getWordCompletion("X");

    assertTrue("result should not be null!", res != null);
    if (res == null)
      return;
    assertTrue("no words start with x!", res.size() == 0);

    sleepFor30miliSec();
    res = autoComplete.getWordCompletion("K");

    assertTrue("result should not be null!", res != null);
    if (res == null)
      return;
    assertTrue("result should be with three options!", res.size() == 3);
    assertTrue("the first word should be Know", res.get(0).equals("Know"));
    assertTrue("the secound word should be Keep", res.get(1).equals("Keep"));
    assertTrue("the therd word should be Kind", res.get(2).equals("Kind"));
  }

  /**
   * test the autoComplete with continue word with the same prefix
   */
  public void testMoreThanOneWord() {
    // An example for testing auto complete

    // Initialize xmlParser!
    preTest();

    List<String> res = null;

    sleepFor30miliSec();
    res = autoComplete.getWordCompletion("a");

    assertTrue("result should not be null!", res != null);
    if (res == null)
      return;
    assertTrue("result should be with three options!", res.size() == 3);
    assertTrue("the first word should be And", res.get(0).equals("and"));
    assertTrue("the secound word should be At", res.get(1).equals("at"));
    assertTrue("the therd word should be As", res.get(2).equals("as"));

    sleepFor30miliSec();
    res = autoComplete.getWordCompletion("at");

    assertTrue("result should not be null!", res != null);
    if (res == null)
      return;
    assertTrue("result should be with three options!", res.size() == 3);
    assertTrue("the first word should be attention",
        res.get(0).equals("attention"));
    assertTrue("the secound word should be attack", res.get(1).equals("attack"));
    assertTrue("the therd word should be attorney",
        res.get(2).equals("attorney"));

    sleepFor30miliSec();
    res = autoComplete.getWordCompletion("att");

    assertTrue("result should be with three options!", res.size() == 3);
    assertTrue("the first word should be attention",
        res.get(0).equals("attention"));
    assertTrue("the secound word should be attack", res.get(1).equals("attack"));
    assertTrue("the therd word should be attorney",
        res.get(2).equals("attorney"));

    sleepFor30miliSec();
    res = autoComplete.getWordCompletion("atta");

    assertTrue("result should not be null!", res != null);
    if (res == null)
      return;
    assertTrue("result should be with two options!", res.size() == 2);
    assertTrue("the first word should be attack", res.get(0).equals("attack"));
    assertTrue("the secound word should be attach", res.get(1).equals("attach"));

  }

  /**
   * test the autoComplete with continue word with the same prefix,with
   * inserting Capital letters to the words
   */
  public void testMoreThanOneWordCL() {
    // An example for testing auto complete

    // Initialize xmlParser!
    preTest();

    List<String> res = null;

    sleepFor30miliSec();
    res = autoComplete.getWordCompletion("a");

    assertTrue("result should not be null!", res != null);
    if (res == null)
      return;
    assertTrue("result should be with three options!", res.size() == 3);
    assertTrue("the first word should be And", res.get(0).equals("and"));
    assertTrue("the secound word should be At", res.get(1).equals("at"));
    assertTrue("the therd word should be As", res.get(2).equals("as"));

    sleepFor30miliSec();
    res = autoComplete.getWordCompletion("aT");

    assertTrue("result should not be null!", res != null);
    if (res == null)
      return;
    assertTrue("result should be with three options!", res.size() == 3);
    assertTrue("the first word should be aTtention",
        res.get(0).equals("aTtention"));
    assertTrue("the secound word should be aTtack", res.get(1).equals("aTtack"));
    assertTrue("the therd word should be aTtorney",
        res.get(2).equals("aTtorney"));

    sleepFor30miliSec();
    res = autoComplete.getWordCompletion("Att");

    assertTrue("result should be with three options!", res.size() == 3);
    assertTrue("the first word should be Attention",
        res.get(0).equals("Attention"));
    assertTrue("the secound word should be Attack", res.get(1).equals("Attack"));
    assertTrue("the therd word should be Attorney",
        res.get(2).equals("Attorney"));

    sleepFor30miliSec();
    res = autoComplete.getWordCompletion("atTA");

    assertTrue("result should not be null!", res != null);
    if (res == null)
      return;
    assertTrue("result should be with two options!", res.size() == 2);
    assertTrue("the first word should be atTAck", res.get(0).equals("atTAck"));
    assertTrue("the secound word should be atTAch", res.get(1).equals("atTAch"));

  }

  /**
   * test the autoComplete with continue word with not the same prefix
   */
  public void testMoreThanOneWordNotSameStart() {
    // An example for testing auto complete

    // Initialize xmlParser!
    preTest();

    List<String> res = null;

    sleepFor30miliSec();
    res = autoComplete.getWordCompletion("a");

    assertTrue("result should not be null!", res != null);
    if (res == null)
      return;
    assertTrue("result should be with three options!", res.size() == 3);
    assertTrue("the first word should be And", res.get(0).equals("and"));
    assertTrue("the secound word should be At", res.get(1).equals("at"));
    assertTrue("the therd word should be As", res.get(2).equals("as"));

    sleepFor30miliSec();
    res = autoComplete.getWordCompletion("ma");

    assertTrue("result should not be null!", res != null);
    if (res == null)
      return;
    assertTrue("result should be with three options!", res.size() == 3);
    assertTrue("the first word should be male", res.get(0).equals("make"));
    assertTrue("the secound word should be map", res.get(1).equals("man"));
    assertTrue("the therd word should be married", res.get(2).equals("many"));

    sleepFor30miliSec();
    res = autoComplete.getWordCompletion("pan");

    assertTrue("result should be with three options!", res.size() == 3);
    assertTrue("the first word should be panel", res.get(0).equals("panel"));
    assertTrue("the secound word should be pant", res.get(1).equals("pant"));
    assertTrue("the therd word should be panic", res.get(2).equals("panic"));

  }

  /**
   * test the autoComplete with continue word with not the same prefix,with
   * inserting Capital letters to the words
   */
  public void testMoreThanOneWordNotSameStartCL() {
    // An example for testing auto complete

    // Initialize xmlParser!
    preTest();

    List<String> res = null;

    sleepFor30miliSec();
    res = autoComplete.getWordCompletion("A");

    assertTrue("result should not be null!", res != null);
    if (res == null)
      return;
    assertTrue("result should be with three options!", res.size() == 3);
    assertTrue("the first word should be And", res.get(0).equals("And"));
    assertTrue("the secound word should be At", res.get(1).equals("At"));
    assertTrue("the therd word should be As", res.get(2).equals("As"));

    sleepFor30miliSec();
    res = autoComplete.getWordCompletion("mA");

    assertTrue("result should not be null!", res != null);
    if (res == null)
      return;
    assertTrue("result should be with three options!", res.size() == 3);
    assertTrue("the first word should be male", res.get(0).equals("mAke"));
    assertTrue("the secound word should be map", res.get(1).equals("mAn"));
    assertTrue("the therd word should be married", res.get(2).equals("mAny"));

    sleepFor30miliSec();
    res = autoComplete.getWordCompletion("paN");
    if (res == null)
      return;
    assertTrue("result should be with three options!", res.size() == 3);
    assertTrue("the first word should be panel", res.get(0).equals("paNel"));
    assertTrue("the secound word should be pant", res.get(1).equals("paNt"));
    assertTrue("the therd word should be panic", res.get(2).equals("paNic"));

  }

}
