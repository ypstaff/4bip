package il.ac.technion.cs.ssdl.cs234311.yp09.KeyboardsTest;
//package il.ac.technion.cs.ssdl.cs234311.yp09.keyboardsTest;
//
//import il.ac.technion.cs.ssdl.cs234311.yp09.gui.FourButtonsFragment;
//import il.ac.technion.cs.ssdl.cs234311.yp09.gui.LoginActivity;
//import il.ac.technion.cs.ssdl.cs234311.yp09.gui.TypingActivity;
//import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.KeyboardFragment;
//import android.test.ActivityInstrumentationTestCase2;
//import android.widget.EditText;
//
//import com.robotium.solo.Solo;
//
//public class KeyboardInLoginTest extends
//    ActivityInstrumentationTestCase2<LoginActivity> {
//  private Solo solo;
//
//  EditText messageEditText;
//
//  /**
//   * Default constructor needs this
//   */
//  public KeyboardInLoginTest() {
//    super(LoginActivity.class);
//  }
//
//  @Override
//  public void setUp() {
//    FourButtonsFragment.debugMode = true;
//    KeyboardFragment.debugMode = true;
//    solo = new Solo(getInstrumentation(), getActivity());
//  }
//
//  @Override
//  public void tearDown() throws Exception {
//    solo.finishOpenedActivities();
//  }
//
//  private void preTest() {
//    solo.assertCurrentActivity("wrong activity", TypingActivity.class);
//
//    messageEditText = (EditText) solo
//        .getView(com.example.graphics.R.id.message_edit);
//
//    assertTrue("message_edit should appear in this activity!",
//        messageEditText != null);
//  }
//
//  private void clickOnButton(String button) {
//    if (button.equals("0"))
//      solo.clickOnButton("1");
//    if (button.equals("1"))
//      solo.clickOnButton("9");
//    if (button.equals("2"))
//      solo.clickOnButton("2");
//    if (button.equals("3"))
//      solo.clickOnButton("a");
//    if (button.equals("4"))
//      solo.clickOnButton("3");
//    if (button.equals("5"))
//      solo.clickOnButton("b");
//    if (button.equals("6"))
//      solo.clickOnButton("4");
//    if (button.equals("s"))
//      solo.clickOnButton("c");
//  }
//
//  private void clickSeveralTimes(String button, int times) {
//    assert times > 0;
//    for (int i = 0; i < times; i++)
//      clickOnButton(button);
//  }
//
//  public void testBasic() {
//
//  }
// }
