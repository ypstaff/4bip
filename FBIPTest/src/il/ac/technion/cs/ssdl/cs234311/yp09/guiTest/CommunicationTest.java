package il.ac.technion.cs.ssdl.cs234311.yp09.guiTest;

import il.ac.technion.cs.ssdl.cs234311.yp09.gui.ContactActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.ContactHistoryActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.FourButtonsFragment;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.LoginActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.MainMenuActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.ServiceActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.SplashActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.TypingActivity;
import android.test.ActivityInstrumentationTestCase2;

import com.robotium.solo.Solo;

/**
 * @author Muhammad
 * 
 */
public class CommunicationTest extends
    ActivityInstrumentationTestCase2<SplashActivity> {

  private Solo solo;

  /**
   * Default constructor
   */
  public CommunicationTest() {
    super(SplashActivity.class);
  }

  @Override
  public void setUp() {
    FourButtonsFragment.debugMode = true;
    solo = new Solo(getInstrumentation(), getActivity());
  }

  @Override
  public void tearDown() throws Exception {
    solo.finishOpenedActivities();
  }

  /**
   * testing contacts activity
   */
  public void testContacts() {
    solo.assertCurrentActivity("Error 1", SplashActivity.class);
    solo.clickOnButton("4");
    solo.waitForActivity(MainMenuActivity.class);
    solo.assertCurrentActivity("Error 2", MainMenuActivity.class);
    solo.clickOnButton("1");
    solo.waitForActivity(TypingActivity.class);
    solo.assertCurrentActivity("Error 3", TypingActivity.class);
    solo.clickOnButton("4");
    solo.waitForActivity(TypingActivity.class);
    solo.assertCurrentActivity("Error 4", TypingActivity.class);
    solo.clickOnButton("8");
    solo.waitForActivity(ServiceActivity.class);
    solo.assertCurrentActivity("Error 5", ServiceActivity.class);
    solo.clickOnButton("4");
    solo.waitForActivity(ContactActivity.class);
    solo.assertCurrentActivity("Error 6", ContactActivity.class);
    solo.clickOnButton("6");
    solo.waitForActivity(TypingActivity.class);
    solo.assertCurrentActivity("Error 7", TypingActivity.class);
  }

  /**
   * testing facebook service
   */
  public void testFacebook() {
    solo.assertCurrentActivity("Error 8", SplashActivity.class);
    solo.clickOnButton("4");
    solo.waitForActivity(MainMenuActivity.class);
    solo.assertCurrentActivity("Error 9", MainMenuActivity.class);
    solo.clickOnButton("1");
    solo.waitForActivity(TypingActivity.class);
    solo.assertCurrentActivity("Error 10", TypingActivity.class);
    solo.clickOnButton("4");
    solo.waitForActivity(TypingActivity.class);
    solo.assertCurrentActivity("Error 11", TypingActivity.class);
    solo.clickOnButton("8");
    solo.waitForActivity(ServiceActivity.class);
    solo.assertCurrentActivity("Error 12", ServiceActivity.class);
    solo.clickOnButton("3");
    solo.waitForActivity(ServiceActivity.class);
    solo.assertCurrentActivity("Error 13", ServiceActivity.class);
    solo.clickOnButton("4");
    solo.waitForActivity(ServiceActivity.class);
    solo.assertCurrentActivity("Error 14", ServiceActivity.class);
    solo.clickOnButton("4");
    solo.waitForActivity(LoginActivity.class);
    solo.assertCurrentActivity("Error 15", LoginActivity.class);
    solo.clickOnButton("6");
    solo.waitForActivity(TypingActivity.class);
    solo.assertCurrentActivity("Error 16", TypingActivity.class);
  }

  /**
   * testing GTalk service
   */
  public void testGTalk() {
    solo.assertCurrentActivity("Error 17", SplashActivity.class);
    solo.clickOnButton("4");
    solo.waitForActivity(MainMenuActivity.class);
    solo.assertCurrentActivity("Error 18", MainMenuActivity.class);
    solo.clickOnButton("1");
    solo.waitForActivity(TypingActivity.class);
    solo.assertCurrentActivity("Error 19", TypingActivity.class);
    solo.clickOnButton("4");
    solo.waitForActivity(TypingActivity.class);
    solo.assertCurrentActivity("Error 20", TypingActivity.class);
    solo.clickOnButton("8");
    solo.waitForActivity(ServiceActivity.class);
    solo.assertCurrentActivity("Error 21", ServiceActivity.class);
    solo.clickOnButton("3");
    solo.waitForActivity(ServiceActivity.class);
    solo.assertCurrentActivity("Error 22", ServiceActivity.class);
    solo.clickOnButton("3");
    solo.waitForActivity(ServiceActivity.class);
    solo.assertCurrentActivity("Error 23", ServiceActivity.class);
    solo.clickOnButton("4");
    solo.waitForActivity(ServiceActivity.class);
    solo.assertCurrentActivity("Error 24", ServiceActivity.class);
    solo.clickOnButton("4");
    solo.waitForActivity(LoginActivity.class);
    solo.assertCurrentActivity("Error 25", LoginActivity.class);
    solo.clickOnButton("6");
    solo.waitForActivity(TypingActivity.class);
    solo.assertCurrentActivity("Error 26", TypingActivity.class);
  }

  /**
   * testing history activity
   */
  public void testHistory() {
    solo.assertCurrentActivity("Error 27", SplashActivity.class);
    solo.clickOnButton("4");
    solo.waitForActivity(MainMenuActivity.class);
    solo.assertCurrentActivity("Error 28", MainMenuActivity.class);
    solo.clickOnButton("1");
    solo.waitForActivity(TypingActivity.class);
    solo.assertCurrentActivity("Error 29", TypingActivity.class);
    solo.clickOnButton("4");
    solo.waitForActivity(TypingActivity.class);
    solo.assertCurrentActivity("Error 30", TypingActivity.class);
    solo.clickOnButton("6");
    solo.waitForActivity(ServiceActivity.class);
    solo.assertCurrentActivity("Error 31", ServiceActivity.class);
    solo.clickOnButton("4");
    solo.waitForActivity(ContactHistoryActivity.class);
    solo.assertCurrentActivity("Error 32", ContactHistoryActivity.class);
  }

}
