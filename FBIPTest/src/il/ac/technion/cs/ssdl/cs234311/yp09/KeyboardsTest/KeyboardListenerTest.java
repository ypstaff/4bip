package il.ac.technion.cs.ssdl.cs234311.yp09.KeyboardsTest;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.FourButtonsFragment;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.TypingActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.KeyboardFragment;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.KeyboardListener;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.EditText;

import com.robotium.solo.Solo;

/**
 * @author Owais Musa
 * 
 */
public class KeyboardListenerTest extends
    ActivityInstrumentationTestCase2<TypingActivity> {
  private Solo solo;

  KeyboardListener mKeybaordListener;
  private EditText messageEditText;

  /**
   * Default constructor needs this
   */
  public KeyboardListenerTest() {
    super(TypingActivity.class);
  }

  @Override
  public void setUp() {
    FourButtonsFragment.debugMode = true;
    KeyboardFragment.debugMode = true;
    solo = new Solo(getInstrumentation(), getActivity());
  }

  @Override
  public void tearDown() throws Exception {
    solo.finishOpenedActivities();
  }

  private void preTest() {
    solo.assertCurrentActivity("wrong activity", TypingActivity.class);

    messageEditText = (EditText) solo.getView(R.id.message_edit);

    assertTrue("message_edit should appear in this activity!",
        messageEditText != null);

    mKeybaordListener = new KeyboardListener(messageEditText, false);
  }

  /**
   * Testing the method insertChar
   * 
   * @throws Exception
   *           in case TextController throws an exception
   */
  public void testInsertChar() throws Exception {
    preTest();

    getActivity().runOnUiThread(new Runnable() {
      @Override
      public void run() {
        int iCursorPossition = mKeybaordListener.getCursorPosition();
        String MessageText = mKeybaordListener.getText();

        if (iCursorPossition != 0 || !MessageText.equals(""))
          fail();

        mKeybaordListener.insertChar(' ');

        iCursorPossition = mKeybaordListener.getCursorPosition();
        MessageText = mKeybaordListener.getText();

        if (iCursorPossition != 1 || !MessageText.equals(" "))
          fail();

        mKeybaordListener.insertChar('!');
        mKeybaordListener.insertChar('!');
        mKeybaordListener.insertChar('t');
        mKeybaordListener.insertChar('e');
        mKeybaordListener.insertChar('s');
        mKeybaordListener.insertChar('t');
        mKeybaordListener.insertChar(':');
        mKeybaordListener.insertChar(')');

        iCursorPossition = mKeybaordListener.getCursorPosition();
        MessageText = mKeybaordListener.getText();

        if (iCursorPossition != 9 || !MessageText.equals(" !!test:)"))
          fail();

        mKeybaordListener.moveCurserToTheLeft();
        mKeybaordListener.moveCurserToTheLeft();
        mKeybaordListener.moveCurserToTheLeft();

        mKeybaordListener.insertChar(':');
        mKeybaordListener.insertChar('(');

        iCursorPossition = mKeybaordListener.getCursorPosition();
        MessageText = mKeybaordListener.getText();

        if (iCursorPossition != 8 || !MessageText.equals(" !!tes:(t:)"))
          fail();
      }
    });

  }

  /**
   * Testing the method insertChars
   * 
   * @throws Exception
   *           in case TextController throws an exception
   */
  public void testInsertChars() throws Exception {
    preTest();

    getActivity().runOnUiThread(new Runnable() {
      @Override
      public void run() {
        int iCursorPossition = mKeybaordListener.getCursorPosition();
        String MessageText = mKeybaordListener.getText();

        if (iCursorPossition != 0 || !MessageText.equals(""))
          fail();

        mKeybaordListener.insertChars("good test:)");

        iCursorPossition = mKeybaordListener.getCursorPosition();
        MessageText = mKeybaordListener.getText();

        if (iCursorPossition != 11 || !MessageText.equals("good test:)"))
          fail();

        for (int i = 0; i < 5; i++)
          mKeybaordListener.moveCurserToTheLeft();
        mKeybaordListener.insertChars("!!!!");

        iCursorPossition = mKeybaordListener.getCursorPosition();
        MessageText = mKeybaordListener.getText();

        if (iCursorPossition != 10 || !MessageText.equals("good t!!!!est:)"))
          fail();
      }
    });
  }

  /**
   * Testing the method deleteChar
   * 
   * @throws Exception
   *           in case TextController throws an exception
   */
  public void testDeletetChar() throws Exception {
    preTest();

    getActivity().runOnUiThread(new Runnable() {
      @Override
      public void run() {
        int iCursorPossition = mKeybaordListener.getCursorPosition();
        String MessageText = mKeybaordListener.getText();

        if (iCursorPossition != 0 || !MessageText.equals(""))
          fail();

        mKeybaordListener.insertChars("good test:)");

        iCursorPossition = mKeybaordListener.getCursorPosition();
        MessageText = mKeybaordListener.getText();

        if (iCursorPossition != 11 || !MessageText.equals("good test:)"))
          fail();

        for (int i = 0; i < 5; i++)
          mKeybaordListener.deleteChar();

        iCursorPossition = mKeybaordListener.getCursorPosition();
        MessageText = mKeybaordListener.getText();

        if (iCursorPossition != 6 || !MessageText.equals("good t"))
          fail();

        for (int i = 0; i < 10; i++)
          mKeybaordListener.moveCurserToTheLeft(); // now cursor = 0

        iCursorPossition = mKeybaordListener.getCursorPosition();
        MessageText = mKeybaordListener.getText();

        if (iCursorPossition != 0 || !MessageText.equals("good t"))
          fail();

        for (int i = 0; i < 5; i++)
          mKeybaordListener.deleteChar(); // do nothing!

        iCursorPossition = mKeybaordListener.getCursorPosition();
        MessageText = mKeybaordListener.getText();

        if (iCursorPossition != 0 || !MessageText.equals("good t"))
          fail();

        mKeybaordListener.deleteAll();

        iCursorPossition = mKeybaordListener.getCursorPosition();
        MessageText = mKeybaordListener.getText();

        if (iCursorPossition != 0 || !MessageText.equals(""))
          fail();

        for (int i = 0; i < 5; i++)
          mKeybaordListener.deleteChar(); // do nothing!

        iCursorPossition = mKeybaordListener.getCursorPosition();
        MessageText = mKeybaordListener.getText();

        if (iCursorPossition != 0 || !MessageText.equals(""))
          fail();
      }
    });

  }

  /**
   * Testing the method moveCursorToTheRight and moveCursorToTheLeft
   * 
   * @throws Exception
   *           in case TextController throws an exception
   */
  public void testMoveCurserToTheRightAndLeft() throws Exception {
    preTest();

    getActivity().runOnUiThread(new Runnable() {
      @Override
      public void run() {
        int iCursorPossition = mKeybaordListener.getCursorPosition();
        String MessageText = mKeybaordListener.getText();

        if (iCursorPossition != 0 || !MessageText.equals(""))
          fail();

        for (int i = 0; i < 20; i++)
          mKeybaordListener.moveCurserToTheRight(); // do nothing!

        iCursorPossition = mKeybaordListener.getCursorPosition();
        MessageText = mKeybaordListener.getText();

        if (iCursorPossition != 0 || !MessageText.equals(""))
          fail();

        for (int i = 0; i < 20; i++)
          mKeybaordListener.moveCurserToTheLeft(); // do nothing!

        iCursorPossition = mKeybaordListener.getCursorPosition();
        MessageText = mKeybaordListener.getText();

        if (iCursorPossition != 0 || !MessageText.equals(""))
          fail();

        mKeybaordListener.insertChars("my test!!!");

        iCursorPossition = mKeybaordListener.getCursorPosition();
        MessageText = mKeybaordListener.getText();

        if (iCursorPossition != 10 || !MessageText.equals("my test!!!"))
          fail();

        for (int i = 0; i < 20; i++)
          mKeybaordListener.moveCurserToTheRight(); // do nothing!

        iCursorPossition = mKeybaordListener.getCursorPosition();
        MessageText = mKeybaordListener.getText();

        if (iCursorPossition != 10 || !MessageText.equals("my test!!!"))
          fail();

        for (int i = 0; i < 5; i++)
          mKeybaordListener.moveCurserToTheLeft();

        iCursorPossition = mKeybaordListener.getCursorPosition();
        MessageText = mKeybaordListener.getText();

        if (iCursorPossition != 5 || !MessageText.equals("my test!!!"))
          fail();

        for (int i = 0; i < 4; i++)
          mKeybaordListener.moveCurserToTheRight();

        iCursorPossition = mKeybaordListener.getCursorPosition();
        MessageText = mKeybaordListener.getText();

        if (iCursorPossition != 9 || !MessageText.equals("my test!!!"))
          fail();

        for (int i = 0; i < 5; i++)
          mKeybaordListener.moveCurserToTheRight();

        iCursorPossition = mKeybaordListener.getCursorPosition();
        MessageText = mKeybaordListener.getText();

        if (iCursorPossition != 10 || !MessageText.equals("my test!!!"))
          fail();

        for (int i = 0; i < 50; i++)
          mKeybaordListener.moveCurserToTheLeft();

        iCursorPossition = mKeybaordListener.getCursorPosition();
        MessageText = mKeybaordListener.getText();

        if (iCursorPossition != 0 || !MessageText.equals("my test!!!"))
          fail();

        mKeybaordListener.moveCurserToTheRight();

        iCursorPossition = mKeybaordListener.getCursorPosition();
        MessageText = mKeybaordListener.getText();

        if (iCursorPossition != 1 || !MessageText.equals("my test!!!"))
          fail();
      }
    });

  }

  /**
   * Testing the method deleteAll
   * 
   * @throws Exception
   *           in case TextController throws an exception
   */
  public void testDeleteAll() throws Exception {
    preTest();
    getActivity().runOnUiThread(new Runnable() {
      @Override
      public void run() {
        int iCursorPossition = mKeybaordListener.getCursorPosition();
        String MessageText = mKeybaordListener.getText();

        if (iCursorPossition != 0 || !MessageText.equals(""))
          fail();

        mKeybaordListener.insertChars("good test:)");

        iCursorPossition = mKeybaordListener.getCursorPosition();
        MessageText = mKeybaordListener.getText();

        if (iCursorPossition != 11 || !MessageText.equals("good test:)"))
          fail();

        mKeybaordListener.deleteAll();

        iCursorPossition = mKeybaordListener.getCursorPosition();
        MessageText = mKeybaordListener.getText();

        if (iCursorPossition != 0 || !MessageText.equals(""))
          fail();

        for (int i = 0; i < 5; i++)
          mKeybaordListener.deleteAll(); // do nothing!

        iCursorPossition = mKeybaordListener.getCursorPosition();
        MessageText = mKeybaordListener.getText();

        if (iCursorPossition != 0 || !MessageText.equals(""))
          fail();

        mKeybaordListener.insertChars("interesting test!");
        for (int i = 0; i < 10; i++)
          mKeybaordListener.moveCurserToTheLeft();

        iCursorPossition = mKeybaordListener.getCursorPosition();
        MessageText = mKeybaordListener.getText();

        if (iCursorPossition != 7 || !MessageText.equals("interesting test!"))
          fail();

        mKeybaordListener.deleteAll();

        iCursorPossition = mKeybaordListener.getCursorPosition();
        MessageText = mKeybaordListener.getText();

        if (iCursorPossition != 0 || !MessageText.equals(""))
          fail();

      }
    });
  }

  /**
   * Testing the method undoLastOp
   * 
   * @throws Exception
   *           in case TextController throws an exception
   */
  public void testUndoLastOp() throws Exception {
    preTest();

    getActivity().runOnUiThread(new Runnable() {
      @Override
      public void run() {
        int iCursorPossition = mKeybaordListener.getCursorPosition();
        String MessageText = mKeybaordListener.getText();

        if (iCursorPossition != 0 || !MessageText.equals(""))
          fail();

        mKeybaordListener.undo(); // do nothing

        iCursorPossition = mKeybaordListener.getCursorPosition();
        MessageText = mKeybaordListener.getText();

        if (iCursorPossition != 0 || !MessageText.equals(""))
          fail();

        mKeybaordListener.insertChars("interesting test");

        iCursorPossition = mKeybaordListener.getCursorPosition();
        MessageText = mKeybaordListener.getText();

        if (iCursorPossition != 16 || !MessageText.equals("interesting test"))
          fail();

        mKeybaordListener.undo();

        iCursorPossition = mKeybaordListener.getCursorPosition();
        MessageText = mKeybaordListener.getText();

        if (iCursorPossition != 0 || !MessageText.equals(""))
          fail();
      }
    });
  }

  /**
   * Testing the method getCursorPosition
   * 
   * @throws Exception
   *           in case TextController throws an exception
   */
  public void testGetCursorPosition() throws Exception {
    preTest();

    getActivity().runOnUiThread(new Runnable() {
      @Override
      public void run() {
        int iCursorPossition = mKeybaordListener.getCursorPosition();
        String MessageText = mKeybaordListener.getText();

        if (iCursorPossition != 0 || !MessageText.equals(""))
          fail();

        mKeybaordListener.insertChars("simple test");

        iCursorPossition = mKeybaordListener.getCursorPosition();
        MessageText = mKeybaordListener.getText();

        if (iCursorPossition != 11 || !MessageText.equals("simple test"))
          fail();
      }
    });
  }

  /**
   * Testing the method getText
   * 
   * @throws Exception
   *           in case TextController throws an exception
   */
  public void testGetText() throws Exception {
    preTest();

    getActivity().runOnUiThread(new Runnable() {
      @Override
      public void run() {
        int iCursorPossition = mKeybaordListener.getCursorPosition();
        String MessageText = mKeybaordListener.getText();

        if (iCursorPossition != 0 || !MessageText.equals(""))
          fail();

        mKeybaordListener.insertChars("simple test");

        iCursorPossition = mKeybaordListener.getCursorPosition();
        MessageText = mKeybaordListener.getText();

        if (iCursorPossition != 11 || !MessageText.equals("simple test"))
          fail();
      }
    });
  }

}
