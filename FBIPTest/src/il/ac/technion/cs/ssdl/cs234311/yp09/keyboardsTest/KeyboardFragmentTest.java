package il.ac.technion.cs.ssdl.cs234311.yp09.KeyboardsTest;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.FourButtonsFragment;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.TypingActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.KeyboardFragment;

import java.util.Stack;

import android.annotation.TargetApi;
import android.os.Build;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.EditText;

import com.robotium.solo.Solo;

/**
 * @author Owais Musa
 * 
 */
@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1)
public class KeyboardFragmentTest extends
    ActivityInstrumentationTestCase2<TypingActivity> {

  private Solo solo;

  EditText messageEditText;

  /**
   * Default constructor needs this
   */
  public KeyboardFragmentTest() {
    super(TypingActivity.class);
  }

  @Override
  public void setUp() {
    FourButtonsFragment.debugMode = true;
    KeyboardFragment.debugMode = true;
    solo = new Solo(getInstrumentation(), getActivity());
  }

  @Override
  public void tearDown() throws Exception {
    solo.finishOpenedActivities();
  }

  private void preTest() {
    solo.assertCurrentActivity("wrong activity", TypingActivity.class);

    messageEditText = (EditText) solo.getView(R.id.message_edit);

    assertTrue("message_edit should appear in this activity!",
        messageEditText != null);
  }

  private void clickOnButton(String button) {
    if (button.equals("0"))
      solo.clickOnButton("1");
    if (button.equals("1"))
      solo.clickOnButton("9");
    if (button.equals("2"))
      solo.clickOnButton("2");
    if (button.equals("3"))
      solo.clickOnButton("a");
    if (button.equals("4"))
      solo.clickOnButton("3");
    if (button.equals("5"))
      solo.clickOnButton("b");
    if (button.equals("6"))
      solo.clickOnButton("4");
    if (button.equals("s"))
      solo.clickOnButton("c");
  }

  private void clickSeveralTimes(String button, int times) {
    assert times > 0;
    for (int i = 0; i < times; i++)
      clickOnButton(button);
  }

  /**
   * Simple test, choose some letters from main table row
   */
  public void testSampleCase() {
    preTest();

    // Choose column 0 (blue)
    clickOnButton("0");

    solo.waitForText("A");

    // First character should be in upper case
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("A"));

    // Choose column 1 (blue + orange)
    clickOnButton("1");

    solo.waitForText("Af");

    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Af"));
  }

  /**
   * Test double clicks on the columns
   */
  public void testDoubleClick() {
    preTest();

    clickOnButton("0");
    solo.waitForText("A");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("A"));

    clickOnButton("1");
    solo.waitForText("Af");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Af"));

    clickSeveralTimes("2", 2);
    solo.waitForText("Afj");

    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Afj"));

    clickSeveralTimes("3", 4);
    solo.waitForText("Afjp");

    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Afjp"));

    clickSeveralTimes("6", 4);
    solo.waitForText("Afjpz");

    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Afjpz"));

    clickSeveralTimes("5", 4);

    // Wait for adding w to the text, when added, throw exception because it is
    // a bug!
    solo.waitForText("Afjpzw");

    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Afjpz"));

    clickSeveralTimes("4", 7);
    solo.waitForText("Afjpzq");

    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Afjpzq"));
  }

  /**
   * Test Switch button (blue + red)
   */
  public void testSwitchButton() {
    preTest();

    clickOnButton("0");
    solo.waitForText("A");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("A"));

    clickOnButton("s");
    solo.waitForText("space");

    clickOnButton("0");
    solo.waitForText("A ");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("A "));

    clickOnButton("0");
    solo.waitForText("A  ");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("A  "));

    clickOnButton("s");

    clickOnButton("0");
    solo.waitForText("A  a");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("A  a"));

    clickOnButton("s");
    clickOnButton("s");

    clickOnButton("0");
    solo.waitForText("A  aa");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("A  aa"));
  }

  /**
   * Test upper case table row!
   */
  public void testUpperCaseKeyboard() {
    preTest();

    // Switch to editing table row
    clickOnButton("s");

    // Switch to capital letters table row
    clickOnButton("5");

    clickSeveralTimes("0", 2);
    solo.waitForText("B");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("B"));

    clickOnButton("1");
    solo.waitForText("BF");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("BF"));

    clickOnButton("0");
    solo.waitForText("BFA");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("BFA"));
  }

  /**
   * 
   */
  public void testSymbolsAndNumbersKeyboard() {
    preTest();

    // Switch to editing table row
    clickOnButton("s");

    // Switch to numbers and symbols table row
    clickOnButton("1");

    clickSeveralTimes("0", 2);
    solo.waitForText("1");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("1"));

    clickOnButton("s");
    clickOnButton("s");

    clickSeveralTimes("1", 2);
    solo.waitForText("14");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("14"));

    clickSeveralTimes("3", 2);
    solo.waitForText("14@");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("14@"));

    clickSeveralTimes("6", 2);
    solo.waitForText("14@\"");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("14@\""));
  }

  /**
   * 
   */
  public void testSmilesKeyboard() {
    preTest();

    // Switch to editing table row
    clickOnButton("s");

    // Switch to smiles table row
    clickOnButton("2");

    clickOnButton("0");
    solo.waitForText(":)");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals(":)"));

    // Expected to move to letters keyboard immediately!

    clickOnButton("0");
    solo.waitForText(":)a");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals(":)a"));

    // Switch to editing table row
    clickOnButton("s");

    // Switch to capital letters table row
    clickOnButton("5");

    // Switch to editing table row
    clickOnButton("s");

    // Switch to smiles table row
    clickOnButton("2");

    clickOnButton("0");
    solo.waitForText(":)a:)");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals(":)a:)"));

    // Expected to move back to capital letters keyboard immediately!

    clickOnButton("0");
    solo.waitForText(":)a:)A");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals(":)a:)A"));
  }

  /**
   * this test is not checking capital letters keyboard! otherwise, it checks if
   * the keyboard converts letter at the start of line to capital letter
   */
  public void testAutoUpperCaseFeature() {
    preTest();

    clickSeveralTimes("0", 2);
    solo.waitForText("B");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("B"));

    // Switch to editing table row
    clickOnButton("s");

    clickSeveralTimes("0", 4);
    solo.waitForText("B    ");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("B    "));

    // Switch to letters table row
    clickOnButton("s");

    clickSeveralTimes("0", 2);
    solo.waitForText("B    b");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("B    b"));

    // Switch to editing table row
    clickOnButton("s");

    clickSeveralTimes("0", 4);
    solo.waitForText("B    b    ");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("B    b    "));

    // Switch to symbols and numbers row table
    clickOnButton("1");

    clickOnButton("3");
    solo.waitForText("B    b    .");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("B    b    ."));

    // Switch to editing table row
    clickOnButton("s");

    clickSeveralTimes("0", 2);
    solo.waitForText("B    b    .  ");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("B    b    .  "));

    // Switch to letters table row
    clickOnButton("1");

    clickSeveralTimes("0", 2);
    solo.waitForText("B    b    .  B");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("B    b    .  B"));

    // Switch to editing table row
    clickOnButton("s");

    // Switch to symbols and numbers table row
    clickOnButton("1");

    clickOnButton("3");
    solo.waitForText("B    b    .  B.");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("B    b    .  B."));

    // Switch to editing table row
    clickOnButton("s");

    clickOnButton("6");
    clickSeveralTimes("4", 2);
    solo.waitForText("B    b    .  B.\n\n");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("B    b    .  B.\n\n"));

    clickOnButton("6");

    // Back to letters table row
    clickOnButton("1");

    clickOnButton("0");
    solo.waitForText("B    b    .  B.\n\nA");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("B    b    .  B.\n\nA"));
  }

  /**
   * 
   */
  public void testDeleteOption() {
    preTest();

    // Switch to editing table row
    clickOnButton("s");

    clickOnButton("4");
    clickSeveralTimes("4", 10); // :)

    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals(""));

    // Switch to letters table row
    clickOnButton("s");

    clickOnButton("0");
    solo.waitForText("A");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("A"));

    clickOnButton("1");
    solo.waitForText("Af");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Af"));

    // Switch to editing table row
    clickOnButton("s");

    clickOnButton("4");
    solo.waitForText("A");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("A"));

    clickOnButton("4");
    solo.waitForText("");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals(""));

    clickSeveralTimes("4", 12); // :D
    clickOnButton("4");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals(""));

  }

  /**
   * 
   */
  public void testMoveCursor() {
    preTest();

    // Switch to editing table row
    clickOnButton("s");

    // Get inside editing
    clickOnButton("6");

    // Move cursor to the left 10 times
    clickSeveralTimes("0", 10);

    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals(""));

    // Move cursor to the right 13 times
    clickSeveralTimes("2", 7);

    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals(""));

    // Move to letters
    clickOnButton("s");

    clickOnButton("0");
    solo.waitForText("A");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("A"));

    clickOnButton("1");
    solo.waitForText("Af");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Af"));

    clickOnButton("0");
    solo.waitForText("Afa");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Afa"));

    // Switch back to editing
    clickOnButton("s");

    clickOnButton("0");

    // Switch to letters
    clickOnButton("s");

    clickSeveralTimes("0", 2);
    solo.waitForText("Afba");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Afba"));

    // Switch back to editing
    clickOnButton("s");

    clickOnButton("2");
    clickOnButton("s");
    clickSeveralTimes("0", 3);
    solo.waitForText("Afbac");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Afbac"));

    // Switch back to editing
    clickOnButton("s");
    clickSeveralTimes("0", 10);

    // Switch to letters
    clickOnButton("s");

    clickSeveralTimes("0", 3);
    solo.waitForText("CAfbac");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("CAfbac"));
  }

  /**
   * 
   */
  public void testMoveCursorWithDelete() {
    preTest();

    clickSeveralTimes("0", 2);
    solo.waitForText("B");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("B"));

    clickOnButton("1");
    clickOnButton("0");
    clickOnButton("1");

    solo.waitForText("Bfaf");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Bfaf"));

    clickOnButton("s");
    clickOnButton("6");
    clickOnButton("0");

    clickOnButton("s");

    clickOnButton("0");
    solo.waitForText("Bfaaf");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Bfaaf"));

    clickOnButton("s");

    clickOnButton("3");
    solo.waitForText("Bfaf");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Bfaf"));

    clickOnButton("3");
    solo.waitForText("Bff");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Bff"));

    clickOnButton("3");
    solo.waitForText("Bf");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Bf"));

    clickOnButton("2");

    clickOnButton("3");
    solo.waitForText("B");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("B"));

    clickOnButton("0");

    clickOnButton("3");
    solo.waitForText("B");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("B"));
  }

  /**
   * 
   */
  public void testEnter() {
    preTest();

    clickSeveralTimes("0", 2);
    solo.waitForText("B");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("B"));

    clickOnButton("1");
    clickOnButton("0");
    clickOnButton("1");

    solo.waitForText("Bfaf");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Bfaf"));

    clickOnButton("s");
    clickOnButton("6");

    // Enter
    clickOnButton("4");

    solo.waitForText("Bfaf\n");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Bfaf\n"));

    // Enter
    clickOnButton("4");

    solo.waitForText("Bfaf\n\n");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Bfaf\n\n"));

    clickOnButton("s");
    clickOnButton("0");

    solo.waitForText("Bfaf\n\na");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Bfaf\n\na"));

    clickOnButton("s");
    clickSeveralTimes("3", 2);

    solo.waitForText("Bfaf\n");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Bfaf\n"));

    clickSeveralTimes("3", 2);

    solo.waitForText("Bfa");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Bfa"));
  }

  /**
   * 
   */
  public void testShift1() {
    preTest();

    clickSeveralTimes("0", 2);
    solo.waitForText("B");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("B"));

    clickOnButton("1");
    clickOnButton("0");
    clickOnButton("1");

    solo.waitForText("Bfaf");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Bfaf"));

    clickOnButton("s");
    clickOnButton("6");

    // Shift
    clickOnButton("1");

    clickOnButton("0");
    clickOnButton("s");
    clickSeveralTimes("0", 3);

    solo.waitForText("CBfaf");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("CBfaf"));

    clickOnButton("s");
    clickOnButton("2");
    clickOnButton("s");
    clickSeveralTimes("0", 3);

    solo.waitForText("CBfafc");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("CBfafc"));

    clickOnButton("s");

    clickOnButton("3");
    solo.waitForText("");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals(""));
  }

  /**
   * 
   */
  public void testShift2() {
    preTest();

    clickSeveralTimes("0", 2);
    solo.waitForText("B");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("B"));

    clickOnButton("1");
    clickOnButton("0");
    clickOnButton("1");

    solo.waitForText("Bfaf");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Bfaf"));

    clickOnButton("s");

    clickOnButton("0");
    clickOnButton("s");
    clickOnButton("0");

    solo.waitForText("Bfaf a");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Bfaf a"));

    clickOnButton("1");
    solo.waitForText("Bfaf af");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Bfaf af"));

    clickOnButton("s");
    clickOnButton("0");
    clickOnButton("0");
    clickOnButton("0");
    clickOnButton("s");
    clickOnButton("0");
    clickOnButton("1");

    solo.waitForText("Bfaf af   af");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Bfaf af   af"));

    clickOnButton("s");
    clickOnButton("6");

    // Shift
    clickOnButton("1");

    clickOnButton("0");
    clickOnButton("s");
    clickSeveralTimes("0", 3);

    solo.waitForText("Bfaf af   caf");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Bfaf af   caf"));

    clickOnButton("s");
    clickOnButton("0");
    clickOnButton("0");
    clickOnButton("s");
    clickSeveralTimes("0", 3);

    solo.waitForText("Bfaf afc   caf");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Bfaf afc   caf"));

    clickOnButton("s");

    clickOnButton("3");
    solo.waitForText("Bfaf    caf");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Bfaf    caf"));

    clickOnButton("3");
    solo.waitForText("Bfaf   caf");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Bfaf   caf"));

    clickOnButton("3");
    solo.waitForText("   caf");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("   caf"));
  }

  /**
   * 
   */
  public void testUndo() {
    preTest();

    Stack<String> stack = new Stack<String>();

    clickOnButton("1");
    stack.push("F");

    clickSeveralTimes("0", 3);
    stack.push("Fc");

    solo.waitForText("Fc");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Fc"));

    clickOnButton("1");
    stack.push("Fcf");

    clickSeveralTimes("0", 3);
    stack.push("Fcfc");

    solo.waitForText("Fcfc");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Fcfc"));

    clickOnButton("s");
    clickOnButton("6");
    clickOnButton("0");
    clickOnButton("0");

    clickOnButton("s");
    clickSeveralTimes("0", 2);
    stack.push("Fcbfc");

    solo.waitForText("Fcbfc");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("Fcbfc"));

    clickOnButton("s");
    clickOnButton("1");
    clickOnButton("0");
    clickOnButton("1");
    clickOnButton("2");
    clickOnButton("3");

    stack.push("cbfc");

    solo.waitForText("cbfc");
    assertTrue("message content is wrong!", messageEditText.getText()
        .toString().equals("cbfc"));

    clickSeveralTimes("3", 4);

    while (!stack.isEmpty()) {
      String text = stack.pop();
      solo.waitForText(text);
      assertTrue("message content is wrong!", messageEditText.getText()
          .toString().equals(text));
      clickOnButton("5"); // undo
    }

  }

}
