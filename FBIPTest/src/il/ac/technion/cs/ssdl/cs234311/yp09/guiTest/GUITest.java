package il.ac.technion.cs.ssdl.cs234311.yp09.guiTest;

import il.ac.technion.cs.ssdl.cs234311.yp09.facebookFeedGui.FeedOptionsActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.FacebookActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.FourButtonsFragment;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.LanguageActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.MainMenuActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.ServiceActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.SettingsActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.TypingActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.VolumeActivity;
import android.test.ActivityInstrumentationTestCase2;

import com.robotium.solo.Solo;

/**
 * @author Muhammad
 * 
 */
public class GUITest extends ActivityInstrumentationTestCase2<MainMenuActivity> {

  private Solo solo;

  /**
   * Default constructor
   */
  public GUITest() {
    super(MainMenuActivity.class);
  }

  @Override
  public void setUp() {
    FourButtonsFragment.debugMode = true;
    solo = new Solo(getInstrumentation(), getActivity());
  }

  @Override
  public void tearDown() throws Exception {
    solo.finishOpenedActivities();
  }

  /**
   * Basic test
   */
  public void testActivityBase() {
    solo.assertCurrentActivity("Error 1", MainMenuActivity.class);
    solo.clickOnButton("1");
    solo.waitForActivity(TypingActivity.class);
    solo.assertCurrentActivity("Error 2", TypingActivity.class);
    solo.clickOnButton("5");
    solo.waitForActivity(SettingsActivity.class);
    solo.assertCurrentActivity("Error 3", SettingsActivity.class);
    solo.clickOnButton("1");
    solo.waitForActivity(LanguageActivity.class);
    solo.assertCurrentActivity("Error 4", LanguageActivity.class);
  }

  /**
   * change language test
   */
  public void testChangingLanguage() {
    testActivityBase();
    solo.clickOnButton("3");
    solo.waitForActivity(LanguageActivity.class);
    solo.assertCurrentActivity("Error 5", LanguageActivity.class);
    solo.clickOnButton("4");
    solo.waitForActivity(SettingsActivity.class);
    solo.assertCurrentActivity("Error 6", SettingsActivity.class);
  }

  /**
   * change language test
   */
  public void testVolume() {
    testChangingLanguage();
    solo.clickOnButton("2");
    solo.waitForActivity(VolumeActivity.class);
    solo.assertCurrentActivity("Error 7", VolumeActivity.class);
    solo.clickOnButton("8");
    solo.waitForActivity(SettingsActivity.class);
    solo.assertCurrentActivity("Error 8", SettingsActivity.class);
  }

  /**
   * testing typing activity
   */
  public void testTyping() {
    testVolume();
    solo.clickOnButton("8");
    solo.waitForActivity(TypingActivity.class);
    solo.assertCurrentActivity("Error 9", TypingActivity.class);
    solo.clickOnButton("c");
    solo.waitForActivity(TypingActivity.class);
    solo.assertCurrentActivity("Error 10", TypingActivity.class);
    solo.clickOnButton("b");
    solo.waitForActivity(TypingActivity.class);
    solo.assertCurrentActivity("Error 11", TypingActivity.class);
    solo.clickOnButton("6");
    solo.waitForActivity(ServiceActivity.class);
    solo.assertCurrentActivity("Error 12", ServiceActivity.class);
  }

  /**
   * testing Service Selection
   */
  public void testSelectService() {
    testTyping();
    solo.clickOnButton("3");
    solo.waitForActivity(ServiceActivity.class);
    solo.assertCurrentActivity("Error 13", ServiceActivity.class);
    solo.clickOnButton("4");
    solo.waitForActivity(FacebookActivity.class);
    solo.assertCurrentActivity("Error 14", FacebookActivity.class);
  }

  /**
   * testing facebook feed options
   */
  public void testFacebookFeed() {
    testSelectService();
    solo.clickOnButton("3");
    solo.waitForActivity(FacebookActivity.class);
    solo.assertCurrentActivity("Error 15", FacebookActivity.class);
    solo.clickOnButton("4");
    solo.waitForActivity(FeedOptionsActivity.class);
    solo.assertCurrentActivity("Error 16", FeedOptionsActivity.class);
  }

  /**
   * testing facebook activity
   */
  public void testShowPosts() {
    testFacebookFeed();
    solo.clickOnButton("8");
    solo.waitForActivity(FacebookActivity.class);
    solo.assertCurrentActivity("Error 17", FacebookActivity.class);
  }

}
