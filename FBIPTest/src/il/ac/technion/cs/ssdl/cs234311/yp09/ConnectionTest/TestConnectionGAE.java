package il.ac.technion.cs.ssdl.cs234311.yp09.ConnectionTest;

import il.ac.technion.cs.ssdl.cs234311.yp09.utilities.Client;
import il.ac.technion.cs.ssdl.cs234311.yp09.utilities.CommonLang;
import il.ac.technion.cs.ssdl.cs234311.yp09.utilities.SendResult;
import il.ac.technion.cs.ssdl.cs234311.yp09.utilities.SendResult.ErrorType;
import il.ac.technion.cs.ssdl.cs234311.yp09.utilities.Statistics;

import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import junit.framework.TestCase;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

/**
 * @author Tal the class tests the connection to the GAE server, and also the
 *         database performance
 */
public class TestConnectionGAE extends TestCase {

  // address of a copy of the server reserved for testing
  private String testServerUrl = "http://bip4-app.appspot.com/talserver";
  // non existing server in the host
  private String badServerUrl = "http://bip4-app.appspot.com/xxxx";
  // non existing host
  private String badUrl = "http://xxx-xxx-xxx.xxx.com/xxxx";

  /**
   * test response for sending of a bad object instead of Statistics
   */
  public void testBadObject() {
    Client client = new Client(testServerUrl);
    SendResult res = client.sendStatistics(Double.valueOf(1.1));
    assertTrue(res.errorType == ErrorType.BAD_RESPONSE);
    assertTrue(res.description.endsWith(String
        .valueOf(CommonLang.BAD_INT_PARAM)));
  }

  /**
   * test response for non existing server in the domain
   */
  public void testBadServer() {
    Client client = new Client(badServerUrl);
    SendResult res = client.sendStatistics(Double.valueOf(1.1));
    assertTrue(res.errorType == ErrorType.BAD_RESPONSE);
    assertTrue(res.description.endsWith("404"));
  }

  /**
   * test response for non existing domain
   */
  public void testBadUrl() {
    Client client = new Client(badUrl);
    SendResult res = client.sendStatistics(Double.valueOf(1.1));
    assertTrue(res.errorType == ErrorType.BAD_RESPONSE);
    assertTrue(res.description.endsWith("403")
        || res.description.endsWith("404"));
  }

  /**
   * test the response for appropriate reset request and not appropriate reset
   * request
   */
  public void testSendAndReset() {
    Client client = new Client(testServerUrl);

    // make sure there is statistics to be deleted
    SendResult res = client.sendStatistics(new Statistics());
    assertTrue(res.errorType == ErrorType.NO_ERROR);

    // send legitimate reset request
    res = client.resetStatistics();
    assertTrue(res.errorType == ErrorType.NO_ERROR);

    // send non legitimate reset request
    res = client.resetStatistics();
    assertTrue(res.errorType == ErrorType.BAD_RESPONSE);
    assertTrue(res.description.endsWith(String.valueOf(CommonLang.NO_DATA)));

    // make sure there is statistics,based on time playing snake to be deleted
    res = client.sendTimePlayingSnake(Double.valueOf(1000));
    assertTrue(res.errorType == ErrorType.NO_ERROR);

    // send legitimate reset request
    res = client.resetStatistics();
    assertTrue(res.errorType == ErrorType.NO_ERROR);

    // send non legitimate reset request
    res = client.resetStatistics();
    assertTrue(res.errorType == ErrorType.BAD_RESPONSE);
    assertTrue(res.description.endsWith(String.valueOf(CommonLang.NO_DATA)));

  }

  /**
   * ultimate Test - test client, severServlet, dataBase, XMLBuilder
   */
  public void testGetXml() {

    Client client = new Client(testServerUrl);
    // reset statistics
    SendResult res = client.resetStatistics();
    if (res.errorType != ErrorType.NO_ERROR
        && !res.description.endsWith(String.valueOf(CommonLang.NO_DATA))) {
      System.out.println("could not connect to server");
      fail("could not connect to server"); // connection to the server problem
    }

    try {
      // get XML file
      URL url = new URL(testServerUrl + "?message=GetStatisticsXML");
      URLConnection conn = url.openConnection();

      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder builder = factory.newDocumentBuilder();
      Document doc = builder.parse(conn.getInputStream());

      // make sure it is a zero values file
      NodeList list = doc.getDocumentElement().getChildNodes();
      for (int i = 0; i < list.getLength(); i++)
        assertTrue(list.item(i).getFirstChild().getTextContent().equals("0.0"));

      // send a statistics with known values
      Statistics s = new Statistics();
      s.timePerWordAvg = Double.valueOf(10000.0);
      s.numberOfWords = Double.valueOf(1.0);
      s.timePerCharAvg = Double.valueOf(2000.0);
      s.numberOfChars = Double.valueOf(1.0);
      res = client.sendStatistics(s);
      if (res.errorType != ErrorType.NO_ERROR) {
        System.out.println("could not connect to server");
        assertTrue(false); // connection to the server problem
      }

      // get new XML file
      conn = url.openConnection();
      factory = DocumentBuilderFactory.newInstance();
      builder = factory.newDocumentBuilder();
      doc = builder.parse(conn.getInputStream());

      // make sure values in XML correspond to set values
      list = doc.getDocumentElement().getElementsByTagName(
          "time_for_word_average_in_sec");
      assertTrue(list.getLength() == 1);
      assertTrue(list.item(0).getFirstChild().getTextContent().equals("10.0"));

      list = doc.getDocumentElement().getElementsByTagName(
          "time_for_char_average_in_millisec");
      assertTrue(list.getLength() == 1);
      assertTrue(list.item(0).getFirstChild().getTextContent().equals("2000.0"));

    } catch (Exception e) {
      System.out.println("exception");
      assertTrue(false);
    }

  }

}
