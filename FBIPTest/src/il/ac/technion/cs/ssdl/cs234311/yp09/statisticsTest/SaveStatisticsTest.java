package il.ac.technion.cs.ssdl.cs234311.yp09.statisticsTest;

import il.ac.technion.cs.ssdl.cs234311.yp09.gui.OpCodeInterpreter.Op;
import il.ac.technion.cs.ssdl.cs234311.yp09.utilities.SaveStatistics;
import il.ac.technion.cs.ssdl.cs234311.yp09.utilities.SaveStatistics.MessageType;
import il.ac.technion.cs.ssdl.cs234311.yp09.utilities.Statistics;
import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * @author husam test the class SaveStatistics
 */
public class SaveStatisticsTest extends TestCase {

  /**
   * test adding chars words and sentences
   */
  public void testAddSentencesWordsAndChars() {
    SaveStatistics.DEBUG_MODE = true;
    SaveStatistics.startApp();
    sleepFor(300);
    SaveStatistics.startTyping();
    sleepFor(300);
    SaveStatistics.addChar();// char1 = 300
    sleepFor(500);
    SaveStatistics.addChar();// char2 = 500
    sleepFor(1500);
    SaveStatistics.addChar();// char3 = 1500
    sleepFor(16000);
    SaveStatistics.addChar();// skipped
    sleepFor(300);
    SaveStatistics.addChar();// cahr4 = 300
    sleepFor(700);
    SaveStatistics.addChar();// char5 = 700
    sleepFor(400);
    SaveStatistics.addChar();// char6 = 400
    sleepFor(500);
    SaveStatistics.addChar();// char7 = 500
    SaveStatistics.addWord();// skipped
    SaveStatistics.addSentence();// skipped
    sleepFor(900);
    SaveStatistics.addChar();// char8 = 900
    sleepFor(300);
    SaveStatistics.addChar();// char9 = 300
    sleepFor(500);
    SaveStatistics.addChar();// char10 = 500
    sleepFor(1400);
    SaveStatistics.addChar();// char11 = 1400
    SaveStatistics.addWord();// word1= 3100
    SaveStatistics.addSentence();// sentence1 = 3100
    sleepFor(120);
    SaveStatistics.addChar();// char12 = 120;
    sleepFor(780);
    SaveStatistics.addChar();// char13 = 780;
    sleepFor(900);
    SaveStatistics.addChar();// char14 = 900;
    sleepFor(4000);
    SaveStatistics.addChar();// char15 = 4000;
    SaveStatistics.addWord();// word2 = 5800;
    sleepFor(120);
    SaveStatistics.addChar();// char16 = 120;
    sleepFor(780);
    SaveStatistics.addChar();// char17 = 780;
    sleepFor(900);
    SaveStatistics.addChar();// char18 = 900;
    sleepFor(3500);
    SaveStatistics.addChar();// char19 = 3500;
    SaveStatistics.addWord();// word3 = 5300;
    SaveStatistics.addSentence();// sentance2 = 11100
    SaveStatistics.stopTyping();

    Statistics res = SaveStatistics.exitTheApp();
    Assert.assertEquals(Double.valueOf(19.0), res.numberOfChars);
    Assert.assertTrue(checkIfTheNumberIsInterval(957.89,
        res.timePerCharAvg.doubleValue(), 20));
    Assert.assertTrue(checkIfTheNumberIsInterval(7100.0,
        res.timePerSentenceAvg.doubleValue(), 50));

    Assert.assertEquals(Double.valueOf(2.0), res.numberOfSentence);
    Assert.assertEquals(Double.valueOf(3.0), res.numberOfWords);
    Assert.assertTrue(checkIfTheNumberIsInterval(4733,
        res.timePerWordAvg.doubleValue(), 30));
    // check if the other fields didn't changed
    Assert.assertEquals(Double.valueOf(0.0), res.numberOfAutoComplete);
    Assert.assertEquals(Double.valueOf(0.0), res.numberOfBlueClicks);
    Assert.assertEquals(Double.valueOf(0.0), res.numberOfRedClicks);
    Assert.assertEquals(Double.valueOf(0.0), res.numberOfOrangeClicks);
    Assert.assertEquals(Double.valueOf(0.0), res.numberOfGreenClicks);
    Assert.assertEquals(Double.valueOf(0.0), res.numberOfOrangeGreenClicks);
    Assert.assertEquals(Double.valueOf(0.0), res.numberOfGreenRedClicks);
    Assert.assertEquals(Double.valueOf(0.0), res.numberOfBlueRedClicks);
    Assert.assertEquals(Double.valueOf(0.0), res.numberOfBlueOrangeClicks);
    Assert.assertEquals(Double.valueOf(0.0), res.numberOfFaceBookMessages);
    Assert.assertEquals(Double.valueOf(0.0), res.numberOfSMS);
    Assert.assertEquals(Double.valueOf(0.0), res.numberOfGoogleMessages);
    // check the averages and counts of sentence words and chars.
    SaveStatistics.DEBUG_MODE = false;
  }

  /**
   * make general test for the fields except time for char, word and sentence
   */
  @SuppressWarnings("static-method")
  public void testOtherFields() {
    SaveStatistics.DEBUG_MODE = true;
    SaveStatistics.startApp();
    SaveStatistics.addAutoComplete();
    SaveStatistics.clickOn(Op.BLUE);
    SaveStatistics.clickOn(Op.BLUE);
    SaveStatistics.clickOn(Op.BLUE);
    SaveStatistics.clickOn(Op.RED);
    SaveStatistics.clickOn(Op.RED);
    SaveStatistics.clickOn(Op.RED);
    SaveStatistics.clickOn(Op.RED);
    SaveStatistics.clickOn(Op.ORANGE);
    SaveStatistics.clickOn(Op.ORANGE);
    SaveStatistics.clickOn(Op.ORANGE);
    SaveStatistics.clickOn(Op.ORANGE);
    SaveStatistics.clickOn(Op.ORANGE);
    SaveStatistics.clickOn(Op.GREEN);
    SaveStatistics.clickOn(Op.GREEN);
    SaveStatistics.clickOn(Op.GREEN);
    SaveStatistics.clickOn(Op.GREEN);
    SaveStatistics.clickOn(Op.GREEN);
    SaveStatistics.clickOn(Op.GREEN);
    SaveStatistics.clickOn(Op.ORANGE_GREEN);
    SaveStatistics.clickOn(Op.ORANGE_GREEN);
    SaveStatistics.clickOn(Op.ORANGE_GREEN);
    SaveStatistics.clickOn(Op.ORANGE_GREEN);
    SaveStatistics.clickOn(Op.ORANGE_GREEN);
    SaveStatistics.clickOn(Op.ORANGE_GREEN);
    SaveStatistics.clickOn(Op.ORANGE_GREEN);
    SaveStatistics.clickOn(Op.GREEN_RED);
    SaveStatistics.clickOn(Op.GREEN_RED);
    SaveStatistics.clickOn(Op.GREEN_RED);
    SaveStatistics.clickOn(Op.GREEN_RED);
    SaveStatistics.clickOn(Op.GREEN_RED);
    SaveStatistics.clickOn(Op.GREEN_RED);
    SaveStatistics.clickOn(Op.GREEN_RED);
    SaveStatistics.clickOn(Op.GREEN_RED);
    SaveStatistics.clickOn(Op.BLUE_RED);
    SaveStatistics.clickOn(Op.BLUE_RED);
    SaveStatistics.clickOn(Op.BLUE_RED);
    SaveStatistics.clickOn(Op.BLUE_RED);
    SaveStatistics.clickOn(Op.BLUE_RED);
    SaveStatistics.clickOn(Op.BLUE_RED);
    SaveStatistics.clickOn(Op.BLUE_RED);
    SaveStatistics.clickOn(Op.BLUE_RED);
    SaveStatistics.clickOn(Op.BLUE_RED);
    SaveStatistics.clickOn(Op.BLUE_ORANGE);
    SaveStatistics.clickOn(Op.BLUE_ORANGE);
    SaveStatistics.clickOn(Op.BLUE_ORANGE);
    SaveStatistics.clickOn(Op.BLUE_ORANGE);
    SaveStatistics.clickOn(Op.BLUE_ORANGE);
    SaveStatistics.clickOn(Op.BLUE_ORANGE);
    SaveStatistics.clickOn(Op.BLUE_ORANGE);
    SaveStatistics.clickOn(Op.BLUE_ORANGE);
    SaveStatistics.clickOn(Op.BLUE_ORANGE);
    SaveStatistics.clickOn(Op.BLUE_ORANGE);
    SaveStatistics.sendMessage(MessageType.FACEBOOK_MESSAGE);
    SaveStatistics.sendMessage(MessageType.FACEBOOK_MESSAGE);
    SaveStatistics.sendMessage(MessageType.FACEBOOK_MESSAGE);
    SaveStatistics.sendMessage(MessageType.FACEBOOK_MESSAGE);
    SaveStatistics.sendMessage(MessageType.FACEBOOK_MESSAGE);
    SaveStatistics.sendMessage(MessageType.FACEBOOK_MESSAGE);
    SaveStatistics.sendMessage(MessageType.FACEBOOK_MESSAGE);
    SaveStatistics.sendMessage(MessageType.FACEBOOK_MESSAGE);
    SaveStatistics.sendMessage(MessageType.FACEBOOK_MESSAGE);
    SaveStatistics.sendMessage(MessageType.FACEBOOK_MESSAGE);
    SaveStatistics.sendMessage(MessageType.FACEBOOK_MESSAGE);
    SaveStatistics.sendMessage(MessageType.SMS_MESSAGE);
    SaveStatistics.sendMessage(MessageType.SMS_MESSAGE);
    SaveStatistics.sendMessage(MessageType.SMS_MESSAGE);
    SaveStatistics.sendMessage(MessageType.SMS_MESSAGE);
    SaveStatistics.sendMessage(MessageType.SMS_MESSAGE);
    SaveStatistics.sendMessage(MessageType.SMS_MESSAGE);
    SaveStatistics.sendMessage(MessageType.SMS_MESSAGE);
    SaveStatistics.sendMessage(MessageType.SMS_MESSAGE);
    SaveStatistics.sendMessage(MessageType.SMS_MESSAGE);
    SaveStatistics.sendMessage(MessageType.SMS_MESSAGE);
    SaveStatistics.sendMessage(MessageType.SMS_MESSAGE);
    SaveStatistics.sendMessage(MessageType.SMS_MESSAGE);
    SaveStatistics.sendMessage(MessageType.GOOGLE_MESSAGE);
    SaveStatistics.sendMessage(MessageType.GOOGLE_MESSAGE);
    SaveStatistics.sendMessage(MessageType.GOOGLE_MESSAGE);
    SaveStatistics.sendMessage(MessageType.GOOGLE_MESSAGE);
    SaveStatistics.sendMessage(MessageType.GOOGLE_MESSAGE);
    SaveStatistics.sendMessage(MessageType.GOOGLE_MESSAGE);
    SaveStatistics.sendMessage(MessageType.GOOGLE_MESSAGE);
    SaveStatistics.sendMessage(MessageType.GOOGLE_MESSAGE);
    SaveStatistics.sendMessage(MessageType.GOOGLE_MESSAGE);
    SaveStatistics.sendMessage(MessageType.GOOGLE_MESSAGE);
    SaveStatistics.sendMessage(MessageType.GOOGLE_MESSAGE);
    SaveStatistics.sendMessage(MessageType.GOOGLE_MESSAGE);
    SaveStatistics.sendMessage(MessageType.GOOGLE_MESSAGE);
    SaveStatistics.clickOn(Op.LONG_BLUE);
    SaveStatistics.clickOn(Op.LONG_BLUE);
    SaveStatistics.clickOn(Op.LONG_GREEN);
    SaveStatistics.clickOn(Op.LONG_RED);
    SaveStatistics.clickOn(Op.LONG_RED);
    SaveStatistics.clickOn(Op.LONG_RED);
    SaveStatistics.clickOn(Op.LONG_RED);
    SaveStatistics.clickOn(Op.LONG_RED);
    SaveStatistics.clickOn(Op.LONG_ORANGE);
    SaveStatistics.clickOn(Op.LONG_ORANGE);
    SaveStatistics.clickOn(Op.LONG_ORANGE);

    Statistics res = SaveStatistics.exitTheApp();
    Assert.assertEquals(Double.valueOf(2.0), res.numberOfLongBlueClicks);
    Assert.assertEquals(Double.valueOf(1.0), res.numberOfLongGreenClicks);
    Assert.assertEquals(Double.valueOf(5.0), res.numberOfLongRedClicks);
    Assert.assertEquals(Double.valueOf(3.0), res.numberOfLongOrangeClicks);

    Assert.assertEquals(Double.valueOf(1.0), res.numberOfAutoComplete);
    Assert.assertEquals(Double.valueOf(3.0), res.numberOfBlueClicks);
    Assert.assertEquals(Double.valueOf(4.0), res.numberOfRedClicks);
    Assert.assertEquals(Double.valueOf(5.0), res.numberOfOrangeClicks);
    Assert.assertEquals(Double.valueOf(6.0), res.numberOfGreenClicks);
    Assert.assertEquals(Double.valueOf(7.0), res.numberOfOrangeGreenClicks);
    Assert.assertEquals(Double.valueOf(8.0), res.numberOfGreenRedClicks);
    Assert.assertEquals(Double.valueOf(9.0), res.numberOfBlueRedClicks);
    Assert.assertEquals(Double.valueOf(10.0), res.numberOfBlueOrangeClicks);
    Assert.assertEquals(Double.valueOf(11.0), res.numberOfFaceBookMessages);
    Assert.assertEquals(Double.valueOf(12.0), res.numberOfSMS);
    Assert.assertEquals(Double.valueOf(13.0), res.numberOfGoogleMessages);
    SaveStatistics.DEBUG_MODE = false;
  }

  /**
   * test clone method
   */
  public void testClone() {
    SaveStatistics.DEBUG_MODE = true;
    Statistics tmp = new Statistics();
    tmp.numberOfAutoComplete = Double.valueOf(1.0);
    tmp.numberOfBlueClicks = Double.valueOf(3.0);
    tmp.numberOfBlueOrangeClicks = Double.valueOf(4.0);
    tmp.numberOfChars = Double.valueOf(5.0);
    tmp.numberOfDeletedChars = Double.valueOf(6.0);
    tmp.numberOfFaceBookMessages = Double.valueOf(7.0);
    tmp.numberOfGoogleMessages = Double.valueOf(8.0);
    tmp.numberOfGreenClicks = Double.valueOf(9.0);
    tmp.numberOfGreenRedClicks = Double.valueOf(10.0);
    tmp.numberOfBlueRedClicks = Double.valueOf(11.0);
    tmp.numberOfRedClicks = Double.valueOf(12.0);
    tmp.numberOfSentence = Double.valueOf(13.0);
    tmp.numberOfSMS = Double.valueOf(14.0);
    tmp.numberOfWords = Double.valueOf(15.0);
    tmp.numberOfOrangeClicks = Double.valueOf(16.0);
    tmp.numberOfOrangeGreenClicks = Double.valueOf(17.0);
    tmp.timePerCharAvg = Double.valueOf(18.0);
    tmp.timePerSentenceAvg = Double.valueOf(19.0 * 1000);
    tmp.timePerWordAvg = Double.valueOf(20.0 * 1000);
    tmp.timeUsingTheApp = Double.valueOf(21.0 * 60 * 1000);
    Statistics tmp2 = tmp.clone();
    assert tmp2.equals(tmp);
    assert getTheNumberOfZeroFields(tmp2) == 24;
    SaveStatistics.DEBUG_MODE = false;
  }

  /**
   * test startApp, resumeOrStartApp and pauseAndSendStatistcs
   */
  public void testPauseStartAndStop() {
    SaveStatistics.DEBUG_MODE = true;
    // ************ the test ************************//
    SaveStatistics.startApp();
    SaveStatistics.addAutoComplete();
    sleepFor(200);
    SaveStatistics.addAutoComplete();
    Statistics stat1 = SaveStatistics.pauseAndSendStatistcs(null);
    // asserts
    checkIfTheNumberIsInterval(200.0, stat1.timeUsingTheApp.doubleValue(), 10);
    Assert.assertEquals(Double.valueOf(2.0), stat1.numberOfAutoComplete);
    assert getTheNumberOfZeroFields(stat1) == 23;
    // asserts
    sleepFor(40);
    SaveStatistics.startApp();
    SaveStatistics.resumeOrStartApp();

    sleepFor(30);
    SaveStatistics.addAutoComplete();
    sleepFor(60);
    Statistics stat2 = SaveStatistics.pauseAndSendStatistcs(null);
    // asserts
    Assert.assertEquals(Double.valueOf(1.0), stat2.numberOfAutoComplete);

    checkIfTheNumberIsInterval(90.0, stat2.timeUsingTheApp.doubleValue(), 10);
    assert getTheNumberOfZeroFields(stat2) == 22;
    // asserts
    SaveStatistics.clickOn(Op.LONG_BLUE);
    sleepFor(40);
    Statistics stat3 = SaveStatistics.exitTheApp();
    // asserts
    Assert.assertEquals(Double.valueOf(1.0), stat3.numberOfLongBlueClicks);
    assert getTheNumberOfZeroFields(stat3) == 24;
    // asserts
    sleepFor(50);
    SaveStatistics.startApp();
    SaveStatistics.resumeOrStartApp();
    sleepFor(30);
    SaveStatistics.addAutoComplete();
    sleepFor(10);

    sleepFor(50);
    Statistics stat4 = SaveStatistics.pauseAndSendStatistcs(null);
    // asserts
    Assert.assertEquals(Double.valueOf(1.0), stat4.numberOfAutoComplete);

    checkIfTheNumberIsInterval(90.0, stat4.timeUsingTheApp.doubleValue(), 10);
    assert getTheNumberOfZeroFields(stat4) == 22;
    sleepFor(20);
    // asserts
    Statistics stat5 = SaveStatistics.exitTheApp();
    // assets
    assert getTheNumberOfZeroFields(stat5) == 24;
    // assets
    sleepFor(30);
    SaveStatistics.resumeOrStartApp();
    sleepFor(20);
    SaveStatistics.startApp();
    sleepFor(40);
    Statistics stat6 = SaveStatistics.exitTheApp();
    // asserts
    checkIfTheNumberIsInterval(60.0, stat6.timeUsingTheApp.doubleValue(), 10);
    assert getTheNumberOfZeroFields(stat6) == 24;
    // asserts
    sleepFor(10);
    SaveStatistics.clickOn(Op.BLUE);
    Statistics stat7 = SaveStatistics.pauseAndSendStatistcs(null);
    // asserts
    Assert.assertEquals(Double.valueOf(1.0), stat7.numberOfBlueClicks);
    assert getTheNumberOfZeroFields(stat7) == 24;
    // asserts
    // **************end of the test ****************//
    SaveStatistics.DEBUG_MODE = false;
  }

  // check if real is between expected -50 and expected +50
  @SuppressWarnings("static-method")
  private boolean checkIfTheNumberIsInterval(double expected, double real,
      double window) {
    return expected - window <= real && real <= expected + window;
  }

  @SuppressWarnings("static-method")
  private void sleepFor(int milisec) {
    try {
      Thread.sleep(milisec);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  @SuppressWarnings("static-method")
  private int getTheNumberOfZeroFields(Statistics s) {
    int count = 0;
    if (0.0 != s.timeUsingTheApp.doubleValue())
      count++;
    if (0.0 != s.timePerSentenceAvg.doubleValue())
      count++;
    if (0.0 != s.numberOfSentence.doubleValue())
      count++;
    if (0.0 != s.timePerWordAvg.doubleValue())
      count++;
    if (0.0 != s.numberOfWords.doubleValue())
      count++;
    if (0.0 != s.timePerCharAvg.doubleValue())
      count++;
    if (0.0 != s.numberOfChars.doubleValue())
      count++;
    if (0.0 != s.numberOfDeletedChars.doubleValue())
      count++;
    if (0.0 != s.numberOfAutoComplete.doubleValue())
      count++;
    if (0.0 != s.numberOfFaceBookMessages.doubleValue())
      count++;
    if (0.0 != s.numberOfSMS.doubleValue())
      count++;
    if (0.0 != s.numberOfGoogleMessages.doubleValue())
      count++;
    if (0.0 != s.numberOfGreenClicks.doubleValue())
      count++;
    if (0.0 != s.numberOfLongGreenClicks.doubleValue())
      count++;
    if (0.0 != s.numberOfOrangeClicks.doubleValue())
      count++;
    if (0.0 != s.numberOfLongOrangeClicks.doubleValue())
      count++;
    if (0.0 != s.numberOfRedClicks.doubleValue())
      count++;
    if (0.0 != s.numberOfLongRedClicks.doubleValue())
      count++;
    if (0.0 != s.numberOfBlueClicks.doubleValue())
      count++;
    if (0.0 != s.numberOfLongBlueClicks.doubleValue())
      count++;
    if (0.0 != s.numberOfBlueOrangeClicks.doubleValue())
      count++;
    if (0.0 != s.numberOfOrangeGreenClicks.doubleValue())
      count++;
    if (0.0 != s.numberOfGreenRedClicks.doubleValue())
      count++;
    if (0.0 != s.numberOfBlueRedClicks.doubleValue())
      count++;
    return count;
  }

}
