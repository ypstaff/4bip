package il.ac.technion.cs.ssdl.cs234311.yp09.states;

/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 6/11/2013
 * 
 */
public abstract class State {

  /**
   * Content of the short pressed operations.
   */
  public final String[] shortPress = new String[4];
  /**
   * Content of the long pressed operations.
   */
  public final String[] longPress = new String[4];

  /**
   * @return next state when the short operation #1 is chosen.
   * @throws Exception
   *           thrown by the controller
   */
  public abstract State onShort1Press() throws Exception;

  /**
   * @return next state when the short operation #2 is chosen.
   * @throws Exception
   *           thrown by the controller
   */
  public abstract State onShort2Press() throws Exception;

  /**
   * @return next state when the short operation #3 is chosen.
   * @throws Exception
   *           thrown by the controller
   */
  public abstract State onShort3Press() throws Exception;

  /**
   * @return next state when the short operation #4 is chosen.
   * @throws Exception
   *           thrown by the controller
   */
  public abstract State onShort4Press() throws Exception;

  /**
   * @return next state when the long operation #1 is chosen.
   * @throws Exception
   *           thrown by the controller
   */
  public abstract State onLong1Press() throws Exception;

  /**
   * @return next state when the long operation #2 is chosen.
   * @throws Exception
   *           thrown by the controller
   */
  public abstract State onLong2Press() throws Exception;

  /**
   * @return next state when the long operation #3 is chosen.
   * @throws Exception
   *           thrown by the controller
   */
  public abstract State onLong3Press() throws Exception;

  /**
   * @return next state when the long operation #4 is chosen.
   * @throws Exception
   *           thrown by the controller
   */
  public abstract State onLong4Press() throws Exception;
}
