package il.ac.technion.cs.ssdl.cs234311.yp09.connectionToServer;

/**
 * @author Tal this interface represents the functionality of sending a message
 *         to the server
 */
public interface ClientCom {

  /**
   * @param message2send
   *          - the message to be sent
   * @param number
   *          - the number to whom the message is sent
   * @param contact
   *          - name of recipient
   * @return ErrorType - in this package this is the function to be used by app
   *         to log messages on server
   */
  public ErrorType send2Server(String message2send, String number,
      String contact);

  /**
   * @param message2send
   *          - the message to be sent
   * @return ErrorType - in this package this is the function to be used by app
   *         to log messages on server
   */
  public ErrorType send2Server(String message2send);
}
