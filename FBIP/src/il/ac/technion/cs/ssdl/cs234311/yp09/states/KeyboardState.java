package il.ac.technion.cs.ssdl.cs234311.yp09.states;

import il.ac.technion.cs.ssdl.cs234311.yp09.controller.Controller;
import il.ac.technion.cs.ssdl.cs234311.yp09.controller.Controller.ActiveOp;

/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 24/11/2013
 * 
 */
public abstract class KeyboardState extends State {

  /**
   * Let the controller know that we are typing a message!
   */
  public KeyboardState() {
    Controller.m_activeOp = ActiveOp.MESSAGE;
  }

  /**
   * The current keyboard being used.
   */
  public static KB currentKB = KB.KB1;

  /**
   * Different types of keyboards
   * 
   */
  public enum KB {
    /**
     * The default keyboard.
     */
    KB1,
    /**
     * Another keyboard (considering the time needed to type a message).
     */
    KB2
  }

  /**
   * Will be replaced in an XML file.
   */
  public static String[] abc = { "a", "b", "c", "d", "e", "f", "g", "h", "i",
      "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w",
      "x", "y", "z" };
  /**
   * Will be replaced in an XML file.
   */
  public static String[] numbers = { "0", "1", "2", "3", "4", "5", "6", "7",
      "8", "9" };
  /**
   * Will be replaced in an XML file.
   */
  public static String[] symbols = { "space", ".", ",", "?", "-", "!", "@",
      "_", "/", "(", ")", "&", ":", ";", "$", "`", "<", ">", "^", "~", "[",
      "]", "{", "}", "\"", "+", "%", "=", "#", "*", "\\", "|", "'" };

  // , "+", "%", "=", "#", "*", "\\", "|", "'"

  /**
   * Will be replaced in an XML file.
   */
  public static String[] smileys = { ":)", ":(", ";)", ":D", ":p", ":-)",
      ":-(", ":|" };

  /**
   * @return an object representing the keyboard being used.
   */
  public static State returnKeyboard() {
    if (KB.KB1 == currentKB)
      return new Keyboard1State();
    return new Keyboard2State();
  }

  @Override
  public State onShort1Press() {
    // Implemented in the sub - classes
    return null;
  }

  @Override
  public State onShort2Press() {
    // Implemented in the sub - classes
    return null;
  }

  @Override
  public State onShort3Press() {
    // Implemented in the sub - classes
    return null;
  }

  @Override
  public State onShort4Press() {
    // Implemented in the sub - classes
    return null;
  }

  // Back
  @Override
  public final State onLong1Press() {
    return new MainState();
  }

  // Done
  @Override
  public final State onLong2Press() {
    // OWAIS : message.done();
    return new MainState();
  }

  // <--
  @Override
  public final State onLong3Press() {
    Controller.m_message.moveCurserToTheLeft();
    return this;
  }

  // -->
  @Override
  public final State onLong4Press() {
    Controller.m_message.moveCurserToTheRight();
    return this;
  }

}
