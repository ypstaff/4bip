package il.ac.technion.cs.ssdl.cs234311.yp09.states;

import il.ac.technion.cs.ssdl.cs234311.yp09.textController.TextController.JustNumbersException;
import junit.framework.TestCase;

/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 25/11/2013
 * 
 */
public class PhoneStateTest extends TestCase {
  /**
   * Test: constructor
   */
  public final static void testConstructor() {
    final PhoneState p = new PhoneState(KeyboardState.numbers);

    assertTrue(p.shortPress[0].equals("0 1 2"));
    assertTrue(p.shortPress[1].equals("3 4 5"));
    assertTrue(p.shortPress[2].equals("6 7 8"));
    assertTrue(p.shortPress[3].equals("9"));

    assertTrue(p.longPress[0].equals("Back"));
    assertTrue(p.longPress[1].equals("Done"));
    assertTrue(p.longPress[2].equals("Delete"));
    assertTrue(p.longPress[3].equals("Clear"));
  }

  /**
   * Test: onShort1Press
   * 
   * @throws JustNumbersException
   *           TextController may throw this exception
   */
  public final static void testOnShort1Press() throws JustNumbersException {
    final PhoneState p = new PhoneState(KeyboardState.numbers);
    final State s = p.onShort1Press();

    assertTrue(s.shortPress[0].equals("0"));
    assertTrue(s.shortPress[1].equals("1"));
    assertTrue(s.shortPress[2].equals("2"));
    assertTrue(s.shortPress[3].equals(""));

    assertTrue(s.longPress[0].equals("Back"));
    assertTrue(s.longPress[1].equals("Done"));
    assertTrue(s.longPress[2].equals("Delete"));
    assertTrue(s.longPress[3].equals("Clear"));
  }

  /**
   * Test: onShort2Press
   * 
   * @throws JustNumbersException
   *           TextController may throw this exception
   */
  public final static void testOnShort2Press() throws JustNumbersException {
    final PhoneState p = new PhoneState(KeyboardState.numbers);
    final State s = p.onShort2Press();

    assertTrue(s.shortPress[0].equals("3"));
    assertTrue(s.shortPress[1].equals("4"));
    assertTrue(s.shortPress[2].equals("5"));
    assertTrue(s.shortPress[3].equals(""));

    assertTrue(s.longPress[0].equals("Back"));
    assertTrue(s.longPress[1].equals("Done"));
    assertTrue(s.longPress[2].equals("Delete"));
    assertTrue(s.longPress[3].equals("Clear"));
  }

  /**
   * Test: onShort3Press
   * 
   * @throws JustNumbersException
   *           TextController may throw this exception
   */
  public final static void testOnShort3Press() throws JustNumbersException {
    final PhoneState p = new PhoneState(KeyboardState.numbers);
    final State s = p.onShort3Press();

    assertTrue(s.shortPress[0].equals("6"));
    assertTrue(s.shortPress[1].equals("7"));
    assertTrue(s.shortPress[2].equals("8"));
    assertTrue(s.shortPress[3].equals(""));

    assertTrue(s.longPress[0].equals("Back"));
    assertTrue(s.longPress[1].equals("Done"));
    assertTrue(s.longPress[2].equals("Delete"));
    assertTrue(s.longPress[3].equals("Clear"));
  }

  /**
   * Test: onShort4Press
   * 
   * @throws JustNumbersException
   *           TextController may throw this exception
   */
  public final static void testOnShort4Press() throws JustNumbersException {
    final PhoneState p = new PhoneState(KeyboardState.numbers);
    final State s = p.onShort4Press();

    assertTrue(p.shortPress[0].equals("0 1 2"));
    assertTrue(p.shortPress[1].equals("3 4 5"));
    assertTrue(p.shortPress[2].equals("6 7 8"));
    assertTrue(p.shortPress[3].equals("9"));

    assertTrue(s.longPress[0].equals("Back"));
    assertTrue(s.longPress[1].equals("Done"));
    assertTrue(s.longPress[2].equals("Delete"));
    assertTrue(s.longPress[3].equals("Clear"));
  }

  /**
   * Test: onLong1Press
   * 
   * @throws JustNumbersException
   *           TextController may throw this exception
   */
  public final static void testOnLong1Press() throws JustNumbersException {
    final PhoneState p = new PhoneState(KeyboardState.numbers);
    final State s = p.onLong1Press();

    assertTrue(s.shortPress[0].equals("Contact"));
    assertTrue(s.shortPress[1].equals("Number"));
    assertTrue(s.shortPress[2].equals(""));
    assertTrue(s.shortPress[3].equals(""));

    assertTrue(s.longPress[0].equals("Back"));
    assertTrue(s.longPress[1].equals("UNUSED"));
    assertTrue(s.longPress[2].equals("UNUSED"));
    assertTrue(s.longPress[3].equals("UNUSED"));
  }

  /**
   * Test: onLong2Press
   * 
   * @throws JustNumbersException
   *           TextController may throw this exception
   */
  public final static void testOnLong2Press() throws JustNumbersException {
    final PhoneState p = new PhoneState(KeyboardState.numbers);
    final State s = p.onLong2Press();

    assertTrue(s.shortPress[0].equals("Keyboard"));
    assertTrue(s.shortPress[1].equals("Protocol"));
    assertTrue(s.shortPress[2].equals("Recipient"));
    assertTrue(s.shortPress[3].equals("Settings"));

    assertTrue(s.longPress[0].equals("Exit"));
    assertTrue(s.longPress[1].equals("Send"));
    assertTrue(s.longPress[2].equals("UNUSED"));
    assertTrue(s.longPress[3].equals("UNUSED"));
  }

  /**
   * Test: onLong3Press
   * 
   * @throws JustNumbersException
   *           TextController may throw this exception
   */
  public final static void testOnLong3Press() throws JustNumbersException {
    final PhoneState p = new PhoneState(KeyboardState.numbers);
    final State s = p.onLong3Press();

    assertTrue(s.shortPress[0].equals("0 1 2"));
    assertTrue(s.shortPress[1].equals("3 4 5"));
    assertTrue(s.shortPress[2].equals("6 7 8"));
    assertTrue(s.shortPress[3].equals("9"));

    assertTrue(s.longPress[0].equals("Back"));
    assertTrue(s.longPress[1].equals("Done"));
    assertTrue(s.longPress[2].equals("Delete"));
    assertTrue(s.longPress[3].equals("Clear"));
  }

  /**
   * Test: onLong4Press
   * 
   * @throws JustNumbersException
   *           TextController may throw this exception
   */
  public final static void testOnLong4Press() throws JustNumbersException {
    final PhoneState p = new PhoneState(KeyboardState.numbers);
    final State s = p.onLong4Press();

    assertTrue(s.shortPress[0].equals("0 1 2"));
    assertTrue(s.shortPress[1].equals("3 4 5"));
    assertTrue(s.shortPress[2].equals("6 7 8"));
    assertTrue(s.shortPress[3].equals("9"));

    assertTrue(s.longPress[0].equals("Back"));
    assertTrue(s.longPress[1].equals("Done"));
    assertTrue(s.longPress[2].equals("Delete"));
    assertTrue(s.longPress[3].equals("Clear"));
  }
}
