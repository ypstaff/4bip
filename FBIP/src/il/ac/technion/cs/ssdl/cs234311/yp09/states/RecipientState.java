package il.ac.technion.cs.ssdl.cs234311.yp09.states;

/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 22/11/2013
 * 
 */
public class RecipientState extends State {

  /**
   * 
   */
  public RecipientState() {
    shortPress[0] = "Contact";
    shortPress[1] = "Number";
    shortPress[2] = "";
    shortPress[3] = "";

    longPress[0] = "Back";
    longPress[1] = "UNUSED";
    longPress[2] = "UNUSED";
    longPress[3] = "UNUSED";
  }

  // Contacts
  @Override
  public State onShort1Press() {
    // Meanwhile the number should be inserted manually.
    return this;
  }

  // Number
  @Override
  public State onShort2Press() {
    return new PhoneState(KeyboardState.numbers);
  }

  // UNUSED
  @Override
  public State onShort3Press() {
    // Not used in this iteration
    return this;
  }

  // UNUSED
  @Override
  public State onShort4Press() {
    // Not used in this iteration
    return this;
  }

  // Back
  @Override
  public State onLong1Press() {
    return new MainState();
  }

  // UNUSED
  @Override
  public State onLong2Press() {
    // Not used in this iteration
    return this;
  }

  // UNUSED
  @Override
  public State onLong3Press() {
    // Not used in this iteration
    return this;
  }

  // UNUSED
  @Override
  public State onLong4Press() {
    // Not used in this iteration
    return this;
  }

}
