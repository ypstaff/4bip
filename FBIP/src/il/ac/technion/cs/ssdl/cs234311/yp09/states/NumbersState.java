package il.ac.technion.cs.ssdl.cs234311.yp09.states;

import il.ac.technion.cs.ssdl.cs234311.yp09.controller.Controller;
import il.ac.technion.cs.ssdl.cs234311.yp09.textController.TextController.JustNumbersException;

/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 6/11/2013
 * 
 */
public final class NumbersState extends State {

  private final AccessedFrom previousState;

  /**
   * Possible previous states.
   * 
   */
  public enum AccessedFrom {
    /**
     * The keyboard state
     */
    Keyboard,
    /**
     * Numbers/Operations state
     */
    NumsOps
  }

  /**
   * @param numbers
   *          decimal digits from 0 to 9.
   * @param af
   *          the previous state of the application.
   */
  public NumbersState(final String[] numbers, final AccessedFrom af) {
    for (int i = 0; i < 4; i++)
      shortPress[i] = Parser.findContent(numbers, 0, numbers.length - 1, i, 4);

    longPress[0] = "Back";
    longPress[1] = "UNUSED";
    longPress[2] = "Lang";
    longPress[3] = "UNUSED";

    previousState = af;
  }

  @Override
  public final State onShort1Press() throws JustNumbersException {
    return calculateNextStateWhenShortPressed(1);
  }

  @Override
  public final State onShort2Press() throws JustNumbersException {
    return calculateNextStateWhenShortPressed(2);
  }

  @Override
  public final State onShort3Press() throws JustNumbersException {
    return calculateNextStateWhenShortPressed(3);
  }

  @Override
  public final State onShort4Press() throws JustNumbersException {
    return calculateNextStateWhenShortPressed(4);
  }

  /**
   * @param index
   *          the index of the button pressed
   * @return the next state.
   * @throws JustNumbersException
   *           TextController may throw this exception
   */
  public final State calculateNextStateWhenShortPressed(final int index)
      throws JustNumbersException {
    if (shortPress[index - 1].equals(""))
      return this;
    if (1 == shortPress[index - 1].length()) {
      Controller.m_message.insertChar(shortPress[index - 1].charAt(0));
      return KeyboardState.returnKeyboard();
    }
    if (previousState == AccessedFrom.Keyboard)
      return new NumbersState(shortPress[index - 1].split(" "),
          AccessedFrom.Keyboard);
    return new NumbersState(shortPress[index - 1].split(" "),
        AccessedFrom.NumsOps);
  }

  // Back
  @Override
  public final State onLong1Press() {
    if (previousState == AccessedFrom.Keyboard)
      return KeyboardState.returnKeyboard();
    return new NumsOpsState();
  }

  // UNUSED
  @Override
  public final State onLong2Press() {
    // Button is UNUSED in the first iteration
    return this;
  }

  // Lang
  @Override
  public final State onLong3Press() {
    /*
     * The format used for numbers in the first iteration is the regular one. We
     * will support more formats in the next iterations.
     */
    return this;
  }

  // UNUSED
  @Override
  public final State onLong4Press() {
    // Button is UNUSED in the first iteration
    return this;
  }

}
