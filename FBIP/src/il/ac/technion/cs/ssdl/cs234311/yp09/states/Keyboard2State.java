package il.ac.technion.cs.ssdl.cs234311.yp09.states;


/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 24/11/2013
 * 
 */
public class Keyboard2State extends KeyboardState {

  /**
   * 
   */
  public Keyboard2State() {
    shortPress[0] = "a - m";
    shortPress[1] = "n - z";
    shortPress[2] = "Symbols";
    shortPress[3] = "Nums/Ops";

    longPress[0] = "Back";
    longPress[1] = "Done";
    longPress[2] = "<--";
    longPress[3] = "-->";
  }

  // a - m
  @Override
  public final State onShort1Press() {
    return new LettersState(Parser.subArray(abc, 0, 12), true);
  }

  // n - z
  @Override
  public final State onShort2Press() {
    return new LettersState(Parser.subArray(abc, 13, 25), true);
  }

  // Symbols
  @Override
  public final State onShort3Press() {
    return new SymbolsState(symbols, true);
  }

  // Nums/Ops
  @Override
  public final State onShort4Press() {
    return new NumsOpsState();
  }

}
