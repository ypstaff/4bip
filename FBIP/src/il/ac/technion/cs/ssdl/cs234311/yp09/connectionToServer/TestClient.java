package il.ac.technion.cs.ssdl.cs234311.yp09.connectionToServer;

import java.math.BigInteger;
import java.security.SecureRandom;

import junit.framework.TestCase;

/**
 * @author Tal the class for testing the client side communication
 * 
 */
public class TestClient extends TestCase {
  static final String stubURL = "http://1.tal-server-stub.appspot.com/serverstub";

  /**
   * Test client communication to server
   */
  static public void testclient() {
    final Client c = new Client(stubURL);

    for (int i = 0; i < 5; i++) {
      String contacts[] = { "Tal", "Husam", "Itamar", "Daniel", "owais" };
      String numbers[] = { "111", "112", "0525564455", " kakakak", "334455"};
      final SecureRandom random = new SecureRandom();
      final String random_message = new BigInteger(130, random).toString(32);
      final ErrorType et = c.send2Server(random_message, numbers[i], contacts[i] );
      if (!et.name().equals(ErrorType.NO_ERROR.toString()))	 fail();
      if (!et.s.equals(("serverStub got message: " + random_message + "  contact: " +contacts[i] + "  number: "+ numbers[i])))
      	{
    	  fail();
      	}
    	  
    }
  }

}
