package il.ac.technion.cs.ssdl.cs234311.yp09.states;

import junit.framework.TestCase;

/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 25/11/2013
 * 
 */
public class Keyboard2StateTest extends TestCase {

  /**
   * Test: constructor
   */
  public final static void testConstructor() {

    final Keyboard2State kb = new Keyboard2State();

    assertTrue(kb.shortPress[0].equals("a - m"));
    assertTrue(kb.shortPress[1].equals("n - z"));
    assertTrue(kb.shortPress[2].equals("Symbols"));
    assertTrue(kb.shortPress[3].equals("Nums/Ops"));

    assertTrue(kb.longPress[0].equals("Back"));
    assertTrue(kb.longPress[1].equals("Done"));
    assertTrue(kb.longPress[2].equals("<--"));
    assertTrue(kb.longPress[3].equals("-->"));
  }

  /**
   * Test: onShort1Press
   */
  public final static void testOnShort1Press() {
    final Keyboard2State kb = new Keyboard2State();

    final State s = kb.onShort1Press();

    assertTrue(s.shortPress[0].equals("a b c d"));
    assertTrue(s.shortPress[1].equals("e f g h"));
    assertTrue(s.shortPress[2].equals("i j k l"));
    assertTrue(s.shortPress[3].equals("m"));

    assertTrue(s.longPress[0].equals("Back"));
    assertTrue(s.longPress[1].equals("UNUSED"));
    assertTrue(s.longPress[2].equals("Lang"));
    assertTrue(s.longPress[3].equals("A"));
  }

  /**
   * Test: onShort2Press
   */
  public final static void testOnShort2Press() {
    final Keyboard2State kb = new Keyboard2State();

    final State s = kb.onShort2Press();

    assertTrue(s.shortPress[0].equals("n o p q"));
    assertTrue(s.shortPress[1].equals("r s t u"));
    assertTrue(s.shortPress[2].equals("v w x y"));
    assertTrue(s.shortPress[3].equals("z"));

    assertTrue(s.longPress[0].equals("Back"));
    assertTrue(s.longPress[1].equals("UNUSED"));
    assertTrue(s.longPress[2].equals("Lang"));
    assertTrue(s.longPress[3].equals("A"));
  }

  /**
   * Test: onShort3Press
   */
  public final static void testOnShort3Press() {
    final Keyboard2State kb = new Keyboard2State();

    final State s = kb.onShort3Press();

    assertTrue(s.shortPress[0].equals("space . , ?"));
    assertTrue(s.shortPress[1].equals("- ! @ _ / ( ) & : ;"));
    assertTrue(s.shortPress[2].equals("$ ` < > ^ ~ [ ] { }"));
    assertTrue(s.shortPress[3].equals("\" + % = # * \\ | '"));

    assertTrue(s.longPress[0].equals("Back"));
    assertTrue(s.longPress[1].equals(":-)"));
    assertTrue(s.longPress[2].equals("UNUSED"));
    assertTrue(s.longPress[3].equals("UNUSED"));
  }

  /**
   * Test: onShort4Press
   */
  public final static void testOnShort4Press() {
    final Keyboard2State kb = new Keyboard2State();

    final State s = kb.onShort4Press();

    assertTrue(s.shortPress[0].equals("Numbers"));
    assertTrue(s.shortPress[1].equals("Operations"));
    assertTrue(s.shortPress[2].equals(""));
    assertTrue(s.shortPress[3].equals(""));

    assertTrue(s.longPress[0].equals("Back"));
    assertTrue(s.longPress[1].equals("UNUSED"));
    assertTrue(s.longPress[2].equals("UNUSED"));
    assertTrue(s.longPress[3].equals("UNUSED"));
  }
}
