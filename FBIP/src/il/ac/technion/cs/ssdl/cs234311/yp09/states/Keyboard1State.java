package il.ac.technion.cs.ssdl.cs234311.yp09.states;

/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 6/11/2013
 * 
 */
public final class Keyboard1State extends KeyboardState {

  /**
   * 
   */
  public Keyboard1State() {
    shortPress[0] = "Letters";
    shortPress[1] = "Numbers";
    shortPress[2] = "Symbols";
    shortPress[3] = "Operations";

    longPress[0] = "Back";
    longPress[1] = "Done";
    longPress[2] = "<--";
    longPress[3] = "-->";
  }

  // Letters
  @Override
  public final State onShort1Press() {
    return new LettersState(abc, true);
  }

  // Numbers
  @Override
  public final State onShort2Press() {
    return new NumbersState(numbers, NumbersState.AccessedFrom.Keyboard);
  }

  // Symbols
  @Override
  public final State onShort3Press() {
    return new SymbolsState(symbols, true);
  }

  // Operations
  @Override
  public final State onShort4Press() {
    return new OperationsState(OperationsState.AccessedFrom.Keyboard);
  }

}
