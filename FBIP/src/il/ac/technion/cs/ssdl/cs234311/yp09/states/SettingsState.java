package il.ac.technion.cs.ssdl.cs234311.yp09.states;


/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 23/11/2013
 * 
 */
public final class SettingsState extends State {

  /**
   * 
   */
  public SettingsState() {
    shortPress[0] = "Input";
    shortPress[1] = "";
    shortPress[2] = "";
    shortPress[3] = "";

    longPress[0] = "Back";
    longPress[1] = "UNUSED";
    longPress[2] = "UNUSED";
    longPress[3] = "UNUSED";
  }

  // Input
  @Override
  public State onShort1Press() {
    return new InputState();
  }

  // UNUSED
  @Override
  public State onShort2Press() {
    // unused in the first iteration
    return this;
  }

  // UNUSED
  @Override
  public State onShort3Press() {
    // unused in the first iteration
    return this;
  }

  // UNUSED
  @Override
  public State onShort4Press() {
    // unused in the first iteration
    return this;
  }

  // Back
  @Override
  public State onLong1Press() {
    return new MainState();
  }

  // UNUSED
  @Override
  public State onLong2Press() {
    // unused in the first iteration
    return this;
  }

  // UNUSED
  @Override
  public State onLong3Press() {
    // unused in the first iteration
    return this;
  }

  // UNUSED
  @Override
  public State onLong4Press() {
    // unused in the first iteration
    return this;
  }

}
