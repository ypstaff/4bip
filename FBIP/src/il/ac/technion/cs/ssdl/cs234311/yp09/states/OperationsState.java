package il.ac.technion.cs.ssdl.cs234311.yp09.states;

import il.ac.technion.cs.ssdl.cs234311.yp09.controller.Controller;
import il.ac.technion.cs.ssdl.cs234311.yp09.textController.TextController.JustNumbersException;

/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 6/11/2013
 * 
 */
public final class OperationsState extends State {

  private final AccessedFrom previousState;

  /**
   * Possible previous states.
   * 
   */
  public enum AccessedFrom {
    /**
     * The keyboard state
     */
    Keyboard,
    /**
     * Numbers/Operations state
     */
    NumsOps
  }

  /**
   * @param af
   *          the previous state of the application.
   */
  public OperationsState(final AccessedFrom af) {
    shortPress[0] = "Delete";
    shortPress[1] = "Delete All";
    shortPress[2] = "New Line";
    shortPress[3] = "Undo";

    longPress[0] = "Back";
    longPress[1] = "UNUSED";
    longPress[2] = "UNUSED";
    longPress[3] = "UNUSED";

    previousState = af;
  }

  // Delete
  @Override
  public final State onShort1Press() {
    Controller.m_message.deletetChar();
    return KeyboardState.returnKeyboard();
  }

  // Delete All
  @Override
  public final State onShort2Press() {
    Controller.m_message.deleteAll();
    return KeyboardState.returnKeyboard();
  }

  // New Line
  @Override
  public final State onShort3Press() throws JustNumbersException {
    Controller.m_message.insertChar('\n');
    return KeyboardState.returnKeyboard();
  }

  // Restore Previous
  @Override
  public final State onShort4Press() {
    Controller.m_message.undoLastOp();
    return KeyboardState.returnKeyboard();
  }

  // Back
  @Override
  public final State onLong1Press() {
    if (previousState == AccessedFrom.Keyboard)
      return KeyboardState.returnKeyboard();
    return new NumsOpsState();
  }

  // UNUSED
  @Override
  public final State onLong2Press() {
    // Button is UNUSED in the first iteration
    return this;
  }

  // UNUSED
  @Override
  public final State onLong3Press() {
    // Button is UNUSED in the first iteration
    return this;
  }

  // UNUSED
  @Override
  public final State onLong4Press() {
    // Button is UNUSED in the first iteration
    return this;
  }

}
