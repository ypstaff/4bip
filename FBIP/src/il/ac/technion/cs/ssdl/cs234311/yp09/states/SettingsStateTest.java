package il.ac.technion.cs.ssdl.cs234311.yp09.states;

import il.ac.technion.cs.ssdl.cs234311.yp09.textController.TextController.JustNumbersException;
import junit.framework.TestCase;

/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 25/11/2013
 * 
 */
public class SettingsStateTest extends TestCase {
  /**
   * Test: constructor
   */
  public final static void testConstructor() {
    final SettingsState ss = new SettingsState();

    assertTrue(ss.shortPress[0].equals("Input"));
    assertTrue(ss.shortPress[1].equals(""));
    assertTrue(ss.shortPress[2].equals(""));
    assertTrue(ss.shortPress[3].equals(""));

    assertTrue(ss.longPress[0].equals("Back"));
    assertTrue(ss.longPress[1].equals("UNUSED"));
    assertTrue(ss.longPress[2].equals("UNUSED"));
    assertTrue(ss.longPress[3].equals("UNUSED"));
  }

  /**
   * Test: onShort1Press
   * 
   * @throws JustNumbersException
   *           TextController may throw this exception
   */
  public final static void testOnShort1Press() throws JustNumbersException {
    final SettingsState ss = new SettingsState();
    final State s = ss.onShort1Press();

    assertTrue(s.shortPress[0].equals("KB - 1"));
    assertTrue(s.shortPress[1].equals("KB - 2"));
    assertTrue(s.shortPress[2].equals(""));
    assertTrue(s.shortPress[3].equals(""));

    assertTrue(s.longPress[0].equals("Back"));
    assertTrue(s.longPress[1].equals("UNUSED"));
    assertTrue(s.longPress[2].equals("UNUSED"));
    assertTrue(s.longPress[3].equals("UNUSED"));
  }

  /**
   * Test: onShort2Press
   * 
   * @throws JustNumbersException
   *           TextController may throw this exception
   */
  public final static void testOnShort2Press() throws JustNumbersException {
    final SettingsState ss = new SettingsState();
    final State s = ss.onShort2Press();

    assertTrue(s == ss);
  }

  /**
   * Test: onShort3Press
   * 
   * @throws JustNumbersException
   *           TextController may throw this exception
   */
  public final static void testOnShort3Press() throws JustNumbersException {
    final SettingsState ss = new SettingsState();
    final State s = ss.onShort3Press();

    assertTrue(s == ss);
  }

  /**
   * Test: onShort4Press
   * 
   * @throws JustNumbersException
   *           TextController may throw this exception
   */
  public final static void testOnShort4Press() throws JustNumbersException {
    final SettingsState ss = new SettingsState();
    final State s = ss.onShort4Press();

    assertTrue(s == ss);
  }

  /**
   * Test: onLong1Press
   * 
   * @throws JustNumbersException
   *           TextController may throw this exception
   */
  public final static void testOnLong1Press() throws JustNumbersException {
    final SettingsState ss = new SettingsState();
    final State s = ss.onLong1Press();

    assertTrue(s.shortPress[0].equals("Keyboard"));
    assertTrue(s.shortPress[1].equals("Protocol"));
    assertTrue(s.shortPress[2].equals("Recipient"));
    assertTrue(s.shortPress[3].equals("Settings"));

    assertTrue(s.longPress[0].equals("Exit"));
    assertTrue(s.longPress[1].equals("Send"));
    assertTrue(s.longPress[2].equals("UNUSED"));
    assertTrue(s.longPress[3].equals("UNUSED"));
  }

  /**
   * Test: onLong2Press
   * 
   * @throws JustNumbersException
   *           TextController may throw this exception
   */
  public final static void testOnLong2Press() throws JustNumbersException {
    final SettingsState ss = new SettingsState();
    final State s = ss.onLong2Press();

    assertTrue(s == ss);
  }

  /**
   * Test: onLong3Press
   * 
   * @throws JustNumbersException
   *           TextController may throw this exception
   */
  public final static void testOnLong3Press() throws JustNumbersException {
    final SettingsState ss = new SettingsState();
    final State s = ss.onLong3Press();

    assertTrue(s == ss);
  }

  /**
   * Test: onLong4Press
   * 
   * @throws JustNumbersException
   *           TextController may throw this exception
   */
  public final static void testOnLong4Press() throws JustNumbersException {
    final SettingsState ss = new SettingsState();
    final State s = ss.onLong4Press();

    assertTrue(s == ss);
  }
}
