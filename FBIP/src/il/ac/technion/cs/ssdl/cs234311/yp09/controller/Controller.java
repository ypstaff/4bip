package il.ac.technion.cs.ssdl.cs234311.yp09.controller;

import il.ac.technion.cs.ssdl.cs234311.yp09.connectionToServer.Client;
import il.ac.technion.cs.ssdl.cs234311.yp09.connectionToServer.ClientCom;
import il.ac.technion.cs.ssdl.cs234311.yp09.connectionToServer.ErrorType;
import il.ac.technion.cs.ssdl.cs234311.yp09.fbip.ControllerListener;
import il.ac.technion.cs.ssdl.cs234311.yp09.fbip.FourButtonsListener;
import il.ac.technion.cs.ssdl.cs234311.yp09.states.MainState;
import il.ac.technion.cs.ssdl.cs234311.yp09.states.State;
import il.ac.technion.cs.ssdl.cs234311.yp09.textController.TextController;

import java.io.Serializable;

import android.os.StrictMode;
import android.util.Log;

/**
 * Controls the UI and the logic part
 * 
 * @date 11/5/2013
 * @email owais.musa@gmail.com
 * @author Owais Musa, Itamar
 */
public class Controller implements Serializable, FourButtonsListener {

  /**
   * Holds the current message text
   */
  static public TextController m_message = new TextController(false);

  /**
   * Holds the current number of the recipient
   */
  static public TextController m_recipientNumber = new TextController(true);

  /**
   * Last operation that was done
   */
  static public ActiveOp m_activeOp = ActiveOp.MESSAGE;

  private static final long serialVersionUID = 4242079014671416782L;

  private static ControllerListener m_controllerListener;
  private static final String TAG = "CONTROLLER";

  static private State m_currentState;

  static final private int MIN_RECIPIENT_LENGTH = 3;

  /**
   *
   */
  public static enum PressID {
    /**
     * 
     */
    BLUE_PRESS,
    /**
     * 
     */
    YELLOW_PRESS,
    /**
     * 
     */
    GREEN_PRESS,
    /**
     * 
     */
    RED_PRESS
  }

  /**
  *
  */
  public static enum ActiveOp {
    /**
    * 
    */
    MESSAGE,
    /**
    * 
    */
    RECIPIENT
  }

  /**
   * Setter to m_controllerListener
   * 
   * @param in_controllerListener
   *          the part from the UI to be listening to the logic part
   */
  public static void setListener(final ControllerListener in_controllerListener) {
    m_controllerListener = in_controllerListener;
  }

  /**
   * Constructs the controller with the main state
   */
  public Controller() {
    m_currentState = new MainState();
  }

  /**
   * change the current content on the screen
   * 
   * @param m_State
   *          next state to be shown on the screen
   */
  public static void displayState(final State m_State) {
    assert m_State != null;

    m_currentState = m_State;

    m_controllerListener.onShortInfoUpdate(m_currentState.shortPress);
    m_controllerListener.onLongInfoUpdate(m_currentState.longPress);
    m_controllerListener.onUpdateMessage(m_message.getText(),
        m_message.getCursorPosition());
    m_controllerListener.onUpdateRecipient(m_recipientNumber.getText(),
        m_recipientNumber.getCursorPosition());

    m_controllerListener.onUpdateActiveEditText(m_activeOp);

    m_controllerListener.onUpdateDisplay();
  }

  /**
   * Starts the activity of the controller
   */
  public static void start() {
    displayState(m_currentState);
  }

  private static void errorWithCommunicationWithServer() {
    m_controllerListener.onToastShow("Error while sending to server!");
  }

  /**
   * To be called from the logic part in order to send the current message as
   * SMS and as a log to the server
   */
  public static void sendSMS() {
    final String recipientText = m_recipientNumber.getText();

    if (recipientText.length() < MIN_RECIPIENT_LENGTH) {
      m_controllerListener.onToastShow("illegal recipient number!");
      return;
    }

    String phoneNumber;

    if (!recipientText.startsWith("+"))
      phoneNumber = "+972" + m_recipientNumber.getText().substring(1);
    else
      phoneNumber = m_recipientNumber.getText();

    m_controllerListener.onToastShow("Sending SMS to" + phoneNumber);

    try {
      m_controllerListener.onSendSMS(m_message.getText(), phoneNumber);
    } catch (final Exception e) {
      m_controllerListener.onToastShow("Error while sending the SMS");
    }

    final StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
        .permitAll().build();

    StrictMode.setThreadPolicy(policy);

    final ClientCom ClientConnection = new Client();
    ErrorType errorType;
    errorType = ClientConnection.send2Server(m_message.getText(), phoneNumber,
        "group9");

    if (errorType != ErrorType.NO_ERROR) {
      errorWithCommunicationWithServer();
      return;
    }

    errorType = ClientConnection.send2Server(m_message.getText(), phoneNumber,
        "");

    if (errorType != ErrorType.NO_ERROR) {
      errorWithCommunicationWithServer();
      return;
    }
  }

  /**
   * Extract the contents from the XML file and return it as arrays to strings
   * 
   * @param in_contentFile
   *          the XML file
   * @param out_shortPress
   *          four strings that represent the content of the short presses
   *          buttons
   * @param out_longPress
   *          four strings that represent the content of the long presses
   */
  public static void getContentFromXMLFile(final String in_contentFile,
      final String[] out_shortPress, final String[] out_longPress) {
    XMLScreenContentParser.parseScreenContent(in_contentFile, out_shortPress,
        out_longPress);
  }

  @Override
  public void onShortPress(final PressID p) {
    Log.d(TAG, "press: emphasizing short " + p);
    m_controllerListener.emphasizeShort(p, true);
  }

  @Override
  public void onShortRelease(final PressID p) {
    Log.d(TAG, "release: de-emphasizing short " + p);
    m_controllerListener.emphasizeShort(p, false);
    State next = null;
    try {
      switch (p) {
      case BLUE_PRESS:
        next = m_currentState.onShort1Press();
        break;
      case YELLOW_PRESS:
        next = m_currentState.onShort2Press();
        break;
      case GREEN_PRESS:
        next = m_currentState.onShort3Press();
        break;
      case RED_PRESS:
        next = m_currentState.onShort4Press();
        break;
      default:
        assert false;
      }
    } catch (final Exception e) {
      m_controllerListener.onToastShow("seems like there is a serious error!");
    }
    displayState(next);
  }

  @Override
  public void onLongPress(final PressID p) {
    Log.d(TAG, "press: de-emphasizing short " + p);
    m_controllerListener.emphasizeShort(p, false);
    Log.d(TAG, "press: emphasizing long " + p);
    m_controllerListener.emphasizeLong(p, true);
  }

  @Override
  public void onLongRelease(final PressID p) {
    Log.d(TAG, "release: de-emphasizing long " + p);
    m_controllerListener.emphasizeLong(p, false);
    State next = null;
    try {
      switch (p) {
      case BLUE_PRESS:
        next = m_currentState.onLong1Press();
        break;
      case YELLOW_PRESS:
        next = m_currentState.onLong2Press();
        break;
      case GREEN_PRESS:
        next = m_currentState.onLong3Press();
        break;
      case RED_PRESS:
        next = m_currentState.onLong4Press();
        break;
      default:
        assert false;
      }
    } catch (final Exception e) {
      m_controllerListener.onToastShow("seems like there is a serious error!");
    }
    displayState(next);
  }

  /**
   * @return the listener registered to handle controller requests
   */
  public static ControllerListener getListener() {
    return m_controllerListener;
  }

}
