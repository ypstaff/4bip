package il.ac.technion.cs.ssdl.cs234311.yp09.states;

/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 23/11/2013
 * 
 */
public final class InputState extends State {

  /**
   * 
   */
  public InputState() {
    shortPress[0] = "KB - 1";
    shortPress[1] = "KB - 2";
    shortPress[2] = "";
    shortPress[3] = "";

    longPress[0] = "Back";
    longPress[1] = "UNUSED";
    longPress[2] = "UNUSED";
    longPress[3] = "UNUSED";
  }

  // KB - 1
  @Override
  public State onShort1Press() {
    KeyboardState.currentKB = KeyboardState.KB.KB1;
    return new MainState();
  }

  // KB - 2
  @Override
  public State onShort2Press() {
    KeyboardState.currentKB = KeyboardState.KB.KB2;
    return new MainState();
  }

  // UNUSED
  @Override
  public State onShort3Press() {
    // not used in this iteration
    return this;
  }

  // UNUSED
  @Override
  public State onShort4Press() {
    // not used in this iteration
    return this;
  }

  // Back
  @Override
  public State onLong1Press() {
    return new SettingsState();
  }

  // UNUSED
  @Override
  public State onLong2Press() {
    // not used in this iteration
    return this;
  }

  // UNUSED
  @Override
  public State onLong3Press() {
    // not used in this iteration
    return this;
  }

  // UNUSED
  @Override
  public State onLong4Press() {
    // not used in this iteration
    return this;
  }

}
