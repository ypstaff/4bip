package il.ac.technion.cs.ssdl.cs234311.yp09.connectionToServer;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

/**
 * @author Tal class Client implements the user side of client-server
 *         communication
 */
public class Client implements ClientCom {

  /*
   * ----------------------- ------------------ --------------------------- ***
   * ------------------------------ CONSTANTS------------------------------ ***
   * ---------------------------------------------------------------------- ***
   */
  private static final String ERROR_URL = "an ERROR acurred while creating URL";
  private static final String ERROR_CONNECT = "an ERROR acurred while connecting to server";
  private static final String ERROR_SET_POST = "problem setting POST";
  private static final String ERROR_BAD_RESPONSE_CODE = " got a bad response code from server : ";
  private static final String ERROR_OPEN_CONNECT = "problem opening output stream";
  private static final String ERROR_OPEN_STREAM = " an ERROR while opening output stream";
  private static final String ERROR_WRITING_MESSAGE = "error ecured while writing message";
  private static final String ERROR_READING_RESPONSE = "could not read response";
  private static final String ERROR_CLOSING_OUTPUTDATASTREAM = "could not close DataOutputStream";
  private static final String URL_ADRRESS_DEFAULT = "http://1.4bip-team9.appspot.com//talserver";
  private static final String DEBUG_LOG_FILE_NAME = "Debug Error Log.txt";
  private static final String PROBLEM_WRITING_TO_FILE = "PROBLEM WRITING TO DEBUG FILE";
  private static final String ERROR_WRITER_CLOSE = "WRITER DID NOT CLOSE";

  /*
   * ----------------------- ------------------ --------------------------- ***
   * ------------------------------ FIELDS--------------------------------- ***
   * ---------------------------------------------------------------------- ***
   */
  private final String URL_ADRRESS;

  /*
   * ----------------------- ------------------ --------------------------- ***
   * --------------------------- CONSTRUCTORS------------------------------ ***
   * ---------------------------------------------------------------------- ***
   */
  /**
   * default constructor - uses the real google app
   */
  public Client() {
    URL_ADRRESS = URL_ADRRESS_DEFAULT;
  }

  /**
   * @param url
   *          - the address of the test google_app a constructor for testing -
   *          use a different google_app to test the application
   */
  public Client(final String url) {
    URL_ADRRESS = url;
  }

  /*
   * ----------------------- ------------------ --------------------------- ***
   * -------------------------- USER FUNCTIONS ---------------------------- ***
   * ---------------------------------------------------------------------- ***
   */

  @Override
  public ErrorType send2Server(final String message2send) {
    return sendPost(message2send, "", "", false);
  }

  @Override
  public ErrorType send2Server(final String message2send, final String number,
      final String contact) {
    return sendPost(message2send, number, contact, false);
  }

  /*
   * ----------------------- ------------------ --------------------------- ***
   * ------------------------- DEBUG FUNCTIONS ---------------------------- ***
   * ---------------------------------------------------------------------- ***
   */

  protected ErrorType send2ServerDebeg(final String message2send,
      final String number, final String contact) {
    return sendPost(message2send, number, contact, true);
  }

  /*
   * ----------------------- ------------------ --------------------------- ***
   * -------------------------- COMM FUNCTIONS ---------------------------- ***
   * ---------------------------------------------------------------------- ***
   */
  private ErrorType sendPost(final String message2send, final String number,
      final String contact, final boolean debugMode) {

    URL obj = null;

    // make a new URL object out of address string
    try {
      obj = new URL(URL_ADRRESS);
    } catch (final MalformedURLException e) {
      return endWithError(ErrorType.SEND_FAIL, ERROR_URL, debugMode);
    }

    HttpURLConnection con = null;
    // open connection
    try {
      con = (HttpURLConnection) obj.openConnection();
    } catch (final IOException e) {
      return endWithError(ErrorType.SEND_FAIL, ERROR_CONNECT, debugMode);
    }

    // post message
    try {
      con.setRequestMethod("POST");
    } catch (final IOException e1) {
      return endWithError(ErrorType.SEND_FAIL, ERROR_SET_POST, debugMode);
    }

    con.setDoOutput(true);
    DataOutputStream wr = null;

    try {
      wr = new DataOutputStream(con.getOutputStream());
    } catch (final IOException e1) {
      return endWithError(ErrorType.SEND_FAIL, ERROR_OPEN_STREAM, debugMode);
    }

    try {
      wr.writeBytes(message2send + "\n");
      wr.writeBytes(number + "\n");
      wr.writeBytes(contact);
    } catch (final IOException e1) {
      return endWithError(ErrorType.SEND_FAIL, ERROR_WRITING_MESSAGE, debugMode);
    }

    int responseCode = -1;
    // sending the GET message
    try {
      responseCode = con.getResponseCode();
    } catch (final IOException e) {
      return endWithError(ErrorType.SEND_FAIL, ERROR_OPEN_CONNECT, debugMode);
    }

    if (responseCode != 200)
      return endWithError(ErrorType.BAD_RESPONSE, ERROR_BAD_RESPONSE_CODE,
          debugMode);

    BufferedReader in = null;
    try {
      in = new BufferedReader(new InputStreamReader(con.getInputStream()));
    } catch (final IOException e) {
      return endWithError(ErrorType.BAD_RESPONSE, ERROR_READING_RESPONSE,
          debugMode);

    }

    final StringBuffer response = new StringBuffer();
    String inputLine;

    try {
      while ((inputLine = in.readLine()) != null)
        response.append(inputLine);
    } catch (final IOException e) {
      return endWithError(ErrorType.BAD_RESPONSE, ERROR_READING_RESPONSE,
          debugMode);
    }

    try {
      in.close();
    } catch (final IOException e) {
      return endWithError(ErrorType.BAD_RESPONSE,
          ERROR_CLOSING_OUTPUTDATASTREAM, debugMode);
    }

    return endWithError(ErrorType.NO_ERROR, response.toString(), debugMode);
  }

  private static ErrorType endWithError(final ErrorType et,
      final String errorMessage, final boolean debugMode) {

    if (debugMode) {
      // System.out.println(errorMessage); // keep this option in mind
      FileWriter writer = null;
      try {
        writer = new FileWriter(DEBUG_LOG_FILE_NAME, true);
        writer.write("at: " + new Date().toString() + "\n");
        writer.write(errorMessage + "\n\n");
      } catch (final IOException e) {
        System.out.println(PROBLEM_WRITING_TO_FILE);
      }
      try {
        if (writer != null)
          writer.close();
      } catch (final IOException e) {
        System.out.println(ERROR_WRITER_CLOSE);
      }

    }
    et.s = errorMessage;
    return et;
  }
}
