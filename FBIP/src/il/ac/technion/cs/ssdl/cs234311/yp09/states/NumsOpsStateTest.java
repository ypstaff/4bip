package il.ac.technion.cs.ssdl.cs234311.yp09.states;

import il.ac.technion.cs.ssdl.cs234311.yp09.textController.TextController.JustNumbersException;
import junit.framework.TestCase;

/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 25/11/2013
 * 
 */
public class NumsOpsStateTest extends TestCase {
  /**
   * Test: constructor
   */
  public final static void testConstructor() {
    final NumsOpsState nos = new NumsOpsState();

    assertTrue(nos.shortPress[0].equals("Numbers"));
    assertTrue(nos.shortPress[1].equals("Operations"));
    assertTrue(nos.shortPress[2].equals(""));
    assertTrue(nos.shortPress[3].equals(""));

    assertTrue(nos.longPress[0].equals("Back"));
    assertTrue(nos.longPress[1].equals("UNUSED"));
    assertTrue(nos.longPress[2].equals("UNUSED"));
    assertTrue(nos.longPress[3].equals("UNUSED"));
  }

  /**
   * Test: onShort1Press
   * 
   * @throws JustNumbersException
   *           TextController may throw this exception
   */
  public final static void testOnShort1Press() throws JustNumbersException {
    final NumsOpsState nos = new NumsOpsState();
    final State s = nos.onShort1Press();

    assertTrue(s.shortPress[0].equals("0 1 2"));
    assertTrue(s.shortPress[1].equals("3 4 5"));
    assertTrue(s.shortPress[2].equals("6 7 8"));
    assertTrue(s.shortPress[3].equals("9"));

    assertTrue(s.longPress[0].equals("Back"));
    assertTrue(s.longPress[1].equals("UNUSED"));
    assertTrue(s.longPress[2].equals("Lang"));
    assertTrue(s.longPress[3].equals("UNUSED"));
  }

  /**
   * Test: onShort2Press
   * 
   * @throws JustNumbersException
   *           TextController may throw this exception
   */
  public final static void testOnShort2Press() throws JustNumbersException {
    final NumsOpsState nos = new NumsOpsState();
    final State s = nos.onShort2Press();

    assertTrue(s.shortPress[0].equals("Delete"));
    assertTrue(s.shortPress[1].equals("Delete All"));
    assertTrue(s.shortPress[2].equals("New Line"));
    assertTrue(s.shortPress[3].equals("Undo"));

    assertTrue(s.longPress[0].equals("Back"));
    assertTrue(s.longPress[1].equals("UNUSED"));
    assertTrue(s.longPress[2].equals("UNUSED"));
    assertTrue(s.longPress[3].equals("UNUSED"));
  }

  /**
   * Test: onShort3Press
   * 
   * @throws JustNumbersException
   *           TextController may throw this exception
   */
  public final static void testOnShort3Press() throws JustNumbersException {
    final NumsOpsState nos = new NumsOpsState();
    final State s = nos.onShort3Press();

    assertTrue(s == nos);
  }

  /**
   * Test: onShort4Press
   * 
   * @throws JustNumbersException
   *           TextController may throw this exception
   */
  public final static void testOnShort4Press() throws JustNumbersException {
    final NumsOpsState nos = new NumsOpsState();
    final State s = nos.onShort4Press();

    assertTrue(s == nos);
  }

  /**
   * Test: onLong1Press
   */
  public final static void testOnLong1Press() {
    final NumsOpsState nos = new NumsOpsState();

    final State s = nos.onLong1Press();

    assertTrue(s.shortPress[0].equals("Letters"));
    assertTrue(s.shortPress[1].equals("Numbers"));
    assertTrue(s.shortPress[2].equals("Symbols"));
    assertTrue(s.shortPress[3].equals("Operations"));

    assertTrue(s.longPress[0].equals("Back"));
    assertTrue(s.longPress[1].equals("Done"));
    assertTrue(s.longPress[2].equals("<--"));
    assertTrue(s.longPress[3].equals("-->"));
  }

  /**
   * Test: onLong2Press
   */
  public final static void testOnLong2Press() {
    final NumsOpsState nos = new NumsOpsState();

    final State s = nos.onLong2Press();

    assertTrue(s == nos);
  }

  /**
   * Test: onLong3Press
   */
  public final static void testOnLong3Press() {
    final NumsOpsState nos = new NumsOpsState();

    final State s = nos.onLong3Press();

    assertTrue(s == nos);
  }

  /**
   * Test: onLong4Press
   */
  public final static void testOnLong4Press() {
    final NumsOpsState nos = new NumsOpsState();

    final State s = nos.onLong4Press();

    assertTrue(s == nos);
  }
}
