package il.ac.technion.cs.ssdl.cs234311.yp09.connectionToServer;

/**
 * @author Tal Error type - used in client side communication to the client
 */
public enum ErrorType {
  /**
   * NO_ERROR - there was no error
   */
  NO_ERROR,

  /**
   * SEND_FAIL - the message was not even sent to the server
   */
  SEND_FAIL,

  /**
   * BAD_RESPONSE - the server got the message but did not recognize the format/
   * had a bad parameter / an internal error occurred in the server and hence,
   * the message was not logged
   */
  BAD_RESPONSE;

  /**
   * s- a string describing the error, if NO_ERROR - the message received by
   * server Otherwise- a description of the error
   */
  public String s;

  ErrorType() {
    s = "";
  }
}
