package il.ac.technion.cs.ssdl.cs234311.yp09.states;

/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 24/11/2013
 * 
 */
public class NumsOpsState extends State {

  /**
   * 
   */
  public NumsOpsState() {
    shortPress[0] = "Numbers";
    shortPress[1] = "Operations";
    shortPress[2] = "";
    shortPress[3] = "";

    longPress[0] = "Back";
    longPress[1] = "UNUSED";
    longPress[2] = "UNUSED";
    longPress[3] = "UNUSED";
  }

  // Numbers
  @Override
  public State onShort1Press() {
    return new NumbersState(KeyboardState.numbers,
        NumbersState.AccessedFrom.NumsOps);
  }

  // Operations
  @Override
  public State onShort2Press() {
    return new OperationsState(OperationsState.AccessedFrom.NumsOps);
  }

  // UNUSED
  @Override
  public State onShort3Press() {
    // unused in this iteration
    return this;
  }

  // UNUSED
  @Override
  public State onShort4Press() {
    // unused in this iteration
    return this;
  }

  // Back
  @Override
  public State onLong1Press() {
    return KeyboardState.returnKeyboard();
  }

  // UNUSED
  @Override
  public State onLong2Press() {
    // unused in this iteration
    return this;
  }

  // UNUSED
  @Override
  public State onLong3Press() {
    // unused in this iteration
    return this;
  }

  // UNUSED
  @Override
  public State onLong4Press() {
    // unused in this iteration
    return this;
  }

}
