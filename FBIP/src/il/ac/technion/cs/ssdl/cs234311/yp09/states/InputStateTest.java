package il.ac.technion.cs.ssdl.cs234311.yp09.states;

import il.ac.technion.cs.ssdl.cs234311.yp09.textController.TextController.JustNumbersException;
import junit.framework.TestCase;

/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 15/11/2013
 * 
 */
public class InputStateTest extends TestCase {

  /**
   * Test: constructor
   */
  public final static void testConstructor() {
    final InputState i = new InputState();

    assertTrue(i.shortPress[0].equals("KB - 1"));
    assertTrue(i.shortPress[1].equals("KB - 2"));
    assertTrue(i.shortPress[2].equals(""));
    assertTrue(i.shortPress[3].equals(""));

    assertTrue(i.longPress[0].equals("Back"));
    assertTrue(i.longPress[1].equals("UNUSED"));
    assertTrue(i.longPress[2].equals("UNUSED"));
    assertTrue(i.longPress[3].equals("UNUSED"));
  }

  /**
   * Test: onShort1Press
   * 
   * @throws JustNumbersException
   *           TextController may throw this exception
   */
  public final static void testOnShort1Press() throws JustNumbersException {
    final InputState i = new InputState();
    KeyboardState.currentKB = KeyboardState.KB.KB1;
    final State s = i.onShort1Press();

    assertTrue(s.shortPress[0].equals("Keyboard"));
    assertTrue(s.shortPress[1].equals("Protocol"));
    assertTrue(s.shortPress[2].equals("Recipient"));
    assertTrue(s.shortPress[3].equals("Settings"));

    assertTrue(s.longPress[0].equals("Exit"));
    assertTrue(s.longPress[1].equals("Send"));
    assertTrue(s.longPress[2].equals("UNUSED"));
    assertTrue(s.longPress[3].equals("UNUSED"));

    assertTrue(KeyboardState.currentKB == KeyboardState.KB.KB1);
  }

  /**
   * Test: onShort2Press
   * 
   * @throws JustNumbersException
   *           TextController may throw this exception
   */
  public final static void testOnShort2Press() throws JustNumbersException {
    final InputState i = new InputState();
    KeyboardState.currentKB = KeyboardState.KB.KB1;
    final State s = i.onShort2Press();

    assertTrue(s.shortPress[0].equals("Keyboard"));
    assertTrue(s.shortPress[1].equals("Protocol"));
    assertTrue(s.shortPress[2].equals("Recipient"));
    assertTrue(s.shortPress[3].equals("Settings"));

    assertTrue(s.longPress[0].equals("Exit"));
    assertTrue(s.longPress[1].equals("Send"));
    assertTrue(s.longPress[2].equals("UNUSED"));
    assertTrue(s.longPress[3].equals("UNUSED"));

    assertTrue(KeyboardState.currentKB == KeyboardState.KB.KB2);
  }

  /**
   * Test: onShort3Press
   * 
   * @throws JustNumbersException
   *           TextController may throw this exception
   */
  public final static void testOnShort3Press() throws JustNumbersException {
    final InputState i = new InputState();
    KeyboardState.currentKB = KeyboardState.KB.KB1;
    final State s = i.onShort3Press();

    assertTrue(s == i);
  }

  /**
   * Test: onShort4Press
   * 
   * @throws JustNumbersException
   *           TextController may throw this exception
   */
  public final static void testOnShort4Press() throws JustNumbersException {
    final InputState i = new InputState();
    KeyboardState.currentKB = KeyboardState.KB.KB1;
    final State s = i.onShort3Press();

    assertTrue(s == i);
  }

  /**
   * Test: onLong1Press
   * 
   * @throws JustNumbersException
   *           TextController may throw this exception
   */
  public final static void testOnLong1Press() throws JustNumbersException {
    final InputState i = new InputState();
    KeyboardState.currentKB = KeyboardState.KB.KB1;
    final State s = i.onLong1Press();

    assertTrue(s.shortPress[0].equals("Input"));
    assertTrue(s.shortPress[1].equals(""));
    assertTrue(s.shortPress[2].equals(""));
    assertTrue(s.shortPress[3].equals(""));

    assertTrue(s.longPress[0].equals("Back"));
    assertTrue(s.longPress[1].equals("UNUSED"));
    assertTrue(s.longPress[2].equals("UNUSED"));
    assertTrue(s.longPress[3].equals("UNUSED"));

  }

  /**
   * Test: onLong2Press
   * 
   * @throws JustNumbersException
   *           TextController may throw this exception
   */
  public final static void testOnLong2Press() throws JustNumbersException {
    final InputState i = new InputState();
    KeyboardState.currentKB = KeyboardState.KB.KB1;
    final State s = i.onLong2Press();

    assertTrue(s == i);

  }

  /**
   * Test: onLong3Press
   * 
   * @throws JustNumbersException
   *           TextController may throw this exception
   */
  public final static void testOnLong3Press() throws JustNumbersException {
    final InputState i = new InputState();
    KeyboardState.currentKB = KeyboardState.KB.KB1;
    final State s = i.onLong2Press();

    assertTrue(s == i);
  }

  /**
   * Test: onLong4Press
   * 
   * @throws JustNumbersException
   *           TextController may throw this exception
   */
  public final static void testOnLong4Press() throws JustNumbersException {
    final InputState i = new InputState();
    KeyboardState.currentKB = KeyboardState.KB.KB1;
    final State s = i.onLong2Press();

    assertTrue(s == i);
  }

}
