package il.ac.technion.cs.ssdl.cs234311.yp09.states;

import il.ac.technion.cs.ssdl.cs234311.yp09.textController.TextController.JustNumbersException;
import junit.framework.TestCase;

/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 25/11/2013
 * 
 */
public class RecipientStateTest extends TestCase {
  /**
   * Test: constructor
   */
  public final static void testConstructor() {
    final RecipientState r = new RecipientState();

    assertTrue(r.shortPress[0].equals("Contact"));
    assertTrue(r.shortPress[1].equals("Number"));
    assertTrue(r.shortPress[2].equals(""));
    assertTrue(r.shortPress[3].equals(""));

    assertTrue(r.longPress[0].equals("Back"));
    assertTrue(r.longPress[1].equals("UNUSED"));
    assertTrue(r.longPress[2].equals("UNUSED"));
    assertTrue(r.longPress[3].equals("UNUSED"));
  }

  /**
   * Test: onShort1Press
   * 
   * @throws JustNumbersException
   *           TextController may throw this exception
   */
  public final static void testOnShort1Press() throws JustNumbersException {
    final RecipientState r = new RecipientState();
    final State s = r.onShort1Press();

    assertTrue(r == s);
  }

  /**
   * Test: onShort2Press
   * 
   * @throws JustNumbersException
   *           TextController may throw this exception
   */
  public final static void testOnShort2Press() throws JustNumbersException {
    final RecipientState r = new RecipientState();
    final State s = r.onShort2Press();

    assertTrue(s.shortPress[0].equals("0 1 2"));
    assertTrue(s.shortPress[1].equals("3 4 5"));
    assertTrue(s.shortPress[2].equals("6 7 8"));
    assertTrue(s.shortPress[3].equals("9"));

    assertTrue(s.longPress[0].equals("Back"));
    assertTrue(s.longPress[1].equals("Done"));
    assertTrue(s.longPress[2].equals("Delete"));
    assertTrue(s.longPress[3].equals("Clear"));
  }

  /**
   * Test: onShort3Press
   * 
   * @throws JustNumbersException
   *           TextController may throw this exception
   */
  public final static void testOnShort3Press() throws JustNumbersException {
    final RecipientState r = new RecipientState();
    final State s = r.onShort3Press();

    assertTrue(r == s);
  }

  /**
   * Test: onShort4Press
   * 
   * @throws JustNumbersException
   *           TextController may throw this exception
   */
  public final static void testOnShort4Press() throws JustNumbersException {
    final RecipientState r = new RecipientState();
    final State s = r.onShort4Press();

    assertTrue(r == s);
  }

  /**
   * Test: onLong1Press
   * 
   * @throws JustNumbersException
   *           TextController may throw this exception
   */
  public final static void testOnLong1Press() throws JustNumbersException {
    final RecipientState r = new RecipientState();
    final State s = r.onLong1Press();

    assertTrue(s.shortPress[0].equals("Keyboard"));
    assertTrue(s.shortPress[1].equals("Protocol"));
    assertTrue(s.shortPress[2].equals("Recipient"));
    assertTrue(s.shortPress[3].equals("Settings"));

    assertTrue(s.longPress[0].equals("Exit"));
    assertTrue(s.longPress[1].equals("Send"));
    assertTrue(s.longPress[2].equals("UNUSED"));
    assertTrue(s.longPress[3].equals("UNUSED"));
  }

  /**
   * Test: onLong2Press
   * 
   * @throws JustNumbersException
   *           TextController may throw this exception
   */
  public final static void testOnLong2Press() throws JustNumbersException {
    final RecipientState r = new RecipientState();
    final State s = r.onLong2Press();

    assertTrue(r == s);
  }

  /**
   * Test: onLong3Press
   * 
   * @throws JustNumbersException
   *           TextController may throw this exception
   */
  public final static void testOnLong3Press() throws JustNumbersException {
    final RecipientState r = new RecipientState();
    final State s = r.onLong3Press();

    assertTrue(r == s);
  }

  /**
   * Test: onLong4Press
   * 
   * @throws JustNumbersException
   *           TextController may throw this exception
   */
  public final static void testOnLong4Press() throws JustNumbersException {
    final RecipientState r = new RecipientState();
    final State s = r.onLong4Press();

    assertTrue(r == s);
  }
}
