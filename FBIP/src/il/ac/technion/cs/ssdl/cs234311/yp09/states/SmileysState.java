package il.ac.technion.cs.ssdl.cs234311.yp09.states;

import il.ac.technion.cs.ssdl.cs234311.yp09.controller.Controller;
import il.ac.technion.cs.ssdl.cs234311.yp09.textController.TextController.JustNumbersException;

/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 9/11/2013
 * 
 */
public final class SmileysState extends State {

  /**
   * @param smileys
   *          different faces.
   */
  public SmileysState(final String[] smileys) {
    for (int i = 0; i < 4; i++)
      shortPress[i] = Parser.findContent(smileys, 0, smileys.length - 1, i, 4);

    longPress[0] = "Back";
    longPress[1] = "UNUSED";
    longPress[2] = "UNUSED";
    longPress[3] = "UNUSED";
  }

  @Override
  public final State onShort1Press() throws JustNumbersException {
    return calculateNextStateWhenShortPressed(1);
  }

  @Override
  public final State onShort2Press() throws JustNumbersException {
    return calculateNextStateWhenShortPressed(2);
  }

  @Override
  public final State onShort3Press() throws JustNumbersException {
    return calculateNextStateWhenShortPressed(3);
  }

  @Override
  public final State onShort4Press() throws JustNumbersException {
    return calculateNextStateWhenShortPressed(4);
  }

  /**
   * @param index
   *          the index of the button pressed
   * @return the next state.
   * @throws JustNumbersException
   *           TextController may throw this exception
   */
  public final State calculateNextStateWhenShortPressed(final int index)
      throws JustNumbersException {
    if (shortPress[index - 1].equals(""))
      /*
       * In case some of the windows are empty. For example: having 3 numbers to
       * be shown on 4 windows. I solved the issue by remaining in the same
       * state.
       */
      return this;
    if (1 == shortPress[index - 1].split(" ").length) {
      Controller.m_message.insertChars(shortPress[index - 1]);
      return KeyboardState.returnKeyboard();
    }
    return new SmileysState(shortPress[index - 1].split(" "));
  }

  // Back
  @Override
  public final State onLong1Press() {
    return KeyboardState.returnKeyboard();
  }

  // UNUSED
  @Override
  public final State onLong2Press() {
    // Button is UNUSED in the first iteration
    return this;
  }

  // UNUSED
  @Override
  public final State onLong3Press() {
    // Button is UNUSED in the first iteration
    return this;
  }

  // UNUSED
  @Override
  public final State onLong4Press() {
    // Button is UNUSED in the first iteration
    return this;
  }
}
