package il.ac.technion.cs.ssdl.cs234311.yp09.states;

import il.ac.technion.cs.ssdl.cs234311.yp09.controller.Controller;
import il.ac.technion.cs.ssdl.cs234311.yp09.controller.Controller.ActiveOp;
import il.ac.technion.cs.ssdl.cs234311.yp09.textController.TextController.JustNumbersException;

/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 22/11/2013
 * 
 */
public class PhoneState extends State {

  /**
   * @param numbers
   *          decimal digits from 0 to 9.
   */
  public PhoneState(final String[] numbers) {
    for (int i = 0; i < 4; i++)
      shortPress[i] = Parser.findContent(numbers, 0, numbers.length - 1, i, 4);

    longPress[0] = "Back";
    longPress[1] = "Done";
    longPress[2] = "Delete";
    longPress[3] = "Clear";

    Controller.m_activeOp = ActiveOp.RECIPIENT;
  }

  @Override
  public State onShort1Press() throws JustNumbersException {
    return calculateNextStateWhenShortPressed(1);
  }

  @Override
  public State onShort2Press() throws JustNumbersException {
    return calculateNextStateWhenShortPressed(2);
  }

  @Override
  public State onShort3Press() throws JustNumbersException {
    return calculateNextStateWhenShortPressed(3);
  }

  @Override
  public State onShort4Press() throws JustNumbersException {
    return calculateNextStateWhenShortPressed(4);
  }

  /**
   * @param index
   *          the index of the button pressed
   * @return the next state.
   * @throws JustNumbersException
   *           TextController may throw this exception
   */
  public final State calculateNextStateWhenShortPressed(final int index)
      throws JustNumbersException {
    if (shortPress[index - 1].equals(""))
      return this;
    if (1 == shortPress[index - 1].length()) {
      Controller.m_recipientNumber.insertChar(shortPress[index - 1].charAt(0));

      return new PhoneState(KeyboardState.numbers);
    }
    return new PhoneState(shortPress[index - 1].split(" "));
  }

  // Back
  @Override
  public State onLong1Press() {
    return new RecipientState();
  }

  // Done
  @Override
  public State onLong2Press() {
    return new MainState();
  }

  // Delete
  @Override
  public State onLong3Press() {
    Controller.m_recipientNumber.deletetChar();
    return this;
  }

  // Clear
  @Override
  public State onLong4Press() {
    Controller.m_recipientNumber.deleteAll();
    return this;
  }
}
