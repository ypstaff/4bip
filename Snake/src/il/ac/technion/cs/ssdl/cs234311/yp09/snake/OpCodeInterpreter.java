package il.ac.technion.cs.ssdl.cs234311.yp09.snake;

/**
 * @author Itamar
 * 
 */
public class OpCodeInterpreter {
  private static final short LONG_MASK = 1, BLUE_MASK = 2, ORANGE_MASK = 4,
      GREEN_MASK = 8, RED_MASK = 16;

  /**
   * @author Itamar allowed operations
   */
  public static enum Op {
    /**
     * invalid operation
     */
    INVALID, /**
     * short blue press
     */
    BLUE, /**
     * short orange press
     */
    ORANGE, /**
     * short green press
     */
    GREEN, /**
     * short red press
     */
    RED, /**
     * long blue press
     */
    LONG_BLUE, /**
     * long orange press
     */
    LONG_ORANGE, /**
     * long green press
     */
    LONG_GREEN, /**
     * long red press
     */
    LONG_RED, /**
     * blue orange combination
     */
    BLUE_ORANGE, /**
     * orange green combination
     */
    ORANGE_GREEN, /**
     * green red combination
     */
    GREEN_RED, /**
     * blue red combination
     */
    BLUE_RED
  }

  /**
   * @param c
   *          - an integer representing the value sent by blth
   * @return Op according to value
   */
  public static Op getOp(final int c) {
    if ((c & LONG_MASK) != 0) {
      if ((c & BLUE_MASK) != 0)
        return Op.LONG_BLUE;
      else if ((c & ORANGE_MASK) != 0)
        return Op.LONG_ORANGE;
      else if ((c & GREEN_MASK) != 0)
        return Op.LONG_GREEN;
      else if ((c & RED_MASK) != 0)
        return Op.LONG_RED;
    } else if ((c & BLUE_MASK) != 0) {
      if ((c & ORANGE_MASK) != 0)
        return Op.BLUE_ORANGE;
      if ((c & RED_MASK) != 0)
        return Op.BLUE_RED;
      if (c != BLUE_MASK)
        return Op.INVALID;
      return Op.BLUE;
    } else if ((c & ORANGE_MASK) != 0) {
      if ((c & GREEN_MASK) != 0)
        return Op.ORANGE_GREEN;
      if (c != ORANGE_MASK)
        return Op.INVALID;
      return Op.ORANGE;
    } else if ((c & GREEN_MASK) != 0) {
      if ((c & RED_MASK) != 0)
        return Op.GREEN_RED;
      if (c != GREEN_MASK)
        return Op.INVALID;
      return Op.GREEN;
    } else if ((c & RED_MASK) != 0) {
      if (c != RED_MASK)
        return Op.INVALID;
      return Op.RED;
    }
    return Op.INVALID;
  }
}
