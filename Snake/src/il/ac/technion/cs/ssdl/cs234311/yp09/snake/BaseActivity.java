package il.ac.technion.cs.ssdl.cs234311.yp09.snake;

import il.ac.technion.cs.ssdl.cs234311.yp09.bluetooth.BluetoothBroadcastReceiver;
import il.ac.technion.cs.ssdl.cs234311.yp09.utilities.SaveUsageTime;
import android.app.Activity;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

/**
 * @author husam
 * 
 *         this Activity used to define an union behavior for all the activity
 *         in the application,by making all the activity inherent it.
 */
public class BaseActivity extends Activity implements OperationListener {

  protected BluetoothBroadcastReceiver mReceiver;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

    mReceiver = new BluetoothBroadcastReceiver(this);
    registerToBluetooth();
  }

  protected void registerToBluetooth() {
    registerReceiver(mReceiver, new IntentFilter("fourBIP"));
  }

  @Override
  public void onOperation(int c) {
    return;
  }

  @Override
  protected void onResume() {
    super.onResume();
    registerToBluetooth();
    SaveUsageTime.resumeOrStart();
  }

  @Override
  protected void onPause() {
    super.onPause();
    unregisterReceiver(mReceiver);
    Log.d("husam", SaveUsageTime.pauseOrStop().toString());
  }

  @Override
  public void onBackPressed() {
    return;
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {

    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.base, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();
    if (id == R.id.action_settings)
      return true;
    return super.onOptionsItemSelected(item);
  }

  // /**
  // * A placeholder fragment containing a simple view.
  // */
  // public static class PlaceholderFragment extends Fragment {
  //
  // public PlaceholderFragment() {
  // }
  //
  // @Override
  // public View onCreateView(LayoutInflater inflater, ViewGroup container,
  // Bundle savedInstanceState) {
  // View rootView = inflater
  // .inflate(R.layout.fragment_base, container, false);
  // return rootView;
  // }
  // }

}
