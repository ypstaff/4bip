package il.ac.technion.cs.ssdl.cs234311.yp09.utilities;

/**
 * @author Tal SendError - used in client side communication to the GAE an
 *         object of this type represents a Result of an attempt to send
 *         Statistics to the app engine
 */

public class SendResult {

  /**
   * errorType - an Enum that represents a type of error that occurs while
   * sending a message to google app engine
   */
  public enum ErrorType {
    /**
     * NO_ERROR - there was no error
     */
    NO_ERROR,

    /**
     * SEND_FAIL - the message was not even sent to the server
     */
    SEND_FAIL,

    /**
     * BAD_RESPONSE - the server got the message but did not recognize the
     * format/ had a bad parameter / an internal error occurred in the server
     * and hence, the message was not logged
     */
    BAD_RESPONSE;
  }

  /**
   * errorType - the type of error
   */
  public ErrorType errorType;

  /**
   * s- a string describing the error, if NO_ERROR - the message received by
   * server Otherwise- a description of the error
   */
  public String description;

  /**
   * Constructor - makes a new SendResult with given params
   * 
   * @param errorT
   *          - type of error
   * @param descriptionStr
   *          - the description string
   */
  public SendResult(ErrorType errorT, String descriptionStr) {
    description = descriptionStr;
    errorType = errorT;
  }

  protected void setError(ErrorType errorT) {
    this.errorType = errorT;
  }
}
