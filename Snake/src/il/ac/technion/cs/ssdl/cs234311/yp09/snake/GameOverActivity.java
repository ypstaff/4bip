package il.ac.technion.cs.ssdl.cs234311.yp09.snake;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.WindowManager;
import android.widget.TextView;

/**
 * the implementation if the activity that will be opened when the game is over.
 * 
 * @author hmassalh
 * 
 */
public class GameOverActivity extends BaseActivity {

  private String boardNumMessage, colsNumMessage, rowsNumMessage,
      timeBetweenStepesMessage, firstScoreMessage, secondScoreMessage;
  private String loser;
  private FourButtonsFragment fourButtonsFragment;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_game_over);
    getPrevActivityMessages();
    setMessages();
    addFourButtons();
    getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
  }

  private void setMessages() {

    TextView loserTextView = (TextView) findViewById(R.id.loserTextView);
    if (loser.equals("1"))
      loserTextView.setText("first player crashed");
    if (loser.equals("2"))
      loserTextView.setText("second player crashed");
    if (loser.equals("BOTH"))
      loserTextView.setText("both players crashed");
    if (loser.equals("EXIT"))
      loserTextView.setText("game Stopped");

    TextView firstPlayerScoreTextView = (TextView) findViewById(R.id.firstScoreTextView);

    firstPlayerScoreTextView.setText("First Player Score : "
        + firstScoreMessage);

    TextView secondPlayerScoreTextView = (TextView) findViewById(R.id.secondScoreTextView);

    secondPlayerScoreTextView.setText("Second Player Score : "
        + secondScoreMessage);

  }

  private void getPrevActivityMessages() {
    Intent intent = getIntent();
    boardNumMessage = intent.getStringExtra(GameActivity.EXTERN_BOARD_NUM);
    colsNumMessage = intent.getStringExtra(GameActivity.EXTERN_COLS_NUM);
    rowsNumMessage = intent.getStringExtra(GameActivity.EXTERN_ROWS_NUM);
    timeBetweenStepesMessage = intent
        .getStringExtra(GameActivity.EXTERN_TIME_BETWEEN_STEPES);
    firstScoreMessage = intent.getStringExtra(GameActivity.EXTERN_FIRST_SCORE);
    secondScoreMessage = intent
        .getStringExtra(GameActivity.EXTERN_SECOND_SCORE);
    loser = intent.getStringExtra(GameActivity.EXTERN_LOSER);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.game_over, menu);
    return true;
  }

  private void addFourButtons() {
    fourButtonsFragment = new FourButtonsFragment();
    fourButtonsFragment.setListener(this);
    getFragmentManager().beginTransaction()
        .add(R.id.buttons_frame, fourButtonsFragment).commit();

  }

  @Override
  public void onOperation(int c) {
    switch (OpCodeInterpreter.getOp(c)) {
    case ORANGE: {
      Intent intent = new Intent(GameOverActivity.this, SnakeMainActivity.class);
      startActivity(intent);
      break;
    }
    case BLUE: {
      Intent intent = new Intent(GameOverActivity.this, GameActivity.class);
      intent.putExtra(ChoseBoardActivity.EXTERN_BOARD_NUM, boardNumMessage);
      intent.putExtra(ChoseBoardActivity.EXTERN_COLS_NUM, colsNumMessage);
      intent.putExtra(ChoseBoardActivity.EXTERN_ROWS_NUM, rowsNumMessage);
      intent.putExtra(ChoseBoardActivity.EXTERN_TIME_BETWEEN_STEPES,
          timeBetweenStepesMessage);
      startActivity(intent);
    }
      break;
    case GREEN_RED:
    case RED: {
      finishAffinity();
    }
      break;
    default:
      break;
    }

  }
}
