package il.ac.technion.cs.ssdl.cs234311.yp09.snake;

/**
 * These values will be used by keyboard fragment to indicate which color was
 * clicked
 * 
 * @author Owais Musa
 * 
 */
public enum InputColor {
  /**
   * 
   */
  blue,

  /**
   * 
   */
  orange,

  /**
   * 
   */
  green,

  /**
   * 
   */
  red
}
