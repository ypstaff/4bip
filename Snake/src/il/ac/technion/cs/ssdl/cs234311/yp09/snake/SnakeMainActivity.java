package il.ac.technion.cs.ssdl.cs234311.yp09.snake;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;

/**
 * 
 * the activity that will used to chose the speed of the snake
 * 
 * @author hmassalh
 * 
 */
public class SnakeMainActivity extends BaseActivity {
  /**
   * used to send time between steps to the next activity.
   */
  public static String EXTRA_MESSAGE = "il.ac.technion.cs.ssdl.cs234311.yp09.snake.MainActivity.timeBetweenSteps";

  private static int VERY_FAST_TIME = 500;
  private static int FAST_TIME = 750;
  private static int MEDEIUM_TIME = 1000;
  private static int SLOW_TIME = 1500;

  Button veryFastButton, fastBtton, mediumButton, slowButton;
  FourButtonsFragment fourButtonsFragment;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    fourButtonsFragment = new FourButtonsFragment();
    fourButtonsFragment.setListener(this);
    getFragmentManager().beginTransaction()
        .add(R.id.buttons_frame_main, fourButtonsFragment).commit();

    final FrameLayout veryFastFram = (FrameLayout) findViewById(R.id.very_fast_box_frame);
    final DrawView veryFastDraw = new DrawView(this, DrawView.Color.Blue,
        DrawView.Position.Center);
    veryFastFram.addView(veryFastDraw);

    final FrameLayout fastFram = (FrameLayout) findViewById(R.id.fast_box_frame);
    final DrawView fastDraw = new DrawView(this, DrawView.Color.Orange,
        DrawView.Position.Center);
    fastFram.addView(fastDraw);

    final FrameLayout mediumFram = (FrameLayout) findViewById(R.id.medium_box_frame);
    final DrawView mediumDraw = new DrawView(this, DrawView.Color.Green,
        DrawView.Position.Center);
    mediumFram.addView(mediumDraw);

    final FrameLayout slowFram = (FrameLayout) findViewById(R.id.slow_box_frame);
    final DrawView slowDraw = new DrawView(this, DrawView.Color.Red,
        DrawView.Position.Center);
    slowFram.addView(slowDraw);
    getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.main, menu);
    return true;
  }

  private void moveToNextActivity(int timeBetweenSteps) {
    Intent intent = new Intent(this, ChoseBoardActivity.class);
    intent
        .putExtra(EXTRA_MESSAGE, Integer.valueOf(timeBetweenSteps).toString());
    startActivity(intent);
  }

  @Override
  public void onOperation(int c) {
    switch (OpCodeInterpreter.getOp(c)) {
    case BLUE:
      moveToNextActivity(VERY_FAST_TIME);
      break;
    case ORANGE:
      moveToNextActivity(FAST_TIME);
      break;
    case GREEN:
      moveToNextActivity(MEDEIUM_TIME);
      break;
    case RED:
      moveToNextActivity(SLOW_TIME);
      break;
    case GREEN_RED:
      finishAffinity();
      break;
    default:
      break;
    }

  }

}
