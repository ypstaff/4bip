package il.ac.technion.cs.ssdl.cs234311.yp09.utilities;

import java.util.Date;

/**
 * @author husam
 * 
 *         used to calculate the use time of the snake, and send it to google
 *         app engine DB
 */
public class SaveUsageTime {
  private static Date startTime = null;

  /**
   * called when the application resumed or start
   */
  public static void resumeOrStart() {
    startTime = new Date();
  }

  /**
   * called when the application paused or stopped,and send the usage time from
   * the last start or resumed to google app engine DB
   * 
   * @return time of the use
   */
  public static Double pauseOrStop() {
    if (startTime == null)
      return Double.valueOf(0);
    final Double newUseTime = getDiffFromNow(startTime);
    new Thread(new Runnable() {
      @Override
      public void run() {
        new Client().sendTimePlayingSnake(newUseTime);
      }
    }).start();
    return newUseTime;
  }

  private static Double getDiffFromNow(Date time) {
    if (time == null)
      return Double.valueOf(0.0);
    Date now = new Date();
    return Double.valueOf(now.getTime() - time.getTime());
  }
}
