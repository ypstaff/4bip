package il.ac.technion.cs.ssdl.cs234311.yp09.snake;

import il.ac.technion.cs.ssdl.cs234311.yp09.snake.DrawView.Color;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TableRow;

/**
 * @author owais
 * 
 */
public class ColorsRowGen {

	private final TableRow mTableRow;
	private final Activity mActivity;

	/**
	 * @param tableRow
	 *            table row
	 * @param activity
	 *            activity
	 */
	public ColorsRowGen(final TableRow tableRow, final Activity activity) {
		mTableRow = tableRow;
		mActivity = activity;
	}

	private static DrawView.Color getMatchColor(final InputColor color) {
		switch (color) {
		case blue:
			return Color.Blue;
		case green:
			return Color.Green;
		case orange:
			return Color.Orange;
		case red:
			return Color.Red;
		default:
			break;
		}

		return null;
	}

	/**
	 * Draw rectangles
	 * 
	 * @param inputColorsArr
	 *            input colors array (combinations)
	 */
	@SuppressLint("NewApi")
	public void draw(final InputColor inputColorsArr[][]) {
		assert inputColorsArr != null;
		for (int i = 0; i < inputColorsArr.length; i++)
			assert inputColorsArr[i] != null;

		// For simplicity, assume that the max number of colors that can be used
		// for
		// each option is 2

		for (int i = 0; i < inputColorsArr.length; i++)
			if (inputColorsArr[i].length == 1) {
				final DrawView rec = new DrawView(mActivity,
						getMatchColor(inputColorsArr[i][0]),
						DrawView.Position.Center);

				final TableRow.LayoutParams recParams = new TableRow.LayoutParams(
						LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1);
				rec.setLayoutParams(recParams);
				mTableRow.addView(rec);
			} else {
				assert inputColorsArr[i].length == 2;

				final LinearLayout lo = new LinearLayout(mActivity);
				lo.setLayoutDirection(LinearLayout.HORIZONTAL);

				final TableRow.LayoutParams params = new TableRow.LayoutParams(
						LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1);
				lo.setLayoutParams(params);

				final DrawView rec1 = new DrawView(mActivity,
						getMatchColor(inputColorsArr[i][0]),
						DrawView.Position.Right);
				final TableRow.LayoutParams recParams = new TableRow.LayoutParams(
						LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1);
				rec1.setLayoutParams(recParams);

				final DrawView rec2 = new DrawView(mActivity,
						getMatchColor(inputColorsArr[i][1]),
						DrawView.Position.Left);
				rec2.setLayoutParams(recParams);

				lo.addView(rec1);
				lo.addView(rec2);
				mTableRow.addView(lo);
			}
	}
}
