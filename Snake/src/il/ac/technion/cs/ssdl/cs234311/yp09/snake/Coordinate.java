package il.ac.technion.cs.ssdl.cs234311.yp09.snake;

import android.util.Pair;

/**
 * 
 * class used to hold tow integer to save coordinate.
 * 
 * @author hmassalh
 * 
 */
class Coordinate extends Pair<Integer, Integer> {
  public Coordinate(Integer first, Integer second) {
    super(first, second);
  }

  @Override
  public boolean equals(Object other) {
    if (!(other instanceof Coordinate))
      return false;
    Coordinate otherCoor = (Coordinate) other;
    return otherCoor.first == this.first && otherCoor.second == this.second;
  }

  @Override
  public int hashCode() {
    return first.intValue() + second.intValue();
  }
}