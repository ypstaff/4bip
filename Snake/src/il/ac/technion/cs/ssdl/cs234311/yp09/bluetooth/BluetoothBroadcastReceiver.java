package il.ac.technion.cs.ssdl.cs234311.yp09.bluetooth;

import il.ac.technion.cs.ssdl.cs234311.yp09.snake.OperationListener;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

/**
 * @author Tal broadcast receiver for the signals of group 7
 */
public class BluetoothBroadcastReceiver extends BroadcastReceiver {

  private OperationListener currentActivity;

  /**
   * the first bit (least significant) - if this bit is on this is a long press
   */
  public static final byte longPush = 1;
  /**
   * the second bit - if this bit is on this is a press involving blue
   */
  public static final byte blueButtonPushed = 2;
  /**
   * the third bit - if this bit is on this is a press involving yellow
   */
  public static final byte yellowButtonPushed = 4;
  /**
   * the fourth bit - if this bit is on this is a press involving green
   */
  public static final byte greenButtonPushed = 8;
  /**
   * the fifth bit - if this bit is on this is a press involving red
   */
  public static final byte redButtonPushed = 16;

  public BluetoothBroadcastReceiver(OperationListener activity) {
    this.currentActivity = activity;
  }

  @Override
  public void onReceive(Context context, Intent intent) {
    Byte code;
    Bundle extras = intent.getExtras();
    if (extras != null)
      if (extras.containsKey("4ButtonsCode")) {
        code = extras.getByte("4ButtonsCode");
        currentActivity.onOperation(code.byteValue());
      }

  }
}
