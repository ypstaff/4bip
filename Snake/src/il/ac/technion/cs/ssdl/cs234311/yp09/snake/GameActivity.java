package il.ac.technion.cs.ssdl.cs234311.yp09.snake;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.GridLayout.Spec;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

/**
 * 
 * the implementation if the activity that will include the game.
 * 
 * @author hmassalh
 * 
 */

public class GameActivity extends BaseActivity implements
    ButtonsOperationListener {
  /**
   * used to turn on and of debug mode
   */
  public static boolean debugMode = false;
  /**
   * used to send time between steps to the next activity
   */
  public static final String EXTERN_TIME_BETWEEN_STEPES = "il.ac.technion.cs.ssdl.cs234311.yp09.snake.GameActivity.EXTERN_TIME_BETWEEN_STEPES";
  /**
   * used to send rows number to the next activity
   */
  public static final String EXTERN_ROWS_NUM = "il.ac.technion.cs.ssdl.cs234311.yp09.snake.GameActivity.EXTERN_ROWS_NUM";
  /**
   * used to send columns number to the next activity
   */
  public static final String EXTERN_COLS_NUM = "il.ac.technion.cs.ssdl.cs234311.yp09.snake.GameActivity.EXTERN_COLS_NUM";
  /**
   * used to send board number to the next activity
   */
  public static final String EXTERN_BOARD_NUM = "il.ac.technion.cs.ssdl.cs234311.yp09.snake.GameActivity.EXTERN_BOARD_NUM";
  /**
   * used to send the score of first player to the next activity
   */
  public static final String EXTERN_FIRST_SCORE = "il.ac.technion.cs.ssdl.cs234311.yp09.snake.GameActivity.EXTERN_FIRST_SCORE";
  /**
   * used to send the score of first player to the next activity
   */
  public static final String EXTERN_SECOND_SCORE = "il.ac.technion.cs.ssdl.cs234311.yp09.snake.GameActivity.EXTERN_SECOND_SCORE";
  /**
   * used to send the the loser to the next activity
   */
  public static final String EXTERN_LOSER = "il.ac.technion.cs.ssdl.cs234311.yp09.snake.GameActivity.EXTERN_LOSER";

  enum Directions {
    UP, DOWN, LEFT, RIGHT
  }

  enum CellType {
    FIRST_SNAKE_BODY, SECOND_SNAKE_BODY, EMPTY, BARRIER, FOOD, SNAKE_HEAD
  }

  enum StepResult {
    FIRST_LOSE, SECOND_LOSE, BOTH_LOSE, FIRST_EAT, SECOND_EAT, NO_ACTION, GAME_STOPED
  }

  private boolean gamePused;
  private GridLayout grid;
  private int columnsNum, rowsNum, boardNum, timeBetweenSteps;
  private Directions firstSnakeDirec, secondSnakeDirec;
  private Map<Coordinate, CellType> cellsMap;
  private TimerTask changeBoard;
  private LinkedList<Coordinate> firstSnake, secondSnake;
  // private FourButtonsFragment fourButtonsFragment;
  private FirstPlayerFourButtonsFragment firstPlayerFourButtonsFragment;
  private TextView firstPlayerScoreTextView, secondPlayerScoreTextView;
  private int firstScore;
  private int secondScore;
  private Set<Coordinate> barriersCoordinates;
  boolean firstPlayerMadeHisMove;
  boolean SecondPlayerMadeHisMove;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_game);

    cellsMap = new HashMap<Coordinate, GameActivity.CellType>();

    initTextViews();

    getMessagesFromPrevActivity();

    addFourButtonsForTheSecondPlayer();

    addFourButtonsForTheFirstPlayer();

    addImagesToRowTable();

    grid.setRowCount(rowsNum);
    grid.setColumnCount(columnsNum);

    InitialBoard initalBoard = new InitialBoard(boardNum, rowsNum, columnsNum);

    firstSnakeDirec = initalBoard.firstInitialDirec;
    secondSnakeDirec = initalBoard.secondInitialDirec;
    barriersCoordinates = initalBoard.barriersCoord;
    firstSnake = initalBoard.firstSnakeInitialCells;
    secondSnake = initalBoard.secondSnakeInitialCells;

    fillInitalBoard();

    PutFoodInRandomCell();

    gamePused = false;

    changeBoard = new ChangeBoard();
    new Timer().schedule(changeBoard, 2500, timeBetweenSteps);
  }

  private void initTextViews() {
    firstPlayerScoreTextView = (TextView) findViewById(R.id.firstPlayerScoreTextView);
    secondPlayerScoreTextView = (TextView) findViewById(R.id.secondPlayerScoreTextView);

    firstPlayerScoreTextView.setText("Player 1 : 0");
    secondPlayerScoreTextView.setText("Player 2 : 0");
  }

  private void addFourButtonsForTheSecondPlayer() {
    // fourButtonsFragment = new FourButtonsFragment();
    // fourButtonsFragment.setListener(this);
    // getFragmentManager().beginTransaction()
    // .add(R.id.buttons_frame, fourButtonsFragment).commit();
  }

  private void addFourButtonsForTheFirstPlayer() {
    firstPlayerFourButtonsFragment = new FirstPlayerFourButtonsFragment();
    // BluetoothReciver.setListener(this);
    firstPlayerFourButtonsFragment.setListener(this);
    getFragmentManager().beginTransaction()
        .add(R.id.buttons_frame, firstPlayerFourButtonsFragment).commit();
  }

  private void getMessagesFromPrevActivity() {
    Intent intent = getIntent();
    boardNum = Integer.parseInt(intent
        .getStringExtra(ChoseBoardActivity.EXTERN_BOARD_NUM));
    timeBetweenSteps = Integer.parseInt(intent
        .getStringExtra(ChoseBoardActivity.EXTERN_TIME_BETWEEN_STEPES));
    rowsNum = Integer.parseInt(intent
        .getStringExtra(ChoseBoardActivity.EXTERN_ROWS_NUM));
    columnsNum = Integer.parseInt(intent
        .getStringExtra(ChoseBoardActivity.EXTERN_COLS_NUM));
    grid = (GridLayout) findViewById(R.id.cellsGridLayout);

    getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
  }

  private void addImagesToRowTable() {

    TableRow imagesTableRow = (TableRow) findViewById(R.id.mainTableRow);

    ImageView leftArrowImg = new ImageView(this);
    leftArrowImg.setImageResource(R.drawable.leftarrow);
    imagesTableRow.addView(getAppropriateFrameLayout(leftArrowImg));

    ImageView stop = new ImageView(this);
    stop.setImageResource(R.drawable.stop);
    imagesTableRow.addView(getAppropriateFrameLayout(stop));

    ImageView upArrowImg = new ImageView(this);
    upArrowImg.setImageResource(R.drawable.uparrow);
    imagesTableRow.addView(getAppropriateFrameLayout(upArrowImg));

    ImageView pauseImg = new ImageView(this);
    pauseImg.setImageResource(R.drawable.pause);
    imagesTableRow.addView(getAppropriateFrameLayout(pauseImg));

    ImageView downImg = new ImageView(this);
    downImg.setImageResource(R.drawable.downarrow);
    imagesTableRow.addView(getAppropriateFrameLayout(downImg));

    ImageView empty2 = new ImageView(this);
    empty2.setImageResource(R.drawable.empty);
    imagesTableRow.addView(getAppropriateFrameLayout(empty2));

    ImageView rightImg = new ImageView(this);
    rightImg.setImageResource(R.drawable.rightarrow);
    imagesTableRow.addView(getAppropriateFrameLayout(rightImg));
  }

  private class ChangeBoard extends TimerTask {
    public ChangeBoard() {

    }

    @Override
    public void run() {
      runOnUiThread(new Runnable() {
        @Override
        public void run() {
          firstPlayerMadeHisMove = false;
          SecondPlayerMadeHisMove = false;
          runTheNextStep();
        }
      });
    }
  }

  private FrameLayout getAppropriateFrameLayout(View view) {
    final TableRow.LayoutParams params = new TableRow.LayoutParams(0,
        android.view.ViewGroup.LayoutParams.MATCH_PARENT, 1);
    params.gravity = Gravity.CENTER;

    FrameLayout $ = new FrameLayout(this);
    $.setLayoutParams(params);
    $.addView(view);

    return $;
  }

  private HashSet<Coordinate> getEmptyCells() {
    HashSet<Coordinate> emptyCells = new HashSet<Coordinate>();
    for (int row = 0; row < rowsNum; row++)
      for (int col = 0; col < columnsNum; col++)
        if (getCellTyprByRowCol(row, col) == CellType.EMPTY)
          emptyCells.add(new Coordinate(Integer.valueOf(row), Integer
              .valueOf(col)));
    return emptyCells;
  }

  private CellType getCellTyprByRowCol(int row, int col) {
    return cellsMap.get(new Coordinate(Integer.valueOf(row), Integer
        .valueOf(col)));
  }

  void runTheNextStep() {
    Coordinate firstNextHeadCoord = getNextHeadCoord(firstSnake.getFirst(),
        firstSnakeDirec);
    Coordinate secondNextHeadCoord = getNextHeadCoord(secondSnake.getFirst(),
        secondSnakeDirec);

    StepResult result = checkTheResut(firstNextHeadCoord, secondNextHeadCoord);

    if (result == StepResult.FIRST_LOSE || result == StepResult.SECOND_LOSE
        || result == StepResult.BOTH_LOSE)
      endGame(result);

    updateViewAndMatrix(firstNextHeadCoord, CellType.SNAKE_HEAD);
    updateViewAndMatrix(secondNextHeadCoord, CellType.SNAKE_HEAD);

    updateViewAndMatrix(firstSnake.getFirst(), CellType.FIRST_SNAKE_BODY);
    updateViewAndMatrix(secondSnake.getFirst(), CellType.SECOND_SNAKE_BODY);

    firstSnake.addFirst(firstNextHeadCoord);
    secondSnake.addFirst(secondNextHeadCoord);

    if (result != StepResult.FIRST_EAT) {
      updateViewAndMatrix(firstSnake.getLast(), CellType.EMPTY);
      firstSnake.removeLast();
    }
    if (result != StepResult.SECOND_EAT) {
      updateViewAndMatrix(secondSnake.getLast(), CellType.EMPTY);
      secondSnake.removeLast();
    }
    if (result == StepResult.FIRST_EAT || result == StepResult.SECOND_EAT) {
      updateScore(result);
      PutFoodInRandomCell();
    }

  }

  private void updateScore(StepResult result) {
    if (result == StepResult.FIRST_EAT) {
      firstScore++;
      firstPlayerScoreTextView.setText("Player 1 : " + firstScore);
    }
    if (result == StepResult.SECOND_EAT) {
      secondScore++;
      secondPlayerScoreTextView.setText("Player 2 : " + secondScore);
    }
  }

  private StepResult checkTheResut(Coordinate firstNextHeadCoord,
      Coordinate secondNextHeadCoord) {
    if (firstNextHeadCoord.equals(secondNextHeadCoord))
      return StepResult.BOTH_LOSE;
    if (checkIfPlayerLose(firstNextHeadCoord)
        && checkIfPlayerLose(secondNextHeadCoord))
      return StepResult.BOTH_LOSE;
    if (checkIfPlayerLose(firstNextHeadCoord))
      return StepResult.FIRST_LOSE;
    if (checkIfPlayerLose(secondNextHeadCoord))
      return StepResult.SECOND_LOSE;
    if (getCellType(firstNextHeadCoord) == CellType.FOOD)
      return StepResult.FIRST_EAT;
    if (getCellType(secondNextHeadCoord) == CellType.FOOD)
      return StepResult.SECOND_EAT;
    return StepResult.NO_ACTION;
  }

  private boolean checkIfPlayerLose(Coordinate coord) {
    CellType headType = getCellType(coord);
    return headType == CellType.SECOND_SNAKE_BODY
        || headType == CellType.BARRIER
        || headType == CellType.FIRST_SNAKE_BODY;

  }

  private CellType getCellType(Coordinate firstNextHeadCoord) {
    return cellsMap.get(firstNextHeadCoord);
  }

  private void endGame(StepResult result) {
    changeBoard.cancel();
    Intent intent = new Intent(this, GameOverActivity.class);
    intent.putExtra(EXTERN_BOARD_NUM, Integer.valueOf(boardNum).toString());
    intent.putExtra(EXTERN_COLS_NUM, Integer.valueOf(columnsNum).toString());
    intent.putExtra(EXTERN_ROWS_NUM, Integer.valueOf(rowsNum).toString());
    intent.putExtra(EXTERN_TIME_BETWEEN_STEPES,
        Integer.valueOf(timeBetweenSteps).toString());
    intent.putExtra(EXTERN_FIRST_SCORE, Integer.valueOf(firstScore).toString());
    intent.putExtra(EXTERN_SECOND_SCORE, Integer.valueOf(secondScore)
        .toString());
    if (result == StepResult.FIRST_LOSE)
      intent.putExtra(EXTERN_LOSER, "1");
    if (result == StepResult.SECOND_LOSE)
      intent.putExtra(EXTERN_LOSER, "2");
    if (result == StepResult.BOTH_LOSE)
      intent.putExtra(EXTERN_LOSER, "BOTH");
    if (result == StepResult.GAME_STOPED)
      intent.putExtra(EXTERN_LOSER, "EXIT");
    startActivity(intent);
  }

  private static Coordinate getNextHeadCoord(Coordinate snakeHead,
      Directions snakeDirec) {
    switch (snakeDirec) {
    case UP:
      return new Coordinate(Integer.valueOf(snakeHead.first.intValue() - 1),
          snakeHead.second);
    case DOWN:
      return new Coordinate(Integer.valueOf(snakeHead.first.intValue() + 1),
          snakeHead.second);
    case LEFT:
      return new Coordinate(snakeHead.first, Integer.valueOf(snakeHead.second
          .intValue() - 1));
    case RIGHT:
      return new Coordinate(snakeHead.first, Integer.valueOf(snakeHead.second
          .intValue() + 1));
    default:
      return null;
    }
  }

  private void PutFoodInRandomCell() {
    Object[] emptyCellsArray = getEmptyCells().toArray();
    Random rand = new Random();
    int randomIndex = rand.nextInt(emptyCellsArray.length);
    Coordinate randomEmptyCell = (Coordinate) emptyCellsArray[randomIndex];
    if (debugMode == true)
      randomEmptyCell = new Coordinate(columnsNum - 2, rowsNum - 3);
    updateViewAndMatrix(randomEmptyCell, CellType.FOOD);
  }

  private void updateViewAndMatrix(Coordinate coor, CellType type) {
    cellsMap.put(coor, type);
    int imageResId = convertEnumToImage(type);
    Spec row = GridLayout.spec(coor.first.intValue(), 1);
    Spec colspan = GridLayout.spec(coor.second.intValue(), 1);
    GridLayout.LayoutParams gridLayoutParam = new GridLayout.LayoutParams(row,
        colspan);
    ImageView cellImage = new ImageView(this);
    cellImage.setImageResource(imageResId);
    grid.addView(cellImage, gridLayoutParam);
  }

  private static int convertEnumToImage(CellType type) {
    switch (type) {
    case EMPTY:
      return R.drawable.graystar;
    case SNAKE_HEAD:
      return R.drawable.blackstar;
    case FIRST_SNAKE_BODY:
      return R.drawable.greenstar;
    case SECOND_SNAKE_BODY:
      return R.drawable.bluestar;
    case BARRIER:
      return R.drawable.redstar;
    case FOOD:
      return R.drawable.yellowstar;
    default:
      return 0;
    }
  }

  private void fillInitalBoard() {
    for (int row = 0; row < rowsNum; row++)
      for (int col = 0; col < columnsNum; col++) {
        Coordinate currCellCoord = new Coordinate(Integer.valueOf(row),
            Integer.valueOf(col));
        if (currCellCoord.equals(firstSnake.getFirst())
            || currCellCoord.equals(secondSnake.getFirst())) {
          updateViewAndMatrix(currCellCoord, CellType.SNAKE_HEAD);
          continue;
        }
        if (firstSnake.contains(currCellCoord)) {
          updateViewAndMatrix(currCellCoord, CellType.FIRST_SNAKE_BODY);
          continue;
        }
        if (secondSnake.contains(currCellCoord)) {
          updateViewAndMatrix(currCellCoord, CellType.SECOND_SNAKE_BODY);
          continue;
        }
        if (barriersCoordinates.contains(currCellCoord)) {
          updateViewAndMatrix(currCellCoord, CellType.BARRIER);
          continue;
        }
        updateViewAndMatrix(currCellCoord, CellType.EMPTY);
      }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.main, menu);
    return true;
  }

  void pushFirstDown() {
    if (!firstPlayerMadeHisMove && firstSnakeDirec != Directions.UP) {
      firstPlayerMadeHisMove = true;
      firstSnakeDirec = Directions.DOWN;
    }
  }

  void pushFirstLeft() {
    if (!firstPlayerMadeHisMove && firstSnakeDirec != Directions.RIGHT) {
      firstPlayerMadeHisMove = true;
      firstSnakeDirec = Directions.LEFT;
    }
  }

  void pushFirstRight() {
    if (!firstPlayerMadeHisMove && firstSnakeDirec != Directions.LEFT) {
      firstPlayerMadeHisMove = true;
      firstSnakeDirec = Directions.RIGHT;
    }
  }

  void pushFirstUp() {
    if (!firstPlayerMadeHisMove && firstSnakeDirec != Directions.DOWN) {
      firstPlayerMadeHisMove = true;
      firstSnakeDirec = Directions.UP;
    }
  }

  void pushSecondDown() {
    if (!SecondPlayerMadeHisMove && secondSnakeDirec != Directions.UP) {
      SecondPlayerMadeHisMove = true;
      secondSnakeDirec = Directions.DOWN;
    }
  }

  void pushSecondLeft() {
    if (!SecondPlayerMadeHisMove && secondSnakeDirec != Directions.RIGHT) {
      SecondPlayerMadeHisMove = true;
      secondSnakeDirec = Directions.LEFT;

    }
  }

  void pushSecondRight() {
    if (!SecondPlayerMadeHisMove && secondSnakeDirec != Directions.LEFT) {
      SecondPlayerMadeHisMove = true;
      secondSnakeDirec = Directions.RIGHT;
    }
  }

  void pushSecondUp() {
    if (!SecondPlayerMadeHisMove && secondSnakeDirec != Directions.DOWN) {
      SecondPlayerMadeHisMove = true;
      secondSnakeDirec = Directions.UP;
    }
  }

  @Override
  public void onOperation(int c) {
    if (gamePused == true) {
      changeBoard = new ChangeBoard();
      new Timer().schedule(changeBoard, 0, timeBetweenSteps);
      gamePused = false;
    }

    switch (OpCodeInterpreter.getOp(c)) {
    case ORANGE:
      pushSecondUp();
      break;
    case GREEN:
      pushSecondDown();
      break;
    case BLUE:
      pushSecondLeft();
      break;
    case RED:
      pushSecondRight();
      break;
    case ORANGE_GREEN:
      if (gamePused == false) {
        gamePused = true;
        changeBoard.cancel();
      }
      break;
    case BLUE_ORANGE:
      endGame(StepResult.GAME_STOPED);
      break;
    case GREEN_RED:
      finishAffinity();
      break;
    default:
      break;
    }

  }

  @Override
  public void onButtonsOperation(int c) {
    if (gamePused == true) {
      changeBoard = new ChangeBoard();
      new Timer().schedule(changeBoard, 0, timeBetweenSteps);
      gamePused = false;
    }

    switch (OpCodeInterpreter.getOp(c)) {
    case ORANGE:
      pushFirstUp();
      break;
    case GREEN:
      pushFirstDown();
      break;
    case BLUE:
      pushFirstLeft();
      break;
    case RED:
      pushFirstRight();
      break;
    case ORANGE_GREEN:
      if (gamePused == false) {
        gamePused = true;
        changeBoard.cancel();
      }
      break;
    case BLUE_ORANGE:
      endGame(StepResult.GAME_STOPED);
      break;
    case GREEN_RED:
      finishAffinity();
      break;
    default:
      break;
    }
  }
}
