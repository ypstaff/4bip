package il.ac.technion.cs.ssdl.cs234311.yp09.utilities;

import java.io.Serializable;

/**
 * @author Tal this interface represents the functionality of sending a message
 *         to the server
 */
public interface ClientCom {

  /**
   * @param object2Send
   *          - the statistics object to be sent - IMPLEMENTS SERIALIZABLE
   * @return SendResult - in this package, this is an Class, representing the
   *         result of the statistics sending attempt to the APP ENGINE
   */
  public SendResult sendStatistics(Serializable object2Send);

  /**
   * @param time
   *          the time that snake was played
   * 
   * @return SendResult - in this package, this is an Class, representing the
   *         result of the statistics sending attempt to the APP ENGINE
   */
  public SendResult sendTimePlayingSnake(Double time);
}
