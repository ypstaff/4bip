package il.ac.technion.cs.ssdl.cs234311.yp09.utilities;

import il.ac.technion.cs.ssdl.cs234311.yp09.utilities.SendResult.ErrorType;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * @author Tal connection to the google app engine functionality
 */
public class Client implements ClientCom {

  // public static void main(String[] args) {
  // Client c = new Client();
  // Statistics s = new Statistics();
  // s.numberOfWords = Double.valueOf(1.0);
  // s.timePerWordAvg = Double.valueOf(16000.0);
  // c.resetStatistics();
  // c.sendStatistics(s);
  // System.out.println("Done");
  // }

  /*
   * ----------------------- ------------------ --------------------------- ***
   * ------------------------------ CONSTANTS------------------------------ ***
   * ---------------------------------------------------------------------- ***
   */
  private static final String ERROR_URL = "an ERROR acurred while creating URL";
  private static final String ERROR_CONNECT = "an ERROR acurred while connecting to server";
  private static final String ERROR_SET_POST = "problem setting POST";
  private static final String ERROR_SET_GET = "problem setting GET";
  private static final String ERROR_BAD_RESPONSE_CODE = " got a bad response code from server : ";
  private static final String ERROR_OPEN_CONNECT = "problem opening output stream";
  private static final String ERROR_OPEN_STREAM = " an ERROR while opening output stream";
  private static final String ERROR_WRITING_MESSAGE = "error ecured while writing message";

  private static final String URL_ADRRESS_DEFAULT = "http://bip4-app.appspot.com/talserver";

  /*
   * ----------------------- ------------------ --------------------------- ***
   * ------------------------------ FIELDS--------------------------------- ***
   * ---------------------------------------------------------------------- **
   */
  private final String URL_ADRRESS;

  /*
   * ----------------------- ------------------ --------------------------- ***
   * --------------------------- CONSTRUCTORS------------------------------ ***
   * ---------------------------------------------------------------------- **
   */

  /**
   * default constructor - uses the real google app
   */
  public Client() {
    URL_ADRRESS = URL_ADRRESS_DEFAULT;
  }

  /**
   * @param url
   *          - the address of the test google_app a constructor for testing -
   *          use a different google_app to test the application
   */
  public Client(final String url) {
    URL_ADRRESS = url;
  }

  /*
   * ----------------------- ------------------ --------------------------- ***
   * -------------------------- USER FUNCTIONS ---------------------------- ***
   * ---------------------------------------------------------------------- **
   */
  @Override
  public SendResult sendStatistics(Serializable object2Send) {
    return sendToGAE(object2Send, true);
  }

  @Override
  public SendResult sendTimePlayingSnake(Double time) {

    return sendToGAE(time, false);
  }

  /*
   * ----------------------- ------------------ --------------------------- ***
   * -------------------------- COMM FUNCTIONS ---------------------------- ***
   * ---------------------------------------------------------------------- **
   */

  /**
   * @param object2Send
   *          - the object to be sent to the GAE
   * @param isStatistics
   *          - if the object is of type Statistics(true) or Double(false)
   * @return of type SendResult - the result of the communication attempt
   */
  private SendResult sendToGAE(Serializable object2Send, boolean isStatistics) {
    URL url = null;
    String resStr = "";
    int responseCode = -1;
    try {
      resStr = ERROR_URL;
      url = new URL(URL_ADRRESS); // make a new URL object out of address string

      HttpURLConnection con = null;
      resStr = ERROR_CONNECT;
      con = (HttpURLConnection) url.openConnection(); // open connection

      resStr = ERROR_SET_POST;
      con.setRequestMethod("POST"); // post message

      con.setDoOutput(true);
      ObjectOutputStream objectOutStream;
      resStr = ERROR_OPEN_STREAM;
      objectOutStream = new ObjectOutputStream(con.getOutputStream()); // open
                                                                       // objectOutStream

      resStr = ERROR_WRITING_MESSAGE;
      if (isStatistics)
        objectOutStream.writeInt(1);
      else
        objectOutStream.writeInt(2);
      objectOutStream.writeObject(object2Send);

      resStr = ERROR_OPEN_CONNECT;
      responseCode = con.getResponseCode();// get response code
    } catch (final IOException e) {
      return new SendResult(ErrorType.SEND_FAIL, resStr);
    }

    if (responseCode != 200)
      return new SendResult(ErrorType.BAD_RESPONSE, ERROR_BAD_RESPONSE_CODE
          + responseCode);
    return new SendResult(ErrorType.NO_ERROR, "");
  }

  /**
   * RESET the statistics on server
   * 
   * @return of type SendResult - the result of the communication attempt
   */
  public SendResult resetStatistics() {
    URL url = null;
    String resStr = "";
    int responseCode = -1;
    try {
      resStr = ERROR_URL;
      url = new URL(URL_ADRRESS + "?message=RemoveAll"); // make a new URL
                                                         // object out of
                                                         // address string

      HttpURLConnection con = null;
      resStr = ERROR_CONNECT;
      con = (HttpURLConnection) url.openConnection(); // open connection

      resStr = ERROR_SET_GET;
      con.setRequestMethod("GET"); // Get method

      responseCode = con.getResponseCode();// get response code
    } catch (final IOException e) {
      return new SendResult(ErrorType.SEND_FAIL, resStr);
    }

    if (responseCode != 200)
      return new SendResult(ErrorType.BAD_RESPONSE, ERROR_BAD_RESPONSE_CODE
          + responseCode);
    return new SendResult(ErrorType.NO_ERROR, "");
  }

}
