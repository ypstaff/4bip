package il.ac.technion.cs.ssdl.cs234311.yp09.snake;

/**
 * @author Itamar any activity that wants to get presses from bluetooth must
 *         implement this interface
 */
public interface OperationListener {

  /**
   * @param c
   *          - the press represented as an int (according to & operation of
   *          press values)
   */
  public void onOperation(int c);

}
