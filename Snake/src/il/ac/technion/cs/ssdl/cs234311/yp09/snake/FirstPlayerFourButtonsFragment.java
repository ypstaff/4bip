package il.ac.technion.cs.ssdl.cs234311.yp09.snake;

import android.app.Fragment;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
//import android.widget.Toast;

/**
 * @author Itamar Bitton
 * 
 */

public class FirstPlayerFourButtonsFragment extends Fragment {

  boolean[] pressed = new boolean[4];
  boolean multitouch = false, valid = false;
  ButtonsOperationListener mListener;

  enum OpCode {
    LONG(1), BLUE(2), ORANGE(4), GREEN(8), RED(16);
    private final int code;

    private OpCode(final int c) {
      code = c;
    }

    public int getCode() {
      return code;
    }
  }

  @Override
  public View onCreateView(final LayoutInflater inflater,
      final ViewGroup container, final Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    final View $ = inflater
        .inflate(R.layout.buttons_fragment, container, false);
    final int[] bIDs = { R.id.blue_button, R.id.orange_button,
        R.id.green_button, R.id.red_button };
    final Resources r = getResources();
    final int[] n = { r.getColor(R.color.blue), r.getColor(R.color.orange),
        r.getColor(R.color.green), r.getColor(R.color.red) };
    final int[] h = { r.getColor(R.color.blue_hi),
        r.getColor(R.color.orange_hi), r.getColor(R.color.green_hi),
        r.getColor(R.color.red_hi) };
    for (int i = 0; i < 4; i++)
      ((Button) $.findViewById(bIDs[i])).setOnTouchListener(new ButtonListener(
          i, n[i], h[i]));
    return $;
  }

  /**
   * @param l
   *          - the activity that listens
   */
  public void setListener(final ButtonsOperationListener l) {
    mListener = l;
  }

  private class ButtonListener implements OnTouchListener {

    private static final int LONG_PRESS_DELAY = 2000;
    private static final int SHORT_PRESS_DELAY = 0;
    long pressTime, duration;
    private final int id, normal, high;

    public ButtonListener(final int id, final int n, final int h) {
      this.id = id;
      normal = n;
      high = h;
    }

    @Override
    public boolean onTouch(final View v, final MotionEvent e) {
      switch (e.getAction()) {
      case MotionEvent.ACTION_DOWN:
        pressTime = System.currentTimeMillis();
        ((Button) v).setBackgroundColor(high);
        pressed[id] = true;
        valid = true;
        return true;
      case MotionEvent.ACTION_UP:
        duration = System.currentTimeMillis() - pressTime;
        ((Button) v).setBackgroundColor(normal);
        final int code = (pressed[0] ? OpCode.BLUE.getCode() : 0)
            | (pressed[1] ? OpCode.ORANGE.getCode() : 0)
            | (pressed[2] ? OpCode.GREEN.getCode() : 0)
            | (pressed[3] ? OpCode.RED.getCode() : 0);
        pressed[id] = false;
        multitouch = pressed[0] || pressed[1] || pressed[2] || pressed[3];
        if (valid) {
          if (duration >= SHORT_PRESS_DELAY && duration < LONG_PRESS_DELAY)
            // Toast.makeText(getActivity(), "short press: " + code,
            // Toast.LENGTH_SHORT).show();
            mListener.onButtonsOperation(code);
          if (!multitouch && duration >= LONG_PRESS_DELAY)
            // Toast.makeText(getActivity(),
            // "long press: " + (code | OpCode.LONG.getCode()),
            // Toast.LENGTH_SHORT).show();
            mListener.onButtonsOperation(code | OpCode.LONG.getCode());
          valid = false;
        }
        return true;
      default:
        return false;
      }
    }
  }
}
