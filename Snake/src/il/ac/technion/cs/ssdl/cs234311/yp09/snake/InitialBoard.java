package il.ac.technion.cs.ssdl.cs234311.yp09.snake;

import il.ac.technion.cs.ssdl.cs234311.yp09.snake.GameActivity.Directions;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

/**
 * 
 * class that will generate some board to chose one of them before start
 * playing.
 * 
 * @author hmassalh
 * 
 */
public class InitialBoard {
  /**
   * linked list that will contain the initial first player snake place, the
   * first Coordinate in the list will be the head of the snake, and the last
   * will contain the tail.
   */
  public LinkedList<Coordinate> firstSnakeInitialCells;

  /**
   * linked list that will contain the initial second player snake place, the
   * first Coordinate in the list will be the head of the snake, and the last
   * will contain the tail.
   */
  public LinkedList<Coordinate> secondSnakeInitialCells;
  /**
   * 
   * set that contain the coordinates of the cells that contain barriers.
   * 
   */
  public Set<Coordinate> barriersCoord;
  /**
   * the initial first player snake direction
   */
  public Directions firstInitialDirec;
  /**
   * the initial second player snake direction
   */
  public Directions secondInitialDirec;

  private InitialBoard() {

  }

  /**
   * 
   * the class constructors, receive parameters of the needed board and create a
   * new one
   * 
   * @param boardNum
   *          the number of needed board
   * @param rows
   *          the number of rows in the needed board
   * @param cols
   *          the number of columns on the needed board
   */
  public InitialBoard(int boardNum, int rows, int cols) {
    InitialBoard tmp = null;
    switch (boardNum) {
    case 1:
      tmp = board1(rows, cols);
      break;
    case 2:
      tmp = board2(rows, cols);
      break;
    case 3:
      tmp = board3(rows, cols);
      break;
    case 4:
      tmp = board4(rows, cols);
      break;
    case 5:
      tmp = board5(rows, cols);
      break;
    default:
      tmp = board6(rows, cols);
      break;
    }
    this.barriersCoord = tmp.barriersCoord;
    this.firstInitialDirec = tmp.firstInitialDirec;
    this.secondInitialDirec = tmp.secondInitialDirec;
    this.firstSnakeInitialCells = tmp.firstSnakeInitialCells;
    this.secondSnakeInitialCells = tmp.secondSnakeInitialCells;

  }

  @SuppressWarnings("boxing")
  private static void fillsides(Set<Coordinate> barriersCoord, int rows,
      int cols) {
    for (int i = 0; i < rows; i++) {
      barriersCoord.add(new Coordinate(Integer.valueOf(i), 0));
      barriersCoord.add(new Coordinate(i, cols - 1));
    }

    for (int j = 1; j < cols - 1; j++) {
      barriersCoord.add(new Coordinate(0, j));
      barriersCoord.add(new Coordinate(rows - 1, j));
    }
  }

  @SuppressWarnings("boxing")
  private static InitialBoard board1(int rows, int cols) {
    InitialBoard b = new InitialBoard();
    b.barriersCoord = new HashSet<Coordinate>();
    fillsides(b.barriersCoord, rows, cols);

    b.firstSnakeInitialCells = new LinkedList<Coordinate>();
    b.firstSnakeInitialCells.add(new Coordinate(rows / 2, cols / 3));
    b.firstSnakeInitialCells.add(new Coordinate(rows / 2 + 1, cols / 3));
    b.firstSnakeInitialCells.add(new Coordinate(rows / 2 + 2, cols / 3));

    b.secondSnakeInitialCells = new LinkedList<Coordinate>();
    b.secondSnakeInitialCells.add(new Coordinate(rows / 2, 2 * cols / 3));
    b.secondSnakeInitialCells.add(new Coordinate(rows / 2 + 1, 2 * cols / 3));
    b.secondSnakeInitialCells.add(new Coordinate(rows / 2 + 2, 2 * cols / 3));

    b.firstInitialDirec = Directions.UP;
    b.secondInitialDirec = Directions.UP;

    return b;
  }

  @SuppressWarnings("boxing")
  private static InitialBoard board2(int rows, int cols) {
    InitialBoard b = new InitialBoard();
    b.barriersCoord = new HashSet<Coordinate>();
    fillsides(b.barriersCoord, rows, cols);

    for (int i = 3; i < cols - 3; i++)
      b.barriersCoord.add(new Coordinate(rows / 2, i));

    b.firstSnakeInitialCells = new LinkedList<Coordinate>();
    b.firstSnakeInitialCells.add(new Coordinate(rows / 4, cols / 2 - 1));
    b.firstSnakeInitialCells.add(new Coordinate(rows / 4, cols / 2));
    b.firstSnakeInitialCells.add(new Coordinate(rows / 4, cols / 2 + 1));

    b.firstInitialDirec = Directions.LEFT;

    b.secondSnakeInitialCells = new LinkedList<Coordinate>();
    b.secondSnakeInitialCells.add(new Coordinate(3 * rows / 4, cols / 2 + 1));
    b.secondSnakeInitialCells.add(new Coordinate(3 * rows / 4, cols / 2));
    b.secondSnakeInitialCells.add(new Coordinate(3 * rows / 4, cols / 2 - 1));

    b.secondInitialDirec = Directions.RIGHT;

    return b;
  }

  @SuppressWarnings("boxing")
  private static InitialBoard board3(int rows, int cols) {
    InitialBoard b = new InitialBoard();
    b.barriersCoord = new HashSet<Coordinate>();
    fillsides(b.barriersCoord, rows, cols);

    for (int i = 3; i < cols - 3; i++)
      b.barriersCoord.add(new Coordinate(rows / 2, i));
    for (int j = 3; j < rows - 3; j++)
      b.barriersCoord.add(new Coordinate(j, cols / 2));

    b.firstSnakeInitialCells = new LinkedList<Coordinate>();
    b.firstSnakeInitialCells.add(new Coordinate(1, cols / 2 - 1));
    b.firstSnakeInitialCells.add(new Coordinate(1, cols / 2));
    b.firstSnakeInitialCells.add(new Coordinate(1, cols / 2 + 1));

    b.firstInitialDirec = Directions.LEFT;

    b.secondSnakeInitialCells = new LinkedList<Coordinate>();
    b.secondSnakeInitialCells.add(new Coordinate(rows - 2, cols / 2 + 1));
    b.secondSnakeInitialCells.add(new Coordinate(rows - 2, cols / 2));
    b.secondSnakeInitialCells.add(new Coordinate(rows - 2, cols / 2 - 1));

    b.firstInitialDirec = Directions.RIGHT;

    return b;
  }

  @SuppressWarnings("boxing")
  private static InitialBoard board4(int rows, int cols) {
    InitialBoard b = new InitialBoard();
    b.barriersCoord = new HashSet<Coordinate>();
    fillsides(b.barriersCoord, rows, cols);

    for (int i = rows / 4; i < rows / 2; i++)
      b.barriersCoord.add(new Coordinate(i, cols / 4));
    for (int i = rows / 2; i < 3 * rows / 4; i++)
      b.barriersCoord.add(new Coordinate(i, 3 * cols / 4));
    for (int i = cols / 4; i < cols / 2; i++)
      b.barriersCoord.add(new Coordinate(rows / 4, i));
    for (int i = cols / 2; i < 3 * cols / 4; i++)
      b.barriersCoord.add(new Coordinate(3 * rows / 4, i));

    b.barriersCoord.add(new Coordinate(rows / 4, cols / 4));
    b.barriersCoord.add(new Coordinate(3 * rows / 4, 3 * cols / 4));

    b.firstSnakeInitialCells = new LinkedList<Coordinate>();
    b.firstSnakeInitialCells.add(new Coordinate(rows / 8, cols / 2 - 1));
    b.firstSnakeInitialCells.add(new Coordinate(rows / 8, cols / 2));
    b.firstSnakeInitialCells.add(new Coordinate(rows / 8, cols / 2 + 1));

    b.firstInitialDirec = Directions.LEFT;

    b.secondSnakeInitialCells = new LinkedList<Coordinate>();

    b.secondSnakeInitialCells.add(new Coordinate(7 * rows / 8, cols / 2 + 1));
    b.secondSnakeInitialCells.add(new Coordinate(7 * rows / 8, cols / 2));
    b.secondSnakeInitialCells.add(new Coordinate(7 * rows / 8, cols / 2 - 1));

    b.secondInitialDirec = Directions.RIGHT;
    return b;
  }

  @SuppressWarnings("boxing")
  private static InitialBoard board5(int rows, int cols) {
    InitialBoard b = new InitialBoard();
    b.barriersCoord = new HashSet<Coordinate>();
    fillsides(b.barriersCoord, rows, cols);

    for (int i = rows / 4; i < rows / 2; i++) {
      b.barriersCoord.add(new Coordinate(i, cols / 4));
      b.barriersCoord.add(new Coordinate(i, 3 * cols / 4));
    }
    for (int i = rows / 2; i < 3 * rows / 4; i++) {
      b.barriersCoord.add(new Coordinate(i, cols / 4));
      b.barriersCoord.add(new Coordinate(i, 3 * cols / 4));
    }
    for (int i = cols / 4; i < cols / 3; i++) {
      b.barriersCoord.add(new Coordinate(rows / 4, i));
      b.barriersCoord.add(new Coordinate(3 * rows / 4, i));

    }
    for (int i = 2 * cols / 3; i < 3 * cols / 4; i++) {
      b.barriersCoord.add(new Coordinate(rows / 4, i));
      b.barriersCoord.add(new Coordinate(3 * rows / 4, i));
    }
    b.barriersCoord.add(new Coordinate(3 * rows / 4, 3 * cols / 4));

    b.firstSnakeInitialCells = new LinkedList<Coordinate>();
    b.firstSnakeInitialCells.add(new Coordinate(rows / 8, cols / 2 - 1));
    b.firstSnakeInitialCells.add(new Coordinate(rows / 8, cols / 2));
    b.firstSnakeInitialCells.add(new Coordinate(rows / 8, cols / 2 + 1));

    b.firstInitialDirec = Directions.LEFT;

    b.secondSnakeInitialCells = new LinkedList<Coordinate>();
    b.secondSnakeInitialCells.add(new Coordinate(7 * rows / 8, cols / 2 + 1));
    b.secondSnakeInitialCells.add(new Coordinate(7 * rows / 8, cols / 2));
    b.secondSnakeInitialCells.add(new Coordinate(7 * rows / 8, cols / 2 - 1));

    b.secondInitialDirec = Directions.RIGHT;

    return b;
  }

  @SuppressWarnings("boxing")
  private static InitialBoard board6(int rows, int cols) {
    InitialBoard b = new InitialBoard();
    b.barriersCoord = new HashSet<Coordinate>();
    for (int i = 0; i < rows; i++) {
      b.barriersCoord.add(new Coordinate(i, 0));
      b.barriersCoord.add(new Coordinate(i, cols - 1));
    }

    for (int j = 1; j < cols - 1; j++) {
      b.barriersCoord.add(new Coordinate(0, j));
      b.barriersCoord.add(new Coordinate(rows - 1, j));
    }

    for (int i = 0; i < cols / 4; i++) {
      b.barriersCoord.add(new Coordinate(rows / 5, i + 3 * cols / 4));
      b.barriersCoord.add(new Coordinate(4 * rows / 5, i + cols / 5));
      b.barriersCoord.add(new Coordinate(2 * rows / 5, i + 2 * cols / 5));

    }

    b.firstSnakeInitialCells = new LinkedList<Coordinate>();
    b.firstSnakeInitialCells.add(new Coordinate(rows / 8, cols / 2 - 1));
    b.firstSnakeInitialCells.add(new Coordinate(rows / 8, cols / 2));
    b.firstSnakeInitialCells.add(new Coordinate(rows / 8, cols / 2 + 1));

    b.firstInitialDirec = Directions.LEFT;

    b.secondSnakeInitialCells = new LinkedList<Coordinate>();
    b.secondSnakeInitialCells.add(new Coordinate(7 * rows / 8, cols / 2 + 1));
    b.secondSnakeInitialCells.add(new Coordinate(7 * rows / 8, cols / 2));
    b.secondSnakeInitialCells.add(new Coordinate(7 * rows / 8, cols / 2 - 1));

    b.secondInitialDirec = Directions.RIGHT;

    return b;
  }
}