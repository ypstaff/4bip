package il.ac.technion.cs.ssdl.cs234311.yp09.snake;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;

/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 3/1/2014
 * 
 */
public class DrawView extends View {

	Paint paint;
	Color fillColor;
	Position location;

	/**
	 * All supported colors for a box
	 * 
	 */
	public enum Color {
		/**
		 * a value for the first possible color
		 */
		Blue,
		/**
		 * a value for the second possible color
		 */
		Orange,
		/**
		 * a value for the third possible color
		 */
		Green,
		/**
		 * a value for the forth possible color
		 */
		Red;
	}

	/**
	 * All supported positions for a box
	 * 
	 */
	public enum Position {
		/**
		 * used to draw the box at the center of parent view
		 */
		Center,
		/**
		 * used to draw the box at the right side of parent view
		 */
		Right,
		/**
		 * used to draw the box at the left side of parent view
		 */
		Left;
	}

	/**
	 * @param context
	 *            current context
	 * @param color
	 *            color of the box
	 * @param position
	 *            position of the box
	 */
	public DrawView(final Context context, final Color color,
			final Position position) {
		super(context);
		fillColor = color;
		location = position;
		paint = new Paint();
	}

	/**
	 * @param context
	 *            current context
	 */
	public DrawView(final Context context) {
		super(context);
	}

	@Override
	public void onDraw(final Canvas canvas) {

		final Resources resources = getResources();
		int fill = 0;

		switch (fillColor) {
		case Blue:
			fill = resources.getColor(R.color.blue);
			break;
		case Orange:
			fill = resources.getColor(R.color.orange);
			break;
		case Green:
			fill = resources.getColor(R.color.green);
			break;
		case Red:
			fill = resources.getColor(R.color.red);
			break;
		default:
			assert false;
		}

		final int borderColor = resources.getColor(R.color.border_color);
		paint.setColor(borderColor);
		// paint.setStrokeWidth(3);

		float length = Math.min(getWidth(), getHeight());
		final float margin = length / 14;
		length = length - 2 * margin;

		if (getWidth() < getHeight()) {
			if (location == DrawView.Position.Center) {
				canvas.drawRect(margin, (getHeight() - length) / 2, margin
						+ length, (getHeight() + length) / 2, paint);
				paint.setColor(fill);
				canvas.drawRect(margin * 2,
						(getHeight() - length) / 2 + margin, length,
						(getHeight() + length) / 2 - margin, paint);
			} else if (location == DrawView.Position.Left) {
				canvas.drawRect(0, (getHeight() - length) / 2, length,
						(getHeight() + length) / 2, paint);
				paint.setColor(fill);
				canvas.drawRect(margin, (getHeight() - length) / 2 + margin,
						length - margin, (getHeight() + length) / 2 - margin,
						paint);
			} else if (location == DrawView.Position.Right) {
				canvas.drawRect(getWidth() - length,
						(getHeight() - length) / 2, getWidth(),
						(getHeight() + length) / 2, paint);
				paint.setColor(fill);
				canvas.drawRect(getWidth() - length + margin,
						(getHeight() - length) / 2 + margin, getWidth()
								- margin, (getHeight() + length) / 2 - margin,
						paint);
			}
		} else if (location == DrawView.Position.Center) {
			canvas.drawRect((getWidth() - length) / 2, margin,
					(getWidth() + length) / 2, margin + length, paint);
			paint.setColor(fill);
			canvas.drawRect((getWidth() - length) / 2 + margin, margin * 2,
					(getWidth() + length) / 2 - margin, length, paint);
		} else if (location == DrawView.Position.Left) {
			canvas.drawRect(0, margin, length, margin + length, paint);
			paint.setColor(fill);
			canvas.drawRect(margin, margin * 2, length - margin, length, paint);
		} else if (location == DrawView.Position.Right) {
			canvas.drawRect(getWidth() - length, margin, getWidth(), margin
					+ length, paint);
			paint.setColor(fill);
			canvas.drawRect(getWidth() - length + margin, margin * 2,
					getWidth() - margin, length, paint);
		}

	}
}
