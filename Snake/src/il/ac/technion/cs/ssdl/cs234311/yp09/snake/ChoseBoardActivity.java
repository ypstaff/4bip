package il.ac.technion.cs.ssdl.cs234311.yp09.snake;

import android.content.Intent;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Display;
import android.view.WindowManager;
import android.widget.GridLayout;
import android.widget.GridLayout.Spec;
import android.widget.ImageView;

/**
 * 
 * 
 * the implement of the activity that will used to choose the size of the board
 * , and the layout of the boards.
 * 
 * @author hmassalh
 * 
 */
public class ChoseBoardActivity extends BaseActivity {
  /**
   * used to send the time between steps to the next activity
   */
  public static final String EXTERN_TIME_BETWEEN_STEPES = "il.ac.technion.cs.ssdl.cs234311.yp09.snake.ChoseBoardActivity.EXTERN_TIME_BETWEEN_STEPES";
  /**
   * used to send the rows number to the next activity
   */
  public static final String EXTERN_ROWS_NUM = "il.ac.technion.cs.ssdl.cs234311.yp09.snake.ChoseBoardActivity.EXTERN_ROWS_NUM";
  /**
   * used to send the columns number to the next activity
   */
  public static final String EXTERN_COLS_NUM = "il.ac.technion.cs.ssdl.cs234311.yp09.snake.ChoseBoardActivity.EXTERN_COLS_NUM";
  /**
   * used to send the board number to the next activity
   */
  public static final String EXTERN_BOARD_NUM = "il.ac.technion.cs.ssdl.cs234311.yp09.snake.ChoseBoardActivity.EXTERN_BOARD_NUM";

  private static final int INITIAL_ROWS_NUM = 10;
  private static final int INITIAL_COLS_NUM = 10;
  private static final int INITIAL_BOARD_NUM = 10;
  // private static final String TAG =
  // "il.ac.technion.cs.ssdl.cs234311.yp09.snake.ChoseBoardActivity.tag";
  private static final int BOARD_MIN_HEIGHT = 9;
  private static final double BOARD_GRID_WEIGHT = 0.60 / (0.67 + 0.12 + 0.12 + 0.09);
  private int timeBetweenSteps, boardNum, rowsNum, colsNum;
  private GridLayout boardGrid;
  private FourButtonsFragment fourButtonsFragment;
  private int pixInCell;

  @Override
  protected void onCreate(Bundle savedInstanceState) {

    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_chose_board);

    Intent intent = getIntent();
    String message = intent.getStringExtra(SnakeMainActivity.EXTRA_MESSAGE);
    timeBetweenSteps = Integer.parseInt(message);

    boardGrid = (GridLayout) findViewById(R.id.boardGrid);

    fourButtonsFragment = new FourButtonsFragment();
    fourButtonsFragment.setListener(this);
    getFragmentManager().beginTransaction()
        .add(R.id.buttons_frame, fourButtonsFragment).commit();

    rowsNum = INITIAL_ROWS_NUM;
    colsNum = INITIAL_COLS_NUM;
    boardNum = INITIAL_BOARD_NUM;
    Drawable drawable = this.getResources().getDrawable(R.drawable.blackstar);

    pixInCell = drawable.getIntrinsicHeight();

    fillGridWithBoard();

    getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
  }

  private void fillGridWithBoard() {

    InitialBoard initialBoard = new InitialBoard(boardNum, rowsNum, colsNum);

    boardGrid.removeAllViewsInLayout();
    boardGrid.setRowCount(rowsNum);
    boardGrid.setColumnCount(colsNum);

    int imageResId = 0;
    for (int i = 0; i < rowsNum; i++)
      for (int j = 0; j < colsNum; j++) {
        Coordinate currCellCoord = new Coordinate(Integer.valueOf(i),
            Integer.valueOf(j));
        if (currCellCoord
            .equals(initialBoard.firstSnakeInitialCells.getFirst())
            || currCellCoord.equals(initialBoard.secondSnakeInitialCells
                .getFirst()))
          imageResId = R.drawable.blackstar;
        else if (initialBoard.firstSnakeInitialCells.contains(currCellCoord))
          imageResId = R.drawable.greenstar;
        else if (initialBoard.secondSnakeInitialCells.contains(currCellCoord))
          imageResId = R.drawable.bluestar;
        else if (initialBoard.barriersCoord.contains(currCellCoord))
          imageResId = R.drawable.redstar;
        else
          imageResId = R.drawable.graystar;

        Spec row = GridLayout.spec(i, 1);
        Spec colspan = GridLayout.spec(j, 1);
        GridLayout.LayoutParams gridLayoutParam = new GridLayout.LayoutParams(
            row, colspan);
        ImageView cellImage = new ImageView(this);
        cellImage.setImageResource(imageResId);

        boardGrid.addView(cellImage, gridLayoutParam);
      }

  }

  @Override
  public void onOperation(int c) {

    switch (OpCodeInterpreter.getOp(c)) {
    case BLUE:
      boardNum = (boardNum + 1) % 6;
      fillGridWithBoard();
      break;
    case ORANGE:
      makeBoardSmaller();
      fillGridWithBoard();
      break;
    case GREEN:
      makeBoardBigger();
      fillGridWithBoard();
      break;
    case RED:
      startPlaying();
      break;
    case GREEN_RED:
      finishAffinity();
      break;
    default:
      break;
    }

  }

  private void makeBoardBigger() {
    Display display = getWindowManager().getDefaultDisplay();
    Point size = new Point();
    display.getSize(size);
    int width = size.x;
    int height = size.y;
    if ((colsNum + 1) * pixInCell > width)
      return;

    if ((rowsNum + 1) * pixInCell > height * BOARD_GRID_WEIGHT)
      return;

    rowsNum++;
    colsNum++;
  }

  private void makeBoardSmaller() {
    if (rowsNum == BOARD_MIN_HEIGHT)
      return;
    rowsNum--;
    colsNum--;
  }

  private void startPlaying() {
    Intent intent = new Intent(ChoseBoardActivity.this, GameActivity.class);
    intent.putExtra(EXTERN_BOARD_NUM, Integer.valueOf(boardNum).toString());
    intent.putExtra(EXTERN_COLS_NUM, Integer.valueOf(colsNum).toString());
    intent.putExtra(EXTERN_ROWS_NUM, Integer.valueOf(rowsNum).toString());
    intent.putExtra(EXTERN_TIME_BETWEEN_STEPES,
        Integer.valueOf(timeBetweenSteps).toString());
    startActivity(intent);

  }

}
