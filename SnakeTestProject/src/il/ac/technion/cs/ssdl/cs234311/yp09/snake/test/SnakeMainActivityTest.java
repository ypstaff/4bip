package il.ac.technion.cs.ssdl.cs234311.yp09.snake.test;

import com.robotium.solo.Solo;

import il.ac.technion.cs.ssdl.cs234311.yp09.snake.ChoseBoardActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.snake.FourButtonsFragment;
import il.ac.technion.cs.ssdl.cs234311.yp09.snake.GameActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.snake.SnakeMainActivity;
import android.test.ActivityInstrumentationTestCase2;

public class SnakeMainActivityTest extends
		ActivityInstrumentationTestCase2<SnakeMainActivity> {

	private Solo solo;

	public SnakeMainActivityTest() {
		super(SnakeMainActivity.class);
	}

	  @Override
	  public void setUp() {
	    FourButtonsFragment.debugMode = true;
	    GameActivity.debugMode = true;
	    solo = new Solo(getInstrumentation(), getActivity());
	  }

	  @Override
	  public void tearDown() throws Exception {
	    solo.finishOpenedActivities();
	    FourButtonsFragment.debugMode = false;
	    GameActivity.debugMode = false;
	  }

	public void testSlowSpeed() {
		solo.assertCurrentActivity("check on first activity",
				SnakeMainActivity.class);
		solo.clickOnButton("4");
		solo.waitForActivity(ChoseBoardActivity.class);
		solo.assertCurrentActivity("Wrong class", ChoseBoardActivity.class);
	}

	public void testMediumSpeed() {
		solo.assertCurrentActivity("check on first activity",
				SnakeMainActivity.class);
		solo.clickOnButton("3");
		solo.waitForActivity(ChoseBoardActivity.class);
		solo.assertCurrentActivity("Wrong class", ChoseBoardActivity.class);
	}

	public void testFastSpeed() {
		solo.assertCurrentActivity("check on first activity",
				SnakeMainActivity.class);
		solo.clickOnButton("2");
		solo.waitForActivity(ChoseBoardActivity.class);
		solo.assertCurrentActivity("Wrong class", ChoseBoardActivity.class);
	}

	public void testVeryFastSpeed() {
		solo.assertCurrentActivity("check on first activity",
				SnakeMainActivity.class);
		solo.clickOnButton("1");
		solo.waitForActivity(ChoseBoardActivity.class);
		solo.assertCurrentActivity("Wrong class", ChoseBoardActivity.class);
	}

}
