package il.ac.technion.cs.ssdl.cs234311.yp09.snake.test;

import il.ac.technion.cs.ssdl.cs234311.yp09.snake.ChoseBoardActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.snake.FourButtonsFragment;
import il.ac.technion.cs.ssdl.cs234311.yp09.snake.GameActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.snake.GameOverActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.snake.R;
import il.ac.technion.cs.ssdl.cs234311.yp09.snake.SnakeMainActivity;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.TextView;

import com.robotium.solo.Solo;

public class TestScoresAndLastMessage extends
		ActivityInstrumentationTestCase2<SnakeMainActivity> {

	private Solo solo;

	public TestScoresAndLastMessage() {
		super(SnakeMainActivity.class);
	}

	@Override
	public void setUp() {
		FourButtonsFragment.debugMode = true;
		GameActivity.debugMode = true;
		solo = new Solo(getInstrumentation(), getActivity());
	}

	@Override
	public void tearDown() throws Exception {
		solo.finishOpenedActivities();
		FourButtonsFragment.debugMode = false;
		GameActivity.debugMode = false;
	}

	public void testTheDefaultSettings() {
		GameActivity.debugMode = true;
		solo.assertCurrentActivity("check on first activity",
				SnakeMainActivity.class);
		solo.clickOnButton("4");
		solo.waitForActivity(ChoseBoardActivity.class);
		solo.assertCurrentActivity("Wrong class", ChoseBoardActivity.class);
		solo.clickOnButton("4");
		solo.waitForActivity(GameActivity.class);
		solo.assertCurrentActivity("Wrong class", GameActivity.class);
		solo.waitForActivity(GameOverActivity.class);
		TextView losrTextView = (TextView) solo.getView(R.id.loserTextView);
		TextView firstScore = (TextView) solo.getView(R.id.firstScoreTextView);
		TextView secondScore = (TextView) solo
				.getView(R.id.secondScoreTextView);
		assertTrue(losrTextView.getText().toString()
				.equals("second player crashed"));
		assertTrue(firstScore.getText().toString()
				.equals("First Player Score : 0"));
		assertTrue(secondScore.getText().toString()
				.equals("Second Player Score : 1"));
	}
	public void testWithBiggerBoard(){
		solo.assertCurrentActivity("check on first activity",
				SnakeMainActivity.class);
		solo.clickOnButton("4");
		solo.waitForActivity(ChoseBoardActivity.class);
		solo.assertCurrentActivity("Wrong class", ChoseBoardActivity.class);
		solo.clickOnButton("3");
		solo.clickOnButton("3");
		solo.clickOnButton("3");
		solo.clickOnButton("3");
		solo.clickOnButton("4");
		solo.waitForActivity(GameActivity.class);
		solo.assertCurrentActivity("Wrong class", GameActivity.class);
		solo.waitForActivity(GameOverActivity.class);
		TextView losrTextView = (TextView) solo.getView(R.id.loserTextView);
		TextView firstScore = (TextView) solo.getView(R.id.firstScoreTextView);
		TextView secondScore = (TextView) solo
				.getView(R.id.secondScoreTextView);
		assertTrue(losrTextView.getText().toString()
				.equals("second player crashed"));
		assertTrue(firstScore.getText().toString()
				.equals("First Player Score : 0"));
		assertTrue(secondScore.getText().toString()
				.equals("Second Player Score : 1"));
	}
	
	public void testAthorBoards(){
		solo.assertCurrentActivity("check on first activity",
				SnakeMainActivity.class);
		solo.clickOnButton("4");
		solo.waitForActivity(ChoseBoardActivity.class);
		solo.assertCurrentActivity("Wrong class", ChoseBoardActivity.class);
		solo.clickOnButton("1");
		solo.clickOnButton("1");
		solo.clickOnButton("1");
		solo.clickOnButton("4");
		solo.waitForActivity(GameActivity.class);
		solo.assertCurrentActivity("Wrong class", GameActivity.class);
		solo.waitForActivity(GameOverActivity.class);
		TextView losrTextView = (TextView) solo.getView(R.id.loserTextView);
		TextView firstScore = (TextView) solo.getView(R.id.firstScoreTextView);
		TextView secondScore = (TextView) solo
				.getView(R.id.secondScoreTextView);
		assertTrue(losrTextView.getText().toString()
				.equals("both players crashed"));
		assertTrue(firstScore.getText().toString()
				.equals("First Player Score : 0"));
		assertTrue(secondScore.getText().toString()
				.equals("Second Player Score : 0"));
		
	}
}
