package il.ac.technion.cs.ssdl.cs234311.DontPanic.activities;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.R;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut.CodeOperation;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Music.MusicServices;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.data.ShortcutsParser;
import java.util.Collections;
import java.util.List;

import com.google.gson.Gson;

import android.content.Intent;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MusicAlbumsMenuActivity extends MenuActivity {

    @Override
    protected List<String> getAllListItems() {
        final List<String> albums = MusicServices
                .GetAllAlbums(getApplicationContext());

        Collections.sort(albums);
        albums.add(0, ShortcutsParser.ALL_SONGS_ALBUM_NAME);

        return albums;
    }

    @Override
    protected ListView getListView() {
        return (ListView) findViewById(R.id.music_albums_list);
    }

    @Override
    protected ArrayAdapter<String> getAdapter() {
        return new ArrayAdapter<String>(this,
                R.layout.list_row_small_highlighted, R.id.listItemText,
                listItems);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.music_albums_menu;
    }

    @Override
    protected void setTitle() {
        title.setText(getString(R.string.title_menu_music_albums));
    }

    @Override
    protected void openNextWindowWithSelection() {
        if (adapter.getCount() == 0)
            return;

        final String selected = adapter.getItem(listView
                .getCheckedItemPosition());
        final Intent intent = new Intent(MusicAlbumsMenuActivity.this,
                CodeAdditionActivity.class);
        intent.putExtra("SHORTCUT_TYPE", CodeOperation.MUSIC_PLAYLIST);
        intent.putExtra("SHORTCUT_OBJ", new Gson().toJson(selected));
        startActivity(intent);
    }

}
