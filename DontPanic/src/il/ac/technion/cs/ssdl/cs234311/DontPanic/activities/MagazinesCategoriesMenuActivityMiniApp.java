package il.ac.technion.cs.ssdl.cs234311.DontPanic.activities;

import android.content.Intent;

public class MagazinesCategoriesMenuActivityMiniApp extends
        MagazinesCategoriesMenuActivity {

    @Override
    protected void openNextWindowWithSelection() {
        if (getAdapter().getCount() == 0)
            return;

        final String selected = getAdapter().getItem(
                getListView().getCheckedItemPosition());

        final Intent intent = new Intent(
                MagazinesCategoriesMenuActivityMiniApp.this,
                MagazinesMenuActivityMiniApp.class);
        intent.putExtra("MAGAZINES_CATEGORY", selected);
        startActivity(intent);
    }

}
