package il.ac.technion.cs.ssdl.cs234311.DontPanic.activities;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.R;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MusicMenuActivity extends MenuActivity {

    @Override
    protected List<String> getAllListItems() {
        final List<String> list = new ArrayList<String>();

        list.add(getString(R.string.music_shortcut_to_an_album));
        list.add(getString(R.string.music_shortcut_to_an_artist));

        return list;
    }

    @Override
    protected ListView getListView() {
        return (ListView) findViewById(R.id.music_options_list);
    }

    @Override
    protected ArrayAdapter<String> getAdapter() {
        return new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_activated_1, listItems);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.music_menu;
    }

    @Override
    protected void openNextWindowWithSelection() {
        if (adapter.getCount() == 0)
            return;
        final String selected = adapter.getItem(listView
                .getCheckedItemPosition());

        final Intent intent = new Intent();

        if (selected.equals(getString(R.string.music_shortcut_to_an_album)))
            intent.setClass(MusicMenuActivity.this,
                    MusicAlbumsMenuActivity.class);

        else if (selected
                .equals(getString(R.string.music_shortcut_to_an_artist)))
            intent.setClass(MusicMenuActivity.this,
                    MusicArtistsMenuActivity.class);
        else
            return;

        startActivity(intent);
    }

    @Override
    protected void setTitle() {
        title.setText(getString(R.string.title_menu_music));
    }
}
