/**
 * @date Apr 6, 2014 
 * @author Or
 * @email or81322@gmail.com
 */
package il.ac.technion.cs.ssdl.cs234311.DontPanic.classes;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.R;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.IllegalShortcutLetter;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.ShortcutLetter;

import java.util.Iterator;
import java.util.List;

import android.app.Activity;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;

/**
 *
 */
public class ShortcutDescription {

    public static void updateDescriptions(final Activity activity,
            final List<boolean[]> word, final int layoutId) {

        final LinearLayout visualDescription = (LinearLayout) activity
                .findViewById(layoutId);
        visualDescription.removeAllViews();

        if (word.isEmpty()) {
            visualDescription.setVisibility(View.GONE);
            return;
        }
        visualDescription.setVisibility(View.VISIBLE);

        for (final Iterator<boolean[]> iterator = word.iterator(); iterator
                .hasNext();) {
            final boolean[] code = iterator.next();
            ShortcutLetter shortcutLetter;
            try {
                shortcutLetter = ShortcutTranslator.array2letter(code);
            } catch (final IllegalShortcutLetter e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                shortcutLetter = ShortcutLetter.ONE;
            }
            int leftColor, rightColor;
            switch (shortcutLetter) {// red -> yellow -> blue -> green
            case ONE_TWO:
                leftColor = activity.getResources().getColor(
                        R.color.holo_red_light);
                rightColor = activity.getResources().getColor(
                        R.color.holo_yellow_light);
                break;
            case TWO_THREE:
                leftColor = activity.getResources().getColor(
                        R.color.holo_yellow_light);
                rightColor = activity.getResources().getColor(
                        R.color.holo_blue_light);
                break;
            case THREE_FOUR:
                leftColor = activity.getResources().getColor(
                        R.color.holo_blue_light);
                rightColor = activity.getResources().getColor(
                        R.color.holo_green_light);
                break;
            case FOUR_ONE:
                leftColor = activity.getResources().getColor(
                        R.color.holo_red_light);
                rightColor = activity.getResources().getColor(
                        R.color.holo_green_light);
                break;
            default:
                continue;
            }

            final LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(
                    LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            lParams.gravity = Gravity.CENTER;

            final LayerDrawable background = (LayerDrawable) activity
                    .getResources().getDrawable(
                            R.drawable.two_colors_round_background);
            final GradientDrawable leftDrawable = (GradientDrawable) background
                    .findDrawableByLayerId(R.id.leftColor);
            final GradientDrawable rightDrawable = (GradientDrawable) background
                    .findDrawableByLayerId(R.id.rightColor);
            leftDrawable.setColor(leftColor);
            rightDrawable.setColor(rightColor);

            final ImageView imgNew = new ImageView(activity);
            imgNew.setPadding(0, 4, 4, 0);
            imgNew.setImageDrawable(background);
            visualDescription.addView(imgNew, lParams);
        }
        if (!word.isEmpty()) {
            final LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(
                    LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            lParams.gravity = Gravity.CENTER;

            final ImageView imgNew = new ImageView(activity);
            imgNew.setPadding(0, 4, 4, 0);
            imgNew.setImageResource(R.drawable.ic_action_forward);
            visualDescription.addView(imgNew, lParams);
        }
    }
}
