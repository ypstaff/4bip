package il.ac.technion.cs.ssdl.cs234311.DontPanic.serverConnection;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.R;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.activities.LoginActivity;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.snake.SnakeView;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.statistics.SnakeData;

import java.util.Date;
import java.util.concurrent.ExecutionException;

import android.content.Context;

import com.google.gson.Gson;

public class GameServerMessages {

    static final String SERVER_URL = "http://yp7dontpanic.appspot.com/";

    public static void sendScore(long mScore, int level, long gameTime,
            Context context) {
        String levelName = null;
        switch (level) {
        
        case SnakeView.EASY:
            levelName = "Easy";
        case SnakeView.MEDIUM:
            levelName = "Medium";
        case SnakeView.HARD:
            levelName = "Hard";
        case SnakeView.HARDEST:
            levelName = "Hardest";
        case SnakeView.PANIC:
            levelName = "Panic";
       
        }

        final String[] packetFields = new String[5];
        String[] snakeInfo = new String[3];
        
        Long l_mScore = Long.valueOf(mScore);
        Long l_gameTime = Long.valueOf(gameTime);
         
        snakeInfo[0] = l_mScore.toString();
        snakeInfo[1] = levelName;
        snakeInfo[2] = l_gameTime.toString();
        Date date = new Date();

        SnakeData snakeData = new SnakeData(LoginActivity.dataManager.getUser().email, date, snakeInfo);

        packetFields[0] = "UploadStatData";
        packetFields[1] = "type";
        packetFields[2] = "snake";
        packetFields[3] = "info";
        packetFields[4] = new Gson().toJson(snakeData);

        new HttpPostPackets(context)
                .execute(packetFields);

    }

    public static long getHighScore(Context context) {
        final String MESSAGE_TYPE = "Snake";
        final String GET_HIGH_SCORE = "High Score";
        String highScore = null;

        final String[] packetFields = new String[3];

        packetFields[0] = MESSAGE_TYPE;
        packetFields[1] = GET_HIGH_SCORE;
        packetFields[2] = highScore;

        try {
            new HttpGetPackets(context).execute(packetFields).get();
        } catch (final InterruptedException e) {
            e.printStackTrace();
        } catch (final ExecutionException e) {
            e.printStackTrace();
        }

        return Long.parseLong(highScore);
    }

}
