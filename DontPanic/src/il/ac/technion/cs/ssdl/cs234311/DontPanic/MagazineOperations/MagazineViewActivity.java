package il.ac.technion.cs.ssdl.cs234311.DontPanic.MagazineOperations;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.DontPanicActivity;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.R;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.ColorArrayLocations;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.activities.LoginActivity;
import android.animation.ObjectAnimator;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;

public class MagazineViewActivity extends DontPanicActivity {
    private CustomWebView webview;
    private ProgressBar progress;

    private Button redButton;
    private Button greenButton;
    private Button blueButton;
    private Button yellowButton;

    private boolean isShift;

    @Override
    protected void initViews() {
        super.initViews();

        redButton = (Button) findViewById(R.id.RedButton);
        greenButton = (Button) findViewById(R.id.GreenButton);
        blueButton = (Button) findViewById(R.id.BlueButton);
        yellowButton = (Button) findViewById(R.id.YellowButton);

        greenButton.setText("Shift");
        updateOnShift();

        progress = (ProgressBar) findViewById(R.id.progress_bar);
    }

    @Override
    protected void initTouchListeners() {
        super.initTouchListeners();

        yellowButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.YELLOW));
        blueButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.BLUE));
        greenButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.GREEN));
        redButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.RED));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.magazine_view);

        initViews();
        initTouchListeners();

        webview = (CustomWebView) findViewById(R.id.webview);
        webview.setWebViewClient(new MyBrowser());

        webview.loadUrl(getIntent().getStringExtra("URL_LINK"));
    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            progress.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            progress.setVisibility(View.INVISIBLE);
        }
    }

    private void updateOnShift() {
        Drawable shiftToggle = getResources().getDrawable(
                R.drawable.ic_action_shift_1_512);
        Drawable upRighttToggle = getResources().getDrawable(
                R.drawable.ic_action_collapse);
        Drawable downLeftToggle = getResources().getDrawable(
                R.drawable.ic_action_expand);

        if (!isShift) {
            greenButton.setTextColor(Color.BLACK);
            blueButton.setText("Up");
            yellowButton.setText("Down");
        } else {
            greenButton.setTextColor(Color.WHITE);
            blueButton.setText("Right");
            yellowButton.setText("Left");

            shiftToggle = getResources().getDrawable(
                    R.drawable.ic_action_shift_1_128);
            upRighttToggle = getResources().getDrawable(
                    R.drawable.ic_action_next_item);
            downLeftToggle = getResources().getDrawable(
                    R.drawable.ic_action_previous_item);

        }
        greenButton.setCompoundDrawablesWithIntrinsicBounds(null, shiftToggle,
                null, null);
        blueButton.setCompoundDrawablesWithIntrinsicBounds(null,
                upRighttToggle, null, null);
        yellowButton.setCompoundDrawablesWithIntrinsicBounds(null,
                downLeftToggle, null, null);
    }

    /**
     * scroll up one item in the list view when the arrowUp is pressed
     */
    private void arrowUp() {
        ObjectAnimator anim = ObjectAnimator.ofInt(webview, "scrollY",
                webview.getScrollY(),
                Math.max(webview.getScrollY() - webview.getHeight() / 4, 0));
        anim.setDuration(350);
        anim.start();
        // webview.scrollBy(0, -webview.getHeight() / 4);
    }

    /**
     * scroll down one item in the list view when the arrowDown is pressed
     */
    private void arrowDown() {
        @SuppressWarnings("deprecation")
        ObjectAnimator anim = ObjectAnimator.ofInt(webview, "scrollY", webview
                .getScrollY(), Math.min(
                webview.getScrollY() + webview.getHeight() / 4,
                (int) Math.floor(webview.getContentHeight()
                        * webview.getScale())
                        - webview.getHeight()));
        anim.setDuration(350);
        anim.start();
        // webview.scrollBy(0, webview.getHeight() / 4);
    }

    /**
     * scroll up one item in the list view when the arrowLeft is pressed
     */
    private void arrowLeft() {
        ObjectAnimator anim = ObjectAnimator.ofInt(webview, "scrollX",
                webview.getScrollX(),
                Math.max(webview.getScrollX() - webview.getWidth() / 4, 0));
        anim.setDuration(350);
        anim.start();
        // webview.scrollBy(0, -webview.getHeight() / 4);
    }

    /**
     * scroll down one item in the list view when the arrowRight is pressed
     */
    private void arrowRight() {
        @SuppressWarnings("deprecation")
        ObjectAnimator anim = ObjectAnimator.ofInt(webview, "scrollX", webview
                .getScrollX(),
                Math.min(
                        webview.getScrollX() + webview.getWidth() / 4,
                        (int) Math.floor(webview.getContentWidth()
                                * webview.getScale())
                                - webview.getWidth()));
        anim.setDuration(350);
        anim.start();
        // webview.scrollBy(0, webview.getHeight() / 4);
    }

    /**
     * all the operations supported in this activity
     */
    private enum Operation {
        SHIFT, BACK, ARROW_UP, ARROW_DOWN, ARROW_RIGHT, ARROW_LEFT, PANIC, OTHER_CODE
    }

    /**
     * matching input code to OPERATIONS constant
     * 
     * @param code
     *            - boolean array that represents the buttons pressed or the
     *            array given by the enterCode method
     * @return - the Operation constant that is matching the given input
     * @see DontPanicActivity.enterCode
     */
    private Operation getOperationFromCode(final boolean[] code) {
        final boolean green_pressed = code[ColorArrayLocations.GREEN.ordinal()];
        final boolean yellow_pressed = code[ColorArrayLocations.YELLOW
                .ordinal()];
        final boolean red_pressed = code[ColorArrayLocations.RED.ordinal()];
        final boolean blue_pressed = code[ColorArrayLocations.BLUE.ordinal()];

        // enter pressed
        if (green_pressed && !(blue_pressed || red_pressed || yellow_pressed))
            return Operation.SHIFT;

        // clear/back pressed
        if (red_pressed && !(blue_pressed || green_pressed || yellow_pressed))
            return Operation.BACK;

        // arrow up/right pressed
        if (blue_pressed && !(green_pressed || red_pressed || yellow_pressed))
            if (!isShift)
                return Operation.ARROW_UP;
            else
                return Operation.ARROW_RIGHT;

        // arrow down/left pressed
        if (yellow_pressed && !(blue_pressed || red_pressed || green_pressed))
            if (!isShift)
                return Operation.ARROW_DOWN;
            else
                return Operation.ARROW_LEFT;

        if (red_pressed && blue_pressed && green_pressed && yellow_pressed)
            return Operation.PANIC;

        return Operation.OTHER_CODE;
    }

    @Override
    public void enterCode(final boolean[] code) {

        switch (getOperationFromCode(code)) {
        case OTHER_CODE:
            return;

        case BACK:
            finish();
            break;

        case SHIFT:
            isShift = !isShift;
            updateOnShift();
            break;

        case ARROW_UP:
            arrowUp();
            break;

        case ARROW_DOWN:
            arrowDown();
            break;

        case ARROW_LEFT:
            arrowLeft();
            break;

        case ARROW_RIGHT:
            arrowRight();
            break;

        case PANIC:
            enterPanic(LoginActivity.dataManager);
            break;

        default:
            break;
        }
    }
}
