package il.ac.technion.cs.ssdl.cs234311.DontPanic.Tests.Robotium;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.R;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.ColorArrayLocations;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.activities.ConfigurationGame;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.activities.Snake;
import android.test.ActivityInstrumentationTestCase2;

import com.robotium.solo.Condition;
import com.robotium.solo.Solo;

public class SnakeTest extends
        ActivityInstrumentationTestCase2<ConfigurationGame> {

    private Solo solo;

    public SnakeTest() {
        super(ConfigurationGame.class);

    }

    @Override
    public void setUp() throws Exception {
        // setUp() is run before a test case is started.
        // This is where the solo object is created.
        solo = new Solo(getInstrumentation(), getActivity());
    }

    @Override
    public void tearDown() throws Exception {
        // tearDown() is run after a test case has finished.
        // finishOpenedActivities() will finish all the activities that have
        // been opened during the test execution.
        solo.finishOpenedActivities();
    }

    /*************************** Help Functions ****************************/
    private void startGame() {
        solo.assertCurrentActivity(
                "Error: Expected ConfigurationGame Activity",
                "ConfigurationGame");
        solo.clickOnView(solo.getView(R.id.GreenButton));
        solo.assertCurrentActivity("Error: Expected Snake Activity", "Snake");
        solo.searchText("Press Right To Play");
        solo.clickOnView(solo.getView(R.id.GreenButton));

    }

    private void delayTime(final int time) {
        final Condition falseCond = new Condition() {
            @Override
            public boolean isSatisfied() {
                return false;
            }
        };
        final boolean res = solo.waitForCondition(falseCond, time);
        assertFalse(res);
    }

    private void waitForGameToEnd(final int time) {
        final Condition gameOverCond = new Condition() {
            @Override
            public boolean isSatisfied() {
                return solo.searchText("Game Over");
            }
        };
        final boolean res = solo.waitForCondition(gameOverCond, time);
        assertTrue("Error: Game Expected to End!", res);
    }

    private void makeCommand(final boolean r, final boolean y, final boolean b, final boolean g,
            final boolean[] command) {
        command[ColorArrayLocations.RED.ordinal()] = r;
        command[ColorArrayLocations.YELLOW.ordinal()] = y;
        command[ColorArrayLocations.BLUE.ordinal()] = b;
        command[ColorArrayLocations.GREEN.ordinal()] = g;

    }

    /***************************
     * Tests
     * 
     * @throws Throwable
     ****************************/
    public void testInitialConfigurations() {
        boolean configurationFound = true;
        configurationFound = configurationFound && solo.searchText("Speed:");
        configurationFound = configurationFound && solo.searchText("Normal");
        configurationFound = configurationFound
                && solo.searchText("Accelerate:");
        configurationFound = configurationFound && solo.searchText("Yes");
        configurationFound = configurationFound && solo.searchText("Level");
        configurationFound = configurationFound && solo.searchText("Easy");

        assertTrue("some Configuration are not correct!", configurationFound);

    }

    public void testStartGame() {
        startGame();

    }

    public void testShowConfiguration() {
        startGame();
        boolean configurationFound = true;
        configurationFound = configurationFound && solo.searchText("Speed:");
        configurationFound = configurationFound && solo.searchText("Normal");
        configurationFound = configurationFound
                && solo.searchText("Accelerate:");
        configurationFound = configurationFound && solo.searchText("Yes");
        configurationFound = configurationFound && solo.searchText("Level");
        configurationFound = configurationFound && solo.searchText("Easy");
        configurationFound = configurationFound && solo.searchText("Score:");

        assertTrue("some Configuration are not correct!", configurationFound);

    }

    public void testChangeGameSpeed() throws Throwable {
        final ConfigurationGame activity = (ConfigurationGame) solo
                .getCurrentActivity();

        assertTrue("Error: Expected Normal!", solo.searchText("Normal"));
        assertFalse("Error: Expect not to find Fast", solo.searchText("Fast"));
        assertFalse("Error: Expect not to find Slow", solo.searchText("Slow"));

        final boolean command[] = new boolean[4];
        makeCommand(true, true, false, false, command);
        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {

                activity.enterCode(command);
                assertTrue("Error: Expected Fast!", solo.searchText("Fast"));
                assertFalse("Error: Expected not to find Normal!",
                        solo.searchText("Normal"));
                activity.enterCode(command);
                assertTrue("Error: Expected Fastest!",
                        solo.searchText("Fastest"));
                activity.enterCode(command);
                assertTrue("Error: Expected Slow!", solo.searchText("Slow"));
                activity.enterCode(command);
                assertTrue("Error: Expected Normal!", solo.searchText("Normal"));

            }
        });
    }

    public void testChangeAccelerate() throws Throwable {
        final ConfigurationGame activity = (ConfigurationGame) solo
                .getCurrentActivity();
        delayTime(100);

        assertTrue("Error: Expected Yes!", solo.searchText("Yes"));
        final boolean command[] = new boolean[4];
        makeCommand(false, true, true, false, command);

        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {

                activity.enterCode(command);
                assertTrue("Error: Expected No!", solo.searchText("No"));
                activity.enterCode(command);
                assertTrue("Error: Expected Yes!", solo.searchText("Yes"));
            }
        });
    }

    public void testChangeGameLevel() throws Throwable {
        final ConfigurationGame activity = (ConfigurationGame) solo
                .getCurrentActivity();

        assertTrue("Error: Expected Easy", solo.searchText("Easy"));
        assertFalse("Error: Expect not to find Panic!",
                solo.searchText("Panic!"));
        assertFalse("Error: Expect not to find Hard", solo.searchText("Hard"));

        final boolean command[] = new boolean[4];
        makeCommand(false, false, true, true, command);
        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {

                activity.enterCode(command);
                assertTrue("Error: Expected Medium!", solo.searchText("Medium"));
                assertFalse("Error: Expected not to find Easy!",
                        solo.searchText("Easy"));
                activity.enterCode(command);
                assertTrue("Error: Expected Hard!", solo.searchText("Hard"));
                activity.enterCode(command);
                assertTrue("Error: Expected Hardest!",
                        solo.searchText("Hardest"));
                activity.enterCode(command);
                assertTrue("Error: Expected Panic!", solo.searchText("Panic!"));
                activity.enterCode(command);
                assertTrue("Error: Expected Easy!", solo.searchText("Easy"));

            }
        });
    }

    public void testLoseByHittingFrame() throws Throwable {
        startGame();
        final Snake activity = (Snake) solo.getCurrentActivity();
        delayTime(3000);
        solo.clickOnView(solo.getView(R.id.BlueButton));
        waitForGameToEnd(100000);
        final boolean command[] = new boolean[4];
        makeCommand(false, true, true, false, command);
        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                activity.enterCode(command);
            }
        });
        startGame();
        delayTime(1000);
        solo.clickOnView(solo.getView(R.id.BlueButton));
        delayTime(600);
        solo.clickOnView(solo.getView(R.id.RedButton));
        waitForGameToEnd(1000000);

    }

    public void testLoseByHittingHimself() throws InterruptedException {
        startGame();
        delayTime(1000);
        solo.clickOnView(solo.getView(R.id.YellowButton));
        solo.clickOnView(solo.getView(R.id.RedButton));
        solo.clickOnView(solo.getView(R.id.BlueButton));
        waitForGameToEnd(5);
    }

    public void testStartNewGame() throws Throwable {
        solo.assertCurrentActivity(
                "Error: Expected ConfigurationGame Activity",
                "ConfigurationGame");
        solo.clickOnView(solo.getView(R.id.GreenButton));
        solo.assertCurrentActivity("Error: Expected Snake Activity", "Snake");
        final Snake activity = (Snake) solo.getCurrentActivity();
        solo.clickOnView(solo.getView(R.id.GreenButton));
        delayTime(1000);
        final boolean command[] = new boolean[4];
        makeCommand(false, true, true, false, command);
        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {

                activity.enterCode(command);
            }
        });
        solo.assertCurrentActivity(
                "Error: Expected ConfigurationGame Activity",
                "ConfigurationGame");
        solo.clickOnView(solo.getView(R.id.GreenButton));
        solo.assertCurrentActivity("Error: Expected Snake Activity", "Snake");
        delayTime(1000);
        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                activity.enterCode(command);
            }
        });
        solo.assertCurrentActivity(
                "Error: Expected ConfigurationGame Activity",
                "ConfigurationGame");

    }

    public void testAAA() throws Throwable {
        final ConfigurationGame activity = (ConfigurationGame) solo
                .getCurrentActivity();
        final boolean command[] = new boolean[4];
        makeCommand(false, false, true, true, command);
        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                activity.enterCode(command);
            }
        });
        startGame();
        final Snake activity2 = (Snake) solo.getCurrentActivity();
        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                activity2.enterCode(command);
            }
        });
        delayTime(2000);
        assertTrue("Error: Expected Paused!", solo.searchText("Paused"));
        assertTrue("Error: Expected Paused String!",
                solo.searchText("Press Right To Resume"));
        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                activity2.enterCode(command);
            }
        });

    }

}
