package il.ac.technion.cs.ssdl.cs234311.DontPanic.serverConnection;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.R;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.User;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.data.CodesDictionary;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class ServerMessages {

    static final String SERVER_URL = "http://yp7dontpanic.appspot.com/";

    /**
     * send to server new shortcut to save
     * 
     * @param shortcut
     */
    public static void sendShortcut(final Shortcut shortcut,
            final Context appContext) {
        final String ADD_SHORTCUT_SERVLET_NAME = "AddCode";
        final String SHORTCUT_PARAM_NAME = "code";

        final String[] packetFields = new String[3];

        packetFields[0] = ADD_SHORTCUT_SERVLET_NAME;
        packetFields[1] = SHORTCUT_PARAM_NAME;
        packetFields[2] = new Gson().toJson(shortcut);

        new HttpPostPackets(appContext,
                appContext.getString(R.string.code_added_to_server),
                appContext.getString(R.string.had_connection_problems))
                .execute(packetFields);
    }

    /**
     * send message to server to delete a given shortcut.
     * 
     * @param shortcut
     *            - the shortcut to be deleted.
     * @param user
     *            - the application user
     */
    public static void deleteShortcut(final Shortcut shortcut, final User user,
            final Context appContext) {
        final String DELETE_SHORTCUT_SERVLET_NAME = "DeleteCode";
        final String USER_PARAM_NAME = "uid";
        final String SHORTCUT_PARAM_NAME = "shortcut";

        final String[] packetFields = new String[5];

        packetFields[0] = DELETE_SHORTCUT_SERVLET_NAME;
        packetFields[1] = USER_PARAM_NAME;
        packetFields[2] = user.email;
        packetFields[3] = SHORTCUT_PARAM_NAME;
        packetFields[4] = new Gson().toJson(shortcut);

        new HttpPostPackets(appContext,
                appContext.getString(R.string.code_deleted_from_the_server),
                appContext.getString(R.string.had_connection_problems))
                .execute(packetFields);
    }

    /**
     * when starting the app on the first time, check to see if server already
     * has data.
     * 
     * @param user
     *            - the user using the app
     * @return - true if server has data, false if the user is unregistered.
     */
    public static User loginUser(final Context _appContext, final String email) {
        final String GET_USER_SERVLET_NAME = "GetUser";
        final String USER_PARAM_NAME = "uid";

        final String[] packetFields = new String[3];

        packetFields[0] = GET_USER_SERVLET_NAME;
        packetFields[1] = USER_PARAM_NAME;
        packetFields[2] = email;

        try {
            final String userGson = new HttpGetPackets(_appContext).execute(
                    packetFields).get();
            return new Gson().fromJson(userGson, User.class);
        } catch (final InterruptedException e) {
            e.printStackTrace();
        } catch (final ExecutionException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * returns the most updated shortcuts list from the server
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
    // because of the json conversion we know it's OK
    public static CodesDictionary getAllShortcuts(final Context _appContext,
            final User user) {
        final String GET_ALL_SHORTCUTS_SERVLET_NAME = "GetCodes";
        final String USER_PARAM_NAME = "uid";

        final CodesDictionary dic = new CodesDictionary();

        final String[] packetFields = new String[3];

        packetFields[0] = GET_ALL_SHORTCUTS_SERVLET_NAME;
        packetFields[1] = USER_PARAM_NAME;
        packetFields[2] = user.email;

        String res;
        try {
            res = new HttpGetPackets(_appContext).execute(packetFields).get();
            Log.i("ServerMessages", "got response = ");
            if (res != null && !res.isEmpty())
                Log.i("ServerMessages", res);
        } catch (final InterruptedException e) {
            return null;
        } catch (final ExecutionException e) {
            return null;
        }

        final Type collectionType = new TypeToken<List<Shortcut>>() {
            /*
             * the empty conversion class
             */
        }.getType();

        List<Shortcut> serverShortcuts;
        if (res != null)
            serverShortcuts = (List<Shortcut>) new Gson().fromJson(res,
                    collectionType);
        else
            serverShortcuts = new ArrayList<Shortcut>();

        Log.i("ServerMessages",
                "getAllShortcuts - list size"
                        + Integer.toString(serverShortcuts.size()));

        for (final Shortcut s : serverShortcuts)
            /*
             * the original code should be the next code.
             * the code won't work because that the current version
             * dosn't support complicated synchronization.
            if (!dic.addCode(s))
                return null;
                
             * for now just add any code that available to add.
            */
            dic.addCode(s);
        Log.i("ServerMessages", "getAllShortcuts - end");
        return dic;
    }

    /**
     * registers a newly created user on the server
     * 
     * @param user
     */
    public static void regUser(final Context appContext, final User user) {
        final String REG_USER_SERVLET_NAME = "RegUser";
        final String USER_PARAM_NAME = "user";

        final String[] packetFields = new String[3];

        packetFields[0] = REG_USER_SERVLET_NAME;
        packetFields[1] = USER_PARAM_NAME;
        packetFields[2] = new Gson().toJson(user);

        new HttpPostPackets(appContext,
                appContext.getString(R.string.user_registered_on_the_server),
                appContext.getString(R.string.had_connection_problems))
                .execute(packetFields);
    }

    /**
     * send a panic alert
     * 
     * @param user
     *            - the user sending the panic alert
     */
    public static void Panic(final Context _appContext, final User user) {
        final String REG_USER_SERVLET_NAME = "Panic";
        final String USER_PARAM_NAME = "uid";

        final String[] packetFields = new String[3];

        packetFields[0] = REG_USER_SERVLET_NAME;
        packetFields[1] = USER_PARAM_NAME;
        packetFields[2] = user.email;

        try {
            new HttpGetPackets(_appContext).execute(packetFields).get();
        } catch (final InterruptedException e) {
            e.printStackTrace();
        } catch (final ExecutionException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unused")
    // is deprecated
    @Deprecated
    private static class toastOnServerReturn extends
            AsyncTask<String, Void, String> {

        AsyncTask<String, Void, String> serverOperation;
        String message;
        Context appContext;

        public toastOnServerReturn(
                final AsyncTask<String, Void, String> _serverOperation,
                final String _message, final Context _appContext) {
            this.serverOperation = _serverOperation;
            this.message = _message;
            this.appContext = _appContext;
        }

        @Override
        protected String doInBackground(final String... strings) {
            try {
                serverOperation.get(5000, TimeUnit.MILLISECONDS);
            } catch (final InterruptedException e) {
                return "";
            } catch (final ExecutionException e) {
                e.printStackTrace();
                return "";
            } catch (final TimeoutException e) {
                e.printStackTrace();
                return "";
            }

            Toast.makeText(appContext, message, Toast.LENGTH_SHORT).show();
            return "";
        }

    }

}
