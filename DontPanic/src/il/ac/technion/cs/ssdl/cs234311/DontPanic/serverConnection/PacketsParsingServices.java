package il.ac.technion.cs.ssdl.cs234311.DontPanic.serverConnection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class PacketsParsingServices {

    /**
     * A parser for the client's parameters. Transfers the client's parameters
     * (parametersStrings) from an array of Strings, to one long string
     * (clientParameters) adjusted to the way the server expects to get it's
     * parameters.
     * 
     * @param parametersStrings
     *            - An array of strings, each representing a type of parameter
     *            of a parameter, sent by the client.
     * @return clientParameters - The client's parameters as one string, as
     *         expected by the server.
     */
    public static String parseClientParameters(final String[] parametersStrings) {
        // encoding the parameters
        String clientParameters = "";
        final int len = parametersStrings.length;

        for (int i = 1; i < len; i += 2) {
            clientParameters += parametersStrings[i] + "="
                    + parametersStrings[i + 1];
            // if it's not the last parameter add "&"
            clientParameters = len > i + 2 ? clientParameters + "&"
                    : clientParameters;
        }

        return clientParameters;
    }

    public static String convertResponseToString(final InputStream content) {
        final BufferedReader reader = new BufferedReader(new InputStreamReader(
                content));

        final StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null)
                sb.append(line + "\n");
        } catch (final IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }
}
