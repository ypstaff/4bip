package il.ac.technion.cs.ssdl.cs234311.DontPanic.phoneOperations;

import java.util.Locale;

public enum PhoneType {
    HOME, WORK, MOBILE;

    @Override
    public String toString() {
        return name().toString().toLowerCase(Locale.US);
    }
}
