/**
 * @date Jun 1, 2014 
 * @author Or
 * @email or81322@gmail.com
 */
package il.ac.technion.cs.ssdl.cs234311.DontPanic.activities;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.DontPanicActivity;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.R;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.ColorArrayLocations;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebSettings.RenderPriority;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;

/**
 *
 */
@SuppressLint("NewApi")
public class Game2048Activity extends DontPanicActivity {

    private Button redButton;
    private Button greenButton;
    private Button blueButton;
    private Button yellowButton;

    private WebView mWebView;

    public enum Directions {
        LEFT, UP, DOWN, RIGHT, OTHER;
    }

    private enum OPERATIONS {
        ARROW_UP, ARROW_DOWN, ARROW_LEFT, ARROW_RIGHT, BACK, SCROLL_UP, SCROLL_DOWN, NEW_GAME, OTHER_CODE
    }

    /**
     * matching input code to OPERATIONS constant
     * 
     * @param code
     *            - boolean array that represent the buttons pressed or the
     *            array given by the enterCode method
     * @return - the OPERATIONS constant that is matching the given input
     * @see DontPanicActivity.enterCode
     */
    private static OPERATIONS getOperationFromCode(final boolean[] code) {
        final boolean green_pressed = code[ColorArrayLocations.GREEN.ordinal()];
        final boolean yellow_pressed = code[ColorArrayLocations.YELLOW
                .ordinal()];
        final boolean red_pressed = code[ColorArrayLocations.RED.ordinal()];
        final boolean blue_pressed = code[ColorArrayLocations.BLUE.ordinal()];

        // enter pressed
        if (green_pressed && !(blue_pressed || red_pressed || yellow_pressed))
            return OPERATIONS.ARROW_RIGHT;

        // clear/back pressed
        if (red_pressed && !(blue_pressed || green_pressed || yellow_pressed))
            return OPERATIONS.ARROW_LEFT;

        // arrow up pressed
        if (blue_pressed && !(green_pressed || red_pressed || yellow_pressed))
            return OPERATIONS.ARROW_UP;

        // arrow down pressed
        if (yellow_pressed && !(blue_pressed || red_pressed || green_pressed))
            return OPERATIONS.ARROW_DOWN;

        // back pressed
        if (yellow_pressed && red_pressed && !(blue_pressed || green_pressed))
            return OPERATIONS.BACK;

        // new game pressed
        if (yellow_pressed && blue_pressed && !(red_pressed || green_pressed))
            return OPERATIONS.NEW_GAME;

        // else
        return OPERATIONS.OTHER_CODE;
    }

    @Override
    public void enterCode(final boolean[] code) {
        switch (getOperationFromCode(code)) {
        case ARROW_DOWN:
            arrowDown();
            break;
        case ARROW_UP:
            arrowUp();
            break;
        case ARROW_LEFT:
            arrowLeft();
            break;
        case ARROW_RIGHT:
            arrowRight();
            break;
        case BACK:
            back();
            break;
        case NEW_GAME:
            startNewGame();
            break;
        case OTHER_CODE:
            otherCode(code);
            break;
        default:
            break;
        }
    }

    private void back() {
        final Intent intent = new Intent(Game2048Activity.this,
                MainWindowActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.putExtra("class_name", "Game2048Activity");
        startActivity(intent);
        return;
    }

    /*
    private void scrollUp() {
        // mWebView.scrollBy(0, -100);

        ObjectAnimator anim = ObjectAnimator.ofInt(mWebView, "scrollY",
                mWebView.getScrollY(),
                Math.max(mWebView.getScrollY() - mWebView.getHeight() / 4, 0));
        anim.setDuration(350);
        anim.start();
    }
    */

    private void scrollDown() {
        // mWebView.scrollBy(0, 100);

        @SuppressWarnings("deprecation")
        ObjectAnimator anim = ObjectAnimator.ofInt(mWebView, "scrollY",
                mWebView.getScrollY(), Math.min(
                        mWebView.getScrollY() + mWebView.getHeight() / 4,
                        (int) Math.floor(mWebView.getContentHeight()
                                * mWebView.getScale())
                                - mWebView.getHeight()));
        anim.setDuration(350);
        anim.start();
    }

    private void otherCode(boolean[] code) {
        // TODO Auto-generated method stub

    }

    private void arrowRight() {
        swipe(Directions.RIGHT);
    }

    private void arrowLeft() {
        swipe(Directions.LEFT);
    }

    private void arrowUp() {
        swipe(Directions.UP);
    }

    private void arrowDown() {
        swipe(Directions.DOWN);
    }

    @SuppressLint("Recycle")
    private void swipe(Directions direction) {
        Point size = new Point();
        getWindowManager().getDefaultDisplay().getSize(size);
        int height = size.y;
        int width = size.x;

        long eventTime = SystemClock.uptimeMillis();
        long downTime = eventTime;

        float xStart = width / 2;
        float xEnd = width / 2;
        float yStart = height / 2;
        float yEnd = height / 2;

        if (direction == Directions.LEFT || direction == Directions.RIGHT) {
            xEnd = (float) ((direction == Directions.LEFT) ? xEnd * 0.5
                    : xEnd * 1.5);
        }
        if (direction == Directions.UP || direction == Directions.DOWN) {
            yEnd = (float) ((direction == Directions.UP) ? yEnd * 0.5
                    : yEnd * 1.5);
        }

        mWebView.onTouchEvent(MotionEvent.obtain(downTime, eventTime,
                MotionEvent.ACTION_DOWN, xStart, yStart, 0));
        mWebView.onTouchEvent(MotionEvent.obtain(downTime, eventTime,
                MotionEvent.ACTION_MOVE, xEnd, yEnd, 0));
        mWebView.onTouchEvent(MotionEvent.obtain(downTime, eventTime + 1000,
                MotionEvent.ACTION_UP, xEnd, yEnd, 0));
    }

    private void startNewGame() {
        Point size = new Point();
        getWindowManager().getDefaultDisplay().getSize(size);
        int height = size.y;
        int width = size.x;

        long eventTime = SystemClock.uptimeMillis();
        long downTime = eventTime;

        float x = width * 4 / 5;
        float y = 5;

        mWebView.onTouchEvent(MotionEvent.obtain(downTime, eventTime,
                MotionEvent.ACTION_DOWN, x, y, 0));
        mWebView.onTouchEvent(MotionEvent.obtain(downTime, eventTime + 500,
                MotionEvent.ACTION_UP, x, y, 0));
    }

    @Override
    protected void initViews() {
        super.initViews();

        redButton = (Button) findViewById(R.id.RedButton);
        greenButton = (Button) findViewById(R.id.GreenButton);
        blueButton = (Button) findViewById(R.id.BlueButton);
        yellowButton = (Button) findViewById(R.id.YellowButton);

        mWebView = (WebView) findViewById(R.id.mainWebView);

    }

    @Override
    protected void initTouchListeners() {
        super.initTouchListeners();

        yellowButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.YELLOW));
        blueButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.BLUE));
        greenButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.GREEN));
        redButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.RED));

    }

    @SuppressLint({ "SetJavaScriptEnabled" })
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // If on android 3.0+ activate hardware acceleration
        if (Build.VERSION.SDK_INT >= 11) {
            getWindow().setFlags(
                    WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                    WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
        }

        setContentView(R.layout.activity_2048);

        initViews();
        initTouchListeners();

        final Activity activity = this;

        // maybe not needed
        mWebView.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode,
                    String description, String failingUrl) {
                Toast.makeText(activity, "Oh no! " + description,
                        Toast.LENGTH_SHORT).show();
            }
        });

        redButton.setText(getString(R.string.left));
        greenButton.setText(getString(R.string.right));

        // Load webview with game
        WebSettings settings = mWebView.getSettings();
        String packageName = getPackageName();
        settings.setJavaScriptEnabled(true);
        settings.setDomStorageEnabled(true);
        settings.setDatabaseEnabled(true);
        settings.setRenderPriority(RenderPriority.HIGH);
        settings.setDatabasePath("/data/data/" + packageName + "/databases");

        // If there is a previous instance restore it in the webview
        if (savedInstanceState != null)
            mWebView.restoreState(savedInstanceState);
        else
            // mWebView.loadUrl("http://gabrielecirulli.github.io/2048/");
            mWebView.loadUrl("file:///android_asset/2048/index.html");

        ViewTreeObserver vto = mWebView.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mWebView.setScrollY(Math.min(
                        mWebView.getHeight() / 4,
                        (int) Math.floor(mWebView.getContentHeight()
                                * mWebView.getScale())
                                - mWebView.getHeight()));

                mWebView.getViewTreeObserver().removeOnGlobalLayoutListener(
                        this);
            }
        });
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        mWebView.setScrollY((int) (mWebView.getContentHeight()
                * mWebView.getScaleY() / 7));

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        mWebView.saveState(outState);
    }

}
