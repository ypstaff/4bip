package il.ac.technion.cs.ssdl.cs234311.DontPanic.MagazineOperations;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.activities.MagazinesCategoriesMenuActivityMiniApp;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.content.Intent;

public class MagazineServices {
    private static Map<String, List<Magazine>> categories_to_magazines;
    private static List<String> categories;

    public static Intent getRssReaderIntentWithMagazine(final Context context,
            final Magazine magazine) {
        Intent intent = new Intent(context, RssReaderActivity.class);
        intent.putExtra("XML_LINK", magazine.xmlLink);

        return intent;
    }

    public static Intent getMagazinesLibraryIntent(final Context context) {
        Intent intent = new Intent(context,
                MagazinesCategoriesMenuActivityMiniApp.class);

        return intent;
    }

    public static List<Magazine> getAllMagazinesForCategory(
            final String category) {
        if (categories_to_magazines == null)
            categories_to_magazines = new HashMap<String, List<Magazine>>();

        return categories_to_magazines.get(category);
    }

    public static void setAllMagazinesForCategory(final String category,
            final List<Magazine> magazines) {
        if (categories_to_magazines == null)
            categories_to_magazines = new HashMap<String, List<Magazine>>();

        MagazineServices.categories_to_magazines.put(category, magazines);
    }

    public static List<String> getAllCategories() {
        return categories;
    }

    public static void setAllCategories(final List<String> categories) {
        MagazineServices.categories = categories;
    }

}
