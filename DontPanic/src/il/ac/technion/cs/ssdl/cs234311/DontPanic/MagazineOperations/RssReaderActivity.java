package il.ac.technion.cs.ssdl.cs234311.DontPanic.MagazineOperations;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.DontPanicActivity;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.R;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.ColorArrayLocations;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.activities.LoginActivity;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.code.rome.android.repackaged.com.sun.syndication.feed.synd.SyndEntry;
import com.google.code.rome.android.repackaged.com.sun.syndication.io.FeedException;
import com.google.code.rome.android.repackaged.com.sun.syndication.io.SyndFeedInput;
import com.google.code.rome.android.repackaged.com.sun.syndication.io.XmlReader;

public class RssReaderActivity extends DontPanicActivity {
    private ListView listView;
    private ArrayList<RssItem> itemlist = null;
    private RSSListAdaptor rssadapter = null;
    private String xmlLink;

    private Button redButton;
    private Button greenButton;
    private Button blueButton;
    private Button yellowButton;

    private void updateListView() {
        listView.setAdapter(rssadapter);
        listView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        listView.setItemChecked(0, true);

        if (itemlist == null || itemlist.size() == 0)
            showToast(getString(R.string.magazine_no_rss_feed_found));
    }

    @Override
    protected void initViews() {
        super.initViews();

        redButton = (Button) findViewById(R.id.RedButton);
        greenButton = (Button) findViewById(R.id.GreenButton);
        blueButton = (Button) findViewById(R.id.BlueButton);
        yellowButton = (Button) findViewById(R.id.YellowButton);

        listView = (ListView) findViewById(R.id.rssList);
    }

    @Override
    protected void initTouchListeners() {
        super.initTouchListeners();

        yellowButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.YELLOW));
        blueButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.BLUE));
        greenButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.GREEN));
        redButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.RED));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rss_reader);

        initViews();
        initTouchListeners();

        itemlist = new ArrayList<RssItem>();

        xmlLink = getIntent().getStringExtra("XML_LINK");

        new RetrieveRSSFeeds().execute();
    }

    private void openArticleFromPosition(int position) {
        RssItem data = itemlist.get(position);

        Intent intent = new Intent(RssReaderActivity.this,
                MagazineViewActivity.class);
        intent.putExtra("URL_LINK", data.link);

        startActivity(intent);
    }

    private void retrieveRSSFeed(String urlToRssFeed, ArrayList<RssItem> list) {
        try {
            URL url = new URL(urlToRssFeed);
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();
            XMLReader xmlreader = parser.getXMLReader();
            RssParser theRssHandler = new RssParser(list);

            xmlreader.setContentHandler(theRssHandler);

            InputSource is = new InputSource(url.openStream());

            xmlreader.parse(is);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getRSS(String urlToRssFeed, ArrayList<RssItem> list) {
        try {
            List<?> entries = new SyndFeedInput().build(
                    new XmlReader(new URL(urlToRssFeed))).getEntries();

            Iterator<?> iterator = entries.listIterator();

            while (iterator.hasNext()) {
                SyndEntry ent = (SyndEntry) iterator.next();

                RssItem item = new RssItem();

                item.title = ent.getTitle();
                if (ent.getPublishedDate() != null)
                    item.date = ent.getPublishedDate().toString();
                item.link = ent.getLink();

                int MAX_LENGTH_DESCRIPTION = 300;
                if (ent.getDescription() != null) {
                    item.description = ent.getDescription().getValue()
                            .replaceAll("\\<[^>]*>", "");

                    item.description = item.description.substring(
                            0,
                            Math.min(MAX_LENGTH_DESCRIPTION,
                                    item.description.length()));
                }

                list.add(item);
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (FeedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private class RetrieveRSSFeeds extends AsyncTask<Void, Void, Void> {
        private ProgressDialog progress = null;

        @Override
        protected Void doInBackground(Void... params) {
            // retrieveRSSFeed(xmlLink, itemlist);
            getRSS(xmlLink, itemlist);

            rssadapter = new RSSListAdaptor(RssReaderActivity.this,
                    R.layout.rssitemview, itemlist);

            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @Override
        protected void onPreExecute() {
            progress = ProgressDialog.show(RssReaderActivity.this, null,
                    "Loading RSS Feeds...");

            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void result) {
            updateListView();

            progress.dismiss();

            super.onPostExecute(result);
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }

    private class RSSListAdaptor extends ArrayAdapter<RssItem> {
        private List<RssItem> objects = null;

        public RSSListAdaptor(Context context, int textviewid,
                List<RssItem> objects) {
            super(context, textviewid, objects);

            this.objects = objects;
        }

        @Override
        public int getCount() {
            return ((null != objects) ? objects.size() : 0);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public RssItem getItem(int position) {
            return ((null != objects) ? objects.get(position) : null);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;

            if (null == view) {
                LayoutInflater vi = (LayoutInflater) RssReaderActivity.this
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = vi.inflate(R.layout.rssitemview, null);
            }

            RssItem data = objects.get(position);

            if (null != data) {
                TextView title = (TextView) view.findViewById(R.id.txtTitle);
                TextView date = (TextView) view.findViewById(R.id.txtDate);
                TextView description = (TextView) view
                        .findViewById(R.id.txtDescription);

                title.setText(data.title);
                date.setText("on " + data.date);
                description.setText(data.description);
            }

            return view;
        }
    }

    /**
     * scroll up one item in the list view when the arrowDown is pressed
     */
    private void arrowUp() {
        final int index = listView.getCheckedItemPosition();

        if (index == 0) {
            listView.setItemChecked(listView.getCount() - 1, true);
            listView.smoothScrollToPositionFromTop(listView.getCount() - 1, 0,
                    0);
            return;
        }
        listView.setItemChecked(index - 1, true);
        listView.smoothScrollToPositionFromTop(index - 1,
                listView.getHeight() / 2);
    }

    /**
     * scroll down one item in the list view when the arrowDown is pressed
     */
    private void arrowDown() {
        final int index = listView.getCheckedItemPosition();

        if (index + 1 == listView.getCount()) {
            listView.setItemChecked(0, true);
            listView.smoothScrollToPositionFromTop(0, 0, 0);
            return;
        }
        listView.setItemChecked(index + 1, true);
        listView.smoothScrollToPositionFromTop(index + 1,
                listView.getHeight() / 2);
    }

    /**
     * all the operations supported in this activity
     */
    private enum Operation {
        ENTER, BACK, ARROW_UP, ARROW_DOWN, PANIC, OTHER_CODE
    }

    /**
     * matching input code to OPERATIONS constant
     * 
     * @param code
     *            - boolean array that represents the buttons pressed or the
     *            array given by the enterCode method
     * @return - the Operation constant that is matching the given input
     * @see DontPanicActivity.enterCode
     */
    private Operation getOperationFromCode(final boolean[] code) {
        final boolean green_pressed = code[ColorArrayLocations.GREEN.ordinal()];
        final boolean yellow_pressed = code[ColorArrayLocations.YELLOW
                .ordinal()];
        final boolean red_pressed = code[ColorArrayLocations.RED.ordinal()];
        final boolean blue_pressed = code[ColorArrayLocations.BLUE.ordinal()];

        // enter pressed
        if (green_pressed && !(blue_pressed || red_pressed || yellow_pressed))
            return Operation.ENTER;

        // clear/back pressed
        if (red_pressed && !(blue_pressed || green_pressed || yellow_pressed))
            return Operation.BACK;

        // arrow up pressed
        if (blue_pressed && !(green_pressed || red_pressed || yellow_pressed))
            return Operation.ARROW_UP;

        // arrow down pressed
        if (yellow_pressed && !(blue_pressed || red_pressed || green_pressed))
            return Operation.ARROW_DOWN;

        if (red_pressed && blue_pressed && green_pressed && yellow_pressed)
            return Operation.PANIC;

        return Operation.OTHER_CODE;
    }

    @Override
    public void enterCode(final boolean[] code) {

        switch (getOperationFromCode(code)) {
        case OTHER_CODE:
            return;

        case BACK:
            finish();
            break;

        case ENTER:
            openNextWindowWithSelection();
            break;

        case ARROW_UP:
            arrowUp();
            break;

        case ARROW_DOWN:
            arrowDown();
            break;

        case PANIC:
            enterPanic(LoginActivity.dataManager);
            break;
        }
    }

    protected void openNextWindowWithSelection() {
        if (rssadapter.getCount() == 0)
            return;

        openArticleFromPosition(listView.getCheckedItemPosition());
    }

    private void showToast(final String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

}