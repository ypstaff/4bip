package il.ac.technion.cs.ssdl.cs234311.DontPanic.MagazineOperations;

public class Magazine {
    public final String xmlLink;
    public final String name;

    public Magazine() {
        xmlLink = "";
        name = "";
    }

    public Magazine(final String xmlLink, final String name) {
        this.xmlLink = xmlLink;
        this.name = name;
    }
}
