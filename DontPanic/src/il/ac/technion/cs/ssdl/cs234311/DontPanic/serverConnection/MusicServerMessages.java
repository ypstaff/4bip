package il.ac.technion.cs.ssdl.cs234311.DontPanic.serverConnection;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.activities.LoginActivity;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.statistics.MusicData;

import java.util.Date;

import android.content.Context;

import com.google.gson.Gson;

public class MusicServerMessages {

    static final String SERVER_URL = "http://yp7dontpanic.appspot.com/";

    public static void sendScore(String songName,String artist,String album,
            Context context) {
        

        final String[] packetFields = new String[5];
        String[] musicInfo = new String[3];
        

         
        musicInfo[0] = songName;
        musicInfo[1] = artist;
        musicInfo[2] = album;
        Date date = new Date();

        MusicData musicData = new MusicData(LoginActivity.dataManager.getUser().email, date, musicInfo);

        packetFields[0] = "UploadStatData";
        packetFields[1] = "type";
        packetFields[2] = "music";
        packetFields[3] = "info";
        packetFields[4] = new Gson().toJson(musicData);

        new HttpPostPackets(context)
                .execute(packetFields);

    }

}
