/*
 * created by: 
 * michael leybovich
 * mishana4life@gmail.com
 * 
 * date: 9.11.13 
 */
package il.ac.technion.cs.ssdl.cs234311.DontPanic.phoneOperations;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.R;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ContactAdapter extends ArrayAdapter<Contact> {

    private final Activity activity;
    private final List<Contact> items;
    private final int row;
    private Contact contact;

    public ContactAdapter(final Activity act, final int row,
            final List<Contact> listItems) {
        super(act, row, listItems);

        this.activity = act;
        this.row = row;
        this.items = listItems;
    }

    @Override
    public View getView(final int position, final View convertView,
            final ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;

        if (view != null)
            holder = (ViewHolder) view.getTag();
        else {
            final LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            view = inflater.inflate(row, null);
            holder = new ViewHolder();
            holder.tvname = (TextView) view.findViewById(R.id.tvname);
            holder.tvPhoneNo = (TextView) view.findViewById(R.id.tvphone);
            view.setTag(holder);
        }

        if (items == null || position + 1 > items.size())
            return view;

        contact = items.get(position);

        if (holder.tvname != null && null != contact.name
                && contact.name.trim().length() > 0)
            holder.tvname.setText(Html.fromHtml(contact.name));

        if (holder.tvPhoneNo != null && null != contact.phoneNo
                && contact.phoneNo.trim().length() > 0)
            holder.tvPhoneNo.setText(Html.fromHtml(contact.type + " - "
                    + contact.phoneNo));

        return view;
    }

    public class ViewHolder {
        public TextView tvname, tvPhoneNo;
    }

}
