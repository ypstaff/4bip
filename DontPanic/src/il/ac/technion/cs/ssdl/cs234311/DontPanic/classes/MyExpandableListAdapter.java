/**
 * @date Jan 11, 2014 
 * @author Or
 * @email or81322@gmail.com
 */
package il.ac.technion.cs.ssdl.cs234311.DontPanic.classes;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.R;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut.NoSuchOpException;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.IllegalShortcutLetter;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.ShortcutLetter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 *
 */
public class MyExpandableListAdapter extends BaseExpandableListAdapter {

    private final Context _context;
    public List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    public HashMap<String, List<Shortcut>> _listDataChild;
    public List<boolean[]> currentWord = new ArrayList<boolean[]>();

    public MyExpandableListAdapter(final Context context,
            final List<String> listDataHeader,
            final HashMap<String, List<Shortcut>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

    /* (non-Javadoc)
     * @see android.widget.ExpandableListAdapter#getChild(int, int)
     */
    @Override
    public Object getChild(final int groupPosition, final int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(final int groupPosition, final int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition,
            final boolean isLastChild, View convertView, final ViewGroup parent) {
        if (convertView == null) {
            final LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item_row, null);
        }

        final Shortcut child = (Shortcut) getChild(groupPosition, childPosition);

        final TextView txtListChild = (TextView) convertView
                .findViewById(R.id.listItemText);

        if (child == null) {
            // TODO
            final LinearLayout visualDescription = (LinearLayout) convertView
                    .findViewById(R.id.listItemShortcutDescription);
            visualDescription.removeAllViews();
            return convertView;
        }

        final LinearLayout visualDescription = (LinearLayout) convertView
                .findViewById(R.id.listItemShortcutDescription);
        visualDescription.removeAllViews();
        try {
            txtListChild.setText(child.getDisplayName(_context));
        } catch (final NoSuchOpException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            txtListChild.setText("no such op");
        }

        if (!(child instanceof ActiveWindowShortcut)) { // Shortcut
            final List<ShortcutLetter> list = child.letteredCode;

            int i = 0, j = 0, k = 0;
            for (final Iterator<ShortcutLetter> iterator = list.iterator(); iterator
                    .hasNext() && i < 4; j++) {// 4
                final ShortcutLetter shortcutLetter = iterator.next();
                if (j < currentWord.size()) {
                    if (k < 1) { // 1
                        k++;// the number of letters in the suggestion to show
                        final LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(
                                LayoutParams.WRAP_CONTENT,
                                LayoutParams.MATCH_PARENT);
                        lParams.gravity = Gravity.CENTER;

                        final ImageView imgNew = new ImageView(this._context);
                        imgNew.setPadding(0, 4, 4, 0);
                        imgNew.setImageResource(R.drawable.ic_action_forward);
                        visualDescription.addView(imgNew, lParams);
                    }
                    ShortcutLetter currentShortcutLetter;
                    try {
                        currentShortcutLetter = ShortcutTranslator
                                .array2letter(currentWord.get(j));
                    } catch (final IllegalShortcutLetter e) {
                        // TODO Auto-generated catch block
                        // Shouldn't get here
                        e.printStackTrace();
                        return convertView;
                    }
                    if (shortcutLetter.equals(currentShortcutLetter))
                        continue;
                }

                int leftColor, rightColor;
                switch (shortcutLetter) {// red -> yellow -> blue -> green
                case ONE_TWO:
                    leftColor = _context.getResources().getColor(
                            R.color.holo_red_light);
                    rightColor = _context.getResources().getColor(
                            R.color.holo_yellow_light);
                    break;
                case TWO_THREE:
                    leftColor = _context.getResources().getColor(
                            R.color.holo_yellow_light);
                    rightColor = _context.getResources().getColor(
                            R.color.holo_blue_light);
                    break;
                case THREE_FOUR:
                    leftColor = _context.getResources().getColor(
                            R.color.holo_blue_light);
                    rightColor = _context.getResources().getColor(
                            R.color.holo_green_light);
                    break;
                case FOUR_ONE:
                    leftColor = _context.getResources().getColor(
                            R.color.holo_red_light);
                    rightColor = _context.getResources().getColor(
                            R.color.holo_green_light);
                    break;
                default:
                    continue;
                }

                final LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(
                        LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
                lParams.gravity = Gravity.CENTER;

                final LayerDrawable background = (LayerDrawable) this._context
                        .getResources().getDrawable(
                                R.drawable.two_colors_round_background);
                final GradientDrawable leftDrawable = (GradientDrawable) background
                        .findDrawableByLayerId(R.id.leftColor);
                final GradientDrawable rightDrawable = (GradientDrawable) background
                        .findDrawableByLayerId(R.id.rightColor);
                leftDrawable.setColor(leftColor);
                rightDrawable.setColor(rightColor);

                final ImageView imgNew = new ImageView(this._context);
                imgNew.setPadding(0, 4, 4, 0);
                imgNew.setImageDrawable(background);
                visualDescription.addView(imgNew, lParams);

                i++; // the number of letters in the suggestion to show
            }
        }

        return convertView;
    }

    @Override
    public int getChildrenCount(final int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(final int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(final int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(final int groupPosition, final boolean isExpanded,
            View convertView, final ViewGroup parent) {
        if (convertView == null) {
            final LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group_row, null);
        }

        final TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.listHeaderText);
        final TextView lblListHeaderNumberOfChilds = (TextView) convertView
                .findViewById(R.id.listHeaderNumberOfChilds);
        

        final String headerTitle = (String) getGroup(groupPosition);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);
        
        if (headerTitle.equals(_context.getString(R.string.mini_apps))) {
            lblListHeaderNumberOfChilds.setVisibility(View.INVISIBLE);
            return convertView;
        }

        final String headerNumberOfChilds = String
                .valueOf(getChildrenCount(groupPosition));
        lblListHeaderNumberOfChilds.setTypeface(null, Typeface.BOLD);
        lblListHeaderNumberOfChilds.setText(headerNumberOfChilds);

        return convertView;
    }

    /* (non-Javadoc)
     * @see android.widget.ExpandableListAdapter#hasStableIds()
     */
    @Override
    public boolean hasStableIds() {
        // TODO Auto-generated method stub
        return false;
    }

    /* (non-Javadoc)
     * @see android.widget.ExpandableListAdapter#isChildSelectable(int, int)
     */
    @Override
    public boolean isChildSelectable(final int arg0, final int arg1) {
        // TODO Auto-generated method stub
        return true;
    }
}
