/**
 * @date Jan 18, 2014 
 * @author Or
 * @email or81322@gmail.com
 */
package il.ac.technion.cs.ssdl.cs234311.DontPanic.classes;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.R;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut;

import java.util.ArrayList;

import android.content.Context;

/**
 *
 */
public class ActiveWindowShortcut extends Shortcut {

    public ActiveWindowShortcut(final CodeOperation op,
            final String propertyDisplayName) {
        this.op = op;
        properties = new ArrayList<String>();
        properties.add(propertyDisplayName);// PROPERTY_DISP_INDEX
    }

    @Override
    public boolean equals(final Object obj) {
        final ActiveWindowShortcut c = (ActiveWindowShortcut) obj;
        boolean $ = true;

        $ &= op == c.op;
        //$ &= properties.equals(c.properties);

        return $;
    }

    @Override
    public String getDisplayName(final Context appcontext)
            throws NoSuchOpException {
        String type = "";
        switch (op) {
        case PHONE_CALL:
            type = appcontext.getString(R.string.shortcut_phone_call_type);
            break;
        case MUSIC_PLAYLIST:
        case MUSIC_ARTIST:
            type = appcontext.getString(R.string.shortcut_music_action);
            break;
        case GAME_2048:
        case GAME_PLAY:
            type = appcontext.getString(R.string.shortcut_game_action);
            break;
        case DEFAULT_OPERTAIONS:
        default:
            type = "no such op";
        }

        return type + " " + properties.get(PROPERTY_DISP_INDEX);
    }
}
