package il.ac.technion.cs.ssdl.cs234311.DontPanic.phoneOperations;

/**
 * @author michael leybovich
 * @email mishana4life@gmail.com
 * @date 7.11.13
 * 
 */
public class Contact {
    public final String name;
    public final String phoneNo;
    public final String type;
    public final String id;

    public Contact() {
        this.name = "";
        this.phoneNo = "";
        this.type = "";
        this.id = "";
    }

    public Contact(final String name, final String phoneNo, final String type,
            final String id) {
        this.name = name;
        this.phoneNo = phoneNo;
        this.type = type;
        this.id = id;
    }
}
