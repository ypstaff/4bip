/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package il.ac.technion.cs.ssdl.cs234311.DontPanic.snake;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.R;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.activities.Snake;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.serverConnection.GameServerMessages;
/**
 * SnakeView: implementation of a simple game of Snake
 */
public class SnakeView extends TileView {

    private static final String TAG = "SnakeView";

    /**
     * Current mode of application: READY to run, RUNNING, or you have already
     * lost. static final ints are used instead of an enum for performance
     * reasons.
     */
    public int mMode = READY;
    public static final int PAUSE = 0;
    public static final int READY = 1;
    public static final int RUNNING = 2;
    public static final int LOSE = 3;

    /**
     * Current direction the snake is headed.
     */
    private int mDirection = NORTH;
    private int mNextDirection = NORTH;
    private static final int NORTH = 1;
    private static final int SOUTH = 2;
    private static final int EAST = 3;
    private static final int WEST = 4;

    /**
     * Labels for the drawables that will be loaded into the TileView class
     */
    private static final int EMPTY_CELL = 0;
    private static final int RED_STAR = 1;
    private static final int YELLOW_STAR = 2;
    private static final int GREEN_STAR = 3;

    /**
     * mScore: Used to track the number of apples captured mMoveDelay: number of
     * milliseconds between snake movements. if we choose accelerate game option
     * this number will decrease as apples are captured.
     */
    public static final int SLOW = 1;
    public static final int NORMAL = 2;
    public static final int FAST = 3;
    public static final int FASTEST = 4;

    public int gameSpeed = NORMAL;

    public static final int YES = 1;
    public static final int NO = 2;

    public int accelerateGame = YES;

    public static final int EASY = 1;
    public static final int MEDIUM = 2;
    public static final int HARD = 3;
    public static final int HARDEST = 4;
    public static final int PANIC = 5;

    private long mScore = 0;
    private long highScore = 0; //GameServerMessages.getHighScore(getContext()); 
    private long mMoveDelay = 500;
    private float accelerateRate = 0.9f;

    public TextView scoreTextView;
    private Button rightButton;
    /**
     * mLastMove: Tracks the absolute time when the snake last moved, and is
     * used to determine if a move should be made based on mMoveDelay.
     */
    private long mLastMove;

    /**
     * mStatusText: Text shows to the user in some run states
     */
    private TextView mStatusText;

    /**
     * mArrowsView: View which shows 4 arrows to signify 4 directions in which
     * the snake can move
     */
    private View mArrowsView;

    /**
     * mBackgroundView: Background View which shows 4 different colored
     * triangles pressing which moves the snake
     */
    private View mBackgroundView;

    /**
     * mSnakeTrail: A list of Coordinates that make up the snake's body
     * mAppleList: The secret location of the juicy apples the snake craves.
     */
    private ArrayList<Coordinate> mSnakeTrail = new ArrayList<Coordinate>();
    private ArrayList<Coordinate> mAppleList = new ArrayList<Coordinate>();

    /**
     * Everyone needs a little randomness in their life
     */
    private static final Random RNG = new Random();

    private static final long MAX_SPEED = 50;

    /**
     * Create a simple handler that we can use to cause animation to happen. We
     * set ourselves as a target and we can use the sleep() function to cause an
     * update/invalidate to occur at a later date.
     */

    private final RefreshHandler mRedrawHandler = new RefreshHandler();

    private Date startGameDate;

    @SuppressLint("HandlerLeak")
    class RefreshHandler extends Handler {

        @Override
        public void handleMessage(final Message msg) {
            SnakeView.this.update();
            SnakeView.this.invalidate();
        }

        public void sleep(final long delayMillis) {
            this.removeMessages(0);
            sendMessageDelayed(obtainMessage(0), delayMillis);
        }
    };

    /**
     * Constructs a SnakeView based on inflation from XML
     * 
     * @param context
     * @param attrs
     */
    public SnakeView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        initSnakeView(context);
    }

    public SnakeView(final Context context, final AttributeSet attrs,
            final int defStyle) {
        super(context, attrs, defStyle);
        initSnakeView(context);
    }

    /**
     * init the game colors and load the the tiles accordingly
     * 
     * @param context
     */
    private void initSnakeView(final Context context) {

        setFocusable(true);

        final Resources r = this.getContext().getResources();

        resetTiles(4);
        loadTile(RED_STAR, r.getDrawable(R.drawable.redstar));
        loadTile(YELLOW_STAR, r.getDrawable(R.drawable.yellowstar));
        loadTile(GREEN_STAR, r.getDrawable(R.drawable.greenstar));

    }

    /**
     * initNewGame: init the snake view location and the two apples and set the
     * game configuration accordingly
     */
    private void initNewGame() {
        mSnakeTrail.clear();
        mAppleList.clear();

        // For now we're just going to load up a short default eastbound snake
        // that's just turned north

        mSnakeTrail.add(new Coordinate(11, 6));
        mSnakeTrail.add(new Coordinate(10, 6));
        mSnakeTrail.add(new Coordinate(9, 6));
        mSnakeTrail.add(new Coordinate(8, 6));
        mSnakeTrail.add(new Coordinate(7, 6));
        mSnakeTrail.add(new Coordinate(6, 6));
        mNextDirection = EAST;

        // Two apples to start with
        addRandomApple();
        addRandomApple();

        switch (gameSpeed) {
        case SLOW:
            mMoveDelay = 600;
            break;
        case NORMAL:
            mMoveDelay = 450;
            break;
        case FAST:
            mMoveDelay = 250;
            break;
        case FASTEST:
            mMoveDelay = 130;
            break;
        default:
            assert false; // illegal game speed
        }

        if (accelerateGame == YES)
            accelerateRate = 0.9f;
        else
            accelerateRate = 1;
        mScore = 0;
    }

    /**
     * Given a ArrayList of coordinates, we need to flatten them into an array
     * of ints before we can stuff them into a map for flattening and storage.
     * 
     * @param cvec
     *            : a ArrayList of Coordinate objects
     * @return : a simple array containing the x/y values of the coordinates as
     *         [x1,y1,x2,y2,x3,y3...]
     */
    private int[] coordArrayListToArray(final ArrayList<Coordinate> cvec) {
        final int[] rawArray = new int[cvec.size() * 2];

        int i = 0;
        for (final Coordinate c : cvec) {
            rawArray[i++] = c.x;
            rawArray[i++] = c.y;
        }

        return rawArray;
    }

    /**
     * Save game state so that the user does not lose anything if the game
     * process is killed while we are in the background.
     * 
     * @return a Bundle with this view's state
     */
    public Bundle saveState() {
        final Bundle map = new Bundle();

        map.putIntArray("mAppleList", coordArrayListToArray(mAppleList));
        map.putInt("mDirection", Integer.valueOf(mDirection));
        map.putInt("mNextDirection", Integer.valueOf(mNextDirection));
        map.putLong("mMoveDelay", Long.valueOf(mMoveDelay));
        map.putLong("mScore", Long.valueOf(mScore));
        map.putIntArray("mSnakeTrail", coordArrayListToArray(mSnakeTrail));

        return map;
    }

    /**
     * Given a flattened array of ordinate pairs, we reconstitute them into a
     * ArrayList of Coordinate objects
     * 
     * @param rawArray
     *            : [x1,y1,x2,y2,...]
     * @return a ArrayList of Coordinates
     */
    private ArrayList<Coordinate> coordArrayToArrayList(final int[] rawArray) {
        final ArrayList<Coordinate> coordArrayList = new ArrayList<Coordinate>();

        final int coordCount = rawArray.length;
        for (int index = 0; index < coordCount; index += 2) {
            final Coordinate c = new Coordinate(rawArray[index],
                    rawArray[index + 1]);
            coordArrayList.add(c);
        }
        return coordArrayList;
    }

    /**
     * Restore game state if our process is being relaunched
     * 
     * @param icicle
     *            a Bundle containing the game state
     */
    public void restoreState(final Bundle icicle) {
        setMode(PAUSE);

        mAppleList = coordArrayToArrayList(icicle.getIntArray("mAppleList"));
        mDirection = icicle.getInt("mDirection");
        mNextDirection = icicle.getInt("mNextDirection");
        mMoveDelay = icicle.getLong("mMoveDelay");
        mScore = icicle.getLong("mScore");
        mSnakeTrail = coordArrayToArrayList(icicle.getIntArray("mSnakeTrail"));
    }

    /**
     * Handles snake movement triggers from Snake Activity and moves the snake
     * accordingly. Ignore events that would cause the snake to immediately turn
     * back on itself.
     * 
     * @param direction
     *            The desired direction of movement
     */
    public void moveSnake(final int direction) {

        if (direction == Snake.MOVE_UP) {
            if (mDirection != SOUTH)
                mNextDirection = NORTH;
            return;
        }

        if (direction == Snake.MOVE_DOWN) {
            if (mDirection != NORTH)
                mNextDirection = SOUTH;
            return;
        }

        if (direction == Snake.MOVE_LEFT) {
            if (mDirection != EAST)
                mNextDirection = WEST;
            return;
        }

        if (direction == Snake.MOVE_RIGHT) {
            if (mMode == READY | mMode == LOSE) {
                /*
                 * At the beginning of the game, or the end of a previous one,
                 * we should start a new game if UP key is clicked.
                 */
                final String scoreShowString = getResources().getString(
                        R.string.snake_score_show_text);
                scoreTextView.setText(scoreShowString + " " + "0");
                initNewGame();
                setMode(RUNNING);
                update();
                startGameDate  = new Date();
                return;
            }

            if (mMode == PAUSE) {
                /*
                 * If the game is merely paused, we should just continue where we left off.
                 */
                updateTime();
                setMode(RUNNING);
                update();
                return;
            }

            if (mDirection != WEST)
                mNextDirection = EAST;
            return;
        }

    }

    /**
     * updateTime: reset the delay timer.
     */
    public void updateTime() {
        if (mMode == PAUSE) {
            // after PAUSE we need to let the user mMoveDelay time to the next
            // snake move.
            // therefore need call updateTime() and then change the mode to
            // RUNNING.
            final long now = System.currentTimeMillis();
            mLastMove = now;
        }
    }

    /**
     * Sets the Dependent views that will be used to give information (such as
     * "Game Over" to the user and also to handle touch events for making
     * movements
     * 
     * @param newView
     */
    public void setDependentViews(final TextView msgView, final View arrowView,
            final View backgroundView) {
        mStatusText = msgView;
        mArrowsView = arrowView;
        mBackgroundView = backgroundView;
    }

    /**
     * Updates the current mode of the application (RUNNING or PAUSED or the
     * like) as well as sets the visibility of textview for notification
     * 
     * @param newMode
     */
    public void setMode(final int newMode) {
        final int oldMode = mMode;
        mMode = newMode;

        if (newMode == RUNNING && oldMode != RUNNING) {
            // hide the game instructions
            rightButton.setText("");
            mStatusText.setVisibility(View.INVISIBLE);
            update();
            // make the background and arrows visible as soon the snake starts
            // moving
            mArrowsView.setVisibility(View.VISIBLE);
            mBackgroundView.setVisibility(View.VISIBLE);
            return;
        }

        final Resources res = getContext().getResources();
        CharSequence str = "";
        CharSequence highScoreStr = "";
        if (newMode == PAUSE) {
            mArrowsView.setVisibility(View.GONE);
            mBackgroundView.setVisibility(View.GONE);
            str = res.getText(R.string.mode_pause);
        }
        if (newMode == READY) {
            mArrowsView.setVisibility(View.GONE);
            mBackgroundView.setVisibility(View.GONE);

            str = res.getText(R.string.mode_ready);
        }
        if (newMode == LOSE) {
            final String startString = getResources().getString(
                    R.string.snake_start);
            rightButton.setText(startString);
            mArrowsView.setVisibility(View.GONE);
            mBackgroundView.setVisibility(View.GONE);
            str = res.getString(R.string.mode_lose, mScore);
            highScoreStr = res.getString(R.string.high_score);
            Date currentDate = new Date(); 
            GameServerMessages.sendScore(mScore, level, currentDate.getTime() - startGameDate.getTime() , getContext());
        }
        String endGameString = null;
        endGameString = str.toString();
        if (mScore > highScore) {
            endGameString = highScoreStr.toString().concat(endGameString);
            highScore = mScore;
        }
        mStatusText.setText(endGameString);
        mStatusText.setVisibility(View.VISIBLE);
    }

    /**
     * @return the Game state as Running, Ready, Paused, Lose
     */
    public int getGameState() {
        return mMode;
    }

    /**
     * Selects a random location within the garden that is not currently covered
     * by the snake. Currently _could_ go into an infinite loop if the snake
     * currently fills the garden, but we'll leave discovery of this prize to a
     * truly excellent snake-player.
     */
    private void addRandomApple() {
        Coordinate newCoord = null;
        boolean found = false;
        while (!found) {
            // Choose a new location for our apple
            final int newX = 1 + RNG.nextInt(mXTileCount - 2);
            final int newY = 1 + RNG.nextInt(mYTileCount - 2);
            newCoord = new Coordinate(newX, newY);

            // Make sure it's not already under the snake
            boolean collision = false;
            final int snakelength = mSnakeTrail.size();
            for (int index = 0; index < snakelength; index++)
                if (mSnakeTrail.get(index).equals(newCoord))
                    collision = true;
            // Make sure it's not already under the wall
            if (isWallCordinate(newX, newY))
                collision = true;
            if (level == PANIC) {
                // we don't need to put the apple in impossible cell to eat it
                // (inside the letter)
                final int PLocX = mXTileCount / 6;
                final int PLocY = mYTileCount / 6;
                final int ALocX = mXTileCount / 3;
                final int ALocY = mYTileCount / 3;
                final int NLocX = mXTileCount / 2;
                final int NLocY = mYTileCount / 2;

                if (newX == PLocX && newY == PLocY - 1 || newX == PLocX
                        && newY == PLocY || newX == ALocX && newY == ALocY - 1
                        || newX == NLocX && newY == NLocY || newX == NLocX - 1
                        && newY == NLocY + 1)
                    collision = true;
            }

            // if we're here and there's been no collision, then we have
            // a good location for an apple. Otherwise, we'll circle back
            // and try again
            found = !collision;
        }
        if (newCoord == null)
            Log.e(TAG, "Somehow ended up with a null newCoord!");
        mAppleList.add(newCoord);
    }

    /**
     * Handles the basic update loop, checking to see if we are in the running
     * state, determining if a move should be made, updating the snake's
     * location.
     */
    public void update() {
        if (mMode == RUNNING) {
            final long now = System.currentTimeMillis();

            if (now - mLastMove > mMoveDelay) {
                updateGrid();
                updateWalls();
                updateSnake();
                updateApples();
                mLastMove = now;
            }
            mRedrawHandler.sleep(mMoveDelay);
        }

    }

    /*
        public void updateAll() {
            if (mMode == RUNNING) {
                long now = System.currentTimeMillis();

                if (now - mLastMove > mMoveDelay) {
                    updateGrid();       // in update() delete this
                    updateWalls();      // in update() delete this
                    updateSnake();
                    updateApples();
                    mLastMove = now;
                }
                mRedrawHandler.sleep(mMoveDelay);
            }
        }
    */
    /**
     * Draws some walls.
     */
    private void updateWalls() {
        for (int x = 0; x < mXTileCount; x++) {
            setTile(GREEN_STAR, x, 0);
            setTile(GREEN_STAR, x, mYTileCount - 1);
        }
        for (int y = 1; y < mYTileCount - 1; y++) {
            setTile(GREEN_STAR, 0, y);
            setTile(GREEN_STAR, mXTileCount - 1, y);
        }
    }

    /**
     * Draws the play grid according to choosen level
     */
    private void updateGrid() {
        for (int x = 0; x < mXTileCount; x++)
            for (int y = 0; y < mYTileCount; y++)
                if (isWallCordinate(x, y))
                    setTile(GREEN_STAR, x, y);
                else
                    setTile(EMPTY_CELL, x, y);

    }

    /**
     * Draws some apples.
     */
    private void updateApples() {
        for (final Coordinate c : mAppleList)
            setTile(YELLOW_STAR, c.x, c.y);
    }

    /**
     * Figure out which way the snake is going, see if he's run into anything
     * (the walls, himself, or an apple). If he's not going to die, we then add
     * to the front and subtract from the rear in order to simulate motion. If
     * we want to grow him, we don't subtract from the rear.
     */
    private void updateSnake() {
        boolean growSnake = false;

        // Grab the snake by the head
        final Coordinate head = mSnakeTrail.get(0);
        Coordinate newHead = new Coordinate(1, 1);

        mDirection = mNextDirection;

        switch (mDirection) {
        case EAST: {
            newHead = new Coordinate(head.x + 1, head.y);
            break;
        }
        case WEST: {
            newHead = new Coordinate(head.x - 1, head.y);
            break;
        }
        case NORTH: {
            newHead = new Coordinate(head.x, head.y - 1);
            break;
        }
        case SOUTH: {
            newHead = new Coordinate(head.x, head.y + 1);
            break;
        }
        }

        // Collision detection
        // For now we have a 1-square wall around the entire arena
        if (newHead.x < 1 || newHead.y < 1 || newHead.x > mXTileCount - 2
                || newHead.y > mYTileCount - 2
                || isWallCordinate(newHead.x, newHead.y)) {
            setMode(LOSE);
            return;

        }

        // Look for collisions with itself
        final int snakelength = mSnakeTrail.size();
        for (int snakeindex = 0; snakeindex < snakelength; snakeindex++) {
            final Coordinate c = mSnakeTrail.get(snakeindex);
            if (c.equals(newHead)) {
                setMode(LOSE);
                return;
            }
        }

        // Look for apples
        final int applecount = mAppleList.size();
        for (int appleindex = 0; appleindex < applecount; appleindex++) {
            final Coordinate c = mAppleList.get(appleindex);
            if (c.equals(newHead)) {
                mAppleList.remove(c);
                addRandomApple();

                mScore++;

                final String scoreString = Long.toString(mScore);
                final String scoreShowString = getResources().getString(
                        R.string.snake_score_show_text);

                scoreTextView.setText(scoreShowString + " " + scoreString);
                mMoveDelay *= accelerateRate;
                if (mMoveDelay < MAX_SPEED)
                    mMoveDelay = MAX_SPEED;
                growSnake = true;
            }
        }

        // push a new head onto the ArrayList and pull off the tail
        mSnakeTrail.add(0, newHead);
        // except if we want the snake to grow
        if (!growSnake) {
            final Coordinate snakeTail = mSnakeTrail
                    .get(mSnakeTrail.size() - 1);
            setTile(EMPTY_CELL, snakeTail.x, snakeTail.y);
            mSnakeTrail.remove(mSnakeTrail.size() - 1);

        }

        int index = 0;
        for (final Coordinate c : mSnakeTrail) {
            if (index == 0)
                setTile(YELLOW_STAR, c.x, c.y);
            else
                setTile(RED_STAR, c.x, c.y);
            index++;
        }

    }

    /**
     * Simple class containing two integer values and a comparison function.
     * There's probably something I should use instead, but this was quick and
     * easy to build.
     */
    private class Coordinate {
        public int x;
        public int y;

        public Coordinate(final int newX, final int newY) {
            x = newX;
            y = newY;
        }

        public boolean equals(final Coordinate other) {
            if (x == other.x && y == other.y)
                return true;
            return false;
        }

        @Override
        public String toString() {
            return "Coordinate: [" + x + "," + y + "]";
        }
    }

    /**
     * setGameConfiguration: sets choosen configuration for the game.
     * 
     * @param _gameSpeed
     *            : snake speed.
     * @param _accelerateGame
     *            : accelerate, yes or no.
     * @param _level
     *            : choosen level (grid).
     */
    public void setGameConfiguration(final int _gameSpeed,
            final int _accelerateGame, final int _level) {
        gameSpeed = _gameSpeed;
        accelerateGame = _accelerateGame;
        level = _level;

    }

    /**
     * setScoreTextView: let the snake view the score text to use it later.
     * 
     * @param scoreText
     */
    public void setScoreTextView(final TextView scoreText) {
        scoreTextView = scoreText;

    }

    /**
     * setRightButton: set the right button to the snake view to use it later.
     * 
     * @param _rightButton
     */
    public void setRightButton(final Button _rightButton) {
        rightButton = _rightButton;

    }

}
