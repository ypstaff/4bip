
package il.ac.technion.cs.ssdl.cs234311.DontPanic.serverConnection;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.serverConnection.PacketsParsingServices;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.serverConnection.ServerMessages;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

/**
 * A class used to open a http connection with a server. <br/>
 * <br/>
 * First, create an {@link HttpPostPackets} object. <br/>
 * In order to create the actual http connection, use the the 'execute' method,
 * inherited from {@link AsyncTask}. The input to 'execute' is an array of
 * strings: The first cell of the array should contain the servlet name as is
 * that the http message is sent to (with no use of Gson or similar tools). <br/>
 * <br/>
 * The rest cells will contain the input to the servlet, each parameter will get
 * two consecutive cells in the array - the first is the parameter's key, the
 * second is the parameter's value. <b>Note:</b> Better to use Gson or similar
 * tools on the actual strings, and put the Gson's output in those cells. <br/>
 * <br/>
 * After executing the {@link HttpPostPackets} object, in order to receive the
 * server's message, use the 'get' method inherited from {@link AsyncTask}. The
 * get will WAIT (if necessary) for the computation to complete, and then will
 * retrieve the message. <br/>
 * <br/>
 * If there was any problem with setting up the http connection with the server,
 * or the connection didn't work well, the message that will be received from
 * the server will be null. <br/>
 * <br/>
 * for more information about AsyncTask's interface, read: {@link http
 * ://developer.android.com/reference/android/os/AsyncTask.html}
 * 
 */
public class HttpPostPackets extends AsyncTask<String, Void, String> {
    // the default kind of request sent to the server
    private static final String REQUST_TYPE = "POST";
    private final String successMessage;
    @SuppressWarnings("unused")
    // kept for using when the feature of bad internet connection will be
    // implemented.
    private final String failureMessage;
    private final Context context;
   
    private final boolean showMessages;

    // a default value for the server URL

    public HttpPostPackets(final Context _appContext,
            final String _successMessage, final String _failureMessage) {
        context = _appContext;
        successMessage = _successMessage;
        failureMessage = _failureMessage;
        showMessages = true;
    }
    
    public HttpPostPackets(final Context _appContext) {
        context = _appContext;
        showMessages = false;

        successMessage = "";
        failureMessage = "";
    }

    /**
     * Creates an output POST http connection
     * 
     * @param servletName
     *            - the specific servlet name.
     * @param messageSize
     *            - the size of message to be sent on this connection.
     * @return a http connection.
     * 
     * @throws IOException
     *             - if there is connections problem.
     */
    private static HttpURLConnection setUpPostHttpConnection(
            final String servletName, final int messageSize) throws IOException {

        URL servletURL = null;
        HttpURLConnection connection = null;
        final String connectionUrl = ServerMessages.SERVER_URL + servletName;
        // try {
        servletURL = new URL(connectionUrl);
        connection = (HttpURLConnection) servletURL.openConnection();
        connection.setDoOutput(true);

        // Sets the request kind to be POST
        connection.setRequestMethod(REQUST_TYPE);
        connection.setFixedLengthStreamingMode(messageSize);

        return connection;
    }

    /**
     * 
     * @param outputStream
     * @param parameters
     */
    private static void sendPost(final OutputStream outputStream,
            final String parameters) {
        final PrintWriter out = new PrintWriter(outputStream);
        // print the parameters to the body of the http message
        out.print(parameters);
        out.close();
    }

    /**
     * @param strings
     *            - strings[0] = servlet name (save, delete etc.) strings[2i-1]
     *            = parameter's i name strings[2i] = parameter i value (i>0)
     * @return - the server's response. if there was an error with the
     *         connection - return null.
     */
    @Override
    protected String doInBackground(final String... strings) {
        String response = "";
        final String servletName = strings[0];
        HttpURLConnection connection = null;
        try {
            // parse the parameters that were sent by the client
            final String parameters = PacketsParsingServices
                    .parseClientParameters(strings);

            // set up the connection
            connection = setUpPostHttpConnection(servletName,
                    parameters.getBytes().length);

            // send the POST out
            sendPost(connection.getOutputStream(), parameters);

            Log.i("HttpPostPacket",
                    Integer.toString(connection.getResponseCode()));

            // get the response from server
            response = PacketsParsingServices
                    .convertResponseToString(connection.getInputStream()) + "s";

        } catch (final IOException e) {
            // can't connect the server, so return null for error.
            response = null;
        }
        if (connection != null)
            connection.disconnect();

        return response;
    }

    @Override
    protected void onPostExecute(final String result) {
        if(showMessages)
        {
          if (result == null || result.isEmpty())
              ;// Toast.makeText(context, failureMessage,
               // Toast.LENGTH_SHORT).show();
          else if (context != null)
              Toast.makeText(context, successMessage, Toast.LENGTH_SHORT).show();
        }
    }
}
