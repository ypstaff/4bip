package il.ac.technion.cs.ssdl.cs234311.DontPanic.MagazineOperations;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;

public class CustomWebView extends WebView {
    public CustomWebView(Context context) {
        super(context);
    }
    
    public CustomWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public int getContentWidth()
    {
        return this.computeHorizontalScrollRange();
    }
}
