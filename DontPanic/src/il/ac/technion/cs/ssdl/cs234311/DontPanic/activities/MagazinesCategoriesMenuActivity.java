package il.ac.technion.cs.ssdl.cs234311.DontPanic.activities;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.DontPanicActivity;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.R;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.ColorArrayLocations;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.MagazineOperations.MagazineServices;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public abstract class MagazinesCategoriesMenuActivity extends DontPanicActivity {

    private Button redButton;
    private Button greenButton;
    private Button blueButton;
    private Button yellowButton;

    private TextView title;

    private ListView listView;
    private List<String> listItems;
    private ArrayAdapter<String> adapter;

    private void updateListView() {
        adapter = new ArrayAdapter<String>(
                MagazinesCategoriesMenuActivity.this,
                R.layout.list_row_small_highlighted, R.id.listItemText,
                listItems);

        listView.setAdapter(adapter);
        listView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);

        if (listItems == null || listItems.size() == 0)
            showToast(getString(R.string.magazine_no_categories_found));
        else
            Collections.sort(listItems);

        listView.setItemChecked(0, true);
    }

    private void getAllListItems() {
        listItems = MagazineServices.getAllCategories();

        if (listItems != null) {
            updateListView();
            return;
        }

        listItems = new ArrayList<String>();
        new RetrieveCategories().execute();
    }

    private void setTitle() {
        title.setText(getString(R.string.title_menu_magazines_categories));
    }

    protected abstract void openNextWindowWithSelection();

    @Override
    protected void initViews() {
        super.initViews();

        redButton = (Button) findViewById(R.id.RedButton);
        greenButton = (Button) findViewById(R.id.GreenButton);
        blueButton = (Button) findViewById(R.id.BlueButton);
        yellowButton = (Button) findViewById(R.id.YellowButton);

        title = (TextView) findViewById(R.id.title);
        listView = (ListView) findViewById(R.id.categories_list);
    }

    @Override
    protected void initTouchListeners() {
        super.initTouchListeners();

        yellowButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.YELLOW));
        blueButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.BLUE));
        greenButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.GREEN));
        redButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.RED));
    }

    protected ArrayAdapter<String> getAdapter() {
        return adapter;
    }

    protected ListView getListView() {
        return listView;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.magazines_categories_menu);

        initViews();
        initTouchListeners();

        setTitle();

        getAllListItems();
    }

    private class RetrieveCategories extends AsyncTask<Void, Void, Void> {
        private ProgressDialog progress = null;

        @Override
        protected Void doInBackground(Void... params) {
            InputStream input = null;
            try {
                input = getAssets().open("rss-directory.htm");
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
            Document doc;

            try {
                doc = Jsoup.parse(input, null, "");
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
            Elements categories = doc.select("li");

            for (Element category : categories)
                listItems.add(category.text());

            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @Override
        protected void onPreExecute() {
            progress = ProgressDialog.show(
                    MagazinesCategoriesMenuActivity.this, null,
                    "Loading Categories List...");

            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void result) {
            updateListView();

            MagazineServices.setAllCategories(listItems);

            progress.dismiss();

            super.onPostExecute(result);
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }

    /**
     * scroll up one item in the list view when the arrowDown is pressed
     */
    private void arrowUp() {
        final int index = listView.getCheckedItemPosition();

        if (index == 0) {
            listView.setItemChecked(listView.getCount() - 1, true);
            listView.smoothScrollToPositionFromTop(listView.getCount() - 1, 0,
                    0);
            return;
        }
        listView.setItemChecked(index - 1, true);
        listView.smoothScrollToPositionFromTop(index - 1,
                listView.getHeight() / 2);
    }

    /**
     * scroll down one item in the list view when the arrowDown is pressed
     */
    private void arrowDown() {
        final int index = listView.getCheckedItemPosition();

        if (index + 1 == listView.getCount()) {
            listView.setItemChecked(0, true);
            listView.smoothScrollToPositionFromTop(0, 0, 0);
            return;
        }
        listView.setItemChecked(index + 1, true);
        listView.smoothScrollToPositionFromTop(index + 1,
                listView.getHeight() / 2);
    }

    /**
     * all the operations supported in this activity
     */
    private enum Operation {
        ENTER, BACK, ARROW_UP, ARROW_DOWN, PANIC, OTHER_CODE
    }

    /**
     * matching input code to OPERATIONS constant
     * 
     * @param code
     *            - boolean array that represents the buttons pressed or the
     *            array given by the enterCode method
     * @return - the Operation constant that is matching the given input
     * @see DontPanicActivity.enterCode
     */
    private Operation getOperationFromCode(final boolean[] code) {
        final boolean green_pressed = code[ColorArrayLocations.GREEN.ordinal()];
        final boolean yellow_pressed = code[ColorArrayLocations.YELLOW
                .ordinal()];
        final boolean red_pressed = code[ColorArrayLocations.RED.ordinal()];
        final boolean blue_pressed = code[ColorArrayLocations.BLUE.ordinal()];

        // enter pressed
        if (green_pressed && !(blue_pressed || red_pressed || yellow_pressed))
            return Operation.ENTER;

        // clear/back pressed
        if (red_pressed && !(blue_pressed || green_pressed || yellow_pressed))
            return Operation.BACK;

        // arrow up pressed
        if (blue_pressed && !(green_pressed || red_pressed || yellow_pressed))
            return Operation.ARROW_UP;

        // arrow down pressed
        if (yellow_pressed && !(blue_pressed || red_pressed || green_pressed))
            return Operation.ARROW_DOWN;

        if (red_pressed && blue_pressed && green_pressed && yellow_pressed)
            return Operation.PANIC;

        return Operation.OTHER_CODE;
    }

    @Override
    public void enterCode(final boolean[] code) {

        switch (getOperationFromCode(code)) {
        case OTHER_CODE:
            return;

        case BACK:
            finish();
            break;

        case ENTER:
            openNextWindowWithSelection();
            break;

        case ARROW_UP:
            arrowUp();
            break;

        case ARROW_DOWN:
            arrowDown();
            break;

        case PANIC:
            enterPanic(LoginActivity.dataManager);
            break;
        }
    }

    private void showToast(final String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}