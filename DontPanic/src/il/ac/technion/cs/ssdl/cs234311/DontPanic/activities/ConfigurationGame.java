package il.ac.technion.cs.ssdl.cs234311.DontPanic.activities;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.DontPanicActivity;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.R;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.ColorArrayLocations;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class ConfigurationGame extends DontPanicActivity {

    public static final int SLOW = 1;
    public static final int NORMAL = 2;
    public static final int FAST = 3;
    public static final int FASTEST = 4;

    public int gameSpeed = NORMAL;

    public static final int YES = 1;
    public static final int NO = 2;

    public int accelerateGame = YES;

    public static final int EASY = 1;
    public static final int MEDIUM = 2;
    public static final int HARD = 3;
    public static final int HARDEST = 4;
    public static final int PANIC = 5;

    public int level = EASY;

    public static final int SNAKE_GAME_REQUEST = 0;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration_game);
        final Button yellowButton = (Button) findViewById(R.id.YellowButton);
        final Button blueButton = (Button) findViewById(R.id.BlueButton);
        final Button greenButton = (Button) findViewById(R.id.GreenButton);
        final Button redButton = (Button) findViewById(R.id.RedButton);

        yellowButton.setText("");
        blueButton.setText("");
        yellowButton.setCompoundDrawablesWithIntrinsicBounds(null, null, null,
                null);
        blueButton.setCompoundDrawablesWithIntrinsicBounds(null, null, null,
                null);

        greenButton.setText(R.string.snake_start_game);
        yellowButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.YELLOW));
        blueButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.BLUE));
        greenButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.GREEN));
        redButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.RED));
        gameSpeed = NORMAL;
        accelerateGame = YES;
        level = EASY;
    }

    @Override
    public void enterCode(final boolean[] code) {
        final boolean yellowPressed = code[ColorArrayLocations.YELLOW.ordinal()];
        final boolean bluePressed = code[ColorArrayLocations.BLUE.ordinal()];
        final boolean greenPressed = code[ColorArrayLocations.GREEN.ordinal()];
        final boolean redPressed = code[ColorArrayLocations.RED.ordinal()];

        if (!yellowPressed && !bluePressed && greenPressed && !redPressed) {
            // start Game! button
            final Intent intent = new Intent(ConfigurationGame.this,
                    Snake.class);
            intent.putExtra("game speed", gameSpeed);
            intent.putExtra("accelerate game", accelerateGame);
            intent.putExtra("level", level);
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intent);
        } else if (yellowPressed && !bluePressed && !greenPressed && redPressed) {
            // + speed
            final TextView speedText = (TextView) findViewById(R.id.textView4);
            if (gameSpeed == SLOW) {
                gameSpeed = NORMAL;
                speedText.setText(R.string.snake_normal);
            }

            else if (gameSpeed == NORMAL) {
                gameSpeed = FAST;
                speedText.setText(R.string.snake_fast);
            } else if (gameSpeed == FAST) {
                gameSpeed = FASTEST;
                speedText.setText(R.string.snake_fastest);
            } else if (gameSpeed == FASTEST) {
                gameSpeed = SLOW;
                speedText.setText(R.string.snake_slow);
            }

        } else if (yellowPressed && bluePressed && !greenPressed && !redPressed) {
            // + accelerate
            final TextView accelerateText = (TextView) findViewById(R.id.TextView01);
            if (accelerateGame == NO) {
                accelerateGame = YES;
                accelerateText.setText(R.string.snake_yes);
            } else {
                accelerateGame = NO;
                accelerateText.setText(R.string.snake_no);
            }

        } else if (!yellowPressed && bluePressed && greenPressed && !redPressed) {
            // + level
            final TextView levelText = (TextView) findViewById(R.id.TextView04);
            if (level == EASY) {
                level = MEDIUM;
                levelText.setText(R.string.snake_medium);
            } else if (level == MEDIUM) {
                level = HARD;
                levelText.setText(R.string.snake_hard);
            } else if (level == HARD) {
                level = HARDEST;
                levelText.setText(R.string.snake_hardest);
            } else if (level == HARDEST) {
                level = PANIC;
                levelText.setText(R.string.snake_panic);
            } else if (level == PANIC) {
                level = EASY;
                levelText.setText(R.string.snake_easy);
            }

        } else if (!yellowPressed && !bluePressed && !greenPressed
                && redPressed)
            // back
            finish();
        else if (yellowPressed && bluePressed && greenPressed && redPressed)
            // panic!
            enterPanic(LoginActivity.dataManager);
    }

}
