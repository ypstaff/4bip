package il.ac.technion.cs.ssdl.cs234311.DontPanic;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.ColorArrayLocations;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.activities.LoginActivity;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.activities.Open4BipActivity;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.data.DontPanicData;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.UUID;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

/**
 * a class for all don't panic application activities. Instead of just writing
 * activities, this abstract class will promise all activities to implement
 * entering a code.
 * 
 * @author Shachar Nudler
 * 
 */
public abstract class DontPanicActivity extends Activity{

    boolean[] isClickedNow = new boolean[4];
    boolean[] operation = new boolean[4];
    boolean operationDone = false;

    private BroadcastReceiver broadcastReceiver;

    /**
     * used to initialize touchListeners for the saved views variables. this
     * should be called after initViews.
     * 
     * @see initViews
     */
    protected void initTouchListeners() {
        return;
    }

    /**
     * use it to initialize and save the views in local variables.
     */
    protected void initViews() {
        return;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        forgroundActivity = this;
        
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        


        //
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("CLOSE_ALL");
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(final Context context, final Intent intent) {
                // close activity
                finish();
            }
        };
        registerReceiver(broadcastReceiver, intentFilter);

        // add an animation between activities transitions
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

        if(mBluetoothAdapter == null) {
          Log.e(TAG, "No Bluetooth Adapter available.");
        }
             
        if(mBluetoothAdapter.isEnabled() && server == null){
          if(!mBluetoothAdapter.getName().startsWith(PREFIX))
              mBluetoothAdapter.setName(PREFIX + mBluetoothAdapter.getName());
            
          if(mBluetoothAdapter.getScanMode() != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE)
            requestBTDiscoverable();
            
            server = new AcceptThread();
            server.start();
        }
        
    }

    public void speakText(final String msg) {
        if (LoginActivity.dataManager != null)
            if (LoginActivity.textToSpeechEnabled)
                LoginActivity.tts.speak(msg, TextToSpeech.QUEUE_FLUSH, null);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        unregisterReceiver(broadcastReceiver);
    }
    
    @Override
    protected void onResume(){
        forgroundActivity = this;
        
        super.onResume();
    }
    
    /*Michael's code*/
    /**
     * Default Serial-Port UUID
     */
    private String defaultUUID = "00001101-0000-1000-8000-00805f9b34fb";

    /**
     * Default bluetooth adapter on the device.
     */
    private BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

    /**
     * String used to identify this application in the log.
     */
    private final String TAG = DontPanicActivity.class.getName();
    
    /**
     * The prefix to identify devices of interest.
     */
    private final static String PREFIX = "BT_";

    /**
     * The Server thread.
     */
    private static AcceptThread server;

    /**
     * Magic number used in the bluetooth enabling request.
     */
    private final int REQ = 111;

    /**
     * Launches Discoverable Bluetooth Intent.
     */
    public void requestBTDiscoverable() {
      Intent i = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
      i.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);

      startActivityForResult(i, REQ);

      int result = 0;

      this.onActivityResult(REQ, result, i);
      Log.i(TAG, "Bluetooth discoverability enabled");
    }
    
    private static BluetoothServerSocket mServerSocket;
    private static BluetoothSocket socket = null;
    
    private static DontPanicActivity forgroundActivity = null;
    

    public static boolean sendBtLocal = true;
    
    /**
     * Thread that handles an incoming connection.
     * Adapted from http://developer.android.com/guide/topics/wireless/bluetooth.html 
     */
    class AcceptThread extends Thread {
      /**
       * Tag that will appear in the log.
       */
      private final String ACCEPT_TAG = AcceptThread.class.getName();

      /**
       * The bluetooth server socket.
       */

      public AcceptThread() {
        BluetoothServerSocket tmp = null;
        try {
          tmp = mBluetoothAdapter.listenUsingRfcommWithServiceRecord(ACCEPT_TAG,
              UUID.fromString(defaultUUID));
        } catch (IOException e) { 
          e.printStackTrace();
        }
        mServerSocket = tmp;
      }
      

      public void run() {
        //socket = null;
        while (true) {
      
          if (socket == null){ 
            try {
              Log.i(ACCEPT_TAG, "Listening for a connection...");
  
              socket = mServerSocket.accept();
              Log.i(ACCEPT_TAG, "Connected to " + socket.getRemoteDevice().getName());
  
            } catch (IOException e) {
              break;
            }
          // If a connection was accepted
            
          }else {
            // Do work to manage the connection (in a separate thread)
            try {
              // Read the incoming string.
              String buffer;
              
              DataInputStream in = new DataInputStream(socket.getInputStream());
              
              do{
                buffer = in.readUTF();
                
                final boolean[] code = new boolean[buffer.length()];
                for(int i = 0; i < buffer.length(); ++i){
                      code[i] = (buffer.charAt(i) == '1');     
                }            
                
                if(!buffer.equals("quit")){
                    if(sendBtLocal)
                      runOnUiThread(new Runnable() {
                          @Override
                          public void run() {
                              forgroundActivity.enterCode(code);
                         }
                     });
                    else {
                        final Intent codeMessage = new Intent("fourBIP");
                        Byte t_9_code = Open4BipActivity.convertCodeToByte(code);
                        codeMessage.putExtra("4ButtonsCode", t_9_code);
                        
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                sendBroadcast(codeMessage);
                           }
                       });
                        
                    }
                    
                }
              }while(!buffer.equals("quit"));
              
            } catch (IOException e) {
              Log.e(ACCEPT_TAG, "Error obtaining InputStream from socket");
              e.printStackTrace();
            }
            try {
              if(mServerSocket != null)
                  mServerSocket.close();
              
              mServerSocket = null;
              socket = null;
              
            } catch (IOException e) { }
            break;
          }
        }
      }
      
      /** Will cancel the listening socket, and cause the thread to finish */
      public void cancel() {
        try {
          if(mServerSocket != null)
              mServerSocket.close();
            
          mServerSocket = null;
          socket = null;
        } catch (IOException e) { }
      }
    }
  
    /*Michael code*/
    /**
     * this method is the main activity method that should be implemented in
     * each activity. the method will get a one lettered code and will be
     * implemented accordingly to it
     * 
     * @param code
     * 
     * @see Shortcut.ColorArrayLocations
     */
    public abstract void enterCode(boolean[] code);

    /**
     * @param dpData
     */
    public void enterPanic(final DontPanicData dpData) {
        Log.i("panic", "panic pressed");
        dpData.panicAlert();
        Toast.makeText(getApplicationContext(),
                getString(R.string.panic_sent_announcement), Toast.LENGTH_SHORT)
                .show();
    }

    public class DontPanicTouchListener implements View.OnTouchListener {
        ColorArrayLocations color;

        public DontPanicTouchListener(final ColorArrayLocations _color) {
            color = _color;
        }

        @Override
        public boolean onTouch(final View v, final MotionEvent m) {
            handleButtonTouch(m, color);
            return false;
        }
    }

    protected void handleButtonTouch(final MotionEvent m,
            final ColorArrayLocations color) {
        final int action = m.getActionMasked();
        if (action == MotionEvent.ACTION_DOWN && !operationDone)
            isClickedNow[color.ordinal()] = true;
        else if (action == MotionEvent.ACTION_UP) {
            if (!operationDone) {
                operation = isClickedNow.clone();
                operationDone = true;
            }

            isClickedNow[color.ordinal()] = false;

            if (!isClickedNow[0] && !isClickedNow[1] && !isClickedNow[2]
                    && !isClickedNow[3]) {
                // the user left all fingers from the buttons
                operationDone = false;
                enterCode(operation.clone());
            }
        }
    }
}
