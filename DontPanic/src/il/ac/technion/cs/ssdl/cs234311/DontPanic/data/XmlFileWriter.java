package il.ac.technion.cs.ssdl.cs234311.DontPanic.data;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.User;

import java.io.FileOutputStream;
import java.io.IOException;

import android.content.Context;

public class XmlFileWriter {

    String codesFileName;

    String userDataFileName;

    Context appContext;

    XmlFileWriter(final String _codesFileName, final String _userDataFileName,
            final Context _appContext) {
        appContext = _appContext;
        codesFileName = _codesFileName;
        userDataFileName = _userDataFileName;
    }

    public void replaceFile(final CodesDictionary dic) throws IOException {

        // not inline to have the file opened for the minimum time.
        final String xmlFormattedDic = CodesXmlFormatter.FormatDictionary(dic);

        final FileOutputStream fos = appContext.openFileOutput(codesFileName,
                Context.MODE_PRIVATE);

        fos.write(xmlFormattedDic.getBytes());

        fos.close();
    }

    public void startCodesFile(final String encodingString) throws IOException {
        final FileOutputStream fos = appContext.openFileOutput(codesFileName,
                Context.MODE_PRIVATE);

        fos.write(encodingString.getBytes());

        fos.close();
    }

    public void regUser(final User user) throws IOException {
        final String xmlFormattedUser = CodesXmlFormatter.FormatUser(user);

        final FileOutputStream fos = appContext.openFileOutput(
                userDataFileName, Context.MODE_PRIVATE);

        fos.write(xmlFormattedUser.getBytes());
        fos.close();
    }

}
