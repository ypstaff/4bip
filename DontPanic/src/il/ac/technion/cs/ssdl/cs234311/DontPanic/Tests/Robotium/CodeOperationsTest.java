package il.ac.technion.cs.ssdl.cs234311.DontPanic.Tests.Robotium;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.DontPanicActivity;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.R;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.ColorArrayLocations;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.User;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.activities.LoginActivity;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.data.ShortcutsParser;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.EditText;

import com.robotium.solo.Solo;

public class CodeOperationsTest extends
        ActivityInstrumentationTestCase2<LoginActivity> {

    private Solo solo;
    private final User user;

    public CodeOperationsTest() {
        super(LoginActivity.class);

        user = ServerMessagesTestUtils.randomUser();

        ServerMessagesTestUtils.runMaintainance();
        ServerMessagesTestUtils.regUserForTest(user);
    }

    @Override
    public void setUp() {
        solo = new Solo(getInstrumentation(), getActivity());

        solo.assertCurrentActivity("Error: Expected Login Activity",
                "LoginActivity");
        try {
            runTestOnUiThread(new Runnable() {
                @Override
                public void run() {
                    final EditText user_name = (EditText) solo
                            .getView(R.id.log_user_name);
                    final EditText email = (EditText) solo
                            .getView(R.id.log_user_email);
                    user_name.setText(user.username);
                    email.setText(user.email);
                }
            });
        } catch (final Throwable e) {
            e.printStackTrace();
        }
        solo.clickOnView(solo.getView(R.id.login_button));
        checkMainWindow();
    }

    @Override
    public void tearDown() throws Exception {
        solo.sleep(5000);
        solo.finishOpenedActivities();
        super.tearDown();
    }

    private void makeCommand(final boolean r, final boolean y, final boolean b,
            final boolean g, final boolean[] command) {
        command[ColorArrayLocations.YELLOW.ordinal()] = y;
        command[ColorArrayLocations.GREEN.ordinal()] = g;
        command[ColorArrayLocations.BLUE.ordinal()] = b;
        command[ColorArrayLocations.RED.ordinal()] = r;
    }

    private void checkMenuNavigationShortcutTypes() {
        solo.assertCurrentActivity("Error: Expected ShortcutTpesActivity",
                "ShortcutTypesActivity");
        assertTrue(solo.searchText(((DontPanicActivity) solo
                .getCurrentActivity())
                .getString(R.string.title_menu_shortcut_types)));

        assertTrue(solo.searchText(((DontPanicActivity) solo
                .getCurrentActivity())
                .getString(R.string.shortcut_phone_call_type)));
        assertTrue(solo.searchText(((DontPanicActivity) solo
                .getCurrentActivity()).getString(R.string.shortcut_music_type)));
        assertTrue(solo.searchText(((DontPanicActivity) solo
                .getCurrentActivity()).getString(R.string.shortcut_game_type)));
    }

    private void checkMenuNavigationPhone() {
        solo.assertCurrentActivity("Error: Expected Phone menu activity",
                "PhoneMenuActivity");
        assertTrue(solo.searchText(((DontPanicActivity) solo
                .getCurrentActivity()).getString(R.string.title_menu_phone)));
    }

    private void checkMenuNavigationMusic() {
        solo.assertCurrentActivity("Error: Expected Music menu activity",
                "MusicMenuActivity");
        assertTrue(solo.searchText(((DontPanicActivity) solo
                .getCurrentActivity()).getString(R.string.title_menu_music)));

        assertTrue(solo.searchText(((DontPanicActivity) solo
                .getCurrentActivity())
                .getString(R.string.music_shortcut_to_an_album)));
        assertTrue(solo.searchText(((DontPanicActivity) solo
                .getCurrentActivity())
                .getString(R.string.music_shortcut_to_an_artist)));
    }

    private void checkMenuNavigationMusicAlbums() {
        solo.assertCurrentActivity(
                "Error: Expected Music albums menu activity",
                "MusicAlbumsMenuActivity");
        assertTrue(solo.searchText(((DontPanicActivity) solo
                .getCurrentActivity())
                .getString(R.string.title_menu_music_albums)));

        assertTrue(solo.searchText(ShortcutsParser.ALL_SONGS_ALBUM_NAME));
    }

    private void checkMenuNavigationMusicArtists() {
        solo.assertCurrentActivity(
                "Error: Expected Music artists menu activity",
                "MusicArtistsMenuActivity");
        assertTrue(solo.searchText(((DontPanicActivity) solo
                .getCurrentActivity())
                .getString(R.string.title_menu_music_artists)));
    }

    private void checkMenuNavigationGames() {
        solo.assertCurrentActivity("Error: Expected Games menu activity",
                "GamesMenuActivity");
        assertTrue(solo.searchText(((DontPanicActivity) solo
                .getCurrentActivity()).getString(R.string.title_menu_games)));

        assertTrue(solo.searchText(((DontPanicActivity) solo
                .getCurrentActivity()).getString(R.string.game_snake)));
    }

    private void checkMenuNavigationCodeAddition() {
        solo.assertCurrentActivity("Error: Expected Code addition activity",
                "CodeAdditionActivity");
    }

    private void checkMenuNavigationCodeDeletion() {
        solo.assertCurrentActivity("Error: Expected Code deletion activity",
                "CodeDeletionActivity");
    }

    private void checkMainWindow() {
        solo.assertCurrentActivity("Error: Expected Main activity",
                "MainWindowActivity");
    }

    /**
     * Go through all the menus back and forth, testing that navigation works
     * properly and all the texts needed appear.
     */
    public void testMenuNavigation() throws Throwable {
        checkMainWindow();

        final boolean command[] = new boolean[4];
        makeCommand(true, true, false, false, command); // new shortcut

        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((DontPanicActivity) solo.getCurrentActivity())
                        .enterCode(command.clone());
            }
        });
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationShortcutTypes();
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationPhone();
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationCodeAddition();
        solo.clickOnView(solo.getView(R.id.RedButton));
        checkMenuNavigationPhone();
        solo.clickOnView(solo.getView(R.id.RedButton));
        checkMenuNavigationShortcutTypes();
        solo.clickOnView(solo.getView(R.id.YellowButton));
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationMusic();
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationMusicAlbums();
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationCodeAddition();
        solo.clickOnView(solo.getView(R.id.RedButton));
        checkMenuNavigationMusicAlbums();
        solo.clickOnView(solo.getView(R.id.RedButton));
        checkMenuNavigationMusic();
        solo.clickOnView(solo.getView(R.id.BlueButton));
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationMusicArtists();
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationCodeAddition();
        solo.clickOnView(solo.getView(R.id.RedButton));
        checkMenuNavigationMusicArtists();
        solo.clickOnView(solo.getView(R.id.RedButton));
        checkMenuNavigationMusic();
        solo.clickOnView(solo.getView(R.id.RedButton));
        checkMenuNavigationShortcutTypes();
        solo.clickOnView(solo.getView(R.id.YellowButton));
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationGames();
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationCodeAddition();
        solo.clickOnView(solo.getView(R.id.RedButton));
        checkMenuNavigationGames();
        solo.clickOnView(solo.getView(R.id.RedButton));
        checkMenuNavigationShortcutTypes();
        solo.clickOnView(solo.getView(R.id.RedButton));
        checkMainWindow();

        makeCommand(false, false, true, true, command); // delete shortcut

        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((DontPanicActivity) solo.getCurrentActivity())
                        .enterCode(command.clone());
            }
        });
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationCodeDeletion();
        solo.clickOnView(solo.getView(R.id.RedButton));
        checkMainWindow();
    }

    /**
     * Add a new phone call shortcut to a specific contact - Ala the selected
     * code will be [Y B] [R G]
     */
    public void testAddNewPhoneCallShortcut() throws Throwable {
        checkMainWindow();
        assertFalse(solo.searchText("Call Ala"));

        final boolean command[] = new boolean[4];
        makeCommand(true, true, false, false, command); // new shortcut

        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((DontPanicActivity) solo.getCurrentActivity())
                        .enterCode(command.clone());
            }
        });
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationShortcutTypes();
        // choose the first category - Phone
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationPhone();
        solo.clickOnView(solo.getView(R.id.GreenButton));
        // choose the first contact - Ala
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationCodeAddition();

        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                makeCommand(false, true, true, false, command); // [Y B]
                ((DontPanicActivity) solo.getCurrentActivity())
                        .enterCode(command.clone());
                makeCommand(true, false, false, true, command); // [R G]
                ((DontPanicActivity) solo.getCurrentActivity())
                        .enterCode(command.clone());
            }
        });
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMainWindow();
        assertTrue(solo.searchText("Call Ala"));
    }

    /**
     * Add a new music-artist shortcut to a specific artist - "AC/DC" the
     * selected code will be [Y B] [Y B] [R Y]
     */
    public void testAddNewMusicArtistShortcut() throws Throwable {
        checkMainWindow();
        assertFalse(solo.searchText("Play AC/DC"));

        final boolean command[] = new boolean[4];
        makeCommand(true, true, false, false, command); // new shortcut

        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((DontPanicActivity) solo.getCurrentActivity())
                        .enterCode(command.clone());
            }
        });
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationShortcutTypes();
        // choose the second category - Music
        solo.clickOnView(solo.getView(R.id.YellowButton));
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationMusic();
        // choose the second music category - Artists
        solo.clickOnView(solo.getView(R.id.YellowButton));
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationMusicArtists();
        // choose the second artist - AC/DC
        solo.clickOnView(solo.getView(R.id.YellowButton));
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationCodeAddition();

        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                makeCommand(false, true, true, false, command); // [Y B]
                ((DontPanicActivity) solo.getCurrentActivity())
                        .enterCode(command.clone());
                ((DontPanicActivity) solo.getCurrentActivity())
                        .enterCode(command.clone());
                makeCommand(true, true, false, false, command); // [R Y]
                ((DontPanicActivity) solo.getCurrentActivity())
                        .enterCode(command.clone());
            }
        });
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMainWindow();
        assertTrue(solo.searchText("Play AC/DC"));
    }

    /**
     * Add a new music-album shortcut to a specific album - "infinite" the
     * selected code will be [R G]
     */
    public void testAddNewMusicAlbumShortcut() throws Throwable {
        checkMainWindow();
        assertFalse(solo.searchText("Play infinite"));

        final boolean command[] = new boolean[4];
        makeCommand(true, true, false, false, command); // new shortcut

        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((DontPanicActivity) solo.getCurrentActivity())
                        .enterCode(command.clone());
            }
        });
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationShortcutTypes();
        // choose the second category - Music
        solo.clickOnView(solo.getView(R.id.YellowButton));
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationMusic();
        // choose the second music category - Albums
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationMusicAlbums();
        // choose the 10th from the bottom album - infinite
        for (int i = 0; i < 10; ++i)
            solo.clickOnView(solo.getView(R.id.BlueButton));
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationCodeAddition();

        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                makeCommand(true, false, false, true, command); // [R G]
                ((DontPanicActivity) solo.getCurrentActivity())
                        .enterCode(command.clone());
            }
        });
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMainWindow();
        assertTrue(solo.searchText("Play infinite"));
    }

    /**
     * Add a new game shortcut, a specific game - Snake the selected code will
     * be [Y B] [B G]
     */
    public void testAddNewGameShortcut() throws Throwable {
        checkMainWindow();
        assertFalse(solo.searchText("Play Snake"));

        final boolean command[] = new boolean[4];
        makeCommand(true, true, false, false, command); // new shortcut

        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((DontPanicActivity) solo.getCurrentActivity())
                        .enterCode(command.clone());
            }
        });
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationShortcutTypes();
        // choose the third category - Games
        solo.clickOnView(solo.getView(R.id.YellowButton));
        solo.clickOnView(solo.getView(R.id.YellowButton));
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationGames();
        // choose the first game - Snake
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationCodeAddition();

        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                makeCommand(false, true, true, false, command); // [Y B]
                ((DontPanicActivity) solo.getCurrentActivity())
                        .enterCode(command.clone());
                makeCommand(false, false, true, true, command); // [B G]
                ((DontPanicActivity) solo.getCurrentActivity())
                        .enterCode(command.clone());
            }
        });
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMainWindow();
        assertTrue(solo.searchText("Play Snake"));
    }

    private void additioanlSetUp() throws Throwable {
        checkMainWindow();
        if (solo.searchText("Play AlbumWrap - Coleman Hawkins"))
            return;

        final boolean command[] = new boolean[4];
        makeCommand(true, true, false, false, command); // new shortcut

        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((DontPanicActivity) solo.getCurrentActivity())
                        .enterCode(command.clone());
            }
        });
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationShortcutTypes();
        // choose the second category - Music
        solo.clickOnView(solo.getView(R.id.YellowButton));
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationMusic();
        // choose the second music category - Artists
        solo.clickOnView(solo.getView(R.id.YellowButton));
        solo.clickOnView(solo.getView(R.id.GreenButton));
        // choose the third artist - AlbumWrap - Coleman Hawkins
        solo.clickOnView(solo.getView(R.id.YellowButton));
        solo.clickOnView(solo.getView(R.id.YellowButton));
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationCodeAddition();

        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                makeCommand(false, true, true, false, command); // [Y B]
                ((DontPanicActivity) solo.getCurrentActivity())
                        .enterCode(command.clone());
            }
        });
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMainWindow();
        assertTrue(solo.searchText("Play AlbumWrap - Coleman Hawkins"));
    }

    /**
     * Delete a pre-saved shortcut, a specific one -
     * "Play AlbumWrap - Coleman Hawkins" code is [Y B] There should be only 1
     * "Play AlbumWrap - Coleman Hawkins" shortcut before this method runs
     * 
     * @throws Throwable
     */
    public void testDeletePreSavedShortcut() throws Throwable {
        additioanlSetUp();
        checkMainWindow();
        assertTrue(solo.searchText("Play AlbumWrap - Coleman Hawkins"));

        final boolean command[] = new boolean[4];
        makeCommand(false, false, true, true, command); // delete shortcut

        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((DontPanicActivity) solo.getCurrentActivity())
                        .enterCode(command.clone());
            }
        });
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationCodeDeletion();
        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                makeCommand(false, true, true, false, command); // [Y B]
                ((DontPanicActivity) solo.getCurrentActivity())
                        .enterCode(command.clone());
            }
        });
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMainWindow();
        assertFalse(solo.searchText("Play AlbumWrap - Coleman Hawkins"));
    }

}
