package il.ac.technion.cs.ssdl.cs234311.DontPanic.activities;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.DontPanicActivity;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.R;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.ColorArrayLocations;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Music.MediaPlayerClass;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.phoneOperations.Contact;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.phoneOperations.ContactOperations;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class CallerActivity extends DontPanicActivity {

    private Button redButton;
    private Button greenButton;
    private Button blueButton;
    private Button yellowButton;

    private TextView contactPhone;
    private TextView contactName;
    private TextView phoneStatus;
    private TextView callDuration;

    private BroadcastReceiver endCallReceiver;
    private BroadcastReceiver startCallReceiver;

    private Timer durationTimer;
    private long callStartTime;

    @Override
    protected void initViews() {
        super.initViews();

        redButton = (Button) findViewById(R.id.RedButton);
        greenButton = (Button) findViewById(R.id.GreenButton);
        blueButton = (Button) findViewById(R.id.BlueButton);
        yellowButton = (Button) findViewById(R.id.YellowButton);

        contactPhone = (TextView) findViewById(R.id.phone_number);
        contactName = (TextView) findViewById(R.id.contact_name);
        phoneStatus = (TextView) findViewById(R.id.phone_status);
        callDuration = (TextView) findViewById(R.id.call_duration);
    }

    @Override
    protected void initTouchListeners() {
        super.initTouchListeners();

        yellowButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.YELLOW));
        blueButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.BLUE));
        redButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.RED));
    }

    private class UpdateDurationTask extends TimerTask {

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                @SuppressLint("SimpleDateFormat")
                public void run() {
                    final Date date = new Date(System.currentTimeMillis()
                            - callStartTime);
                    final SimpleDateFormat formatter = new SimpleDateFormat(
                            "HH:mm:ss");
                    formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
                    final String dateFormatted = formatter.format(date);

                    callDuration.setText(dateFormatted);
                }
            });
        }
    }

    private void startCallDurationTimer() {
        callStartTime = System.currentTimeMillis();
        durationTimer = new Timer();
        // initial delay 0 seconds, interval 1 second
        durationTimer.schedule(new UpdateDurationTask(), 0, 1000);
    }

    private class EndCallReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(final Context context, final Intent intent) {
            if (!intent.getAction().equals("android.intent.action.PHONE_STATE"))
                return;// shouldn't happen because of the intent-filter
                       // registered
            final String state = intent
                    .getStringExtra(TelephonyManager.EXTRA_STATE);

            if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                restoreAudioSettings();
                phoneStatus.setText(getString(R.string.caller_call_ended));
                
                MediaPlayerClass player = MediaPlayerClass.MediaPlayerGetInstance();
                if (player != null)
                    player.doPlay();
                
                finish();
            }
        }
    }

    private class StartCallReciever extends BroadcastReceiver {
        @Override
        public void onReceive(final Context context, final Intent intent) {
            if (!intent.getAction().equals("android.intent.action.PHONE_STATE"))
                return;// shouldn't happen because of the intent-filter
                       // registered
            final String state = intent
                    .getStringExtra(TelephonyManager.EXTRA_STATE);

            if (state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
                greenButton.setEnabled(false);
                startCallDurationTimer();
            }
        }
    };

    private void restoreAudioSettings() {
        final AudioManager audioManager = (AudioManager) getApplicationContext()
                .getSystemService(Context.AUDIO_SERVICE);

        audioManager.setSpeakerphoneOn(false);
        audioManager.setMicrophoneMute(false);
    }

    private void updateSpeakerButton() {
        final AudioManager audioManager = (AudioManager) getApplicationContext()
                .getSystemService(Context.AUDIO_SERVICE);
        Drawable speakerToggle = getResources().getDrawable(
                R.drawable.ic_action_volume_muted);

        if (!audioManager.isSpeakerphoneOn())
            blueButton.setTextColor(Color.BLACK);
        else {
            blueButton.setTextColor(Color.WHITE);
            speakerToggle = getResources().getDrawable(
                    R.drawable.ic_action_volume_on);
        }
        blueButton.setCompoundDrawablesWithIntrinsicBounds(null, speakerToggle,
                null, null);
    }

    private void updateMuteButton() {
        final AudioManager audioManager = (AudioManager) getApplicationContext()
                .getSystemService(Context.AUDIO_SERVICE);
        Drawable muteToggle = getResources().getDrawable(
                R.drawable.ic_action_mic);

        if (!audioManager.isMicrophoneMute())
            yellowButton.setTextColor(Color.BLACK);
        else {
            yellowButton.setTextColor(Color.WHITE);
            muteToggle = getResources().getDrawable(
                    R.drawable.ic_action_mic_muted);
        }
        yellowButton.setCompoundDrawablesWithIntrinsicBounds(null, muteToggle,
                null, null);
    }

    private void updateUI() {
        updateSpeakerButton();
        updateMuteButton();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateUI();
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        getWindow().addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        setContentView(R.layout.caller);

        initViews();
        initTouchListeners();

        final String phoneNo = getIntent().getStringExtra("PHONE_NUMBER");
        final boolean isIncomingCall = getIntent().getBooleanExtra(
                "IS_INCOMING_CALL", false);

        if (isIncomingCall)
            phoneStatus.setText(getString(R.string.caller_incoming_call));
        else {
            phoneStatus.setText(getString(R.string.caller_outgoing_call));
            startCallDurationTimer();
        }

        // get cursor over all contacts
        final Cursor phones = getContentResolver().query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null,
                null, null);

        for (final Contact c : ContactOperations.getAllContacts(phones,
                getApplicationContext()))
            if (PhoneNumberUtils.compare(c.phoneNo, phoneNo)) {
                contactName.setText(c.name);
                break;
            }
        contactPhone.setText(phoneNo);

        if (contactName.getText().equals("")) {
            contactName.setText(phoneNo);
            contactPhone.setText("");
        }

        redButton.setText(getString(R.string.caller_hang_up));
        greenButton.setText(getString(R.string.caller_answer));
        blueButton.setText(getString(R.string.caller_speaker));
        yellowButton.setText(getString(R.string.caller_mute));

        final Drawable endCall = getResources().getDrawable(
                R.drawable.ic_action_end_call);
        final Drawable answerCall = getResources().getDrawable(
                R.drawable.ic_action_call);

        redButton.setCompoundDrawablesWithIntrinsicBounds(null, endCall, null,
                null);
        greenButton.setCompoundDrawablesWithIntrinsicBounds(null, answerCall,
                null, null);

        updateUI();

        if (isIncomingCall)
            greenButton.setOnTouchListener(new DontPanicTouchListener(
                    ColorArrayLocations.GREEN));
        else
            greenButton.setEnabled(false);

        endCallReceiver = new EndCallReceiver();
        startCallReceiver = new StartCallReciever();

        registerReceiver(endCallReceiver, new IntentFilter(
                "android.intent.action.PHONE_STATE"));
        registerReceiver(startCallReceiver, new IntentFilter(
                "android.intent.action.PHONE_STATE"));
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(endCallReceiver);
        unregisterReceiver(startCallReceiver);
        super.onDestroy();
    }

    /**
     * all the operations supported in this activity
     */
    private enum Operation {
        ANSWER_CALL, END_CALL, SPEAKER_TOGGLE, MUTE_TOGGLE, PANIC, OTHER_CODE
    }

    /**
     * matching input code to OPERATIONS constant
     * 
     * @param code
     *            - boolean array that represents the buttons pressed or the
     *            array given by the enterCode method
     * @return - the Operation constant that is matching the given input
     * @see DontPanicActivity.enterCode
     */
    private Operation getOperationFromCode(final boolean[] code) {
        final boolean green_pressed = code[ColorArrayLocations.GREEN.ordinal()];
        final boolean yellow_pressed = code[ColorArrayLocations.YELLOW
                .ordinal()];
        final boolean red_pressed = code[ColorArrayLocations.RED.ordinal()];
        final boolean blue_pressed = code[ColorArrayLocations.BLUE.ordinal()];

        if (red_pressed)
            return Operation.END_CALL;
        if (green_pressed)
            return Operation.ANSWER_CALL;
        if (blue_pressed)
            return Operation.SPEAKER_TOGGLE;
        if (yellow_pressed)
            return Operation.MUTE_TOGGLE;

        if (red_pressed && blue_pressed && green_pressed && yellow_pressed)
            return Operation.PANIC;

        return Operation.OTHER_CODE;
    }

    @Override
    public void enterCode(final boolean[] code) {

        switch (getOperationFromCode(code)) {
        case OTHER_CODE:
            return;

        case END_CALL:
            ContactOperations.endCall(getApplicationContext());
            break;

        case ANSWER_CALL:
            ContactOperations.answerCall(getApplicationContext());
            break;

        case SPEAKER_TOGGLE:
            ContactOperations.toggleSpeakerPhone(getApplicationContext());
            updateSpeakerButton();
            break;

        case MUTE_TOGGLE:
            ContactOperations.toggleMuteMic(getApplicationContext());
            updateMuteButton();
            break;

        case PANIC:
            enterPanic(LoginActivity.dataManager);
            break;
        }
    }
}
