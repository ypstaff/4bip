package il.ac.technion.cs.ssdl.cs234311.DontPanic.activities;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.DontPanicActivity;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.R;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut.CodeOperation;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.ColorArrayLocations;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.phoneOperations.Contact;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.phoneOperations.ContactAdapter;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.phoneOperations.ContactOperations;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

public class PhoneMenuActivity extends DontPanicActivity {

    private Button redButton;
    private Button greenButton;
    private Button blueButton;
    private Button yellowButton;

    protected TextView title;

    protected ListView listView;
    protected List<Contact> listItems;
    private ArrayAdapter<Contact> adapter;

    protected List<Contact> getAllListItems() {
        return ContactOperations.getAllContacts(
                getContentResolver().query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        null, null, null, null), getApplicationContext());
    }

    @Override
    protected void initViews() {
        super.initViews();

        redButton = (Button) findViewById(R.id.RedButton);
        greenButton = (Button) findViewById(R.id.GreenButton);
        blueButton = (Button) findViewById(R.id.BlueButton);
        yellowButton = (Button) findViewById(R.id.YellowButton);

        title = (TextView) findViewById(R.id.title);

        listView = (ListView) findViewById(R.id.contacts_list);
    }

    @Override
    protected void initTouchListeners() {
        super.initTouchListeners();

        yellowButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.YELLOW));
        blueButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.BLUE));
        greenButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.GREEN));
        redButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.RED));
    }

    protected void setTitle() {
        title.setText(getString(R.string.title_menu_phone));
    }

    protected ListView getListView() {
        return (ListView) findViewById(R.id.contacts_list);
    }

    protected ArrayAdapter<Contact> getAdapter() {
        return new ContactAdapter(PhoneMenuActivity.this, R.layout.alluser_row,
                listItems);
    }

    protected int getLayoutId() {
        return R.layout.phone_menu;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());

        initViews();
        initTouchListeners();

        setTitle();

        listItems = getAllListItems();
        adapter = getAdapter();
        listView.setAdapter(adapter);
        listView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);

        if (listItems == null || listItems.size() == 0)
            showToast(getString(R.string.phone_no_contacts_found));
        else
            Collections.sort(listItems, new Comparator<Contact>() {

                @Override
                public int compare(final Contact lhs, final Contact rhs) {
                    return lhs.name.compareTo(rhs.name);
                }
            });

        listView.setItemChecked(0, true);
    }

    protected void openNextWindowWithSelection() {
        if (adapter.getCount() == 0)
            return;

        final Contact selected = adapter.getItem(listView
                .getCheckedItemPosition());

        final Intent intent = new Intent(PhoneMenuActivity.this,
                CodeAdditionActivity.class);
        intent.putExtra("SHORTCUT_TYPE", CodeOperation.PHONE_CALL);
        intent.putExtra("SHORTCUT_OBJ", new Gson().toJson(selected));
        startActivity(intent);
    }

    /**
     * scroll up one item in the list view when the arrowDown is pressed
     */
    private void arrowUp() {
        final int index = listView.getCheckedItemPosition();

        if (index == 0) {
            listView.setItemChecked(listView.getCount() - 1, true);
            listView.smoothScrollToPositionFromTop(listView.getCount() - 1, 0,
                    0);
            return;
        }
        listView.setItemChecked(index - 1, true);
        listView.smoothScrollToPositionFromTop(index - 1,
                listView.getHeight() / 2);
    }

    /**
     * scroll down one item in the list view when the arrowDown is pressed
     */
    private void arrowDown() {
        final int index = listView.getCheckedItemPosition();

        if (index + 1 == listView.getCount()) {
            listView.setItemChecked(0, true);
            listView.smoothScrollToPositionFromTop(0, 0, 0);
            return;
        }
        listView.setItemChecked(index + 1, true);
        listView.smoothScrollToPositionFromTop(index + 1,
                listView.getHeight() / 2);
    }

    /**
     * all the operations supported in this activity
     */
    private enum Operation {
        ENTER, BACK, ARROW_UP, ARROW_DOWN, PANIC, OTHER_CODE
    }

    /**
     * matching input code to OPERATIONS constant
     * 
     * @param code
     *            - boolean array that represents the buttons pressed or the
     *            array given by the enterCode method
     * @return - the Operation constant that is matching the given input
     * @see DontPanicActivity.enterCode
     */
    private Operation getOperationFromCode(final boolean[] code) {
        final boolean green_pressed = code[ColorArrayLocations.GREEN.ordinal()];
        final boolean yellow_pressed = code[ColorArrayLocations.YELLOW
                .ordinal()];
        final boolean red_pressed = code[ColorArrayLocations.RED.ordinal()];
        final boolean blue_pressed = code[ColorArrayLocations.BLUE.ordinal()];

        // enter pressed
        if (green_pressed && !(blue_pressed || red_pressed || yellow_pressed))
            return Operation.ENTER;

        // clear/back pressed
        if (red_pressed && !(blue_pressed || green_pressed || yellow_pressed))
            return Operation.BACK;

        // arrow up pressed
        if (blue_pressed && !(green_pressed || red_pressed || yellow_pressed))
            return Operation.ARROW_UP;

        // arrow down pressed
        if (yellow_pressed && !(blue_pressed || red_pressed || green_pressed))
            return Operation.ARROW_DOWN;

        if (red_pressed && blue_pressed && green_pressed && yellow_pressed)
            return Operation.PANIC;

        return Operation.OTHER_CODE;
    }

    @Override
    public void enterCode(final boolean[] code) {

        switch (getOperationFromCode(code)) {
        case OTHER_CODE:
            return;

        case BACK:
            finish();
            break;

        case ENTER:
            openNextWindowWithSelection();
            break;

        case ARROW_UP:
            arrowUp();
            break;

        case ARROW_DOWN:
            arrowDown();
            break;

        case PANIC:
            enterPanic(LoginActivity.dataManager);
            break;
        }
    }

    private void showToast(final String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
