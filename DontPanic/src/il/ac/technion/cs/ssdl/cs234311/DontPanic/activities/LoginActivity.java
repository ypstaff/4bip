package il.ac.technion.cs.ssdl.cs234311.DontPanic.activities;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.R;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.User;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.data.DontPanicData;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.data.DontPanicData.IsNewException;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.data.DontPanicData.UserNotRegisteredExeption;

import java.util.Locale;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

//@SuppressLint("NewApi")
public class LoginActivity extends Activity implements OnInitListener {

    public static DontPanicData dataManager = null;
    public static BluetoothAdapter mBluetoothAdapter;

    private static final int REQUEST_ENABLE_BT = 111;

    // tts
    public static TextToSpeech tts;
    public static boolean textToSpeechEnabled = false;
    private BroadcastReceiver broadcastReceiver;

    @Override
    public void onInit(final int status) {
        if (status == TextToSpeech.SUCCESS) {
            tts.setLanguage(Locale.US);
            tts.setSpeechRate((float) 0.8);
        } else {
            // TODO
        }
    }

    private void ensureDiscoverable() {
        // if(D) Log.d(TAG, "ensure discoverable");
        if (mBluetoothAdapter.getScanMode() != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            final Intent discoverableIntent = new Intent(
                    BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(
                    BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            startActivity(discoverableIntent);
        }
    }

    public void changeLang(final String lang) {
        final Locale myLocale = new Locale(lang);

        final Resources res = getResources();
        final DisplayMetrics dm = res.getDisplayMetrics();
        final Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);

        final Intent intent = getIntent();
        overridePendingTransition(0, 0);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();

        overridePendingTransition(0, 0);
        startActivity(intent);
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_window);

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // TODO add here the bluetooth check and do according to it
        final ToggleButton keyboardToggle = (ToggleButton) findViewById(R.id.log_keyboard_toggle);
        keyboardToggle.setAlpha(1f);
        keyboardToggle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (keyboardToggle.isChecked()) {
                    if (!mBluetoothAdapter.isEnabled()) {
                        final Intent enableIntent = new Intent(
                                BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
                    }
                    
                    ensureDiscoverable();
                }
            }
        });

        final Locale myLocale = getResources().getConfiguration().locale;
        final RadioGroup langSelect = (RadioGroup) findViewById(R.id.langSelector);

        final String curr_lang = myLocale.getLanguage();
        Log.i("language", curr_lang);
        if (curr_lang == "he" || curr_lang == "iw")
            langSelect.check(R.id.LangHe);
        else if (curr_lang == "ar")
            langSelect.check(R.id.LangAr);
        else
            langSelect.check(R.id.langEn);

        langSelect
                .setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(final RadioGroup group,
                            final int checkedId) {

                        switch (checkedId) {
                        case R.id.langEn:
                            changeLang("en");
                            break;
                        case R.id.LangHe:
                            changeLang("he");
                            break;
                        case R.id.LangAr:
                            changeLang("ar");
                            break;
                        default:
                            break;
                        }

                    }
                });

        final TextView registerScreen = (TextView) findViewById(R.id.link_to_register);
        // Listening to register new account link
        registerScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                // Switching to Register screen
                final Intent i = new Intent(getApplicationContext(),
                        RegisterActivity.class);
                startActivityForResult(i, 1);
            }
        });

        if (DontPanicData.isNew(getApplicationContext())) {
            // do nothing
        } else {
            try {
                dataManager = new DontPanicData(getApplicationContext());
            } catch (final IsNewException e) {
                // TODO Auto-generated catch block
                // shouldn't get here
                e.printStackTrace();
                return;
            }

            // restore user details from previous login
            final EditText user_name = (EditText) findViewById(R.id.log_user_name);
            final EditText email = (EditText) findViewById(R.id.log_user_email);
            final User user = dataManager.getUser();
            user_name.setText(user.username);
            email.setText(user.email);
        }

        // tts
        final ToggleButton ttsToggle = (ToggleButton) findViewById(R.id.tts_toggle);
        ttsToggle.setAlpha(1);
        ttsToggle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (ttsToggle.isChecked()) {
                    textToSpeechEnabled = true;
                    // tts = new TextToSpeech(this, this);
                } else {
                    textToSpeechEnabled = false;
                    // tts = null;
                }
            }
        });
        tts = new TextToSpeech(this, this);

        //
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("CLOSE_ALL");
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(final Context context, final Intent intent) {
                // close activity
                finish();
            }
        };
        registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();

        unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void onActivityResult(final int requestCode,
            final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
        case 1:
            if (resultCode == RESULT_OK) {
                final EditText user_name = (EditText) findViewById(R.id.log_user_name);
                final EditText email = (EditText) findViewById(R.id.log_user_email);
                user_name.setText(data.getStringExtra("username"));
                email.setText(data.getStringExtra("email"));
            }
            if (resultCode == RESULT_CANCELED) {
                // Write your code if there's no result
            }
            break;
        case REQUEST_ENABLE_BT:
            // When the request to enable Bluetooth returns
            if (resultCode == Activity.RESULT_OK) {

            } else
                // User did not enable Bluetooth or an error occurred
                // Log.d(TAG, "BT not enabled");
                showMessageAsToastInLength(R.string.bt_not_enabled_leaving,
                        Toast.LENGTH_SHORT);
            break;
        default:
            break;
        }
    }

    public void loginButtonClicked(final View view) {

        final EditText user_name = (EditText) findViewById(R.id.log_user_name);
        final EditText email = (EditText) findViewById(R.id.log_user_email);
        final String username = user_name.getText().toString();
        final String emailAddress = email.getText().toString();

        // check if all the required fields are properly assigned
        /*
        if (!RegisterActivity.isValidEmail(emailAddress)) {
            showMessageAsToastInLength(R.string.the_user_email_is_invalid,
                    Toast.LENGTH_SHORT);
            return;
        }
        */
        /*
         * the username is useless now so this check is commented
        if (username.matches("")) {
            showMessageAsToastInLength(
                    R.string.you_did_not_enter_a_user_name_please_fill_all_the_fileds,
                    Toast.LENGTH_SHORT);
            return;
        }
        */

        try {
            dataManager = new DontPanicData(getApplicationContext(), username,
                    emailAddress);
        } catch (final UserNotRegisteredExeption e) {
            // TODO Auto-generated catch block
            showMessageAsToastInLength(
                    R.string.login_failed_this_user_is_not_registered,
                    Toast.LENGTH_SHORT);
            e.printStackTrace();
            return;
        } catch (final Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return;
        }

        final Intent intent = new Intent(LoginActivity.this,
                MainWindowActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        // login activity should be closed when moving to the main activity
        // finish();
    }

    /**
     * @param msg
     *            - the message to show by toast
     * @param length
     *            - toast length
     */
    private void showMessageAsToastInLength(final int msg, final int length) {
        Toast.makeText(this, msg, length).show();
    }

}