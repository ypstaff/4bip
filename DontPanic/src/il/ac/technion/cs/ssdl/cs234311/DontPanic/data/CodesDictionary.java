package il.ac.technion.cs.ssdl.cs234311.DontPanic.data;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.DefaultCodes;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.IllegalShortcutLetter;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.ShortcutLetter;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Trie.Trie;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * A class to represent the codes for shortcuts saved by the user. giving easy
 * to use interface for adding, deleting and getting messages.
 * 
 * @author Shachar Nudler
 * 
 */
public class CodesDictionary {

    private static final int MIN_AVAILABLE_NUM = 8;
    private final Map<Integer, Shortcut> dic;
    private final Map<Integer, Shortcut> DeafultOperationsDic;
    private final Trie trie;

    /**
     * c'tor of the class. creates an empty dictionary.
     */
    public CodesDictionary() {
        dic = new TreeMap<Integer, Shortcut>();
        DeafultOperationsDic = new TreeMap<Integer, Shortcut>();
        trie = new Trie();

        DeafultOperationsDic.put(
                Integer.valueOf(DefaultCodes.getAddShortcut().code),
                DefaultCodes.getAddShortcut());
        DeafultOperationsDic.put(
                Integer.valueOf(DefaultCodes.getDeleteShortcut().code),
                DefaultCodes.getDeleteShortcut());
        DeafultOperationsDic.put(
                Integer.valueOf(DefaultCodes.getPanicShortcut().code),
                DefaultCodes.getPanicShortcut());
        /*for (final DeafaultOperations op : DeafaultOperations.values())
            DeafultOperationsDic.put(Integer.valueOf(op.getShortcut().code),
                    op.getShortcut());*/
    }

    /**
     * add a shortcut code to the dictionary. won't allow to add a shortcut with
     * same encoding as existing one, or that there exist a one letter code that
     * the entered code starts with the same letter.
     * 
     * @param _code
     *            - the shortcut to add.
     * @return true for success. false for an attempt to add an illegal code.
     */
    public boolean addCode(final Shortcut _code) {
        if (!isLleagal(_code))
            return false;

        dic.put(Integer.valueOf(_code.code), _code);
        trie.insert(_code.letteredCode);
        return true;

    }

    /**
     * returns a code
     * 
     * @param code
     * @return
     */
    public Shortcut getShortcut(final Integer code) {
        if (DeafultOperationsDic.containsKey(code))
            return DeafultOperationsDic.get(code);
        return dic.get(code);
    }

    /**
     * returns a code. boxing.
     * 
     * @param code
     * @return
     */
    public Shortcut getShortcut(final int code) {
        return getShortcut(Integer.valueOf(code));
    }

    /**
     * deletes a code.
     * 
     * @param _code
     */
    public void deleteCode(final List<ShortcutLetter> code) {
        dic.remove(Integer.valueOf(ShortcutTranslator.code2int(code)));
        trie.remove(code);
    }

    public boolean isremovable(final int _code) {
        // check if exists in the user defined codes
        return dic.containsKey(Integer.valueOf(_code));
    }

    private boolean isExist(final int _code) {
        // check if already exists
        if (dic.containsKey(Integer.valueOf(_code)))
            return true;
        // check if is a default operation
        if (DeafultOperationsDic.containsKey(Integer.valueOf(_code)))
            return true;

        return false;
    }

    /**
     * check if a given code is legal to add.
     * 
     * @param _code
     * @return
     */
    private boolean isLleagalCoded(final List<ShortcutLetter> _code) {
        if (_code.isEmpty())
            return false;
        if (isExist(ShortcutTranslator.code2int(_code)))
            return false;
        // check if one of the letters is a default operation
        for (final ShortcutLetter letter : _code)
            if (letter.isDefaultLetter())
                return false;
        return true;
    }

    /**
     * check if a given code is legal to add.
     * 
     * @param _code
     * @return
     */
    public boolean isLleagal(final List<boolean[]> _code) {
        try {
            return isLleagalCoded(ShortcutTranslator.word2code(_code));
        } catch (final IllegalShortcutLetter e) {
            return false;
        }
    }

    private static List<ShortcutLetter> wordListCopy(
            final List<ShortcutLetter> _word) {
        final List<ShortcutLetter> newWord = new ArrayList<ShortcutLetter>();

        for (final ShortcutLetter letter : _word)
            newWord.add(letter);

        return newWord;
    }

    private final List<List<ShortcutLetter>> getAllAvailableShortcuts(
            final List<ShortcutLetter> word, final int lettersToAdd)
            throws IllegalShortcutLetter {

        final List<List<ShortcutLetter>> availableWords = new ArrayList<List<ShortcutLetter>>();

        if (lettersToAdd == 0) {
            if (isLleagalCoded(word))
                availableWords.add(wordListCopy(word));
        } else
            for (final ShortcutLetter letter : ShortcutLetter.values()) {
                word.add(letter);
                availableWords.addAll(getAllAvailableShortcuts(word,
                        lettersToAdd - 1));
                word.remove(word.size() - 1);
            }

        return availableWords;
    }

    /**
     * method for the adding of a new shortcut. adding an interface that it will
     * be possible to iterate over all the available shortcuts.
     * 
     * @return - a list of all the shortcuts code that can be added to the
     *         dictionary.
     * @throws IllegalShortcutLetter
     */
    public List<List<ShortcutLetter>> getAllAvailabeShortcuts(
            final List<boolean[]> _initWord) throws IllegalShortcutLetter {
        final List<List<ShortcutLetter>> availableWords = new ArrayList<List<ShortcutLetter>>();
        final List<ShortcutLetter> word = ShortcutTranslator
                .word2code(_initWord);
        int i = 0;
        while (availableWords.size() < MIN_AVAILABLE_NUM)
            availableWords.addAll(getAllAvailableShortcuts(word, i++));

        return availableWords;
    }

    /**
     * check if a given code is legal to add.
     * 
     * @param _code
     * @return
     */
    public boolean isLleagal(final Shortcut _code) {
        return isLleagalCoded(_code.letteredCode);
    }

    /**
     * @return all saved shortcuts to iterate over.
     */
    public List<Shortcut> getAllShortcuts() {
        return new ArrayList<Shortcut>(dic.values());
    }

    public final List<Shortcut> codesStartingWith(final List<boolean[]> _word)
            throws IllegalShortcutLetter {
        final List<Shortcut> shortcuts = new ArrayList<Shortcut>();

        for (final List<ShortcutLetter> s : trie.getAllSons(ShortcutTranslator
                .word2code(_word)))
            shortcuts.add(getShortcut(ShortcutTranslator.code2int(s)));
        return shortcuts;
    }

}
