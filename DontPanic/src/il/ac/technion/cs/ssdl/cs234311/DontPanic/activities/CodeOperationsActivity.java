package il.ac.technion.cs.ssdl.cs234311.DontPanic.activities;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.DontPanicActivity;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.R;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.ColorArrayLocations;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.ShortcutLetter;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.classes.ShortcutDescription;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public abstract class CodeOperationsActivity extends DontPanicActivity {

    private Button redButton;
    private Button greenButton;
    private Button blueButton;
    private Button yellowButton;

    protected TextView title;

    private TextView enteredCode;

    protected ListView listView;
    protected List<Shortcut> listItems;
    protected ArrayAdapter<Shortcut> adapter;

    protected List<boolean[]> word = new ArrayList<boolean[]>();

    protected HashMap<String, List<ShortcutLetter>> stringToCode;

    protected abstract void initAdditionalViews();

    protected abstract void additionalUpdates();

    protected abstract void setTitle();

    protected abstract void openMainWindow();

    /**
     * execute the shortcut entered so far, when the enter code pressed
     * 
     * @throws IOException
     */
    protected abstract void enter();

    @Override
    protected void initTouchListeners() {
        super.initTouchListeners();

        yellowButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.YELLOW));
        blueButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.BLUE));
        greenButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.GREEN));
        redButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.RED));
    }

    @Override
    protected void initViews() {
        super.initViews();

        redButton = (Button) findViewById(R.id.RedButton);
        greenButton = (Button) findViewById(R.id.GreenButton);
        blueButton = (Button) findViewById(R.id.BlueButton);
        yellowButton = (Button) findViewById(R.id.YellowButton);

        title = (TextView) findViewById(R.id.title);

        enteredCode = (TextView) findViewById(R.id.code_entered);
        enteredCode.setText(getString(R.string.code_entered));

        initAdditionalViews();
    }

    protected void updateUI() {
        updateClearButton();
        updateEnteredCode();
        additionalUpdates();
    }

    protected void clearEnteredCode() {
        word.clear();
    }

    private void otherWord(final boolean[] code) {
        if (!ShortcutTranslator.isLegal(code))
            return;

        word.add(code);
        updateUI();
    }

    private void updateClearButton() {
        if (!word.isEmpty()) {
            redButton.setText(getString(R.string.code_clear));
            redButton.setCompoundDrawablesWithIntrinsicBounds(0,
                    R.drawable.ic_action_backspace, 0, 0);
        } else {
            redButton.setText(getString(R.string.window_back));
            redButton.setCompoundDrawablesWithIntrinsicBounds(0,
                    R.drawable.ic_action_back, 0, 0);
        }
    }

    private void updateEnteredCode() {
        ShortcutDescription.updateDescriptions(this, word,
                R.id.code_entered_description);
        /*try {
            enteredCode.setText(getString(R.string.code_entered) + " "
                    + printWord(ShortcutTranslator.word2code(word)));
        } catch (IllegalShortcutLetter e) {
            e.printStackTrace();
        }*/
    }

    private void uncheckList() {
        listView.setItemChecked(AdapterView.INVALID_POSITION, true);
    }

    private void backSpaceEnteredCode() {
        word.remove(word.size() - 1);
    }

    /**
     * clear the the last word entered when the clear code pressed. go back to
     * previous window if no code pressed.
     */
    private void clearBack() {
        if (word.isEmpty()) {
            finish();
            return;
        }

        uncheckList();
        backSpaceEnteredCode();
        updateUI();
    }

    /**
     * scroll up one item in the list view when the arrowDown is pressed
     */
    private void arrowUp() {
        if (listItems.isEmpty())
            return;

        final int index = listView.getCheckedItemPosition();

        if (index == AdapterView.INVALID_POSITION)
            return;

        if (index == 0) {
            uncheckList();
            return;
        }
        listView.setItemChecked(index - 1, true);
        listView.smoothScrollToPositionFromTop(index - 1,
                listView.getHeight() / 2);
    }

    /**
     * scroll down one item in the list view when the arrowDown is pressed
     */
    private void arrowDown() {
        if (listItems.isEmpty())
            return;

        int index = listView.getCheckedItemPosition();

        if (index == AdapterView.INVALID_POSITION)
            index = -1;

        if (index + 1 == listView.getCount()) {
            listView.setItemChecked(0, true);
            listView.smoothScrollToPositionFromTop(0, 0, 0);
            return;
        }
        listView.setItemChecked(index + 1, true);
        listView.smoothScrollToPositionFromTop(index + 1,
                listView.getHeight() / 2);
    }

    /**
     * all the operations supported in this activity
     */
    private enum OPERATIONS {
        ENTER, ClEAR_BACK, PANIC, OTHER_CODE, ARROW_UP, ARROW_DOWN
    }

    /**
     * matching input code to OPERATIONS constant
     * 
     * @param code
     *            - boolean array that represent the buttons pressed or the
     *            array given by the enterCode method
     * @return - the OPERATIONS constant that is matching the given input
     * @see DontPanicActivity.enterCode
     */
    private OPERATIONS getOperationFromCode(final boolean[] code) {
        final boolean green_pressed = code[ColorArrayLocations.GREEN.ordinal()];
        final boolean yellow_pressed = code[ColorArrayLocations.YELLOW
                .ordinal()];
        final boolean red_pressed = code[ColorArrayLocations.RED.ordinal()];
        final boolean blue_pressed = code[ColorArrayLocations.BLUE.ordinal()];

        // panic pressed
        if (red_pressed && blue_pressed && green_pressed && yellow_pressed)
            return OPERATIONS.PANIC;

        // panic pressed
        if (red_pressed && blue_pressed && green_pressed && yellow_pressed)
            return OPERATIONS.PANIC;

        // enter pressed
        if (green_pressed && !(blue_pressed || red_pressed || yellow_pressed))
            return OPERATIONS.ENTER;

        // clear/back pressed
        if (red_pressed && !(blue_pressed || green_pressed || yellow_pressed))
            return OPERATIONS.ClEAR_BACK;

        // arrow up pressed
        if (blue_pressed && !(green_pressed || red_pressed || yellow_pressed))
            return OPERATIONS.ARROW_UP;

        // arrow down pressed
        if (yellow_pressed && !(blue_pressed || red_pressed || green_pressed))
            return OPERATIONS.ARROW_DOWN;

        // else
        return OPERATIONS.OTHER_CODE;
    }

    @Override
    public void enterCode(final boolean[] code) {

        switch (getOperationFromCode(code)) {
        case ARROW_DOWN:
            arrowDown();
            break;
        case ARROW_UP:
            arrowUp();
            break;
        case ClEAR_BACK:
            clearBack();
            break;
        case ENTER:
            enter();
            break;
        case OTHER_CODE:
            otherWord(code);
            break;
        case PANIC:
            enterPanic(LoginActivity.dataManager);
            break;
        }
    }

    protected String printWord(final List<ShortcutLetter> word) {
        String printed = "";

        for (final ShortcutLetter l : word) {
            final boolean[] bword = new boolean[4];
            printed += "[";

            switch (l) {
            case FOUR:
                bword[3] = true;
                break;
            case FOUR_ONE:
                bword[3] = bword[0] = true;
                break;
            case ONE:
                bword[0] = true;
                break;
            case ONE_TWO:
                bword[0] = bword[1] = true;
                break;
            case THREE:
                bword[2] = true;
                break;
            case THREE_FOUR:
                bword[2] = bword[3] = true;
                break;
            case TWO:
                bword[1] = true;
                break;
            case TWO_THREE:
                bword[1] = bword[2] = true;
                break;
            default:

            }

            if (bword[0] == true)
                printed = printed + " " + "R";
            if (bword[1] == true)
                printed = printed + " " + "Y";
            if (bword[2] == true)
                printed = printed + " " + "B";
            if (bword[3] == true)
                printed = printed + " " + "G";
            printed += "] ";
        }

        return printed;
    }

    protected void showToast(final String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    protected void showLongToast(final String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }
}
