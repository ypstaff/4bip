/*
 * created by: 
 * or maayan
 * or81322@gmail.com
 * 
 * date: 6.11.13 
 */

package il.ac.technion.cs.ssdl.cs234311.DontPanic.phoneOperations;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.Music.MediaPlayerClass;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.activities.CallerActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.WindowManager;

public class IncomingCallReceiver extends BroadcastReceiver {
    private static final String TAG = "Phone call";
    private String number;
    private Context context;

    private class CallerActivation implements Runnable {

        @Override
        public void run() {
            try {
                synchronized (this) {
                    Log.d(TAG, "Waiting for 0.5 sec ");
                    this.wait(500);
                }
            } catch (final Exception e) {
                Log.d(TAG, "Exception while waiting !!");
                e.printStackTrace();
            } finally {
                
                MediaPlayerClass player = MediaPlayerClass.MediaPlayerGetInstance();
                if (player != null)
                    player.doPause();

                final Intent caller = new Intent(context, CallerActivity.class);
                caller.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                caller.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                // TODO check if needed
                // caller.addFlags(Intent.FLAG_ACTIVITY_NO_USER_ACTION);
                caller.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
                caller.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

                if (number != null)
                    caller.putExtra("PHONE_NUMBER", number);

                caller.putExtra("IS_INCOMING_CALL", true);
                context.startActivity(caller);
            }
        }
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {

        if (!intent.getAction().equals("android.intent.action.PHONE_STATE"))
            return;// shouldn't happen because of the intent-filter in the
                   // manifest file

        this.context = context;
        final String state = intent
                .getStringExtra(TelephonyManager.EXTRA_STATE);

        if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
            number = intent
                    .getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);

            new Thread(new CallerActivation()).start();
        }

        return;
    }
}