package il.ac.technion.cs.ssdl.cs234311.DontPanic.Trie;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.ShortcutLetter;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

public class Trie {

    private final TrieNode<ShortcutLetter> root;

    private int numberEntries;

    public Trie() {
        root = new TrieNode<ShortcutLetter>(null); // "empty value", usually
                                                   // some "null" value or
                                                   // "empty string"
        numberEntries = 0;
    }

    public void insert(final List<ShortcutLetter> values) {
        TrieNode<ShortcutLetter> current = root;
        if (values != null) {
            if (values.size() == 0)
                current.setEndMarker(true);
            for (int i = 0; i < values.size(); i++) {
                final TrieNode<ShortcutLetter> child = current.findChild(values
                        .get(i));
                if (child != null)
                    current = child;
                else
                    current = current.addChild(values.get(i));
                if (i == values.size() - 1)
                    if (!current.isEndMarker()) {
                        current.setEndMarker(true);
                        numberEntries++;
                    }
            }
        } else
            System.out.println("Not adding anything");
    }

    private TrieNode<ShortcutLetter> get(final List<ShortcutLetter> values) {
        TrieNode<ShortcutLetter> current = root;
        for (int i = 0; i < values.size(); i++) {
            if (current.findChild(values.get(i)) == null)
                return null;
            current = current.findChild(values.get(i));
        }

        return current;
    }

    public void remove(final List<ShortcutLetter> values) {
        final TrieNode<ShortcutLetter> curr = get(values);
        if (curr != null && curr.isEndMarker()) {
            curr.setEndMarker(false);
            numberEntries--;
        }
    }

    public boolean search(final List<ShortcutLetter> values) {
        final TrieNode<ShortcutLetter> current = get(values);

        /*
         * Array T[] values found in ObjectTrie. Must verify that the "endMarker" flag
         * is true
         */

        return current != null && current.isEndMarker();
    }

    private static <T> List<T> listDeepCopy(final List<T> l) {
        final List<T> newList = new ArrayList<T>();
        for (final T t : l)
            newList.add(t);

        return newList;

    }

    public List<List<ShortcutLetter>> getAllSons(
            final List<ShortcutLetter> values) {
        TrieNode<ShortcutLetter> current = get(values);
        final List<List<ShortcutLetter>> list = new ArrayList<List<ShortcutLetter>>();

        if (current == null)
            return list;

        // now current is the node from which to search sons.
        final Queue<TrieNode<ShortcutLetter>> nodesQueue = new ArrayDeque<TrieNode<ShortcutLetter>>();
        final Queue<List<ShortcutLetter>> wordsQueue = new ArrayDeque<List<ShortcutLetter>>();

        nodesQueue.add(current);
        wordsQueue.add(listDeepCopy(values));

        // BFS on all the available words
        while (nodesQueue.size() > 0) {
            current = nodesQueue.remove();
            final List<ShortcutLetter> currentWord = wordsQueue.remove();

            if (current.isEndMarker())
                list.add(listDeepCopy(currentWord));

            for (final ShortcutLetter l : ShortcutLetter.values()) {
                final TrieNode<ShortcutLetter> child = current.findChild(l);
                if (child != null) {
                    // add the child to the queue
                    nodesQueue.add(child);
                    final List<ShortcutLetter> childWord = listDeepCopy(currentWord);
                    childWord.add(l);
                    wordsQueue.add(childWord);
                }

            }
        }
        return list;
    }

    public int numberEntries() {
        return numberEntries;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("number entries: ");
        sb.append(numberEntries);

        return sb.toString();

    }
}
