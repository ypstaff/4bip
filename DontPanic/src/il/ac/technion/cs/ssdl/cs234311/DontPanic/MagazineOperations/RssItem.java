package il.ac.technion.cs.ssdl.cs234311.DontPanic.MagazineOperations;

public class RssItem {
    public String title = "";
    public String date = "";
    public String link = "";
    public String description = "";
}