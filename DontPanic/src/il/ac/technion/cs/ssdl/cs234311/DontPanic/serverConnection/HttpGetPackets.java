package il.ac.technion.cs.ssdl.cs234311.DontPanic.serverConnection;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.R;
import java.io.IOException;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

public class HttpGetPackets extends AsyncTask<String, Void, String> {

    Context context;

    public HttpGetPackets(final Context _appContext) {
        context = _appContext;
    }

    @Override
    protected String doInBackground(final String... strs) {
        final HttpClient httpclient = new DefaultHttpClient();

        String serverUrl = ServerMessages.SERVER_URL;
        serverUrl += strs[0]; // adding servlet name
        serverUrl += "?" + PacketsParsingServices.parseClientParameters(strs);

        final HttpGet httpget = new HttpGet(serverUrl);
        String stringResponse = null;
        try {
            final HttpResponse response = httpclient.execute(httpget);
            Log.i("HttpGetPacket", "got response. status = ");
            Log.i("HttpGetPacket",
                    Integer.toString(response.getStatusLine().getStatusCode()));

            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                Log.i("HttpGetPacket", "response status = OK ");

                stringResponse = PacketsParsingServices
                        .convertResponseToString(response.getEntity()
                                .getContent());
                Log.i("HttpGetPacket", stringResponse);

            }

        } catch (final ClientProtocolException e) {
            e.printStackTrace();
        } catch (final IOException e) {
            e.printStackTrace();
        } catch (final Exception e) {
            e.printStackTrace();
        }
        return stringResponse;
    }

    @Override
    protected void onPostExecute(final String result) {
        if ((result == null || result.isEmpty()) && context != null)
            Toast.makeText(context,
                    context.getString(R.string.had_internet_problems),
                    Toast.LENGTH_SHORT).show();
    }

}
