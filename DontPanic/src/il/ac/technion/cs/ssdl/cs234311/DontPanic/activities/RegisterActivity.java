/**
 * @date Dec 24, 2013 
 * @author Or
 * @email or81322@gmail.com
 */
package il.ac.technion.cs.ssdl.cs234311.DontPanic.activities;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.R;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.data.DontPanicData;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.data.DontPanicData.UserAlreadyRegisteredExeption;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 *
 */
public class RegisterActivity extends Activity {
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.registration_window);

        final TextView loginScreen = (TextView) findViewById(R.id.link_to_login);

        // Listening to Login Screen link
        loginScreen.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View arg0) {
                // Closing registration screen
                // Switching to Login Screen/closing register screen
                final Intent returnIntent = new Intent();
                setResult(RESULT_CANCELED, returnIntent);
                finish();
            }
        });
    }

    public final static boolean isValidEmail(final CharSequence target) {
        if (target == null)
            return false;
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public void registerButtonClicked(final View view) {

        final EditText user_email = (EditText) findViewById(R.id.reg_user_email);
        final String userEmailAddress = user_email.getText().toString();
        final EditText user_name = (EditText) findViewById(R.id.reg_user_name);
        final String userName = user_name.getText().toString();
        final EditText supervisor_email = (EditText) findViewById(R.id.reg_supervisor_email);
        final String supervisorEmailAddress = supervisor_email.getText()
                .toString();
        final EditText supervisor_phone = (EditText) findViewById(R.id.reg_supervisor_phone);
        final String supervisorPhoneNumber = supervisor_phone.getText()
                .toString();
        // final EditText supervisor_name = (EditText)
        // findViewById(R.id.reg_supervisor_name);
        // final String supervisorName = supervisor_name.getText().toString();

        // check if all the required fields are properly assigned
        if (!isValidEmail(userEmailAddress)) {
            showMessageAsToastInLength(R.string.the_user_email_is_invalid,
                    Toast.LENGTH_SHORT);
            return;
        }
        if (userName.matches("")) {
            showMessageAsToastInLength(
                    R.string.you_did_not_enter_a_user_name_please_fill_all_the_fileds,
                    Toast.LENGTH_SHORT);
            return;
        }
        if (!isValidEmail(supervisorEmailAddress)) {
            showMessageAsToastInLength(
                    R.string.the_supervisor_email_is_invalid,
                    Toast.LENGTH_SHORT);
            return;
        }
        if (supervisorPhoneNumber.matches("")) {
            showMessageAsToastInLength(
                    R.string.you_did_not_enter_a_supervisor_phone_number_please_fill_all_the_fileds,
                    Toast.LENGTH_SHORT);
            return;
        }

        if (DontPanicData.isUserRegistered(getApplicationContext(),
                userEmailAddress)) {
            showMessageAsToastInLength(
                    R.string.this_user_is_already_registered, Toast.LENGTH_LONG);
            return;
        }
        try {
            LoginActivity.dataManager = new DontPanicData(
                    getApplicationContext(), userName, userEmailAddress,
                    supervisorEmailAddress, supervisorPhoneNumber);
        } catch (final UserAlreadyRegisteredExeption e) {
            // shouldn't get here because this block called when the user isn't
            // registered
            e.printStackTrace();
            showMessageAsToastInLength(R.string.registration_failed,
                    Toast.LENGTH_LONG);
            finish();
        } catch (final Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Toast.makeText(getApplicationContext(),
                    R.string.registration_failed_check_your_connectivity,
                    Toast.LENGTH_LONG).show();
            finish();
        }
        showMessageAsToastInLength(R.string.this_user_successfully_registered,
                Toast.LENGTH_LONG);

        final Intent returnIntent = new Intent();
        returnIntent.putExtra("username", userName);
        returnIntent.putExtra("email", userEmailAddress);
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    /**
     * @param msg
     *            - the message to show by toast
     * @param length
     *            - toast length
     */
    private void showMessageAsToastInLength(final int msg, final int length) {
        Toast.makeText(this, msg, length).show();
    }
}