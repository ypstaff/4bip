package il.ac.technion.cs.ssdl.cs234311.DontPanic.Tests.Robotium;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.R;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.ColorArrayLocations;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Music.MusicServices;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.activities.MusicActivity;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.data.ShortcutsParser;

import java.util.List;
import java.util.Random;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.ListView;
import android.widget.TextView;
import com.robotium.solo.Solo;

/**
 * 
 * @author Ameer Assi
 * @date 18.12.2013
 * @passed 06.04.2014
 */
public class MusicTest extends ActivityInstrumentationTestCase2<MusicActivity> {

    private Solo solo;

    public MusicTest() {
        super(MusicActivity.class);

    }

    @Override
    public void setUp() throws Exception {
        // setUp() is run before a test case is started.
        // This is where the solo object is created.

    }

    @Override
    public void tearDown() throws Exception {
        // tearDown() is run after a test case has finished.
        // finishOpenedActivities() will finish all the activities that have
        // been opened during the test execution.
        solo.sleep(5000);
        solo.finishOpenedActivities();
        solo.sleep(2000);
    }

    public void autoPlay() throws Throwable {
        final ListView songsView = (ListView) solo.getView(R.id.songsListView);
        final TextView songName = (TextView) solo.getView(R.id.SongText);
        if (songsView.getCount() == 0)
            return;
        final AudioManager manager = (AudioManager) getInstrumentation()
                .getContext().getSystemService(Context.AUDIO_SERVICE);
        // verify starts with playing.
        assertTrue(manager.isMusicActive());
        if (songsView.getCount() == 1)
            return;
        String currsong = (String) songName.getText();
        // runs the next song
        solo.clickOnView(solo.getView(R.id.GreenButton));
        assertTrue(manager.isMusicActive());
        assertFalse(currsong.equals(songName.getText()));
        currsong = (String) songName.getText();
        // runs the next song
        solo.clickOnView(solo.getView(R.id.GreenButton));
        assertTrue(manager.isMusicActive());
        assertFalse(currsong.equals(songName.getText()));
        currsong = (String) songName.getText();
        // stops the play of the music
        solo.clickOnView(solo.getView(R.id.YellowButton));
        assertFalse(manager.isMusicActive());
        // replay the music
        solo.clickOnView(solo.getView(R.id.YellowButton));
        assertTrue(manager.isMusicActive());
        assertTrue(currsong.equals(songName.getText()));
        solo.clickOnView(solo.getView(R.id.RedButton));
        assertTrue(manager.isMusicActive());

        final Random rand = new Random();
        final int y = rand.nextInt(songsView.getCount() / 2);
        for (int i = 0; i < y && i < 10; i++) {
            // runs the previous song
            solo.clickOnView(solo.getView(R.id.RedButton));
            // assertFalse(currsong.equals(songName.getText())); // found in
            // shachar tablet that there is two songs with the same album and
            // name.
            currsong = (String) songName.getText();
        }
        solo.clickOnView(solo.getView(R.id.BlueButton));
        assertFalse(manager.isMusicActive());
        solo.clickOnView(solo.getView(R.id.GreenButton));
        assertTrue(manager.isMusicActive());
        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                final MusicActivity music = (MusicActivity) solo
                        .getCurrentActivity();
                music.shuffle();
            }
        });
    }

    public void testAllSongs() throws Throwable {
        final Intent i = new Intent();
        i.putExtra("Album_Name", ShortcutsParser.ALL_SONGS_ALBUM_NAME);
        setActivityIntent(i);
        solo = new Solo(getInstrumentation(), getActivity());
        autoPlay();
    }

    public void testFirstAlbum() throws Throwable {
        final Context ctx = getInstrumentation().getContext();
        final List<String> albums = MusicServices.GetAllAlbums(ctx);
        final Intent i = new Intent();
        i.putExtra("Album_Name", albums.get(0));
        setActivityIntent(i);
        solo = new Solo(getInstrumentation(), getActivity());
        autoPlay();

    }

    public void testLastAlbum() throws Throwable {
        final Context ctx = getInstrumentation().getContext();
        final List<String> albums = MusicServices.GetAllAlbums(ctx);
        final Intent i = new Intent();
        i.putExtra("Album_Name", albums.get(albums.size() - 1));
        setActivityIntent(i);
        solo = new Solo(getInstrumentation(), getActivity());
        autoPlay();
        solo.finishOpenedActivities();

    }

    public void testMiddleAlbum() throws Throwable {
        final Context ctx = getInstrumentation().getContext();
        final List<String> albums = MusicServices.GetAllAlbums(ctx);
        final Intent i = new Intent();
        i.putExtra("Album_Name", albums.get(albums.size() / 2));
        setActivityIntent(i);
        solo = new Solo(getInstrumentation(), getActivity());
        autoPlay();
        solo.finishOpenedActivities();

    }

    public void testRandomAlbum() throws Throwable {
        final Context ctx = getInstrumentation().getContext();
        final List<String> albums = MusicServices.GetAllAlbums(ctx);
        final Intent i = new Intent();
        final Random rand = new Random();
        i.putExtra("Album_Name", albums.get(rand.nextInt(albums.size() - 1)));
        setActivityIntent(i);
        solo = new Solo(getInstrumentation(), getActivity());
        autoPlay();
        solo.finishOpenedActivities();

    }

    public void testFirstArtists() throws Throwable {
        final Context ctx = getInstrumentation().getContext();
        final List<String> artists = MusicServices.GetAllArtists(ctx);
        final Intent i = new Intent();
        i.putExtra("Artist_Name", artists.get(0));
        setActivityIntent(i);
        solo = new Solo(getInstrumentation(), getActivity());
        autoPlay();

    }

    public void tesLastArtists() throws Throwable {
        final Context ctx = getInstrumentation().getContext();
        final List<String> artists = MusicServices.GetAllArtists(ctx);
        final Intent i = new Intent();
        i.putExtra("Artist_Name", artists.get(artists.size() - 1));
        setActivityIntent(i);
        solo = new Solo(getInstrumentation(), getActivity());
        autoPlay();

    }

    public void testMiddleArtists() throws Throwable {
        final Context ctx = getInstrumentation().getContext();
        final List<String> artists = MusicServices.GetAllArtists(ctx);
        final Intent i = new Intent();
        i.putExtra("Artist_Name", artists.get(artists.size() / 2));
        setActivityIntent(i);
        solo = new Solo(getInstrumentation(), getActivity());
        autoPlay();

    }

    public void testRandomArtists() throws Throwable {
        final Context ctx = getInstrumentation().getContext();
        final List<String> artists = MusicServices.GetAllArtists(ctx);
        final Random rand = new Random();
        final Intent i = new Intent();
        i.putExtra("Artist_Name", artists.get(rand.nextInt(artists.size() - 1)));
        setActivityIntent(i);
        solo = new Solo(getInstrumentation(), getActivity());
        autoPlay();

    }

    void makeCommand(final boolean y, final boolean g, final boolean b,
            final boolean r, final boolean[] command) {
        command[ColorArrayLocations.YELLOW.ordinal()] = y;
        command[ColorArrayLocations.GREEN.ordinal()] = g;
        command[ColorArrayLocations.BLUE.ordinal()] = b;
        command[ColorArrayLocations.RED.ordinal()] = r;
    }

    public void testShuffleSongs() throws Throwable {

        final AudioManager manager = (AudioManager) getInstrumentation()
                .getContext().getSystemService(Context.AUDIO_SERVICE);
        final Intent i = new Intent();
        i.putExtra("Album_Name", ShortcutsParser.ALL_SONGS_ALBUM_NAME);
        setActivityIntent(i);
        final MusicActivity Music = getActivity();
        solo = new Solo(getInstrumentation(), Music);
        final boolean shuffleCommand[] = new boolean[4];
        makeCommand(true, false, true, false, shuffleCommand);
        final boolean yellowCommand[] = new boolean[4];
        makeCommand(true, false, false, false, yellowCommand);
        final boolean blueCommand[] = new boolean[4];
        makeCommand(false, false, true, false, blueCommand);
        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                Music.enterCode(shuffleCommand);
                Music.enterCode(blueCommand);
                Music.enterCode(shuffleCommand);
                assertTrue(manager.isMusicActive());
                Music.enterCode(yellowCommand);
                Music.enterCode(shuffleCommand);
                Music.enterCode(shuffleCommand);
                Music.enterCode(yellowCommand);
                assertFalse(manager.isMusicActive());
                Music.enterCode(shuffleCommand);
                assertTrue(manager.isMusicActive());
            }
        });
    }

}
