var xmlhttp;
function loadXMLDoc()
{
//var URL_ADDRESS="http://four-buttons-interface.appspot.com/" ;
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
{
  if (xmlhttp.readyState==4)
  {
	if (xmlhttp.status==200)
    {
		parseXML(document.getElementById("statDiv"), xmlhttp.responseXML);
    }
	else
	{
		var x = "data was no retrieved from server";
		if(xmlhttp.status==444){ x=x+", because there aren't any";}
		else {x=x+", server status =" + xmlhttp.status;}
		alert(x);
    }
  }
}
var reqAddress ="talserver?message=GetStatisticsXML";
xmlhttp.open("GET",reqAddress,true);
xmlhttp.send();
}

function parseXML(tableElement, xmlFile) {
	var txt="<table border='1' >";
	var message = xmlFile.documentElement.getElementsByTagName("message");
	var elements = xmlFile.getElementsByTagName("*");

	for (i=0;i<elements.length;i++)
    {
		if(elements[i]!= xmlhttp.responseXML.documentElement)
		{
		 	var str = elements[i].nodeName.replace(/_/g, " ");
			txt= txt + "<tr>"
			txt=txt + "<th>" + str + ": </th>";
			txt=txt + "<td>" + elements[i].firstChild.nodeValue + "</td>"
			txt= txt + "</tr>"
		}
    }
	 tableElement.innerHTML=txt;
}

function deleteStatistics()
{
	var r=confirm("Delete all statistics content ?");
	if (r==true)
	  {
		mydelete();
	  }
	else
	  {
		alert("You pressed Cancel!");
	  }
}
function mydelete()
{
 var httpReq;
  var requestUrl ="talserver?message=removeall";
  //make the object
  if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  	  httpReq=new XMLHttpRequest();
  }
  else
  {// code for IE6, IE5
  	  httpReq=new ActiveXObject("Microsoft.XMLHTTP");
  }
  //set function for result
  httpReq.onreadystatechange=function()
  {
    if (httpReq.readyState==4)
    {
    	if (httpReq.status==200)
        {
    		alert("Statisctics were deleted");
			document.getElementById("statDiv").innerHTML="<table border='1' ><tr><th>Statistics Deleted</th></tr>";
        }
    	else
    	{	
			var x ="Statisctics were NOT deleted";
			if(httpReq.status==444) x = x+", because there are non on the server";
			alert(x);
      	}
    }
  }
  //send request
  httpReq.open("GET",requestUrl,true);
  httpReq.send();
}

function exportToCsv() {
var xmlText= xmlhttp.responseText;
var blob = new Blob([xmlText], {type: "text/xml;charset=utf-8"});
date=new Date();
var fileName = 'statistics ' + date.getFullYear() +'-' + (date.getMonth()+1)+'-'+date.getDate() +' ' + date.getHours()+'h' + date.getMinutes()+'m';
saveAs(blob, fileName);
}