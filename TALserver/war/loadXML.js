	var reader;
	var fileName;
	if (window.File && window.FileReader && window.FileList && window.Blob) {
		} else {
		  alert('The File APIs are not fully supported in this browser.');
		}
	
	document.getElementById('files').addEventListener('change', handleFileSelect, false);
	
	function handleFileSelect(evt) {
    var files = evt.target.files; // FileList object
	var f = files[0];
	fileName=f.name;
	reader = new FileReader();
	reader.readAsText(f);
  }

  
function exportToCsv() {

	var doc = document.implementation.createHTMLDocument("");
	doc.open();
	doc.write(reader.result);
	doc.close();
	var elements = doc.getElementsByTagName("*");
	
	var txt="<h2 id=\"statistics_header\"><u> Statistics of use table</u></br> File: " + fileName + " </h2> </br> <table border='1' >";
	
	for (i=0;i<elements.length;i++)
	{
		if(elements[i].nodeName != "HEAD" && elements[i].nodeName != "BODY"&& elements[i].nodeName != "STATISTICS"&& elements[i].nodeName != "HTML" )
		{
			var str = elements[i].nodeName.replace(/_/g, " ");
			txt= txt + "<tr>"
			txt=txt + "<th>" + str + ": </th>";
			txt=txt + "<td>" + elements[i].firstChild.nodeValue + "</td>"
			txt= txt + "</tr>"
		}
	}
	document.getElementById('list').innerHTML =txt;
	
}

var button
function doneLoad(){
 button= document.getElementById('b');
button.addEventListener('click', exportToCsv);
}