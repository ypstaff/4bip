package il.ac.technion.cs.ssdl.cs234311.yp09.utilities;

/**
 * @author Tal class representing the statistics that will be presented
 */
public class UsageData {
  /**
   * snake average daily use
   */
  public Double snake_average_daily_use_in_min;
  /**
   * the app's daily use average
   */
  public Double chat_app_average_daily_use_in_min;
  /**
   * the average time it takes to type a sentence in the app
   */
  public Double time_for_sentence_average_in_sec;
  /**
   * the average time it takes to type a word in the app
   */
  public Double time_for_word_average_in_sec;
  /**
   * the average time it takes to type a character in the app in milliseconds
   */
  public Double time_for_char_average_in_millisec;
  /**
   * the average number of time a char is deleted daily
   */
  public Double deleted_chars_daily_average;
  /**
   * average number of uses in AC a day
   */
  public Double autocomplete_uses_daily_average;
  /**
   * average number of facebook messages sent daily
   */
  public Double facebook_messages_daily_average;
  /**
   * average number of google messages sent daily
   */
  public Double google_messages_daily_average;
  /**
   * average number of SMS messages sent daily
   */
  public Double sms_messages_daily_average;
  /**
   * average number of daily clicks on green button
   */
  public Double green_clicks_daily_average;
  /**
   * average number of daily clicks on yellow button
   */
  public Double orange_clicks_daily_average;
  /**
   * average number of daily clicks on red button
   */
  public Double red_clicks_daily_average;
  /**
   * average number of daily clicks on blue button
   */
  public Double blue_clicks_daily_average;
  /**
   * average number of daily clicks on blue_orange combination
   */
  public Double blue_orange_clicks_daily_average;
  /**
   * average number of daily clicks on yellow_green combination
   */
  public Double orange_green_clicks_daily_average;
  /**
   * average number of daily clicks on green_red combination
   */
  public Double green_red_clicks_daily_average;
  /**
   * average number of daily clicks on red_blue combination
   */
  public Double red_blue_clicks_daily_average;
  /**
   * average number of daily LONG clicks on blue button
   */
  public Double long_blue_clicks_daily_average;
  /**
   * average number of daily LONG clicks on green button
   */
  public Double long_green_clicks_daily_average;
  /**
   * average number of daily LONG clicks on yellow button
   */
  public Double long_orange_clicks_daily_average;
  /**
   * average number of daily LONG clicks on red button
   */
  public Double long_red_clicks_daily_average;

}
