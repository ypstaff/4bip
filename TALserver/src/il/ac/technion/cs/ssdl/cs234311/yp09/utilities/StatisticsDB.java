package il.ac.technion.cs.ssdl.cs234311.yp09.utilities;

import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.Date;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Transaction;

/**
 * @author Tal the statistics database main class, handles data processing and
 *         storage on GAE
 */
public class StatisticsDB {
  private static final String IsDBContainsDefaults = "IsDBContainsDefaults";
  /**
   * DEBUG_MODE - for testing
   */
  public static boolean DEBUG_MODE = false;
  /**
   * for testing - simulates passing of days
   */
  public static Integer DEBUG_MODE_DAYS_TO_JUMP = Integer.valueOf(0);

  private static String lastDaySnakeUseTime = "lastDaySnakeUseTime";

  private static String[][] usageDataFieldsAndTherePropertiesInDB = {
      { "snake_average_daily_use_in_min", "snake_average_daily_use" },
      { "chat_app_average_daily_use_in_min", "chat_app_average_daily_use" },
      { "time_for_sentence_average_in_sec", "time_for_sentence_average" },
      { "time_for_word_average_in_sec", "time_for_word_average" },
      { "time_for_char_average_in_millisec", "time_for_char_average" },
      { "deleted_chars_daily_average", "deleted_chars_daily_average" },
      { "autocomplete_uses_daily_average", "autocomplete_uses_daily_average" },
      { "facebook_messages_daily_average", "facebook_messages_daily_average" },
      { "google_messages_daily_average", "google_messages_daily_average" },
      { "sms_messages_daily_average", "sms_messages_daily_average" },
      { "green_clicks_daily_average", "green_clicks_daily_average" },
      { "orange_clicks_daily_average", "orange_clicks_daily_average" },
      { "red_clicks_daily_average", "red_clicks_daily_average" },
      { "blue_clicks_daily_average", "blue_clicks_daily_average" },
      { "blue_orange_clicks_daily_average", "blue_orange_clicks_daily_average" },
      { "orange_green_clicks_daily_average",
          "orange_green_clicks_daily_average" },
      { "green_red_clicks_daily_average", "green_red_clicks_daily_average" },
      { "red_blue_clicks_daily_average", "red_blue_clicks_daily_average" },
      { "long_blue_clicks_daily_average", "long_blue_clicks_daily_average" },
      { "long_green_clicks_daily_average", "long_green_clicks_daily_average" },
      { "long_orange_clicks_daily_average", "long_orange_clicks_daily_average" },
      { "long_red_clicks_daily_average", "long_red_clicks_daily_average" } };

  // first entry is the name in usageData
  private static int D_IN_USAGEDATA = 0;
  // second is the name of the property that contains the data for the last
  // day
  private static int D_PROP_FOR_LAST_DAY = 1;
  // Third is the name of corresponding field in Statistics
  private static int D_FIELD_IN_STATIS = 2;
  private static String[][] dailyAvergeNames = {
      { "chat_app_average_daily_use", "lastDayUsingChatAppTime",
          "timeUsingTheApp" },
      { "deleted_chars_daily_average", "lastDayDeletedCharsNum",
          "numberOfDeletedChars" },
      { "autocomplete_uses_daily_average", "lastDayAutoCompleteNum",
          "numberOfAutoComplete" },
      { "facebook_messages_daily_average", "lastDayFaceBookMessagesNum",
          "numberOfFaceBookMessages" },
      { "google_messages_daily_average", "lastDayGoolgeMessagesNum",
          "numberOfGoogleMessages" },
      { "sms_messages_daily_average", "lastDaySMSMessagesNum", "numberOfSMS" },
      { "green_clicks_daily_average", "lastDayGreenNum", "numberOfGreenClicks" },
      { "orange_clicks_daily_average", "lastDayOrangeNum",
          "numberOfOrangeClicks" },
      { "red_clicks_daily_average", "lastDayRedNum", "numberOfRedClicks" },
      { "blue_clicks_daily_average", "lastDayBlueNum", "numberOfBlueClicks" },
      { "blue_orange_clicks_daily_average", "lastDayBlueOrangeNum",
          "numberOfBlueOrangeClicks" },
      { "orange_green_clicks_daily_average", "lastDayOrangeGreen",
          "numberOfOrangeGreenClicks" },
      { "green_red_clicks_daily_average", "lastDayGreenRed",
          "numberOfGreenRedClicks" },
      { "red_blue_clicks_daily_average", "lastDayRedBlue",
          "numberOfBlueRedClicks" },
      { "long_blue_clicks_daily_average", "lastDayLongBlue",
          "numberOfLongBlueClicks" },
      { "long_green_clicks_daily_average", "lastDayLongGreen",
          "numberOfLongGreenClicks" },
      { "long_orange_clicks_daily_average", "lastDayLongOrange",
          "numberOfLongOrangeClicks" },
      { "long_red_clicks_daily_average", "lastDayLongRed",
          "numberOfLongRedClicks" },
      { "snake_average_daily_use", lastDaySnakeUseTime, null } };

  // first entry in each array is for the name on property in usage Data
  private static int N_NAME_IN_USAGE = 0;
  // second is for name in Statistics
  private static int N_NAME_IN_STATIS = 1;
  // third one is for number of elements in statistics
  private static int N_ELEM_NUM_IN_STATIS = 2;
  // forth is for the total elements number
  private static int N_TOTAL_ELEMS = 3;
  private static String[][] normalAveragesNames = {
      { "time_for_sentence_average", "timePerSentenceAvg", "numberOfSentence",
          "totalNumberOfSentence" },
      { "time_for_word_average", "timePerWordAvg", "numberOfWords",
          "totalNumberOfWords" },
      { "time_for_char_average", "timePerCharAvg", "numberOfChars",
          "totalNumberOfChars" } };

  private static String Statistics = "Statistics";

  private static String daysNum = "daysNum";

  private static String lastUpdateDay = "lastUpdateDay";

  /**
   * @author Tal for the server servlet - to know what type of result the op
   *         ended with
   */
  public enum Result {
    /**
     * success
     */
    SUCCESS,
    /**
     * Database failure - due to lack of statistics(not init)
     * 
     */
    FAIL,
    /**
     * internal database error
     */
    ERROR
  }

  /**
   * constructor - constructs a new object which allows communication with DB
   */
  public StatisticsDB() {
    DEBUG_MODE = false;
    DEBUG_MODE_DAYS_TO_JUMP = Integer.valueOf(0);
    // create a new query
    DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
    Transaction txn = datastore.beginTransaction();
    Query q = new Query(Statistics);
    PreparedQuery pq = datastore.prepare(q);
    Entity currStatistics = pq.asSingleEntity();
    // if no Statistics object
    if (currStatistics == null) {
      Entity newStatistics = new Entity(Statistics);
      // set the fields to default
      setPropertiesToDefult(newStatistics);
      Date now = getCurrentDate();
      newStatistics.setProperty(lastUpdateDay, now);
      newStatistics.setProperty(daysNum, Integer.valueOf(0));
      newStatistics.setProperty(IsDBContainsDefaults, Boolean.FALSE);
      // insert the new object to data base
      datastore.put(newStatistics);
    }
    txn.commit();
  }

  /**
   * @param time
   *          - the time that the game was played
   * @return result
   */
  public static Result saveNewTimePlayingSnake(Double time) {
    // crate new query to bring statistics object
    DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
    Transaction txn = datastore.beginTransaction();
    Query q = new Query(Statistics);
    PreparedQuery pq = datastore.prepare(q);
    Entity statistics = pq.asSingleEntity();
    // check if the average are updated
    if (checkAvgAndUpdate(statistics, datastore) == Result.ERROR) {
      txn.commit();
      return Result.ERROR;
    }
    Double oldLastDaySum = (Double) statistics.getProperty(lastDaySnakeUseTime);
    statistics.setProperty(lastDaySnakeUseTime,
        Double.valueOf(oldLastDaySum.doubleValue() + time.doubleValue()));
    statistics.setUnindexedProperty(IsDBContainsDefaults, Boolean.TRUE);
    // save the current object to data base
    datastore.put(statistics);
    // end transaction if the transaction was create locally
    if (txn != null)
      txn.commit();
    return Result.SUCCESS;
  }

  /**
   * @param newStatistics
   *          - the statistics object sent from app
   * @return result of operation
   */
  public static Result saveNewStatistics(Statistics newStatistics) {
    // crate new query to bring statistics object
    DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
    Transaction txn = datastore.beginTransaction();
    Query q = new Query(Statistics);
    PreparedQuery pq = datastore.prepare(q);
    Entity statistics = pq.asSingleEntity();
    // check if the average are updated
    if (checkAvgAndUpdate(statistics, datastore) == Result.ERROR) {
      txn.commit();
      return Result.ERROR;
    }
    // save the last day fields in Statistics object to last day fields
    for (String[] namesArray : dailyAvergeNames) {
      // to skip snake array
      if (namesArray[D_FIELD_IN_STATIS] == null)
        continue;

      Double oldLastDaySum = (Double) statistics
          .getProperty(namesArray[D_PROP_FOR_LAST_DAY]);

      Double newElementToAdd = getFieldValueFromStatistics(newStatistics,
          namesArray[D_FIELD_IN_STATIS]);

      Double totalSum = Double.valueOf(oldLastDaySum.doubleValue()
          + newElementToAdd.doubleValue());

      statistics.setProperty(namesArray[D_PROP_FOR_LAST_DAY], totalSum);

    }

    // calculate new averages according to new statistics
    for (String[] namesArray : normalAveragesNames) {
      // get fields and properties
      double oldAverage = ((Double) statistics
          .getProperty(namesArray[N_NAME_IN_USAGE])).doubleValue();

      Integer oldNumberOfParticipants = longToInteger(statistics
          .getProperty(namesArray[N_TOTAL_ELEMS]));

      Double newAverage = getFieldValueFromStatistics(newStatistics,
          namesArray[N_NAME_IN_STATIS]);

      Double newNumberOfParticipants = getFieldValueFromStatistics(
          newStatistics, namesArray[N_ELEM_NUM_IN_STATIS]);

      // calculate new average
      Double newAvergaeToSave = Double.valueOf(calculateNewNormalAverge(
          oldAverage, oldNumberOfParticipants, newAverage.doubleValue(),
          newNumberOfParticipants));

      // update fields in data base
      statistics.setProperty(namesArray[N_NAME_IN_USAGE], newAvergaeToSave);
      statistics.setProperty(
          namesArray[N_TOTAL_ELEMS],
          Integer.valueOf(oldNumberOfParticipants.intValue()
              + newNumberOfParticipants.intValue()));
    }
    statistics.setUnindexedProperty(IsDBContainsDefaults, Boolean.TRUE);
    // save the current object to data base
    datastore.put(statistics);
    // end transaction if the transaction was create locally
    if (txn != null)
      txn.commit();
    // return success status
    return Result.SUCCESS;
  }

  /**
   * @return a UsageData type object representing statistics of use to be
   *         displayed to user
   */
  public static UsageData getUsageData() {
    // crate new query to bring statistics object
    DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
    Transaction txn = datastore.beginTransaction();
    Query q = new Query(Statistics);
    PreparedQuery pq = datastore.prepare(q);
    Entity statistics = pq.asSingleEntity();
    // check if the average are updated
    if (checkAvgAndUpdate(statistics, datastore) == Result.ERROR) {
      if (txn != null)
        txn.commit();
      return null;
    }
    // the object that we will insert the values of statistics to it.
    UsageData returnedObject = new UsageData();
    // get the values from data base and insert them to object
    for (String[] fieldAndProperty : usageDataFieldsAndTherePropertiesInDB) {
      String fieldNameInUsageData = fieldAndProperty[0];
      String propertyNameInDB = fieldAndProperty[1];
      double valueInDB = ((Double) statistics.getProperty(propertyNameInDB))
          .doubleValue();

      // check need to change the unites
      double valueToInsert;
      if (fieldNameInUsageData.endsWith("min"))
        valueToInsert = getWithTwoDigitsAfterPoint(valueInDB / (1000 * 60));
      else if (fieldNameInUsageData.endsWith("sec")
          && !fieldNameInUsageData.endsWith("millisec"))
        valueToInsert = getWithTwoDigitsAfterPoint(valueInDB / 1000);
      else
        valueToInsert = getWithTwoDigitsAfterPoint(valueInDB);

      if (insertValueToUsageDataObject(returnedObject, fieldNameInUsageData,
          Double.valueOf(valueToInsert)) == Result.ERROR) {
        if (txn != null)
          txn.commit();
        return null;
      }
    }
    return returnedObject;
  }

  /**
   * @return result of operation
   */
  public static Result resetDataBase() {
    // crate new query to bring statistics object
    DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
    Transaction txn = datastore.beginTransaction();
    Query q = new Query(Statistics);
    PreparedQuery pq = datastore.prepare(q);
    Entity statistics = pq.asSingleEntity();
    // check if the average are updated
    if (checkAvgAndUpdate(statistics, datastore) == Result.ERROR) {
      if (txn != null)
        txn.commit();
      return Result.ERROR;
    }
    Boolean DBCanBeReset = (Boolean) statistics
        .getProperty(IsDBContainsDefaults);
    if (DBCanBeReset.booleanValue() == false) {
      if (txn != null)
        txn.commit();
      return Result.FAIL;
    }
    setPropertiesToDefult(statistics);
    Date now = getCurrentDate();
    statistics.setProperty(lastUpdateDay, now);
    statistics.setProperty(daysNum, Integer.valueOf(0));
    statistics.setProperty(IsDBContainsDefaults, Boolean.FALSE);
    // insert the new object to data base
    datastore.put(statistics);
    if (txn != null)
      txn.commit();
    return Result.SUCCESS;
  }

  private static void setPropertiesToDefult(Entity newStatistics) {
    for (String[] nameArray : dailyAvergeNames) {
      newStatistics.setProperty(nameArray[D_IN_USAGEDATA], Double.valueOf(0.0));
      newStatistics.setProperty(nameArray[D_PROP_FOR_LAST_DAY],
          Double.valueOf(0.0));
    }
    for (String[] nameArray : normalAveragesNames) {
      newStatistics
          .setProperty(nameArray[N_NAME_IN_USAGE], Double.valueOf(0.0));
      newStatistics.setProperty(nameArray[N_TOTAL_ELEMS], Integer.valueOf(0));
    }
  }

  private static Date getCurrentDate() {
    Calendar c = Calendar.getInstance();
    c.set(Calendar.HOUR_OF_DAY, 0);
    c.set(Calendar.MINUTE, 0);
    c.set(Calendar.SECOND, 0);
    c.set(Calendar.MILLISECOND, 0);
    if (DEBUG_MODE && DEBUG_MODE_DAYS_TO_JUMP.intValue() != 0)
      c.add(Calendar.DAY_OF_YEAR, DEBUG_MODE_DAYS_TO_JUMP.intValue());
    return c.getTime();
  }

  private static Integer getDiffInDayes(Calendar firstDate, Calendar secondDate) {
    Integer diffInYears = Integer.valueOf(secondDate.get(Calendar.YEAR)
        - firstDate.get(Calendar.YEAR));
    Integer diffInDays = Integer.valueOf(secondDate.get(Calendar.DAY_OF_YEAR)
        - firstDate.get(Calendar.DAY_OF_YEAR));
    return Integer
        .valueOf(diffInDays.intValue() + diffInYears.intValue() * 365);
  }

  private static double calcNewAverage(double prevAvg, double newElem,
      Integer daysDeff, Integer currDaysNum) {
    return (prevAvg * currDaysNum.intValue() + newElem)
        / (currDaysNum.intValue() + daysDeff.intValue());
  }

  private static Result checkAvgAndUpdate(Entity receivedStatistics,
      DatastoreService receivedDatastore) {
    Entity statistics = receivedStatistics;
    Transaction txn = null;
    DatastoreService datastore = receivedDatastore;
    // if no received statistics
    if (receivedStatistics == null) {
      // crate new query to bring statistics object
      datastore = DatastoreServiceFactory.getDatastoreService();
      txn = datastore.beginTransaction();
      Query q = new Query(Statistics);
      PreparedQuery pq = datastore.prepare(q);
      statistics = pq.asSingleEntity();
    }
    // get the needed dates
    Date lastUpdateDate = (Date) statistics.getProperty(lastUpdateDay);
    Date currDate = getCurrentDate();
    if (lastUpdateDate == null || currDate == null) {
      if (txn != null)
        txn.commit();
      return Result.ERROR;
    }
    // if the values was updated to day then return
    if (currDate.compareTo(lastUpdateDate) == 0)
      return Result.SUCCESS;

    // calculate the number of dates that we didn't update the systemdaysNum
    Calendar currDateCal = Calendar.getInstance();
    Calendar lastUpdateDateCal = Calendar.getInstance();
    currDateCal.setTime(currDate);
    lastUpdateDateCal.setTime(lastUpdateDate);
    Integer daysDeff = getDiffInDayes(lastUpdateDateCal, currDateCal);
    // the number of days that the current average was calculated for them
    Integer currDaysNum = longToInteger(statistics.getProperty(daysNum));

    for (String[] namesArray : dailyAvergeNames) {

      // get needed properties and calculate new average
      Double currAverage = (Double) statistics
          .getProperty(namesArray[D_IN_USAGEDATA]);

      Double lastDayValue = (Double) statistics
          .getProperty(namesArray[D_PROP_FOR_LAST_DAY]);

      Double newAverge = Double.valueOf(calcNewAverage(
          currAverage.doubleValue(), lastDayValue.doubleValue(), daysDeff,
          currDaysNum));

      // update statistics object in data base
      statistics.setProperty(namesArray[D_IN_USAGEDATA], newAverge);
      statistics
          .setProperty(namesArray[D_PROP_FOR_LAST_DAY], Double.valueOf(0));

    }
    // update properties in statistics
    statistics.setProperty(daysNum,
        Integer.valueOf(currDaysNum.intValue() + daysDeff.intValue()));
    statistics.setProperty(lastUpdateDay, currDate);
    // save the current object to data base
    datastore.put(statistics);
    // end transaction if the transaction was create locally
    if (txn != null)
      txn.commit();
    // return success status
    return Result.SUCCESS;
  }

  private static Integer longToInteger(Object object) {
    if (object instanceof Integer)
      return (Integer) object;
    return Integer.valueOf(((Long) object).intValue());
  }

  private static double calculateNewNormalAverge(double oldAverage,
      Integer oldNumberOfParticipants, double newAverage,
      Double newNumberOfParticipants) {
    if (oldNumberOfParticipants.doubleValue()
        + newNumberOfParticipants.doubleValue() == 0)
      return 0;
    return (oldAverage * oldNumberOfParticipants.doubleValue() + newAverage
        * newNumberOfParticipants.doubleValue())
        / (oldNumberOfParticipants.doubleValue() + newNumberOfParticipants
            .doubleValue());
  }

  private static Double getFieldValueFromStatistics(Statistics statistics,
      String fieldName) {
    Double newElementToAdd = Double.valueOf(0.0);
    try {
      Field currField = Statistics.class.getDeclaredField(fieldName);
      newElementToAdd = (Double) currField.get(statistics);
    } catch (NoSuchFieldException | SecurityException
        | IllegalArgumentException | IllegalAccessException e) {

      e.printStackTrace();
    }
    return newElementToAdd;
  }

  private static Result insertValueToUsageDataObject(UsageData usageDataObject,
      String fieldName, Double valueToInsert) {
    try {
      Field currField = UsageData.class.getDeclaredField(fieldName);
      currField.set(usageDataObject, valueToInsert);
    } catch (IllegalArgumentException | IllegalAccessException
        | NoSuchFieldException | SecurityException e) {
      return Result.ERROR;
    }
    return Result.SUCCESS;
  }

  private static double getWithTwoDigitsAfterPoint(double d) {
    Integer tmpVal = Integer.valueOf(Double.valueOf(d * 100).intValue());
    return tmpVal.doubleValue() / 100.0;
  }
}
