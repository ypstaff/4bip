package il.ac.technion.cs.ssdl.cs234311.yp09.servlet;

import il.ac.technion.cs.ssdl.cs234311.yp09.useStatistics.XmlBuilder;
import il.ac.technion.cs.ssdl.cs234311.yp09.utilities.CommonLang;
import il.ac.technion.cs.ssdl.cs234311.yp09.utilities.Statistics;
import il.ac.technion.cs.ssdl.cs234311.yp09.utilities.StatisticsDB;
import il.ac.technion.cs.ssdl.cs234311.yp09.utilities.StatisticsDB.Result;
import il.ac.technion.cs.ssdl.cs234311.yp09.utilities.UsageData;

import java.io.IOException;
import java.io.ObjectInputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Tal the only servlet - responsible for receiving HTTP messages and
 *         responding accordingly
 */
@SuppressWarnings("serial")
public class TALserverServlet extends HttpServlet {

  @Override
  public void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws IOException {

    String message = req.getParameter("message");

    if (message.equalsIgnoreCase("GetStatistics"))
      // getStatisticsString( req, resp);
      return;

    if (message.equalsIgnoreCase("GetStatisticsXML")) {
      UsageData data = StatisticsDB.getUsageData();
      XmlBuilder.buildXmlFromStatistics(data, resp);
      return;
    }

    if (message.equalsIgnoreCase(new String("RemoveAll"))) {
      Result r = StatisticsDB.resetDataBase();
      if (r == Result.FAIL) {// database is already reset
        resp.sendError(CommonLang.NO_DATA);
        return;
      }
      if (r == Result.ERROR) {
        resp.sendError(CommonLang.DATABASE_ERROR);
        return;
      }
      if (r == Result.SUCCESS)
        return;
    }

    // error
    resp.getWriter().println(
        " ERROR TALserver got GET  message ???/ !!!!! " + message);

  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, java.io.IOException {

    Statistics stats = null;
    Double timePlayedDouble = new Double(0.0);
    int recievedInt;
    ObjectInputStream Oreader = new ObjectInputStream(req.getInputStream());

    try {
      recievedInt = Oreader.readInt();
      if (recievedInt == 1)
        stats = (Statistics) Oreader.readObject();
      if (recievedInt == 2)
        timePlayedDouble = (Double) Oreader.readObject();
    } catch (Exception e) {// report to user of error and exit
      resp.getWriter().println(
          "server had an error while reading from buffer: " + e.getMessage());
      resp.sendError(CommonLang.BAD_INT_PARAM);
      return;
    }

    Result res = Result.SUCCESS;

    if (recievedInt == 1)
      res = StatisticsDB.saveNewStatistics(stats);
    else if (recievedInt == 2)
      res = StatisticsDB.saveNewTimePlayingSnake(timePlayedDouble);
    else {
      resp.sendError(CommonLang.BAD_INT_PARAM);
      return;
    }
    if (res == Result.ERROR) {
      resp.sendError(CommonLang.DATABASE_ERROR);
      return;
    }
  }

}