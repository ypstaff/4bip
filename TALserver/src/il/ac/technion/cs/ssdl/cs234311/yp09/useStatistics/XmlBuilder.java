package il.ac.technion.cs.ssdl.cs234311.yp09.useStatistics;


import java.io.IOException;
import java.lang.reflect.Field;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import il.ac.technion.cs.ssdl.cs234311.yp09.utilities.CommonLang;

/**
 * @author Tal
 * 
 * a class with only one capability - making an XML file from any object by taking it's fields 
 * and making XML elements of them
 */
public  class XmlBuilder {
	
	/**
	 * function takes a UsageData(or any other type) object and makes an XML file from it, then sends it to client
	 * @param usageData - the UsageData(or any other type) object that will be made into an XML file and sent to user 
	 * @param resp - the response object in the servlet 
	 * @throws IOException - only when trying to send an error response
	 */
	public static void  buildXmlFromStatistics(Object usageData, HttpServletResponse resp) throws IOException{
		
		try{
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			
			Document doc = docBuilder.newDocument();// root elements
			Element rootElement = doc.createElement("Statistics");
			doc.appendChild(rootElement);

			Field[] fileds = usageData.getClass().getDeclaredFields();		
			for(Field f : fileds){
				String fieldName = f.getName();
				if (!fieldName.equalsIgnoreCase("serialVersionUID")){
					Element element = doc.createElement(fieldName);
					Object o = f.get(usageData);
					Double d;
					if(o==null){
						d=Double.valueOf(0);
					}else{
						d = (Double)o;
					}
					String s = Double.toString(d.doubleValue());
					element.appendChild(doc.createTextNode(s) );
					rootElement.appendChild(element);
				}
			}
			
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result =  new StreamResult(resp.getOutputStream());
			resp.setContentType("application/xml");
			transformer.transform(source, result);
		}catch(IllegalAccessException e){
			resp.sendError(CommonLang.ILLEGAL_EXCESS);
		} catch (ParserConfigurationException e) {
			resp.sendError(CommonLang.PARSER_CONF);
		} catch (TransformerConfigurationException e) {
			resp.sendError(CommonLang.TRANSFORMER_CONF);
		} catch (TransformerException e) {
			resp.sendError(CommonLang.TRANSFORMER_EXCEP);
		}
		return ;	
	}
	
}
