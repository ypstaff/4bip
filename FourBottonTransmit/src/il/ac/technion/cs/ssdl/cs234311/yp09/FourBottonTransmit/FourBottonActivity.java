package il.ac.technion.cs.ssdl.cs234311.yp09.FourBottonTransmit;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ProgressBar;

/**
 * @author Husam FourBottonActivity is the user's interface for sending clicks
 *         to the user
 */
public class FourBottonActivity extends Activity {

  // the number of milliseconds in two seconds
  private static final int MILLISECONDS_IN_2_SEC = 2000;

  // the time between ache two updates of the progress bar
  private static final int progressStep = 10;

  // the views in the main activity
  private Button blueButton;
  private Button yellowButton;
  private Button greenButton;
  private Button redButton;
  ProgressBar pushTimeProgressBar;

  // used to send messages to the main application.
  ClicksFilterAndSend clicksSender;

  // the task that should increment the progress bar
  private CancelSafeTimeTask progressBarIncrement;
  // the number of pushed buttons,entailed to zero
  int numberOfPushedButoons;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_four_botton);
    blueButton = (Button) findViewById(R.id.blueButton);
    yellowButton = (Button) findViewById(R.id.orangeButton);
    greenButton = (Button) findViewById(R.id.greenButton);
    redButton = (Button) findViewById(R.id.redButton);
    pushTimeProgressBar = (ProgressBar) findViewById(R.id.pushTimeProgressBar);

    blueButton.setOnTouchListener(blueButtonOnTouchListener);
    yellowButton.setOnTouchListener(yellowButtonOnTouchListener);
    greenButton.setOnTouchListener(greenButtonOnTouchListener);
    redButton.setOnTouchListener(redButtonOnTouchListener);

    progressBarIncrement = new CancelSafeTimeTask();

    clicksSender = new ClicksFilterAndSend();
  }

  private OnTouchListener blueButtonOnTouchListener = new OnTouchListener() {
    @Override
    public boolean onTouch(View v, MotionEvent event) {
      switch (event.getAction()) {
      case MotionEvent.ACTION_DOWN:
        numberOfPushedButoons++;
        createProgressParTaskActionDown();
        clicksSender.blueButtonPush();
        break;
      case MotionEvent.ACTION_UP:
        numberOfPushedButoons--;
        createProgressParTaskActionUp();
        clicksSender.blueButtonStopPush();
        break;
      default:
        break;
      }
      return false;
    }
  };

  private OnTouchListener yellowButtonOnTouchListener = new OnTouchListener() {
    @Override
    public boolean onTouch(View v, MotionEvent event) {
      switch (event.getAction()) {
      case MotionEvent.ACTION_DOWN:
        numberOfPushedButoons++;
        createProgressParTaskActionDown();
        clicksSender.yellowButtonPush();
        break;
      case MotionEvent.ACTION_UP:
        numberOfPushedButoons--;
        createProgressParTaskActionUp();
        clicksSender.yellowButtonStopPush();
        break;
      default:
        break;
      }
      return false;
    }
  };

  private OnTouchListener greenButtonOnTouchListener = new OnTouchListener() {
    @Override
    public boolean onTouch(View v, MotionEvent event) {
      switch (event.getAction()) {
      case MotionEvent.ACTION_DOWN:
        numberOfPushedButoons++;
        createProgressParTaskActionDown();
        clicksSender.greenButtonPush();
        break;
      case MotionEvent.ACTION_UP:
        numberOfPushedButoons--;
        createProgressParTaskActionUp();
        clicksSender.greenButtonStopPush();
        break;
      default:
        break;
      }
      return false;
    }
  };

  private OnTouchListener redButtonOnTouchListener = new OnTouchListener() {
    @Override
    public boolean onTouch(View v, MotionEvent event) {
      switch (event.getAction()) {
      case MotionEvent.ACTION_DOWN:
        numberOfPushedButoons++;
        createProgressParTaskActionDown();
        clicksSender.redButtonPush();
        break;
      case MotionEvent.ACTION_UP:
        numberOfPushedButoons--;
        createProgressParTaskActionUp();
        clicksSender.redButtonStopPush();
        break;
      default:
        break;
      }
      return false;
    }
  };

  void createProgressParTaskActionDown() {
    // Cancel the previous task, and start another one
    progressBarIncrement.cancel();
    pushTimeProgressBar.setProgress(0);
    progressBarIncrement = new CancelSafeTimeTask();
    new Timer().schedule(progressBarIncrement, 0, progressStep);
  }

  void createProgressParTaskActionUp() {
    // Cancel the previous task, and start another one if there are pushed
    // buttons
    progressBarIncrement.cancel();
    pushTimeProgressBar.setProgress(0);
    if (numberOfPushedButoons != 0) {
      progressBarIncrement = new CancelSafeTimeTask();
      new Timer().schedule(progressBarIncrement, 0, progressStep);
    }
  }

  private class CancelSafeTimeTask extends TimerTask {
    Object synchronizer;
    boolean canceled;

    public CancelSafeTimeTask() {
      synchronizer = new Object();
      canceled = true;
    }

    @Override
    public void run() {
      // set it as not canceled
      synchronized (synchronizer) {
        canceled = false;
      }
      if (pushTimeProgressBar.getProgress() < MILLISECONDS_IN_2_SEC)
        pushTimeProgressBar.incrementProgressBy(progressStep);
      if (pushTimeProgressBar.getProgress() >= MILLISECONDS_IN_2_SEC)
        cancel();
    }

    @Override
    public boolean cancel() {
      synchronized (synchronizer) {
        if (canceled == false) {
          super.cancel();
          canceled = true;
        }
      }
      return false;
    }
  }
}
