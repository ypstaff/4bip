package il.ac.technion.cs.ssdl.cs234311.yp09.FourBottonTransmit;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

/**
 * @author Tal the opening acticity of the app enables user to pair new devices
 * 
 */
public class Pairing_Activity extends Activity {
  Button goConnectButton, showDetectableButton, nextButton, pairButton;
  TextView textViewTop, textViewButtom;
  Set<BluetoothDevice> devicesSet = new HashSet<BluetoothDevice>();
  String s;
  BluetoothDevice chosenDevice;
  int currentChoice = 0;

  BluetoothAdapter mBluetoothAdapter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    chosenDevice = null;
    currentChoice = 0;
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_pairing_);

    goConnectButton = (Button) findViewById(R.id.goConnect);
    showDetectableButton = (Button) findViewById(R.id.showDetectable);
    nextButton = (Button) findViewById(R.id.nextDetectable);
    pairButton = (Button) findViewById(R.id.pairB);
    textViewTop = (TextView) findViewById(R.id.textViewPair1);
    textViewButtom = (TextView) findViewById(R.id.textViewPair2);

    goConnectButton.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        goToConnect();
      }
    });

    showDetectableButton.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        showDetectable();
      }
    });

    nextButton.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        nextDetected();
      }
    });

    pairButton.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        pairButton();
      }
    });

    s = "";

    textViewTop.setText(R.string.openingMessage);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {

    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.pairing_, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();
    if (id == R.id.action_settings)
      return true;
    return super.onOptionsItemSelected(item);
  }

  protected void goToConnect() {
    BluetoothAdapter ba = BluetoothAdapter.getDefaultAdapter();
    if (ba != null)
      ba.cancelDiscovery();
    Intent connectIntent = new Intent(this, MainActivity.class);
    startActivity(connectIntent);

  }

  protected void showDetectable() {
    mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    if (mBluetoothAdapter == null) {
      textViewTop.setText(R.string.bluetoothNotSupported);
      return;
    }
    if (!mBluetoothAdapter.isEnabled()) {
      textViewTop.setText(R.string.enableBluetooth);
      mBluetoothAdapter.enable();
      return;
    }
    devicesSet.clear();
    s = "";
    textViewTop.setText(s);

    final BroadcastReceiver mReceiver = new BroadcastReceiver() {

      @Override
      public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        if (BluetoothDevice.ACTION_FOUND.equals(action)) {
          // Get the BluetoothDevice object from the Intent
          BluetoothDevice device = intent
              .getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
          // Add the name device to set and display name
          if (device != null && !devicesSet.contains(device)) {
            devicesSet.add(device);
            if (device.getBondState() == BluetoothDevice.BOND_BONDED)
              s += device.getName() + " - Paired \n";
            else
              s += device.getName() + "\n";
            textViewTop.setText(s);
          }
        }
      }
    };
    // Register the BroadcastReceiver
    IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
    registerReceiver(mReceiver, filter);

    if (mBluetoothAdapter.startDiscovery())
      textViewButtom.setText(R.string.discoveryBegan);
    else
      textViewButtom.setText(R.string.discoveryNotDone);

  }

  protected void nextDetected() {
    if (devicesSet == null)
      return;

    if (devicesSet.isEmpty()) {
      textViewButtom.setText(R.string.noDevicesFound);
      return;
    }
    if (currentChoice >= devicesSet.size())
      currentChoice = 0;

    Iterator<BluetoothDevice> iterator = devicesSet.iterator();
    for (int i = 0; i <= currentChoice; i++)
      chosenDevice = iterator.next();
    textViewButtom.setText("current choice : " + chosenDevice.getName());
    currentChoice++;
  }

  protected void pairButton() {
    BluetoothAdapter ba = BluetoothAdapter.getDefaultAdapter();
    if (ba != null)
      ba.cancelDiscovery();
    if (chosenDevice == null) {
      textViewButtom.setText(R.string.noDeviceChosen);
      return;
    }
    if (chosenDevice.getBondState() == BluetoothDevice.BOND_BONDED)
      textViewButtom.setText(R.string.alreadyPaired);
    else
      chosenDevice.createBond();
  }

}
