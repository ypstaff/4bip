package il.ac.technion.cs.ssdl.cs234311.yp09.FourBottonTransmit;

import java.util.Timer;
import java.util.TimerTask;

/**
 * @author Husam
 * 
 *         the class is in charge of filtering the output of the clicking screen
 * 
 */
public class ClicksFilterAndSend {
  private static final int milliSInTwoSec = 2000;

  // a boolean indicates if this push at the buttons is handled or not
  boolean pushHandled;

  // save the currently pushed buttons, as bit set in the places where button
  // pushed
  private byte pushedButtons;

  // this task used to send long push in the end Of two seconds
  private CancelSafeTimeTask afterTwoSecTask;

  // this object used to send bluetooth signals to main application
  // private SendToUser sendToUser;

  // object used to synchronize the access to the data
  private Object synchronizer;

  // count the number of pushed buttons
  private int numberOfPushedButtons;

  // this function will called after two seconds from the last change in the
  // set of pushed buttons.
  void twoSecondsListener() {
    if (pushHandled == false) {
      // to tell the other listener that i handle that push
      pushHandled = true;
      sendAsynchroneMessage((byte) (pushedButtons | SendToUser.longPush));
    }
  }

  /**
   * @param message
   *          - the message (full press) to be sent to user
   */
  private static void sendAsynchroneMessage(final byte message) {
    new Thread(new Runnable() {
      @Override
      public void run() {
        Data.sendClick(message);
      }
    }).start();
  }

  private class CancelSafeTimeTask extends TimerTask {

    boolean canceled;

    public CancelSafeTimeTask() {

      canceled = false;
    }

    @Override
    public void run() {
      // set it as not canceled
      canceled = false;
      twoSecondsListener();
      // after this there are no need for this task
      cancel();
    }

    @Override
    public boolean cancel() {
      if (canceled == false) {
        super.cancel();
        canceled = true;
      }
      return false;
    }
  }

  // cancel the last task that was waiting for 2 second to finish and start
  // another one
  private void cancelTheLastTaskAndStartAnotherOne() {
    if (afterTwoSecTask != null)
      afterTwoSecTask.cancel();
    if (numberOfPushedButtons != 0) {
      afterTwoSecTask = new CancelSafeTimeTask();
      // call milliSInTwoSec after two seconds
      new Timer().schedule(afterTwoSecTask, milliSInTwoSec, milliSInTwoSec);
    }
  }

  /**
   * Constructor , no parameters needed synchronizer is made into a new object
   */
  public ClicksFilterAndSend() {
    // this.sendToUser = sendToUser;
    synchronizer = new Object();
  }

  /**
   * represents a press on the blue button adds the value of pressing the blue
   * button to the pushedButtons byte - the value that will later be sent to
   * receiver
   */
  public void blueButtonPush() {
    synchronized (synchronizer) {
      numberOfPushedButtons++;
      pushedButtons = (byte) (pushedButtons | SendToUser.blueButtonPushed);
      pushHandled = false;
      cancelTheLastTaskAndStartAnotherOne();
    }
  }

  /**
   * represents a press on the yellow button adds the value of pressing the
   * yellow button to the pushedButtons byte - the value that will later be sent
   * to receiver
   */
  public void yellowButtonPush() {
    synchronized (synchronizer) {
      numberOfPushedButtons++;
      pushedButtons = (byte) (pushedButtons | SendToUser.yellowButtonPushed);
      pushHandled = false;
      cancelTheLastTaskAndStartAnotherOne();
    }
  }

  /**
   * represents a press on the green button adds the value of pressing the green
   * button to the pushedButtons byte - the value that will later be sent to
   * receiver
   */
  public void greenButtonPush() {
    synchronized (synchronizer) {
      numberOfPushedButtons++;
      pushedButtons = (byte) (pushedButtons | SendToUser.greenButtonPushed);
      pushHandled = false;
      cancelTheLastTaskAndStartAnotherOne();
    }
  }

  /**
   * represents a press on the red button adds the value of pressing the red
   * button to the pushedButtons byte - the value that will later be sent to
   * receiver
   */
  public void redButtonPush() {
    synchronized (synchronizer) {
      numberOfPushedButtons++;
      pushedButtons = (byte) (pushedButtons | SendToUser.redButtonPushed);
      pushHandled = false;
      cancelTheLastTaskAndStartAnotherOne();
    }
  }

  /**
   * represents the stop of a press on the blue button adds the value of long
   * pressing to the pushedButtons byte (in neccesery) - the value that will
   * later be sent to receiver
   */
  public void blueButtonStopPush() {
    synchronized (synchronizer) {
      numberOfPushedButtons--;
      if (pushHandled == false) {
        sendAsynchroneMessage(pushedButtons);
        pushHandled = true;
      }
      // to set the blue button to 0 and leave the other in the same
      // status.
      pushedButtons = (byte) (pushedButtons ^ SendToUser.blueButtonPushed);
      cancelTheLastTaskAndStartAnotherOne();
    }
  }

  /**
   * represents the stop of a press on the yellow button adds the value of long
   * pressing to the pushedButtons byte (in neccesery) - the value that will
   * later be sent to receiver
   */
  public void yellowButtonStopPush() {
    synchronized (synchronizer) {
      numberOfPushedButtons--;
      if (pushHandled == false) {
        sendAsynchroneMessage(pushedButtons);
        pushHandled = true;
      }
      // to set the yellow button to 0 and leave the other in the same
      // status.
      pushedButtons = (byte) (pushedButtons ^ SendToUser.yellowButtonPushed);
      cancelTheLastTaskAndStartAnotherOne();
    }
  }

  /**
   * represents the stop of a press on the green button adds the value of long
   * pressing to the pushedButtons byte (in neccesery) - the value that will
   * later be sent to receiver
   */
  public void greenButtonStopPush() {
    synchronized (synchronizer) {
      numberOfPushedButtons--;
      if (pushHandled == false) {
        sendAsynchroneMessage(pushedButtons);
        pushHandled = true;
      }
      // to set the green button to 0 and leave the other in the same
      // status.
      pushedButtons = (byte) (pushedButtons ^ SendToUser.greenButtonPushed);
      cancelTheLastTaskAndStartAnotherOne();
    }
  }

  /**
   * represents the stop of a press on the red button adds the value of long
   * pressing to the pushedButtons byte (in neccesery) - the value that will
   * later be sent to receiver
   */
  public void redButtonStopPush() {
    synchronized (synchronizer) {
      numberOfPushedButtons--;
      if (pushHandled == false) {
        sendAsynchroneMessage(pushedButtons);
        pushHandled = true;
      }
      // to set the blue red to 0 and leave the other in the same status.
      pushedButtons = (byte) (pushedButtons ^ SendToUser.redButtonPushed);
      cancelTheLastTaskAndStartAnotherOne();
    }
  }

}
