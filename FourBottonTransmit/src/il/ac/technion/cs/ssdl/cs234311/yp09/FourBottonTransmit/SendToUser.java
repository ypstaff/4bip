package il.ac.technion.cs.ssdl.cs234311.yp09.FourBottonTransmit;

/**
 * @author Tal
 * 
 */
public interface SendToUser {
  /**
   * the first bit (least significant) - if this bit is on this is a long press
   */
  public static final byte longPush = 1;
  /**
   * the second bit - if this bit is on this is a press involving blue
   */
  public static final byte blueButtonPushed = 2;
  /**
   * the third bit - if this bit is on this is a press involving yellow
   */
  public static final byte yellowButtonPushed = 4;
  /**
   * the fourth bit - if this bit is on this is a press involving green
   */
  public static final byte greenButtonPushed = 8;
  /**
   * the fifth bit - if this bit is on this is a press involving red
   */
  public static final byte redButtonPushed = 16;

  /**
   * @param b
   *          - byte to be sent to user, this byte is the outcome of the
   *          operation & on the values involved in the press (colors pressed
   *          and long press if pressed for long)
   * 
   *          the implementing bluetooth module will provide this method for
   *          sending clicks to receiver
   */
  public void sendClick(byte b);
}
