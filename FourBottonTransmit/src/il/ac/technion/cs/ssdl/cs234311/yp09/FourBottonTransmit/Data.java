package il.ac.technion.cs.ssdl.cs234311.yp09.FourBottonTransmit;

import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.CountDownTimer;

/**
 * @author Tal a static class in charge of transmitting signals
 */
public class Data {

  /**
   * outputStream - the outputstream threw which the data will be sent
   */
  static public OutputStream outputStream;
  /**
   * 
   */
  static public BluetoothDevice chosenDevice;
  /**
   * chosenSocket - the socket threw which
   */
  static public BluetoothSocket chosenSocket;
  /**
   * timer is in charge of keeping connection alive by sending -1 every two
   * seconds
   */
  public static CountDownTimer timer;

  /**
   * @param b
   *          - a byte representing a full click
   * @return 1 for success negative for faliure
   */
  public static int sendClick(byte b) {
    // if (!bool )connectionKeepAlive();// TODO
    if (outputStream == null || chosenSocket == null || chosenDevice == null)
      return -2;
    try {
      outputStream.write(b);
    } catch (IOException e) {
      return -1;
    }
    return 1;

  }

  /**
   * @return 0 for success negative value for faliure
   */
  public static int connect() {

    long l1 = 111;
    long l2 = 111;
    UUID my_uuid = new UUID(l1, l2);

    try {
      // MY_UUID is the app's UUID string, also used by the server code
      chosenSocket = chosenDevice.createRfcommSocketToServiceRecord(my_uuid);
    } catch (Exception e) {
      return -3;
    }

    try {
      chosenSocket.connect();
    } catch (IOException e) {
      return -2;
    }

    try {
      outputStream = chosenSocket.getOutputStream();
    } catch (IOException e) {
      return -4;
    }

    return 0;
  }

  /**
   * connectionKeepAlive - keeps the connection alive by sending a -1 byte every
   * two seconds the receiver knows the connection is dead when it has'nt
   * received a -1 for more than 5 seconds
   */
  public static void connectionKeepAlive() {
    timer = new CountDownTimer(10000, 2000) {
      @Override
      public void onTick(long millisUntilFinished) {
        sendClick((byte) -1);
      }

      @Override
      public void onFinish() {
        this.start();
      }
    };
    timer.start();
  }

}
