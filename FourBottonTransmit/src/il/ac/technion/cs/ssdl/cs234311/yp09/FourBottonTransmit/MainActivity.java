package il.ac.technion.cs.ssdl.cs234311.yp09.FourBottonTransmit;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

/**
 * @author Tal the main activity in the app - in charge of establishing a
 *         connection for the four botton app
 */
public class MainActivity extends Activity {
  Button b1, b2, b3, b4;
  TextView textView, textView2;
  BluetoothAdapter mBluetoothAdapter;

  boolean deviceSupportsBlth;
  boolean blthTurnedOn;
  BluetoothSocket chosenSocket;

  int choiceNum;
  Set<BluetoothDevice> devices;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    b1 = (Button) findViewById(R.id.next);
    b2 = (Button) findViewById(R.id.choose);
    b3 = (Button) findViewById(R.id.button3);
    b4 = (Button) findViewById(R.id.button4);
    textView = (TextView) findViewById(R.id.textView1);
    textView2 = (TextView) findViewById(R.id.textView2);

    // Init Data static class members
    choiceNum = 0;
    devices = null;
    Data.outputStream = null;
    Data.chosenDevice = null;
    Data.chosenSocket = null;

    mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    if (mBluetoothAdapter == null) {
      deviceSupportsBlth = false;
      textView.setText(R.string.bluetoothNotSupported);
    } else
      textView.setText(R.string.openingConnectMessage);

    b1.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        OnTurnOnBlth();

      }
    });

    b2.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        OnShowPaired();

      }
    });
    b3.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        OnNext();

      }
    });
    b4.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        OnConnect();

      }
    });
  }

  // turn on bluetooth if it is supported and is off
  void OnTurnOnBlth() {
    Intent connectIntent = new Intent(this, Pairing_Activity.class);
    startActivity(connectIntent);
  }

  // Show choices
  void OnShowPaired() {
    mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    if (mBluetoothAdapter == null) {
      deviceSupportsBlth = false;
      textView.setText(R.string.openingConnectMessage);
      return;
    }
    if (!mBluetoothAdapter.isEnabled()) {
      textView.setText(R.string.enableBluetooth);
      mBluetoothAdapter.enable();
      return;
    }
    // ArrayList<String> arrLstStr;
    // Set<BluetoothDevice> pairedDevices =
    // mBluetoothAdapter.getBondedDevices();
    // arrLstStr = new ArrayList<String>();
    // for (BluetoothDevice d : pairedDevices)
    // arrLstStr.add(d.getName());
    showchoices();
  }

  void showchoices() {
    String devicesStr = "avalible paired devices are: \n";

    devices = mBluetoothAdapter.getBondedDevices();
    if (devices.size() == 0) {
      textView.setText(R.string.noDevicesFound);
      return;
    }
    choiceNum = 1;
    Iterator<BluetoothDevice> iterator = devices.iterator();
    BluetoothDevice device;
    while (iterator.hasNext()) {
      device = iterator.next();
      devicesStr += Integer.valueOf(choiceNum++).toString() + ". "
          + device.getName() + "\n";
    }
    textView.setText(devicesStr);

    choiceNum = 0;
    String sBottom = "your current choice is: "
        + Integer.valueOf(choiceNum + 1).toString() + ". "
        + devices.iterator().next().getName();

    textView2.setText(sBottom);

  }

  // NEXT
  void OnNext() {
    if (devices == null)
      return;
    if (devices.isEmpty())
      return;

    choiceNum++;
    choiceNum = choiceNum % devices.size();

    Iterator<BluetoothDevice> itr = devices.iterator();
    BluetoothDevice device = itr.next();

    for (int i = 0; i < choiceNum; i++)
      device = itr.next();

    String sBottom = "your current choice is: "
        + Integer.valueOf(choiceNum + 1).toString() + ". " + device.getName();
    textView2.setText(sBottom);

  }

  void OnConnect() {

    if (devices == null) {
      textView.setText(R.string.firstChooseDevice);
      return;
    }
    if (devices.size() == 0) {
      textView.setText(R.string.noDevicesFound);
      return;
    }
    Iterator<BluetoothDevice> itr = devices.iterator();
    BluetoothDevice device = itr.next();
    // textView.setText("\n choiceNum = " + Integer.toString(choiceNum));

    for (int i = 0; i < choiceNum; i++)
      device = itr.next();
    String sBottom = "connecting to: "
        + Integer.valueOf(choiceNum + 1).toString() + ". " + device.getName();
    textView2.setText(sBottom);

    Data.chosenDevice = device;

    try {
      TimeUnit.SECONDS.sleep(3);
    } catch (InterruptedException e) {
      /* this is an empty -no harm done */
    }

    int res = Data.connect();
    if (res == 0) {
      Data.connectionKeepAlive();
      Intent fourBF = new Intent(this, FourBottonActivity.class);
      startActivity(fourBF);
    } else
      textView2.setText(R.string.connectionFailed);

  }

  void showString(String s) {
    textView.setText(s);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.main, menu);
    return true;
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    String s;
    if (resultCode == 0)
      s = "was not able to turn on bluetooth : ";
    else
      s = "bluetooth now turned on  : ";
    showString(s);
  }

  // function turns on bluetooth connectivity
  // void tryTurnOnBlth(String s) {
  // String b = new String(s);
  // mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
  //
  // if (!mBluetoothAdapter.isEnabled()) {
  // Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
  // startActivityForResult(enableBtIntent, 3);
  // // onActivityResult(requestCode, resultCode, enableBtIntent);
  // b += "bluetooth was not turned on \n";
  // showString(s);
  // } else {
  // b += "bluetooth is already turned on...  \n";
  // showString(b);
  // }
  // }
  //
}
