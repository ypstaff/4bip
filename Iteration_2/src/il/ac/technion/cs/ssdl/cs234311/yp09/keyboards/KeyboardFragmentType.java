package il.ac.technion.cs.ssdl.cs234311.yp09.keyboards;

/**
 * This class will be used from activities, to indicate the type of the
 * keyboard!
 * 
 * @author Owais Musa
 * 
 */
public enum KeyboardFragmentType {
  /**
   * 
   */
  DEFAULT,

  /**
   * 
   */
  ALPHABETICAL,

  /**
   * 
   */
  KEYBOARD2
}