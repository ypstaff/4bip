package il.ac.technion.cs.ssdl.cs234311.yp09.utilities;

//import il.ac.technion.cs.ssdl.cs234311.yp09.gui.GeneralActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.GeneralActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.OpCodeInterpreter.Op;

import java.util.Date;

/**
 * @author husam
 * 
 *         classed used to update the object Statistics , and send it to the
 *         google app engine
 */
public class SaveStatistics {
  /**
   * used to turn on and off the debug mode
   */
  public static boolean DEBUG_MODE = false;
  private static Date startAppTime;
  private static Statistics statistics;
  private static Date timeForLastChar;
  private static int timeForLastSentence, timeForLastWord;
  private static int MAX_TIME_FOR_CHAR = 10000;

  /**
   * the types of the optional clicks, including long and combination of clicks.
   * 
   */
  public enum ClickType {
    /**
     * blue button click
     */
    BLUE,
    /**
     * long blue button click
     * 
     */
    LONG_BLUE,
    /**
     * yellow button click
     */
    ORANGE,
    /**
     * long orange button click
     */
    LONG_ORANGE,
    /**
     * green button click
     */
    GREEN,
    /**
     * long green button click
     */
    LONG_GREEN,
    /**
     * red button click
     */
    RED,
    /**
     * long red button click
     */
    LONG_RED,
    /**
     * 
     */
    BLUE_ORANGE,
    /**
     * 
     */
    YELLOW_GREEN,
    /**
     * 
     */
    GREEN_RED,
    /**
     * 
     */
    RED_BLUE
  }

  /**
   * convert Op enum to Click type enum
   * 
   * @param op
   *          value to convert
   * @return the Appropriate ClickType enum
   */
  public static ClickType convertOpToClickType(Op op) {
    switch (op) {
    case BLUE:
      return ClickType.BLUE;
    case LONG_BLUE:
      return ClickType.LONG_BLUE;
    case ORANGE:
      return ClickType.ORANGE;
    case LONG_ORANGE:
      return ClickType.LONG_ORANGE;
    case GREEN:
      return ClickType.GREEN;
    case LONG_GREEN:
      return ClickType.LONG_GREEN;
    case RED:
      return ClickType.RED;
    case LONG_RED:
      return ClickType.LONG_RED;
    case BLUE_ORANGE:
      return ClickType.BLUE_ORANGE;
    case ORANGE_GREEN:
      return ClickType.YELLOW_GREEN;
    case GREEN_RED:
      return ClickType.GREEN_RED;
    default:
      break;
    }
    return null;
  }

  /**
   * @author husam , enum that contain the type of messages
   */
  public enum MessageType {
    /**
     * facebook message
     */
    FACEBOOK_MESSAGE,
    /**
     * sms message
     */
    SMS_MESSAGE,
    /**
     * google talk message
     */
    GOOGLE_MESSAGE;
  }

  /**
   * this have to be called in the start of the application use.
   */
  public static void startApp() {
    if (startAppTime == null)
      startAppTime = new Date();
    statistics = new Statistics();
  }

  /**
   * this have to be called in resuming the application, or in the start of the
   * use
   */
  public static void resumeOrStartApp() {
    if (startAppTime == null)
      startAppTime = new Date();
    if (statistics == null)
      statistics = new Statistics();
  }

  /**
   * add click to the statistic of the usage
   * 
   * @param type
   *          the click type
   */
  public static void clickOn(Op type) {
    switch (type) {
    case RED:
      statistics.numberOfRedClicks = increment(statistics.numberOfRedClicks);
      break;
    case LONG_RED:
      statistics.numberOfLongRedClicks = increment(statistics.numberOfLongRedClicks);
      break;
    case BLUE:
      statistics.numberOfBlueClicks = increment(statistics.numberOfBlueClicks);
      break;
    case LONG_BLUE:
      statistics.numberOfLongBlueClicks = increment(statistics.numberOfLongBlueClicks);
      break;
    case GREEN:
      statistics.numberOfGreenClicks = increment(statistics.numberOfGreenClicks);
      break;
    case LONG_GREEN:
      statistics.numberOfLongGreenClicks = increment(statistics.numberOfLongGreenClicks);
      break;
    case ORANGE:
      statistics.numberOfOrangeClicks = increment(statistics.numberOfOrangeClicks);
      break;
    case LONG_ORANGE:
      statistics.numberOfLongOrangeClicks = increment(statistics.numberOfLongOrangeClicks);
      break;
    case BLUE_ORANGE:
      statistics.numberOfBlueOrangeClicks = increment(statistics.numberOfBlueOrangeClicks);
      break;
    case ORANGE_GREEN:
      statistics.numberOfOrangeGreenClicks = increment(statistics.numberOfOrangeGreenClicks);
      break;
    case GREEN_RED:
      statistics.numberOfGreenRedClicks = increment(statistics.numberOfGreenRedClicks);
      break;
    case BLUE_RED:
      statistics.numberOfBlueRedClicks = increment(statistics.numberOfBlueRedClicks);
      break;
    default:
      break;
    }
  }

  private static Double increment(Double number) {
    return Double.valueOf(number.doubleValue() + 1.0);
  }

  /**
   * WATAD
   */
  public static void startTyping() {
    timeForLastChar = new Date();
    timeForLastSentence = timeForLastWord = 0;
  }

  private static Double calcNewAvg(Double oldAverage,
      Double oldNumberOfElements, int newElem) {
    return Double.valueOf((oldAverage.doubleValue()
        * oldNumberOfElements.doubleValue() + newElem)
        / (oldNumberOfElements.doubleValue() + 1));
  }

  /**
   * add sentence to statistics, and save the time it take the user to type it
   */
  public static void addSentence() {
    if (timeForLastSentence == -1) {
      timeForLastSentence = 0;
      return;
    }
    statistics.timePerSentenceAvg = calcNewAvg(statistics.timePerSentenceAvg,
        statistics.numberOfSentence, timeForLastSentence);
    timeForLastSentence = 0;
    statistics.numberOfSentence = increment(statistics.numberOfSentence);
  }

  /**
   * add char to statistics, and save the time it take the user to type it
   */
  public static void addChar() {
    if (timeForLastChar != null) {
      Date currTime = new Date();
      int diffFromLastTime = (int) (currTime.getTime() - timeForLastChar
          .getTime());
      if (diffFromLastTime <= MAX_TIME_FOR_CHAR) {
        statistics.timePerCharAvg = calcNewAvg(statistics.timePerCharAvg,
            statistics.numberOfChars, diffFromLastTime);
        statistics.numberOfChars = increment(statistics.numberOfChars);
        if (timeForLastSentence != -1)
          timeForLastSentence += diffFromLastTime;
        if (timeForLastWord != -1)
          timeForLastWord += diffFromLastTime;
      } else {
        timeForLastSentence = -1;
        timeForLastWord = -1;
      }
    }
    timeForLastChar = new Date();
  }

  /**
   * add char to statistics, and save the time it take the user to type it
   */
  public static void addWord() {
    if (timeForLastWord == -1) {
      timeForLastWord = 0;
      return;
    }
    statistics.timePerWordAvg = calcNewAvg(statistics.timePerWordAvg,
        statistics.numberOfWords, timeForLastWord);
    timeForLastWord = 0;
    statistics.numberOfWords = increment(statistics.numberOfWords);
  }

  /**
   * add char to the number of deleted char saved in statistics
   */
  public static void deleteChar() {
    statistics.numberOfDeletedChars = increment(statistics.numberOfDeletedChars);
  }

  /**
   * add auto complete use to the number of auto completes saved is statistics
   */
  public static void addAutoComplete() {
    statistics.numberOfAutoComplete = increment(statistics.numberOfAutoComplete);
  }

  /**
   * add message to the number of total send messages
   * 
   * @param type
   *          , the message type
   */
  public static void sendMessage(MessageType type) {
    switch (type) {
    case FACEBOOK_MESSAGE:
      statistics.numberOfFaceBookMessages = increment(statistics.numberOfFaceBookMessages);
      break;
    case SMS_MESSAGE:
      statistics.numberOfSMS = increment(statistics.numberOfSMS);
      break;
    case GOOGLE_MESSAGE:
      statistics.numberOfGoogleMessages = increment(statistics.numberOfGoogleMessages);
      break;
    default:
      break;
    }
  }

  /**
   * need to be called when the user finish typing
   */
  public static void stopTyping() {
    timeForLastChar = null;
    timeForLastSentence = 0;
    timeForLastWord = 0;
  }

  /**
   * need to be called when the user exit the application
   * 
   * @return the statistic that will be send to the GAE
   */
  public static Statistics exitTheApp() {
    timeForLastChar = null;
    statistics.timeUsingTheApp = getDiffFromNow(startAppTime);
    if (DEBUG_MODE == false)
      sendStatistics(statistics);
    startAppTime = null;
    Statistics $ = statistics;
    statistics = new Statistics();
    return $;
  }

  /**
   * need to be called when the application is paused
   * 
   * @param activity
   *          the activity that called this method
   * 
   * @return the statistics that will be send to GAE
   */
  public static Statistics pauseAndSendStatistcs(GeneralActivity activity) {
    timeForLastChar = null;
    statistics.timeUsingTheApp = getDiffFromNow(startAppTime);
    if (DEBUG_MODE == false)
      sendStatistics(statistics);
    Statistics oldStatistics = statistics;
    statistics = new Statistics();
    startAppTime = null;
    return oldStatistics;

  }

  private static void sendStatistics(final Statistics statistics2send) {
    new Thread(new Runnable() {
      @Override
      public void run() {
        new Client().sendStatistics(statistics2send);
      }
    }).start();
  }

  private static Double getDiffFromNow(Date time) {
    if (time == null)
      return Double.valueOf(0.0);
    Date now = new Date();
    return Double.valueOf(now.getTime() - time.getTime());
  }
}
