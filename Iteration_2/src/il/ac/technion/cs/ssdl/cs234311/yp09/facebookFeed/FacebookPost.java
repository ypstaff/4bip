package il.ac.technion.cs.ssdl.cs234311.yp09.facebookFeed;

import java.util.Date;

/**
 * Represents a single facebook post
 * 
 * @author Owais Musa
 * 
 */
public class FacebookPost {

  private String mPOstId;

  private String mAuthorImageUrl;
  private String mAuthorName;
  private String mText;

  private Date mCreatingTime;

  private long mLikesNum;
  private long mCommentsNum;
  private long mSharesNum;

  /**
   * @param postId
   *          post id
   * @param authorImageUrl
   *          author image URL
   * @param authorName
   *          author name
   * @param text
   *          text
   * @param likesNum
   *          number of likes
   * @param commentsNum
   *          number of comments
   * @param sharesNum
   *          number of shares
   * @param creatingTime
   *          creation date
   */
  public FacebookPost(String postId, String authorImageUrl, String authorName,
      String text, long likesNum, long commentsNum, long sharesNum,
      Date creatingTime) {
    if (null == authorImageUrl || null == authorName || null == text)
      throw new IllegalArgumentException(
          "One of the arguments or more are null!");

    mPOstId = postId;
    mAuthorImageUrl = authorImageUrl;
    mAuthorName = authorName;
    mText = text;
    mLikesNum = likesNum;
    mCommentsNum = commentsNum;
    mSharesNum = sharesNum;
    mCreatingTime = creatingTime;
  }

  /**
   * @return post id
   */
  public String getPOstId() {
    return mPOstId;
  }

  /**
   * @return author image URL
   */
  public String getAuthorImageUrl() {
    return mAuthorImageUrl;
  }

  /**
   * @return author name
   */
  public String getAuthorName() {
    return mAuthorName;
  }

  /**
   * @return text
   */
  public String getText() {
    return mText;
  }

  /**
   * @return number of likes
   */
  public long getLikesNum() {
    return mLikesNum;
  }

  /**
   * @return number of comments
   */
  public long getCommentsNum() {
    return mCommentsNum;
  }

  /**
   * @return number of shares
   */
  public long getSharesNum() {
    return mSharesNum;
  }

  /**
   * @return the time this post was created in
   */
  public Date getCreatingTime() {
    return mCreatingTime;
  }
}
