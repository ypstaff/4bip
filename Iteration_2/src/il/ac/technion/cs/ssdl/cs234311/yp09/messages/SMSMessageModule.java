package il.ac.technion.cs.ssdl.cs234311.yp09.messages;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;

/**
 * @author Daniel
 * @desc This class is used to provide the SMS conversation in a comprehensive
 *       format
 */
public class SMSMessageModule {

  /**
   * @param currAct
   *          - Type Activity. The main activity, in which the SMS storage is
   *          parsed.
   * @param phoneNum
   *          - Type String. The phone number of the person whose conversation
   *          we wish to present
   * @return - Type ArrayList<SMSMessageItem>. An ArrayList of SMSMessageItem
   *         objects, in which holds the conversation's messages.
   */
  static public ArrayList<SMSMessageItem> SMSParser(Activity currAct,
      String phoneNum) {
    ArrayList<SMSMessageItem> messagesList = new ArrayList<SMSMessageItem>();
    Uri SMS_INBOX = Uri.parse("content://sms");
    Cursor c = currAct.getContentResolver().query(SMS_INBOX, null, null, null,
        "date desc");

    String threadid = null;
    PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
    PhoneNumber phoneNumber = null;

    try {
      phoneNumber = phoneUtil.parse(phoneNum, "IL");
    } catch (NumberParseException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    String formatedE164 = phoneUtil.format(phoneNumber, PhoneNumberFormat.E164);
    String formatedInter = phoneUtil.format(phoneNumber,
        PhoneNumberFormat.INTERNATIONAL);
    String formatedNat = phoneUtil.format(phoneNumber,
        PhoneNumberFormat.NATIONAL);
    String formatedNatNoDash = formatedNat.replace("-", "");
    c.moveToFirst();
    for (int i = 0; i < c.getCount(); i++) {
      String p = c.getString(c.getColumnIndexOrThrow("address"));

      if (p != null
          && (p.equals(formatedE164) || p.equals(formatedInter)
              || p.equals(formatedNat) || p.equals(formatedNatNoDash))) {
        threadid = c.getString(c.getColumnIndexOrThrow("thread_id"));
        break;
      }

      c.moveToNext();
    }
    c.close();

    String where = "thread_id=" + threadid;
    Cursor mycursor = currAct.getContentResolver().query(SMS_INBOX, null,
        where, null, null);
    if (mycursor.getCount() > 0)
      while (mycursor.moveToNext()) {
        int type = Integer.parseInt(mycursor.getString(mycursor
            .getColumnIndexOrThrow("type")));
        String body = mycursor
            .getString(mycursor.getColumnIndexOrThrow("body"));
        long time = Long.parseLong(mycursor.getString(mycursor
            .getColumnIndexOrThrow("date")));

        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        calendar.setTimeInMillis(time);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd h:mma");
        String date = sdf.format(calendar.getTime());
        SMSMessageItem newMessage = new SMSMessageItem(type, body, date);
        messagesList.add(newMessage);
      }
    mycursor.close();
    return messagesList;
  }
}
