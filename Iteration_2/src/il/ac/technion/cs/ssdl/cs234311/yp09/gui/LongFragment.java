package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import android.app.Fragment;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 3/1/2014
 * 
 */
public class LongFragment extends Fragment {

  private static final String DESCRIBABLE_KEY = "describable_key";
  private Screen screenType;
  private TextView backText;

  /**
   * @author Muhammad
   * 
   */
  public enum Screen {
    /**
     * Typing screen
     */
    Typing, /**
     * Login screen
     */
    Login, /**
     * Contact screen
     */
    Contact, /**
     * Settings screen
     */
    Settings, /**
     * Language screen
     */
    Language, /**
     * Service screen
     */
    Service, /**
     * Bluetooth screen
     */
    Bluetooth, /**
     * Contact History screen
     */
    ContactHistory, /**
     * Conversation screen
     */
    Conversation, /**
     * Volume Screen
     */
    Volume, /**
     * Posts screen
     */
    Posts, /**
     * Feed Options screen
     */
    FeedOptions
  }

  /**
   * @param type
   *          screen type
   * @return Instance of LongFragment
   */
  public static LongFragment newInstance(final Screen type) {
    final LongFragment longFragment = new LongFragment();
    final Bundle bundle = new Bundle();
    bundle.putSerializable(DESCRIBABLE_KEY, type);
    longFragment.setArguments(bundle);

    return longFragment;
  }

  @Override
  public View onCreateView(final LayoutInflater inflater,
      final ViewGroup container, final Bundle savedInstanceState) {

    final View $ = inflater.inflate(R.layout.long_press_view, container, false);

    final Screen type = (Screen) getArguments()
        .getSerializable(DESCRIBABLE_KEY);

    screenType = type;

    if (type == Screen.Settings) {

      final FrameLayout box1 = (FrameLayout) $.findViewById(R.id.box1);
      final DrawView rec1 = new DrawView(getActivity(), DrawView.Color.Blue,
          DrawView.Position.Center);
      box1.addView(rec1);

      final FrameLayout icon1 = (FrameLayout) $.findViewById(R.id.icon1);
      final ImageView img1 = new ImageView(getActivity());
      img1.setImageResource(R.drawable.ic_action_help);
      icon1.addView(img1);

      final FrameLayout box4 = (FrameLayout) $.findViewById(R.id.box4);
      final DrawView rec4 = new DrawView(getActivity(), DrawView.Color.Red,
          DrawView.Position.Center);
      box4.addView(rec4);

      final FrameLayout icon4 = (FrameLayout) $.findViewById(R.id.icon4);

      backText = new TextView(getActivity());
      backText.setText(R.string.done);
      backText.setGravity(Gravity.CENTER);
      icon4.addView(backText);

    }

    else if (type == Screen.Language) {

      final FrameLayout box4 = (FrameLayout) $.findViewById(R.id.box4);
      final DrawView rec4 = new DrawView(getActivity(), DrawView.Color.Red,
          DrawView.Position.Center);
      box4.addView(rec4);

      final FrameLayout icon4 = (FrameLayout) $.findViewById(R.id.icon4);

      backText = new TextView(getActivity());
      backText.setText(R.string.back);
      backText.setGravity(Gravity.CENTER);
      icon4.addView(backText);

    }

    else if (type == Screen.Service || type == Screen.FeedOptions) {

      final FrameLayout box1 = (FrameLayout) $.findViewById(R.id.box1);
      final DrawView rec1 = new DrawView(getActivity(), DrawView.Color.Blue,
          DrawView.Position.Center);
      box1.addView(rec1);

      final FrameLayout icon1 = (FrameLayout) $.findViewById(R.id.icon1);
      final ImageView img1 = new ImageView(getActivity());
      img1.setImageResource(R.drawable.ic_action_settings);
      icon1.addView(img1);

      final FrameLayout box2 = (FrameLayout) $.findViewById(R.id.box2);
      final DrawView rec2 = new DrawView(getActivity(), DrawView.Color.Orange,
          DrawView.Position.Center);
      box2.addView(rec2);

      final FrameLayout icon2 = (FrameLayout) $.findViewById(R.id.icon2);
      final ImageView img2 = new ImageView(getActivity());
      img2.setImageResource(R.drawable.home_option);
      icon2.addView(img2);

      final FrameLayout box4 = (FrameLayout) $.findViewById(R.id.box4);
      final DrawView rec4 = new DrawView(getActivity(), DrawView.Color.Red,
          DrawView.Position.Center);
      box4.addView(rec4);

      final FrameLayout icon4 = (FrameLayout) $.findViewById(R.id.icon4);

      backText = new TextView(getActivity());
      backText.setText(R.string.back);
      backText.setGravity(Gravity.CENTER);
      icon4.addView(backText);

    }

    else if (type == Screen.Typing) {

      final FrameLayout box1 = (FrameLayout) $.findViewById(R.id.box1);
      final DrawView rec1 = new DrawView(getActivity(), DrawView.Color.Blue,
          DrawView.Position.Center);
      box1.addView(rec1);

      final FrameLayout icon1 = (FrameLayout) $.findViewById(R.id.icon1);
      final ImageView img1 = new ImageView(getActivity());
      img1.setImageResource(R.drawable.ic_action_settings);
      icon1.addView(img1);

      final FrameLayout box2 = (FrameLayout) $.findViewById(R.id.box2);
      final DrawView rec2 = new DrawView(getActivity(), DrawView.Color.Orange,
          DrawView.Position.Center);
      box2.addView(rec2);

      final FrameLayout icon2 = (FrameLayout) $.findViewById(R.id.icon2);
      final ImageView img2 = new ImageView(getActivity());
      img2.setImageResource(R.drawable.new_message);
      icon2.addView(img2);

      final FrameLayout box3 = (FrameLayout) $.findViewById(R.id.box3);
      final DrawView rec3 = new DrawView(getActivity(), DrawView.Color.Green,
          DrawView.Position.Center);
      box3.addView(rec3);

      final FrameLayout icon3 = (FrameLayout) $.findViewById(R.id.icon3);

      final ImageView img3 = new ImageView(getActivity());
      img3.setImageResource(R.drawable.ic_content_discard);
      icon3.addView(img3);

      final FrameLayout box4 = (FrameLayout) $.findViewById(R.id.box4);
      final DrawView rec4 = new DrawView(getActivity(), DrawView.Color.Red,
          DrawView.Position.Center);
      box4.addView(rec4);

      final FrameLayout icon4 = (FrameLayout) $.findViewById(R.id.icon4);

      final ImageView img4 = new ImageView(getActivity());
      img4.setImageResource(R.drawable.ic_social_send_now);
      icon4.addView(img4);

    }

    else if (type == Screen.Bluetooth) {

      final FrameLayout box4 = (FrameLayout) $.findViewById(R.id.box4);
      final DrawView rec4 = new DrawView(getActivity(), DrawView.Color.Red,
          DrawView.Position.Center);
      box4.addView(rec4);

      final FrameLayout icon4 = (FrameLayout) $.findViewById(R.id.icon4);

      backText = new TextView(getActivity());
      backText.setText(R.string.done);
      backText.setGravity(Gravity.CENTER);
      icon4.addView(backText);

    }

    else if (type == Screen.ContactHistory) {

      final FrameLayout box1 = (FrameLayout) $.findViewById(R.id.box1);
      final DrawView rec1 = new DrawView(getActivity(), DrawView.Color.Blue,
          DrawView.Position.Center);
      box1.addView(rec1);

      final FrameLayout icon1 = (FrameLayout) $.findViewById(R.id.icon1);
      final ImageView img1 = new ImageView(getActivity());
      img1.setImageResource(R.drawable.ic_action_settings);
      icon1.addView(img1);

      final FrameLayout box2 = (FrameLayout) $.findViewById(R.id.box2);
      final DrawView rec2 = new DrawView(getActivity(), DrawView.Color.Orange,
          DrawView.Position.Center);
      box2.addView(rec2);

      final FrameLayout icon2 = (FrameLayout) $.findViewById(R.id.icon2);
      final ImageView img2 = new ImageView(getActivity());
      img2.setImageResource(R.drawable.home_option);
      icon2.addView(img2);

      final FrameLayout box3 = (FrameLayout) $.findViewById(R.id.box3);
      final DrawView rec3 = new DrawView(getActivity(), DrawView.Color.Green,
          DrawView.Position.Center);
      box3.addView(rec3);

      final FrameLayout icon3 = (FrameLayout) $.findViewById(R.id.icon3);

      final ImageView img3 = new ImageView(getActivity());
      img3.setImageResource(R.drawable.ic_content_discard);
      icon3.addView(img3);

      final FrameLayout box4 = (FrameLayout) $.findViewById(R.id.box4);
      final DrawView rec4 = new DrawView(getActivity(), DrawView.Color.Red,
          DrawView.Position.Center);
      box4.addView(rec4);

      final FrameLayout icon4 = (FrameLayout) $.findViewById(R.id.icon4);

      backText = new TextView(getActivity());
      backText.setText(R.string.back);
      backText.setGravity(Gravity.CENTER);
      icon4.addView(backText);

    }

    else if (type == Screen.Conversation) {

      final FrameLayout box1 = (FrameLayout) $.findViewById(R.id.box1);
      final DrawView rec1 = new DrawView(getActivity(), DrawView.Color.Blue,
          DrawView.Position.Center);
      box1.addView(rec1);

      final FrameLayout icon1 = (FrameLayout) $.findViewById(R.id.icon1);
      final ImageView img1 = new ImageView(getActivity());
      img1.setImageResource(R.drawable.ic_action_settings);
      icon1.addView(img1);

      final FrameLayout box4 = (FrameLayout) $.findViewById(R.id.box4);
      final DrawView rec4 = new DrawView(getActivity(), DrawView.Color.Red,
          DrawView.Position.Center);
      box4.addView(rec4);

      final FrameLayout icon4 = (FrameLayout) $.findViewById(R.id.icon4);

      backText = new TextView(getActivity());
      backText.setText(R.string.back);
      backText.setGravity(Gravity.CENTER);
      icon4.addView(backText);

    }

    else if (type == Screen.Contact || type == Screen.Login) {

      final FrameLayout box1 = (FrameLayout) $.findViewById(R.id.box1);
      final DrawView rec1 = new DrawView(getActivity(), DrawView.Color.Blue,
          DrawView.Position.Center);
      box1.addView(rec1);

      final FrameLayout icon1 = (FrameLayout) $.findViewById(R.id.icon1);
      final ImageView img1 = new ImageView(getActivity());
      img1.setImageResource(R.drawable.ic_action_settings);
      icon1.addView(img1);

      final FrameLayout box2 = (FrameLayout) $.findViewById(R.id.box2);
      final DrawView rec2 = new DrawView(getActivity(), DrawView.Color.Orange,
          DrawView.Position.Center);
      box2.addView(rec2);

      final FrameLayout icon2 = (FrameLayout) $.findViewById(R.id.icon2);
      final ImageView img2 = new ImageView(getActivity());
      img2.setImageResource(R.drawable.home_option);
      icon2.addView(img2);

      final FrameLayout box3 = (FrameLayout) $.findViewById(R.id.box3);
      final DrawView rec3 = new DrawView(getActivity(), DrawView.Color.Green,
          DrawView.Position.Center);
      box3.addView(rec3);

      final FrameLayout icon3 = (FrameLayout) $.findViewById(R.id.icon3);

      final ImageView img3 = new ImageView(getActivity());
      img3.setImageResource(R.drawable.ic_content_discard);
      icon3.addView(img3);

      final FrameLayout box4 = (FrameLayout) $.findViewById(R.id.box4);
      final DrawView rec4 = new DrawView(getActivity(), DrawView.Color.Red,
          DrawView.Position.Center);
      box4.addView(rec4);

      final FrameLayout icon4 = (FrameLayout) $.findViewById(R.id.icon4);

      backText = new TextView(getActivity());
      backText.setText(R.string.back);
      backText.setGravity(Gravity.CENTER);
      icon4.addView(backText);

    }

    else if (type == Screen.Volume) {
      final FrameLayout box4 = (FrameLayout) $.findViewById(R.id.box4);
      final DrawView rec4 = new DrawView(getActivity(), DrawView.Color.Red,
          DrawView.Position.Center);
      box4.addView(rec4);

      final FrameLayout icon4 = (FrameLayout) $.findViewById(R.id.icon4);

      backText = new TextView(getActivity());
      backText.setText(R.string.done);
      backText.setGravity(Gravity.CENTER);
      icon4.addView(backText);
    }

    else if (type == Screen.Posts) {

      final FrameLayout box1 = (FrameLayout) $.findViewById(R.id.box1);
      final DrawView rec1 = new DrawView(getActivity(), DrawView.Color.Blue,
          DrawView.Position.Center);
      box1.addView(rec1);

      final FrameLayout icon1 = (FrameLayout) $.findViewById(R.id.icon1);
      final ImageView img1 = new ImageView(getActivity());
      img1.setImageResource(R.drawable.ic_action_settings);
      icon1.addView(img1);

      final FrameLayout box2 = (FrameLayout) $.findViewById(R.id.box2);
      final DrawView rec2 = new DrawView(getActivity(), DrawView.Color.Orange,
          DrawView.Position.Center);
      box2.addView(rec2);

      final FrameLayout icon2 = (FrameLayout) $.findViewById(R.id.icon2);
      final ImageView img2 = new ImageView(getActivity());
      img2.setImageResource(R.drawable.refresh);
      icon2.addView(img2);

      final FrameLayout box3 = (FrameLayout) $.findViewById(R.id.box3);
      final DrawView rec3 = new DrawView(getActivity(), DrawView.Color.Green,
          DrawView.Position.Center);
      box3.addView(rec3);

      final FrameLayout icon3 = (FrameLayout) $.findViewById(R.id.icon3);
      final ImageView img3 = new ImageView(getActivity());
      img3.setImageResource(R.drawable.add_post);
      icon3.addView(img3);

      final FrameLayout box4 = (FrameLayout) $.findViewById(R.id.box4);
      final DrawView rec4 = new DrawView(getActivity(), DrawView.Color.Red,
          DrawView.Position.Center);
      box4.addView(rec4);

      final FrameLayout icon4 = (FrameLayout) $.findViewById(R.id.icon4);

      backText = new TextView(getActivity());
      backText.setText(R.string.back);
      backText.setGravity(Gravity.CENTER);
      icon4.addView(backText);

    }

    else {
      final FrameLayout box1 = (FrameLayout) $.findViewById(R.id.box1);
      final DrawView rec1 = new DrawView(getActivity(), DrawView.Color.Blue,
          DrawView.Position.Center);
      box1.addView(rec1);

      final FrameLayout icon1 = (FrameLayout) $.findViewById(R.id.icon1);
      final ImageView img1 = new ImageView(getActivity());
      img1.setImageResource(R.drawable.ic_action_settings);
      icon1.addView(img1);

      final FrameLayout box2 = (FrameLayout) $.findViewById(R.id.box2);
      final DrawView rec2 = new DrawView(getActivity(), DrawView.Color.Orange,
          DrawView.Position.Center);
      box2.addView(rec2);

      final FrameLayout icon2 = (FrameLayout) $.findViewById(R.id.icon2);
      final ImageView img2 = new ImageView(getActivity());
      img2.setImageResource(R.drawable.new_message);
      icon2.addView(img2);

      final FrameLayout box3 = (FrameLayout) $.findViewById(R.id.box3);
      final DrawView rec3 = new DrawView(getActivity(), DrawView.Color.Green,
          DrawView.Position.Center);
      box3.addView(rec3);

      final FrameLayout icon3 = (FrameLayout) $.findViewById(R.id.icon3);

      final ImageView img3 = new ImageView(getActivity());
      img3.setImageResource(R.drawable.ic_content_discard);
      icon3.addView(img3);

      final FrameLayout box4 = (FrameLayout) $.findViewById(R.id.box4);
      final DrawView rec4 = new DrawView(getActivity(), DrawView.Color.Red,
          DrawView.Position.Center);
      box4.addView(rec4);

      final FrameLayout icon4 = (FrameLayout) $.findViewById(R.id.icon4);

      backText = new TextView(getActivity());
      backText.setText(R.string.back);
      backText.setGravity(Gravity.CENTER);
      icon4.addView(backText);

    }

    return $;

  }

  @Override
  public void onResume() {
    super.onResume();
    refreshFragment();
  }

  private void refreshFragment() {

    switch (screenType) {
    case Typing:
      break;
    case Login:
      backText.setText(R.string.back);
      break;
    case Contact:
      backText.setText(R.string.back);
      break;
    case Settings:
      backText.setText(R.string.done);
      break;
    case Language:
      backText.setText(R.string.back);
      break;
    case Service:
      backText.setText(R.string.back);
      break;
    case Volume:
      backText.setText(R.string.done);
      break;
    default:
      backText.setText(R.string.back);
      break;

    }

  }

}
