package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import il.ac.technion.cs.ssdl.cs234311.yp09.commsModule.GoogleItem;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 30/5/2014
 * 
 */
public class GoogleContactListViewAdapter extends ArrayAdapter<GoogleItem> {

  Context context;

  /**
   * @param context
   *          current context
   * @param resourceId
   *          resource identifier
   * @param items
   *          list of sms items
   */
  public GoogleContactListViewAdapter(final Context context,
      final int resourceId, final List<GoogleItem> items) {
    super(context, resourceId, items);
    this.context = context;
  }

  /* private view holder class */
  private class ViewHolder {
    public ViewHolder() {
      // TODO Auto-generated constructor stub
    }

    ImageView googleImage;
    TextView contactName;
    TextView contactID;
  }

  @Override
  public View getView(final int position, View convertView,
      final ViewGroup parent) {

    ViewHolder holder = null;
    View $ = convertView;
    final GoogleItem googleRow = getItem(position);

    final LayoutInflater mInflater = (LayoutInflater) context
        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    if ($ == null) {
      $ = mInflater.inflate(R.layout.contact_listview, null);
      holder = new ViewHolder();
      holder.contactName = (TextView) $.findViewById(R.id.name);
      holder.contactID = (TextView) $.findViewById(R.id.number);
      holder.googleImage = (ImageView) $.findViewById(R.id.type);
      $.setTag(holder);
    } else
      holder = (ViewHolder) $.getTag();

    holder.contactName.setText(googleRow.contactName);
    if (googleRow.contactID != null
        && googleRow.contactID.endsWith("gmail.com"))
      holder.contactName.setText(googleRow.contactID);

    if (googleRow.contactPresence.equals("available")) {
      holder.googleImage.setImageDrawable(context.getResources().getDrawable(
          R.drawable.available));
      holder.contactID.setText("available");
    } else if (googleRow.contactPresence.equals("unavailable")) {
      holder.googleImage.setImageDrawable(context.getResources().getDrawable(
          R.drawable.signout));
      holder.contactID.setText("unavailable");
    } else if (googleRow.contactPresence.equals("dnd")) {
      holder.googleImage.setImageDrawable(context.getResources().getDrawable(
          R.drawable.busy));
      holder.contactID.setText("busy");
    } else if (googleRow.contactPresence.equals("chat")) {
      holder.googleImage.setImageDrawable(context.getResources().getDrawable(
          R.drawable.available));
      holder.contactID.setText("available");
    } else if (googleRow.contactPresence.equals("xa")) {
      holder.googleImage.setImageDrawable(context.getResources().getDrawable(
          R.drawable.away));
      holder.contactID.setText("away");
    } else if (googleRow.contactPresence.equals("away")) {
      holder.googleImage.setImageDrawable(context.getResources().getDrawable(
          R.drawable.away));
      holder.contactID.setText("away");
    } else {
      holder.googleImage.setImageDrawable(context.getResources().getDrawable(
          R.drawable.signout));
      holder.contactID.setText("unavailable");
    }
    if (googleRow.contactID != null
        && googleRow.contactID.endsWith("gmail.com"))
      holder.contactName.setText(googleRow.contactID);

    return $;

  }

}
