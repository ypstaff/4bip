package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import android.app.Fragment;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 31/5/2014
 * 
 */
public class ConversationFragment extends Fragment {

  private static final String DESCRIBABLE_KEY = "describable_key";
  private TextView backText;
  private TextView contactTextView;
  private String name;

  /**
   * @param contactName
   *          the cantact's name - passed from the previous activity
   * @return Instance of LongFragment
   */
  public static ConversationFragment newInstance(final String contactName) {
    final ConversationFragment conversationFragment = new ConversationFragment();
    final Bundle bundle = new Bundle();
    bundle.putSerializable(DESCRIBABLE_KEY, contactName);
    conversationFragment.setArguments(bundle);

    return conversationFragment;
  }

  @Override
  public View onCreateView(final LayoutInflater inflater,
      final ViewGroup container, final Bundle savedInstanceState) {

    final View $ = inflater.inflate(R.layout.conversation_long_fragment,
        container, false);

    final String contactName = (String) getArguments().getSerializable(
        DESCRIBABLE_KEY);

    name = contactName;

    final FrameLayout box1 = (FrameLayout) $.findViewById(R.id.box1);
    final DrawView rec1 = new DrawView(getActivity(), DrawView.Color.Blue,
        DrawView.Position.Center);
    box1.addView(rec1);

    final FrameLayout icon1 = (FrameLayout) $.findViewById(R.id.icon1);
    final ImageView img1 = new ImageView(getActivity());
    img1.setImageResource(R.drawable.ic_action_settings);
    icon1.addView(img1);

    contactTextView = (TextView) $.findViewById(R.id.contactName);
    contactTextView.setText(contactName);
    contactTextView.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);

    final FrameLayout box4 = (FrameLayout) $.findViewById(R.id.box4);
    final DrawView rec4 = new DrawView(getActivity(), DrawView.Color.Red,
        DrawView.Position.Center);
    box4.addView(rec4);

    final FrameLayout icon4 = (FrameLayout) $.findViewById(R.id.icon4);

    backText = new TextView(getActivity());
    backText.setText(R.string.back);
    backText.setGravity(Gravity.CENTER);
    icon4.addView(backText);

    return $;

  }

  @Override
  public void onResume() {
    super.onResume();
    refreshFragment();
  }

  private void refreshFragment() {
    backText.setText(R.string.back);
    contactTextView.setText(name);
  }
}
