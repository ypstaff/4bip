package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import il.ac.technion.cs.ssdl.cs234311.yp09.commsModule.FacebookItem;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 17/1/2014
 * 
 */
public class FacebookListViewAdapter extends ArrayAdapter<FacebookItem> {

  Context context;

  /**
   * @param context
   *          current context
   * @param resourceId
   *          identifier of resource
   * @param items
   *          list of facebook contacts
   */
  public FacebookListViewAdapter(final Context context, final int resourceId,
      final List<FacebookItem> items) {
    super(context, resourceId, items);
    this.context = context;
  }

  /* private view holder class */
  private class ViewHolder {
    public ViewHolder() {
      // TODO Auto-generated constructor stub
    }

    ImageView contactAvatar;
    TextView contactName;
    TextView contactID;
    CheckBox checkbox;
  }

  @Override
  public View getView(final int position, View convertView,
      final ViewGroup parent) {

    ViewHolder holder = null;
    View $ = convertView;
    final FacebookItem facebookRow = getItem(position);

    final LayoutInflater mInflater = (LayoutInflater) context
        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    if ($ == null) {
      $ = mInflater.inflate(R.layout.facebook_listview, null);
      holder = new ViewHolder();
      holder.contactName = (TextView) $.findViewById(R.id.username);
      holder.contactID = (TextView) $.findViewById(R.id.mail);
      holder.contactAvatar = (ImageView) $.findViewById(R.id.avatar);
      holder.checkbox = (CheckBox) $.findViewById(R.id.check_box);
      $.setTag(holder);
    } else
      holder = (ViewHolder) $.getTag();

    holder.contactName.setText(facebookRow.contactName);

    if (facebookRow.contactPresence.equals("available")) {
      holder.contactAvatar.setImageDrawable(context.getResources().getDrawable(
          R.drawable.available));
      holder.contactID.setText("available");
    } else if (facebookRow.contactPresence.equals("unavailable")) {
      holder.contactAvatar.setImageDrawable(context.getResources().getDrawable(
          R.drawable.signout));
      holder.contactID.setText("unavailable");
    } else if (facebookRow.contactPresence.equals("dnd")) {
      holder.contactAvatar.setImageDrawable(context.getResources().getDrawable(
          R.drawable.busy));
      holder.contactID.setText("busy");
    } else if (facebookRow.contactPresence.equals("chat")) {
      holder.contactAvatar.setImageDrawable(context.getResources().getDrawable(
          R.drawable.available));
      holder.contactID.setText("available");
    } else if (facebookRow.contactPresence.equals("xa")) {
      holder.contactAvatar.setImageDrawable(context.getResources().getDrawable(
          R.drawable.away));
      holder.contactID.setText("away");
    } else if (facebookRow.contactPresence.equals("away")) {
      holder.contactAvatar.setImageDrawable(context.getResources().getDrawable(
          R.drawable.away));
      holder.contactID.setText("away");
    } else {
      holder.contactAvatar.setImageDrawable(context.getResources().getDrawable(
          R.drawable.signout));
      holder.contactID.setText("unavailable");
    }

    if (facebookRow.checked)
      holder.checkbox.setChecked(true);
    else
      holder.checkbox.setChecked(false);

    return $;

  }
}
