package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import android.app.Fragment;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 10/1/2014
 * 
 */
public class ScrollingFragment extends Fragment {

  private static final String SCROLL = "il.ac.technion.cs.ssdl.cs234311.yp09.scroll";
  private TextView selectView;
  private TextView doneView;
  private Screen type;

  /**
   * @author Muhammad
   * 
   */
  public enum Screen {
    /**
     * Typing Screen
     */
    Typing, /**
     * Login Screen
     */
    Login, /**
     * Contact Screen
     */
    Contact, /**
     * Settings Screen
     */
    Settings, /**
     * Language Screen
     */
    Language, /**
     * Service Screen
     */
    Service, /**
     * Contact History screen
     */
    ContactHistory, /**
     * Conversation screen
     */
    Conversation
  }

  /**
   * @param type
   *          current screen type
   * @return instance of a scrolling fragment
   */
  public static ScrollingFragment newInstance(final Screen type) {
    final ScrollingFragment scrollingFragment = new ScrollingFragment();
    final Bundle bundle = new Bundle();
    bundle.putSerializable(SCROLL, type);
    scrollingFragment.setArguments(bundle);

    return scrollingFragment;
  }

  @Override
  public View onCreateView(final LayoutInflater inflater,
      final ViewGroup container, final Bundle savedInstanceState) {

    final View $ = inflater.inflate(R.layout.scroll_fragment, container, false);

    selectView = (TextView) $.findViewById(R.id.select_icon_frame);

    final FrameLayout up_icon_frame = (FrameLayout) $
        .findViewById(R.id.up_icon_frame);
    final ImageView up_icon_img = new ImageView(getActivity());
    up_icon_img.setImageResource(R.drawable.up);
    up_icon_frame.addView(up_icon_img);

    final FrameLayout up_box_frame = (FrameLayout) $
        .findViewById(R.id.up_box_frame);
    final DrawView rec_up = new DrawView(getActivity(), DrawView.Color.Orange,
        DrawView.Position.Center);
    up_box_frame.addView(rec_up);

    final FrameLayout down_icon_frame = (FrameLayout) $
        .findViewById(R.id.down_icon_frame);
    final ImageView down_icon_img = new ImageView(getActivity());
    down_icon_img.setImageResource(R.drawable.down);
    down_icon_frame.addView(down_icon_img);

    final FrameLayout down_box_frame = (FrameLayout) $
        .findViewById(R.id.down_box_frame);
    final DrawView rec_down = new DrawView(getActivity(), DrawView.Color.Green,
        DrawView.Position.Center);
    down_box_frame.addView(rec_down);

    final FrameLayout select_box_frame = (FrameLayout) $
        .findViewById(R.id.select_box_frame);
    final DrawView rec_select = new DrawView(getActivity(), DrawView.Color.Red,
        DrawView.Position.Center);
    select_box_frame.addView(rec_select);

    type = (Screen) getArguments().getSerializable(SCROLL);
    if (type == Screen.Contact) {
      final FrameLayout done_box_frame = (FrameLayout) $
          .findViewById(R.id.done_box_frame);
      final DrawView rec_done = new DrawView(getActivity(),
          DrawView.Color.Blue, DrawView.Position.Center);
      done_box_frame.addView(rec_done);

      final FrameLayout done_frame = (FrameLayout) $
          .findViewById(R.id.done_frame);
      doneView = new TextView(getActivity());
      doneView.setText(R.string.done);
      doneView.setGravity(Gravity.CENTER);
      done_frame.addView(doneView);

      final FrameLayout keyboard_box_frame_1 = (FrameLayout) $
          .findViewById(R.id.keyboard_box_frame_1);
      final DrawView rec_keyboard_1 = new DrawView(getActivity(),
          DrawView.Color.Orange, DrawView.Position.Right);
      keyboard_box_frame_1.addView(rec_keyboard_1);

      final FrameLayout keyboard_box_frame_2 = (FrameLayout) $
          .findViewById(R.id.keyboard_box_frame_2);
      final DrawView rec_keyboard_2 = new DrawView(getActivity(),
          DrawView.Color.Green, DrawView.Position.Left);
      keyboard_box_frame_2.addView(rec_keyboard_2);

      final FrameLayout keyboard_icon_frame = (FrameLayout) $
          .findViewById(R.id.keyboard_icon_frame);
      final ImageView img1 = new ImageView(getActivity());
      img1.setImageResource(R.drawable.keyboard);
      keyboard_icon_frame.addView(img1);
    }

    if (type == Screen.ContactHistory) {
      final FrameLayout keyboard_box_frame_1 = (FrameLayout) $
          .findViewById(R.id.keyboard_box_frame_1);
      final DrawView rec_keyboard_1 = new DrawView(getActivity(),
          DrawView.Color.Orange, DrawView.Position.Right);
      keyboard_box_frame_1.addView(rec_keyboard_1);

      final FrameLayout keyboard_box_frame_2 = (FrameLayout) $
          .findViewById(R.id.keyboard_box_frame_2);
      final DrawView rec_keyboard_2 = new DrawView(getActivity(),
          DrawView.Color.Green, DrawView.Position.Left);
      keyboard_box_frame_2.addView(rec_keyboard_2);

      final FrameLayout keyboard_icon_frame = (FrameLayout) $
          .findViewById(R.id.keyboard_icon_frame);
      final ImageView img1 = new ImageView(getActivity());
      img1.setImageResource(R.drawable.keyboard);
      keyboard_icon_frame.addView(img1);
    }

    if (type == Screen.Conversation) {
      final FrameLayout done_box_frame = (FrameLayout) $
          .findViewById(R.id.done_box_frame);
      final DrawView rec_done = new DrawView(getActivity(),
          DrawView.Color.Blue, DrawView.Position.Center);
      done_box_frame.addView(rec_done);

      final FrameLayout done_frame = (FrameLayout) $
          .findViewById(R.id.done_frame);
      final ImageView home_img = new ImageView(getActivity());
      home_img.setImageResource(R.drawable.home_option);
      done_frame.addView(home_img);
    }

    return $;

  }

  @Override
  public void onResume() {
    super.onResume();
    refreshFragment();
  }

  private void refreshFragment() {
    selectView.setText(R.string.select);
    if (type == Screen.Contact)
      doneView.setText(R.string.done);
  }
}
