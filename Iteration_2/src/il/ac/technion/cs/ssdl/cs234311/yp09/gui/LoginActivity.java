package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.BaseKeyboardFragment;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.IActivityWithKeyboard;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.IKeyboardListener;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.ILoginActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.InputColor;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.KeyboardFragment;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.KeyboardFragment.KeyboardStyle;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.KeyboardFragment2;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.KeyboardListener;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.KeyboardType;
import il.ac.technion.cs.ssdl.cs234311.yp09.user.UserInfoFileIO;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 3/1/2014
 * 
 */
public class LoginActivity extends GeneralActivity implements
    IActivityWithKeyboard, ILoginActivity {

  LongFragment longFragment;
  ProgressBarFragment progressBarFragment;
  BaseKeyboardFragment keyboardFragment;

  private String value;
  String nextActivity;

  private boolean debugMode = false;

  private EditText username;
  private EditText password;
  static Dialog splashDialog = null;
  /**
   * G-Talk username
   */
  public static String googleUser;
  /**
   * G-Talk password
   */
  public static String googlePass;
  /**
   * Facebook username
   */
  public static String facebookUser;
  /**
   * Facebook password
   */
  public static String facebookPass;

  /**
   * a key used to send a parameter to an activity
   */
  public static final String Login = "il.ac.technion.cs.ssdl.cs234311.yp09.gui.login";

  /**
   * a key used to save info about the next activity.
   */
  public static final String Next = "il.ac.technion.cs.ssdl.cs234311.yp09.gui.login.next";

  @Override
  protected void onCreate(final Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    splashDialog = null;
    setContentView(R.layout.activity_login);

    longFragment = LongFragment.newInstance(LongFragment.Screen.Login);
    getFragmentManager().beginTransaction()
        .add(R.id.long_press_info_frame, longFragment).commit();

    progressBarFragment = ProgressBarFragment
        .newInstance(ProgressBarFragment.Screen.Login);
    getFragmentManager().beginTransaction()
        .add(R.id.progress_bar_frame, progressBarFragment).commit();

    SwitchFragment switchFragment = new SwitchFragment();
    getFragmentManager().beginTransaction()
        .add(R.id.switch_frame, switchFragment).commit();

    switch (TypingActivity.getKeyboardFragmentType()) {
    case DEFAULT:
      KeyboardFragment.setKeyboardStyle(KeyboardStyle.DEFAULT);
      keyboardFragment = new KeyboardFragment();
      setKeyboardType();
      getFragmentManager().beginTransaction()
          .add(R.id.typing_frame, keyboardFragment).commit();
      break;
    case ALPHABETICAL:
      KeyboardFragment.setKeyboardStyle(KeyboardStyle.ALPHABETICAL);
      keyboardFragment = new KeyboardFragment();
      setKeyboardType();
      getFragmentManager().beginTransaction()
          .add(R.id.typing_frame, keyboardFragment).commit();
      break;
    case KEYBOARD2:
      keyboardFragment = new KeyboardFragment2();
      setKeyboardType();
      getFragmentManager().beginTransaction()
          .add(R.id.typing_frame, keyboardFragment).commit();
      break;
    default:
      // TODO
      break;
    }

    username = (EditText) findViewById(R.id.username_frame);
    password = (EditText) findViewById(R.id.password_frame);

    final FrameLayout icon_frame = (FrameLayout) findViewById(R.id.protocol_icon);
    final ImageView img = new ImageView(this);

    final Intent intent = getIntent();
    value = intent.getStringExtra(Login);
    nextActivity = intent.getStringExtra(Next);

    if (value.equals("google")) {
      img.setImageResource(R.drawable.google_talk);
      icon_frame.addView(img);
    } else if (value.equals("facebook")) {
      img.setImageResource(R.drawable.facebook_big);
      icon_frame.addView(img);
    }

    getFragmentManager().beginTransaction()
        .add(R.id.buttons_frame, mFBFragment).commit();

    refreshActivity();

  }

  @Override
  protected void onPause() {
    super.onPause();
  }

  private void setKeyboardType() {
    try {
      keyboardFragment.setKeyboardType(KeyboardType.USERNAME_PASSWORD);
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  @Override
  protected void onResume() {
    super.onResume();
    refreshActivity();

    if (!TypingActivity.getChangeKeyboard())
      return;

    TypingActivity.setChangeKeyboard(false);

    switch (TypingActivity.getKeyboardFragmentType()) {
    case DEFAULT:
      KeyboardFragment.setKeyboardStyle(KeyboardStyle.DEFAULT);
      keyboardFragment = new KeyboardFragment();
      setKeyboardType();
      getFragmentManager().beginTransaction()
          .replace(R.id.typing_frame, keyboardFragment).commit();

      break;
    case ALPHABETICAL:
      KeyboardFragment.setKeyboardStyle(KeyboardStyle.ALPHABETICAL);
      keyboardFragment = new KeyboardFragment();
      setKeyboardType();
      getFragmentManager().beginTransaction()
          .replace(R.id.typing_frame, keyboardFragment).commit();

      break;
    case KEYBOARD2:
      keyboardFragment = new KeyboardFragment2();
      setKeyboardType();
      getFragmentManager().beginTransaction()
          .replace(R.id.typing_frame, keyboardFragment).commit();
      break;
    default:
      // TODO
      break;
    }
  }

  @Override
  protected void onStart() {
    super.onStart();
    EditText userNameEditText = (EditText) findViewById(R.id.username_frame);
    EditText passwordEditText = (EditText) findViewById(R.id.password_frame);

    if (userNameEditText == null || passwordEditText == null)
      return;

    userNameEditText.setOnTouchListener(new NoTouchListener());
    passwordEditText.setOnTouchListener(new NoTouchListener());

    if (debugMode)
      if (value.equals("facebook")) {
        userNameEditText.setText("4biptest@gmail.com");
        passwordEditText.setText("google4life");
      } else if (value.equals("google")) {
        userNameEditText.setText("4biptest");
        passwordEditText.setText("google4life");
      }
  }

  @Override
  public boolean onCreateOptionsMenu(final Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.login, menu);
    return true;
  }

  @Override
  public IKeyboardListener getKeyboardListener() {
    final EditText messageEditText = (EditText) findViewById(R.id.username_frame);
    if (null == messageEditText)
      return null;
    attached = true;
    messageEditText.setOnTouchListener(new NoTouchListener());
    return new KeyboardListener(messageEditText, false);
  }

  @Override
  public boolean onPrepareOptionsMenu(Menu menu) {
    return false;
  }

  @Override
  public void onOperation(final int c) {
    super.onOperation(c);
    Intent intent;
    switch (OpCodeInterpreter.getOp(c)) {
    case BLUE:
      makeSound(0);
      keyboardFragment.click(new InputColor[] { InputColor.blue });
      break;
    case BLUE_ORANGE:
      makeSound(0);
      keyboardFragment.click(new InputColor[] { InputColor.blue,
          InputColor.orange });
      break;
    case BLUE_RED:
      makeSound(0);
      keyboardFragment
          .click(new InputColor[] { InputColor.red, InputColor.blue });
      break;
    case GREEN:
      makeSound(0);
      keyboardFragment.click(new InputColor[] { InputColor.green });
      break;
    case GREEN_RED:
      makeSound(0);
      keyboardFragment.click(new InputColor[] { InputColor.green,
          InputColor.red });
      break;
    case INVALID:
      break;
    case LONG_BLUE:
      makeSound(1);
      intent = new Intent(this, SettingsActivity.class);
      intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
      intent.putExtra(LanguageActivity.languageKey, Controller.language);
      startActivity(intent);
      break;
    case LONG_GREEN: // Delete all
      makeSound(1);
      keyboardFragment.longClick(new InputColor[] { InputColor.green });
      break;
    case LONG_ORANGE:
      makeSound(1);
      intent = new Intent(this, TypingActivity.class);
      intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
      startActivity(intent);
      break;
    case LONG_RED:
      makeSound(1);
      finish();
      overridePendingTransition(0, 0);
      break;
    case ORANGE:
      makeSound(0);
      keyboardFragment.click(new InputColor[] { InputColor.orange });
      break;
    case ORANGE_GREEN:
      makeSound(0);
      keyboardFragment.click(new InputColor[] { InputColor.orange,
          InputColor.green });
      break;
    case RED:
      makeSound(0);
      keyboardFragment.click(new InputColor[] { InputColor.red });
      break;
    default:
      break;
    }
  }

  @Override
  public void onLoginClick() {

    EditText userText = (EditText) findViewById(R.id.username_frame);
    userText.setOnTouchListener(new NoTouchListener());
    String usernameTyped = userText.getText().toString();

    EditText passText = (EditText) findViewById(R.id.password_frame);
    passText.setOnTouchListener(new NoTouchListener());
    String passwordTyped = passText.getText().toString();
    if (splashDialog == null) {
      splashDialog = new Dialog(this);
      splashDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
      splashDialog.getWindow().setBackgroundDrawable(
          new ColorDrawable(Color.TRANSPARENT));
      splashDialog.setCancelable(false);
      splashDialog.setContentView(R.layout.dialog_login_splash);
      WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
      lp.copyFrom(splashDialog.getWindow().getAttributes());
      lp.width = 500;
      lp.height = 500;
      splashDialog.getWindow().setAttributes(lp);
    }

    if (value.equals("google")) {
      @SuppressLint("HandlerLeak")
      class ConnHandlerGTalk extends Handler {
        public LoginActivity logAct;

        public ConnHandlerGTalk(LoginActivity logAct) {
          this.logAct = logAct;

        }

        @Override
        public void handleMessage(android.os.Message msg) {
          switch (msg.what) {
          case 0:
            splashDialog.dismiss();
            splashDialog.cancel();
            if (commsModule.isConnectedGTalk()) {
              // Added by muhammad
              if (nextActivity == null) {
                Intent newIntent = new Intent(logAct, ContactActivity.class);
                newIntent.putExtra(ContactActivity.Service, "google");
                newIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(newIntent);
              } else {

                Intent newIntent = new Intent(logAct,
                    ContactHistoryActivity.class);
                newIntent.putExtra(ContactHistoryActivity.contactHistoryKey,
                    "google");
                newIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(newIntent);
              }
            } else
              Toast.makeText(logAct, R.string.no_internet, Toast.LENGTH_SHORT)
                  .show();

            break;
          case 1:
            splashDialog.dismiss();
            Toast.makeText(logAct,
                getResources().getString(R.string.cannot_connect),
                Toast.LENGTH_SHORT).show();
            break;
          case 2:
            splashDialog.dismiss();
            Toast.makeText(logAct,
                getResources().getString(R.string.cannot_login),
                Toast.LENGTH_SHORT).show();
            break;
          default:
            break;
          }
        }

      }
      ConnHandlerGTalk myHandler = new ConnHandlerGTalk(this);

      googleUser = usernameTyped;
      googlePass = passwordTyped;
      commsModule.loginGTalk(usernameTyped, passwordTyped, myHandler);
      splashDialog.show();
      final Timer t = new Timer();
      t.schedule(new TimerTask() {
        @Override
        public void run() {
          if (splashDialog != null) {
            splashDialog.dismiss();
            splashDialog.cancel();
          }// when the task active then close the dialog
          t.cancel(); // also just top the timer thread, otherwise, you may
                      // receive a crash report
        }
      }, 20000);

    } else {
      @SuppressLint("HandlerLeak")
      class ConnHandlerFacebook extends Handler {
        public LoginActivity logAct;

        public ConnHandlerFacebook(LoginActivity logAct) {
          this.logAct = logAct;

        }

        @Override
        public void handleMessage(android.os.Message msg) {
          switch (msg.what) {
          case 0:
            splashDialog.dismiss();
            splashDialog.cancel();
            if (commsModule.isConnectedFacebook()) {
              if (nextActivity == null) {
                Intent newIntent = new Intent(logAct, ContactActivity.class);
                newIntent.putExtra(ContactActivity.Service, "facebook");
                newIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(newIntent);
              } else {
                Intent newIntent = new Intent(logAct,
                    ContactHistoryActivity.class);
                newIntent.putExtra(ContactHistoryActivity.contactHistoryKey,
                    "facebook");
                newIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(newIntent);
              }
            } else
              Toast.makeText(logAct, R.string.no_internet, Toast.LENGTH_SHORT)
                  .show();

            break;
          case 1:
            splashDialog.dismiss();
            splashDialog.cancel();
            Toast.makeText(logAct,
                getResources().getString(R.string.cannot_connect),
                Toast.LENGTH_SHORT).show();
            break;
          case 2:
            splashDialog.dismiss();
            splashDialog.cancel();
            Toast.makeText(logAct,
                getResources().getString(R.string.cannot_login),
                Toast.LENGTH_SHORT).show();
            break;
          default:
            break;
          }
        }
      }

      ConnHandlerFacebook myHandler = new ConnHandlerFacebook(this);
      facebookUser = usernameTyped;
      facebookPass = passwordTyped;
      commsModule.loginFacebook(usernameTyped, passwordTyped, myHandler);
      splashDialog.show();
      final Timer t = new Timer();
      t.schedule(new TimerTask() {
        @Override
        public void run() {
          if (splashDialog != null) {
            splashDialog.dismiss();
            splashDialog.cancel();
          }// when the task active then close the dialog
          t.cancel(); // also just top the timer thread, otherwise, you may
                      // receive a crash report
        }
      }, 20000);

    }

  }

  @Override
  public void onSetFocusOnPassword() {
    final EditText messageEditText = (EditText) findViewById(R.id.password_frame);
    messageEditText.requestFocus();
    keyboardFragment.setKeyboardListener(new KeyboardListener(messageEditText,
        false));
  }

  @Override
  public void onSetFocusOnUsername() {
    final EditText messageEditText = (EditText) findViewById(R.id.username_frame);
    messageEditText.requestFocus();
    keyboardFragment.setKeyboardListener(new KeyboardListener(messageEditText,
        false));
  }

  private boolean attached = false;

  private void refreshActivity() {

    setTitle(R.string.login);

    if (keyboardFragment != null && attached)
      keyboardFragment.refreshFragment();

    username.setHint(R.string.username);
    password.setHint(R.string.password);

    if (value.equals("facebook")) {
      ArrayList<String> details = UserInfoFileIO.getInfoFromFile(this,
          "facebook");
      if (details != null) {
        username.setText(details.get(0));
        password.setText(details.get(1));
      }
    } else if (value.equals("google")) {
      ArrayList<String> details = UserInfoFileIO
          .getInfoFromFile(this, "google");
      if (details != null) {
        username.setText(details.get(0));
        password.setText(details.get(1));
      }

    }
  }
}
