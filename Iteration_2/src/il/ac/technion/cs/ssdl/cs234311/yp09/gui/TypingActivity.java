package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import il.ac.technion.cs.ssdl.cs234311.yp09.contacts.SMSItem;
import il.ac.technion.cs.ssdl.cs234311.yp09.facebookFeed.FacebookManager;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.BaseKeyboardFragment;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.IActivityWithKeyboard;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.IKeyboardListener;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.ITypingDefaultActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.InputColor;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.KeyboardFragment;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.KeyboardFragment.KeyboardStyle;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.KeyboardFragment2;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.KeyboardFragmentType;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.KeyboardListener;
import il.ac.technion.cs.ssdl.cs234311.yp09.utilities.AutoComplete;
import il.ac.technion.cs.ssdl.cs234311.yp09.utilities.SaveStatistics;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import android.telephony.SmsMessage;
import android.view.Gravity;
import android.view.Menu;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;

/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 3/1/2014
 * 
 */
public class TypingActivity extends GeneralActivity implements
    IActivityWithKeyboard, ITypingDefaultActivity {

  /**
   * a key for passing an argument to the next activity
   */
  public static final String Facebook_Command = "__Facebook_Command";

  /**
   * In case there is a need for post id in order to do the Facebook command
   */
  public static final String Facebook_Command_Post_Id = "__Facebook_Command_Post_Id";
  private String mPostId = null;

  LongFragment longFragment;
  ProgressBarFragment progressBarFragment;
  MessageFragment messageFragment;
  AutoCompleteFragment autoCompleteFragment;
  BaseKeyboardFragment keyboardFragment;
  static BroadcastReceiver smsReceiver = null;
  static boolean recieverRegistered = false;

  private enum FacebookCommand {
    POST, COMMENT, NOTHING
  }

  private FacebookCommand mFacebookCommand = FacebookCommand.NOTHING;

  /**
   * For auto complete
   */
  public AutoComplete autoComplete;

  /**
   * Keyboard type
   */
  private static KeyboardFragmentType keyboardFragmentType = KeyboardFragmentType.DEFAULT;

  private static boolean changeKeyboard = false;

  /**
   * @param inChangeKeyboard
   *          changeKeyboard
   */
  public static void setChangeKeyboard(boolean inChangeKeyboard) {
    changeKeyboard = inChangeKeyboard;
  }

  /**
   * @return changeKeyboard
   */
  public static boolean getChangeKeyboard() {
    return changeKeyboard;
  }

  /**
   * @return keyboardFragmentType
   */
  public static KeyboardFragmentType getKeyboardFragmentType() {
    return keyboardFragmentType;
  }

  /**
   * @param inKeyboardFragmentType
   *          keyboard type
   */
  public static void setKeyboardFragmentType(
      KeyboardFragmentType inKeyboardFragmentType) {
    keyboardFragmentType = inKeyboardFragmentType;
    changeKeyboard = true;
  }

  @Override
  protected void onCreate(final Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.activity_typing);
    setTitle(R.string.typing);
    longFragment = LongFragment.newInstance(LongFragment.Screen.Typing);
    getFragmentManager().beginTransaction()
        .add(R.id.long_press_info_frame, longFragment).commit();

    progressBarFragment = ProgressBarFragment
        .newInstance(ProgressBarFragment.Screen.Typing);
    getFragmentManager().beginTransaction()
        .add(R.id.progress_bar_frame, progressBarFragment).commit();

    messageFragment = new MessageFragment();
    getFragmentManager().beginTransaction()
        .add(R.id.message_frame, messageFragment).commit();

    autoCompleteFragment = new AutoCompleteFragment();
    getFragmentManager().beginTransaction()
        .add(R.id.auto_complete_frame, autoCompleteFragment).commit();

    switch (keyboardFragmentType) {
    case DEFAULT:
      KeyboardFragment.setKeyboardStyle(KeyboardStyle.DEFAULT);
      keyboardFragment = new KeyboardFragment();
      getFragmentManager().beginTransaction()
          .add(R.id.keyboard_frame, keyboardFragment).commit();
      break;
    case ALPHABETICAL:
      keyboardFragment = new KeyboardFragment();
      KeyboardFragment.setKeyboardStyle(KeyboardStyle.ALPHABETICAL);
      getFragmentManager().beginTransaction()
          .add(R.id.keyboard_frame, keyboardFragment).commit();
      break;
    case KEYBOARD2:
      keyboardFragment = new KeyboardFragment2();
      getFragmentManager().beginTransaction()
          .add(R.id.keyboard_frame, keyboardFragment).commit();
      break;
    default:
      // TODO
      break;
    }

    getFragmentManager().beginTransaction()
        .add(R.id.buttons_frame, mFBFragment).commit();

    Intent intent = getIntent();
    final String value = intent.getStringExtra(Facebook_Command);
    if (null != value)
      if (value.equals("post"))
        mFacebookCommand = FacebookCommand.POST;
      else if (value.equals("comment")) {
        mFacebookCommand = FacebookCommand.COMMENT;
        mPostId = intent.getStringExtra(Facebook_Command_Post_Id);
      }
    if (!recieverRegistered && smsReceiver == null) {
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        smsReceiver = new BroadcastReceiver() {

          @Override
          public void onReceive(Context context, Intent intent1) {
            String action = intent1.getAction();
            if (Telephony.Sms.Intents.SMS_RECEIVED_ACTION.equals(action)
                || "android.provider.Telephony.SMS_RECEIVED".equals(action)) {
              String address;
              SmsMessage[] msgs = getMessagesFromIntent(intent1);
              if (msgs != null)
                for (int i = 0; i < msgs.length; i++) {
                  address = msgs[i].getOriginatingAddress();
                  String name = foundContactName(address);
                  Toast toast = Toast.makeText(getBaseContext(), name + "\n"
                      + getResources().getString(R.string.sent_message_sms),
                      Toast.LENGTH_LONG);
                  LinearLayout layout = (LinearLayout) toast.getView();
                  if (layout.getChildCount() > 0) {
                    TextView tv = (TextView) layout.getChildAt(0);
                    tv.setGravity(Gravity.CENTER_VERTICAL
                        | Gravity.CENTER_HORIZONTAL);
                  }
                  toast.show();

                }

            }

          }

          private String foundContactName(String address) {
            ArrayList<SMSItem> smsList = Controller.smsList;
            PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
            PhoneNumber phoneNumber = null;

            try {
              phoneNumber = phoneUtil.parse(address, "IL");

            } catch (NumberParseException e) {
              // TODO Auto-generated catch block
              e.printStackTrace();
            }

            String formatedE164 = phoneUtil.format(phoneNumber,
                PhoneNumberFormat.E164);
            String formatedInter = phoneUtil.format(phoneNumber,
                PhoneNumberFormat.INTERNATIONAL);
            String formatedNat = phoneUtil.format(phoneNumber,
                PhoneNumberFormat.NATIONAL);
            String formatedNatNoDash = formatedNat.replace("-", "");
            for (int i = 0; i < smsList.size(); i++)
              if (formatedE164.equals(smsList.get(i).contactPhone)
                  || formatedInter.equals(smsList.get(i).contactPhone)
                  || formatedNat.equals(smsList.get(i).contactPhone)
                  || formatedNatNoDash.equals(smsList.get(i).contactPhone))
                return smsList.get(i).contactName;
            return address;
          }

          public SmsMessage[] getMessagesFromIntent(Intent intent1) {
            Object[] messages = (Object[]) intent1.getSerializableExtra("pdus");
            byte[][] pduObjs = new byte[messages.length][];

            for (int i = 0; i < messages.length; i++)
              pduObjs[i] = (byte[]) messages[i];
            byte[][] pdus = new byte[pduObjs.length][];
            int pduCount = pdus.length;
            SmsMessage[] msgs = new SmsMessage[pduCount];
            for (int i = 0; i < pduCount; i++) {
              pdus[i] = pduObjs[i];
              msgs[i] = SmsMessage.createFromPdu(pdus[i]);
            }
            return msgs;
          }

        };
        IntentFilter filter = new IntentFilter(
            Telephony.Sms.Intents.SMS_RECEIVED_ACTION);
        registerReceiver(smsReceiver, filter);

      } else {
        smsReceiver = new BroadcastReceiver() {

          @Override
          public void onReceive(Context context, Intent intent1) {
            String action = intent1.getAction();
            if ("android.provider.Telephony.SMS_RECEIVED".equals(action)) {
              String address;
              SmsMessage[] msgs = getMessagesFromIntent(intent1);
              if (msgs != null)
                for (int i = 0; i < msgs.length; i++) {
                  address = msgs[i].getOriginatingAddress();
                  String name = foundContactName(address);
                  Toast toast = Toast.makeText(getBaseContext(), name + "\n"
                      + getResources().getString(R.string.sent_message_sms),
                      Toast.LENGTH_LONG);
                  LinearLayout layout = (LinearLayout) toast.getView();
                  if (layout.getChildCount() > 0) {
                    TextView tv = (TextView) layout.getChildAt(0);
                    tv.setGravity(Gravity.CENTER_VERTICAL
                        | Gravity.CENTER_HORIZONTAL);
                  }
                  toast.show();

                }

            }

          }

          private String foundContactName(String address) {
            ArrayList<SMSItem> smsList = Controller.smsList;
            PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
            PhoneNumber phoneNumber = null;

            try {
              phoneNumber = phoneUtil.parse(address, getResources()
                  .getConfiguration().locale.getCountry());
            } catch (NumberParseException e) {
              // TODO Auto-generated catch block
              e.printStackTrace();
            }

            String formatedE164 = phoneUtil.format(phoneNumber,
                PhoneNumberFormat.E164);
            System.out.println(formatedE164);
            String formatedInter = phoneUtil.format(phoneNumber,
                PhoneNumberFormat.INTERNATIONAL);
            System.out.println(formatedInter);
            String formatedNat = phoneUtil.format(phoneNumber,
                PhoneNumberFormat.NATIONAL);
            System.out.println(formatedNat);
            String formatedNatNoDash = formatedNat.replace("-", "");

            for (int i = 0; i < smsList.size(); i++)
              if (formatedE164.equals(smsList.get(i).contactPhone)
                  || formatedInter.equals(smsList.get(i).contactPhone)
                  || formatedNat.equals(smsList.get(i).contactPhone)
                  || formatedNatNoDash.equals(smsList.get(i).contactPhone))
                return smsList.get(i).contactName;
            return address;
          }

          public SmsMessage[] getMessagesFromIntent(Intent intent1) {
            Object[] messages = (Object[]) intent1.getSerializableExtra("pdus");
            byte[][] pduObjs = new byte[messages.length][];

            for (int i = 0; i < messages.length; i++)
              pduObjs[i] = (byte[]) messages[i];
            byte[][] pdus = new byte[pduObjs.length][];
            int pduCount = pdus.length;
            SmsMessage[] msgs = new SmsMessage[pduCount];
            for (int i = 0; i < pduCount; i++) {
              pdus[i] = pduObjs[i];
              msgs[i] = SmsMessage.createFromPdu(pdus[i]);
            }
            return msgs;
          }

        };
        IntentFilter filter = new IntentFilter(
            "android.provider.Telephony.SMS_RECEIVED");
        registerReceiver(smsReceiver, filter);

      }
      recieverRegistered = true;
    }

    autoComplete = new AutoComplete(this);

  }

  @Override
  protected void onResume() {
    super.onResume();
    refreshActivity();

    if (GeneralActivity.statisticsMode)
      SaveStatistics.startTyping();

    if (!changeKeyboard)
      return;

    changeKeyboard = false;

    switch (keyboardFragmentType) {
    case DEFAULT:
      KeyboardFragment.setKeyboardStyle(KeyboardStyle.DEFAULT);
      keyboardFragment = new KeyboardFragment();
      getFragmentManager().beginTransaction()
          .replace(R.id.keyboard_frame, keyboardFragment).commit();
      break;
    case ALPHABETICAL:
      keyboardFragment = new KeyboardFragment();
      KeyboardFragment.setKeyboardStyle(KeyboardStyle.ALPHABETICAL);
      getFragmentManager().beginTransaction()
          .replace(R.id.keyboard_frame, keyboardFragment).commit();
      break;
    case KEYBOARD2:
      keyboardFragment = new KeyboardFragment2();
      getFragmentManager().beginTransaction()
          .replace(R.id.keyboard_frame, keyboardFragment).commit();
      break;
    default:
      // TODO
      break;
    }

  }

  @Override
  public boolean onCreateOptionsMenu(final Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.main, menu);
    return true;
  }

  @Override
  public IKeyboardListener getKeyboardListener() {
    final EditText messageEditText = (EditText) findViewById(R.id.message_edit);
    if (null == messageEditText)
      return null;
    attached = true;
    messageEditText.setOnTouchListener(new NoTouchListener());
    return new KeyboardListener(messageEditText, true, this);
  }

  @Override
  public boolean onPrepareOptionsMenu(Menu menu) {
    return false;
  }

  @Override
  public void onOperation(final int c) {
    super.onOperation(c);
    Intent intent;
    switch (OpCodeInterpreter.getOp(c)) {
    case BLUE:
      makeSound(0);
      keyboardFragment.click(new InputColor[] { InputColor.blue });
      break;
    case BLUE_ORANGE:
      makeSound(0);
      keyboardFragment.click(new InputColor[] { InputColor.blue,
          InputColor.orange });
      break;
    case BLUE_RED:
      makeSound(0);
      keyboardFragment
          .click(new InputColor[] { InputColor.red, InputColor.blue });
      break;
    case GREEN:
      makeSound(0);
      keyboardFragment.click(new InputColor[] { InputColor.green });
      break;
    case GREEN_RED:
      makeSound(0);
      keyboardFragment.click(new InputColor[] { InputColor.green,
          InputColor.red });
      break;
    case INVALID:
      break;
    case LONG_BLUE:
      makeSound(1);
      if (statisticsMode)
        SaveStatistics.stopTyping();
      intent = new Intent(this, SettingsActivity.class);
      intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
      intent.putExtra(LanguageActivity.languageKey, Controller.language);
      startActivity(intent);
      break;
    case LONG_GREEN:
      makeSound(1);
      // Delete all
      keyboardFragment.longClick(new InputColor[] { InputColor.green });
      break;
    case LONG_ORANGE:
      makeSound(1);
      if (statisticsMode)
        SaveStatistics.stopTyping();
      intent = new Intent(this, ServiceActivity.class);
      intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
      intent.putExtra(ServiceActivity.serviceKey, "messagesHistory");
      startActivity(intent);
      break;
    case LONG_RED:
      makeSound(1);
      String messageText = ((EditText) findViewById(R.id.message_edit))
          .getText().toString();

      if (messageText == null || messageText.equals("")) {
        Toast.makeText(this, R.string.empty_message, Toast.LENGTH_SHORT).show();
        return;
      }

      if (statisticsMode)
        SaveStatistics.stopTyping();
      Controller.message = messageText;

      if (FacebookCommand.NOTHING == mFacebookCommand) {
        intent = new Intent(this, ServiceActivity.class);
        intent.putExtra(ServiceActivity.serviceKey, "services");
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
      } else if (FacebookCommand.POST == mFacebookCommand) {
        FacebookManager.addPost(this, messageText);
        finish();
        overridePendingTransition(0, 0);
      } else {
        FacebookManager.addComment(this, mPostId, messageText);
        finish();
        overridePendingTransition(0, 0);
      }
      break;
    case ORANGE:
      makeSound(0);
      keyboardFragment.click(new InputColor[] { InputColor.orange });
      break;
    case ORANGE_GREEN:
      makeSound(0);
      keyboardFragment.click(new InputColor[] { InputColor.orange,
          InputColor.green });
      break;
    case RED:
      makeSound(0);
      keyboardFragment.click(new InputColor[] { InputColor.red });
      break;
    default:
      break;
    }
  }

  private boolean attached = false;

  private void refreshActivity() {
    setTitle(R.string.typing);

    if (null != keyboardFragment && attached)
      keyboardFragment.refreshFragment();
  }

  @Override
  public List<String> autoComplete(String str) {
    if (null == str) {
      autoCompleteFragment.setOptionsText("");
      return null;
    }

    List<String> options = autoComplete.getWordCompletion(str
        .toLowerCase(Locale.US));

    if (null == options) {
      autoCompleteFragment.setOptionsText("");
      return null;
    }

    showOptions(options);
    return options;

  }

  private void showOptions(List<String> options) {
    String toBeShown = "";

    switch (options.size()) {

    case 1:
      toBeShown = options.get(0);
      break;
    case 2:
      toBeShown = options.get(0) + " | " + options.get(1);
      break;
    case 3:
      toBeShown = options.get(0) + " | " + options.get(1) + " | "
          + options.get(2);
      break;
    default:
      break;

    }

    autoCompleteFragment.setOptionsText(toBeShown);

  }
}
