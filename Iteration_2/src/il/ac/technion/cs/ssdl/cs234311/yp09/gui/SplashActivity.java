package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import il.ac.technion.cs.ssdl.cs234311.yp09.commsModule.CommsModule;
import il.ac.technion.cs.ssdl.cs234311.yp09.contacts.ContactsModule;

import java.util.Locale;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.AsyncTask;
import android.os.Bundle;

/**
 * The first activity launched in the app. Displays a splash screen and starts
 * the appropriate activity.
 * 
 * @author Itamar (redesigned by Muhammad)
 * 
 */
public class SplashActivity extends GeneralActivity {
  Intent intent;
  SharedPreferences preferences;

  static SoundPool mSoundPool;
  static int mSoundId;
  static AudioManager mAudioManager;
  static boolean mCanPlayAudio;

  @Override
  protected void onCreate(Bundle savedInstanceState) {

    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_splash);

    setAppLanguage();
    mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
    mSoundPool = new SoundPool(1, AudioManager.STREAM_SYSTEM, 0);

    new PrefetchContacts().execute();
  }

  private static void setAppLanguage() {
    String devideLanguage = Locale.getDefault().getLanguage();
    if (devideLanguage.equals("en"))
      Controller.language = "English";
    else if (devideLanguage.equals("iw"))
      Controller.language = "Hebrew";
    else if (devideLanguage.equals("ar"))
      Controller.language = "Arabic";
    else if (devideLanguage.equals("ru"))
      Controller.language = "Russian";
    else
      Controller.language = "English";
  }

  private class PrefetchContacts extends AsyncTask<Void, Void, Void> {

    public PrefetchContacts() {
    }

    @Override
    protected Void doInBackground(Void... arg0) {
      preferences = getSharedPreferences(
          "il.ac.technion.cs.ssdl.cs234311.yp09.fbip", MODE_PRIVATE);
      commsModule = new CommsModule(SplashActivity.this);
      Controller.smsList = ContactsModule.contactParser(SplashActivity.this);

      return null;
    }

    @Override
    protected void onPostExecute(Void result) {
      if (preferences.getBoolean("showWelcomeScreen", false)
          || preferences.getBoolean("firstRun", true)) {
        intent = new Intent(SplashActivity.this, WelcomeActivity.class);
        preferences.edit().putBoolean("firstRun", false).commit();
        preferences.edit().putBoolean("showWelcomeScreen", true).commit();
      } else
        intent = new Intent(SplashActivity.this, MainMenuActivity.class);
      startActivity(intent);
      // close this activity
      finish();
    }

  }
}
