package il.ac.technion.cs.ssdl.cs234311.yp09.facebookFeed;

import il.ac.technion.cs.ssdl.cs234311.yp09.facebookFeedGui.PostsActivity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.Activity;

/**
 * A stub for facebook manager
 * 
 * @author Owais Musa
 * 
 */
public class FacebookManagerStub {

  private static List<FacebookPost> mPosts = new ArrayList<FacebookPost>();
  private static long mSimiRandomPostId = 0;

  /**
   * 
   * @return posts
   */
  public static List<FacebookPost> getPosts() {
    return mPosts;
  }

  /**
   * reset posts to empty list
   */
  public static void resetPosts() {
    mPosts = new ArrayList<FacebookPost>();
  }

  /**
   * If this field = true, this stub will be used!
   */
  public static boolean debugMode = false;

  /**
   * @param activity
   *          activity
   */
  public static void getNewsFeed(Activity activity) {
    ((PostsActivity) activity).postFeedFetch(mPosts);
  }

  /**
   * @param activity
   *          activity
   * @param postId
   *          post id
   */
  public static void addLike(Activity activity, String postId) {
    for (FacebookPost p : mPosts)
      if (p.getPOstId().equals(postId)) {
        FacebookPost newP = new FacebookPost(p.getPOstId(),
            p.getAuthorImageUrl(), p.getAuthorName(), p.getText(),
            -(p.getLikesNum() + 1), p.getCommentsNum(), p.getSharesNum(),
            p.getCreatingTime());
        mPosts.remove(p);
        mPosts.add(newP);
        return;
      }
  }

  /**
   * @param activity
   *          activity
   * @param postId
   *          post id
   */
  public static void unLike(Activity activity, String postId) {
    addLike(activity, postId);
  }

  /**
   * @param activity
   *          activity
   * @param postId
   *          post id
   */
  public static void share(Activity activity, String postId) {
    for (FacebookPost p : mPosts)
      if (p.getPOstId().equals(postId)) {
        FacebookPost newP = new FacebookPost(p.getPOstId(),
            p.getAuthorImageUrl(), p.getAuthorName(), p.getText(),
            p.getLikesNum(), p.getCommentsNum(), p.getSharesNum() + 1,
            p.getCreatingTime());
        mPosts.remove(p);
        mPosts.add(newP);
        return;
      }
  }

  /**
   * @param activity
   *          activity
   * @param postId
   *          post id
   * @param comment
   *          comment to be shared
   */
  public static void addComment(Activity activity, String postId, String comment) {
    for (FacebookPost p : mPosts)
      if (p.getPOstId().equals(postId)) {
        FacebookPost newP = new FacebookPost(p.getPOstId(),
            p.getAuthorImageUrl(), p.getAuthorName(), p.getText(),
            p.getLikesNum(), p.getCommentsNum() + 1, p.getSharesNum(),
            p.getCreatingTime());
        mPosts.remove(p);
        mPosts.add(newP);
        return;
      }
  }

  /**
   * @param activity
   *          activity
   * @param postMessage
   *          message to be shared as post
   * @return facebook post
   */
  public static FacebookPost addPost(Activity activity, String postMessage) {
    mSimiRandomPostId++;
    FacebookPost newP = new FacebookPost("" + mSimiRandomPostId, "nothing",
        "Dummy", postMessage, 0, 0, 0, new Date());
    mPosts.add(newP);
    return newP;
  }
}
