package il.ac.technion.cs.ssdl.cs234311.yp09.keyboards;

import android.app.Activity;
import android.app.Fragment;
import android.util.Log;

/**
 * A base fragment to be extended by concrete keyboards
 * 
 * @date 30/3/2013
 * @email owais.musa@gmail.com
 * @author Owais Musa
 * 
 */

public abstract class BaseKeyboardFragment extends Fragment {

  protected IKeyboardListener mKeyboardListener;
  protected ISearchInContactsActivity mSearchInContactsActivity;
  protected ILoginActivity mLoginActivity;

  protected KeyboardType mKeyboardType = KeyboardType.TYPING_DEFAULT;

  // This member field is used in login keyboard
  // It indicates whether user moved to type the password or not!
  protected boolean mMovedToPasswordTextView = false;

  // Set this to true when calling onAttach
  protected boolean mAttached = false;

  protected static InputColor convertStringToColor(final String color) {
    if (color.equals("red"))
      return InputColor.red;
    if (color.equals("green"))
      return InputColor.green;
    if (color.equals("blue"))
      return InputColor.blue;
    if (color.equals("orange"))
      return InputColor.orange;

    assert false;
    return null;
  }

  protected static boolean equalCombination(final String res[],
      final InputColor colors[]) {
    if (res.length != colors.length)
      return false;

    for (final InputColor inputColor : colors) {
      boolean found = false;

      for (final String strColor : res)
        if (convertStringToColor(strColor) == inputColor)
          found = true;

      if (found == false)
        return false;
    }

    return true;
  }

  @Override
  public void onAttach(final Activity activity) {
    super.onAttach(activity);
    if (!(activity instanceof IActivityWithKeyboard))
      throw new ClassCastException(activity.toString()
          + " must implemenet IActivityWithKeyboard");

    if (activity instanceof ITypingDefaultActivity)
      Log.d("ITypingDefaultActivity", "ITypingDefaultActivity");
    else if (activity instanceof ITypingDefaultActivity)
      Log.d("ISearchInContactsActivity", "ISearchInContactsActivity");
    else if (activity instanceof ILoginActivity)
      Log.d("ILoginActivity", "ILoginActivity");

    mKeyboardListener = ((IActivityWithKeyboard) activity)
        .getKeyboardListener();

    switch (mKeyboardType) {
    case TYPING_DEFAULT:
      if (!(activity instanceof ITypingDefaultActivity))
        throw new ClassCastException(activity.toString()
            + " must implemenet ITypingDefaultActivity");
      break;
    case SEARCH_IN_CONTACTS:
      if (!(activity instanceof ISearchInContactsActivity))
        throw new ClassCastException(activity.toString()
            + " must implemenet ISearchInContactsActivity");
      mSearchInContactsActivity = (ISearchInContactsActivity) activity;
      break;
    case USERNAME_PASSWORD:
      if (!(activity instanceof ILoginActivity))
        throw new ClassCastException(activity.toString()
            + " must implemenet ILoginActivity");
      mLoginActivity = (ILoginActivity) activity;
      break;
    default:
      break;

    }

    mAttached = true;
  }

  @Override
  public void onDetach() {
    super.onDetach();
    mKeyboardListener = null;
    mSearchInContactsActivity = null;
    mLoginActivity = null;
    mAttached = false;
  }

  /**
   * Call this function to tell the fragment that a specific colors combination
   * was "clicked".
   * 
   * @param colors
   *          colors combination
   */
  public abstract void click(final InputColor colors[]);

  /**
   * To load the text again, this will be used for language change, talk to
   * Muhammad
   */
  public abstract void refreshFragment();

  /**
   * Call this function to tell the fragment that a specific colors combination
   * was "clicked" while "click" == long press.
   * 
   * @param colors
   *          colors combination
   */
  public void longClick(final InputColor colors[]) {
    assert colors != null;

    // Currently, just long green press is supported
    if (colors.length == 1 && colors[0] == InputColor.green)
      mKeyboardListener.deleteAll();
  }

  /**
   * @param keyboardType
   *          keyboard type
   * @throws Exception
   *           too late exception :)
   */
  public void setKeyboardType(KeyboardType keyboardType) throws Exception {
    // Should not call this method after attaching!
    if (mAttached)
      throw new Exception("too late :(");

    mKeyboardType = keyboardType;
  }

  /**
   * Use this method just when you want to change the listener to point to
   * another text edit in the same activity!
   * 
   * @param keyboardListener
   *          keyboard listener
   */
  public void setKeyboardListener(KeyboardListener keyboardListener) {
    assert mKeyboardListener != null;
    mKeyboardListener = keyboardListener;
  }
}
