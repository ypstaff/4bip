package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.Menu;
import android.widget.AbsListView;
import android.widget.ListView;

/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 16/1/2014
 * 
 */
public class LanguageActivity extends GeneralActivity {

  /**
   * a key to send a parameter with an intent
   */
  static public String languageKey = "il.ac.technion.cs.ssdl.cs234311.yp09.gui.key";

  /**
   * supported languages
   */
  public static final String[] languages = new String[] { "English", "עברית",
      "العربية", "Русский" };

  /**
   * icons of supported languages
   */
  public static final int[] icons = { R.drawable.usa, R.drawable.israel,
      R.drawable.saudi, R.drawable.russia };

  private int currentLanguage = 0;
  private LongFragment longFragment;

  ListView languagesListView;
  List<Language> languagesList;

  @Override
  protected void onCreate(final Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.activity_language);

    longFragment = LongFragment.newInstance(LongFragment.Screen.Language);
    getFragmentManager().beginTransaction()
        .add(R.id.long_press_info_frame, longFragment).commit();

    final ProgressBarFragment progressBarFragment = ProgressBarFragment
        .newInstance(ProgressBarFragment.Screen.Language);
    getFragmentManager().beginTransaction()
        .add(R.id.progress_bar_frame, progressBarFragment).commit();

    final ScrollingFragment scrollingFragment = ScrollingFragment
        .newInstance(ScrollingFragment.Screen.Language);
    getFragmentManager().beginTransaction()
        .add(R.id.scrolling_fragment, scrollingFragment).commit();

    languagesList = new ArrayList<Language>();
    for (int i = 0; i < icons.length; i++) {
      final Language language = new Language(icons[i], languages[i]);
      languagesList.add(language);
    }

    languagesListView = (ListView) findViewById(R.id.languages_fragment);
    languagesListView.setOnTouchListener(new NoTouchListener());
    languagesListView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
    final LanguageListViewAdapter adapter = new LanguageListViewAdapter(this,
        R.layout.language_listview, languagesList);
    languagesListView.setAdapter(adapter);
    languagesListView.setItemChecked(0, true);

    refreshActivity();

    getFragmentManager().beginTransaction()
        .add(R.id.buttons_frame_main, mFBFragment).commit();
  }

  @Override
  protected void onResume() {
    super.onResume();
    refreshActivity();
  }

  @Override
  public boolean onCreateOptionsMenu(final Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.language, menu);
    return true;
  }

  @Override
  public boolean onPrepareOptionsMenu(Menu menu) {
    return false;
  }

  @Override
  public void onOperation(final int c) {
    super.onOperation(c);
    // final Intent intent;
    switch (OpCodeInterpreter.getOp(c)) {
    case BLUE:
      break;
    case BLUE_ORANGE:
      break;
    case BLUE_RED:
      break;
    case GREEN:
      makeSound(0);
      currentLanguage = (currentLanguage + 1) % languagesListView.getCount();
      languagesListView.setItemChecked(currentLanguage, true);
      languagesListView.smoothScrollToPosition(currentLanguage);
      break;
    case GREEN_RED:
      break;
    case INVALID:
      break;
    case LONG_BLUE:
      // Watad: TODO
      break;
    case LONG_GREEN:
      break;
    case LONG_ORANGE:
      break;
    case LONG_RED:
      makeSound(1);
      finish();
      overridePendingTransition(0, 0);
      break;
    case ORANGE:
      makeSound(0);
      currentLanguage = (currentLanguage - 1 + languagesListView.getCount())
          % languagesListView.getCount();
      languagesListView.setItemChecked(currentLanguage, true);
      languagesListView.smoothScrollToPosition(currentLanguage);
      break;
    case ORANGE_GREEN:
      break;
    case RED:
      makeSound(0);
      if (currentLanguage == 0)
        Controller.language = "English";
      else if (currentLanguage == 1)
        Controller.language = "Hebrew";
      else if (currentLanguage == 2)
        Controller.language = "Arabic";
      else if (currentLanguage == 3)
        Controller.language = "Russian";
      updateLocale();
      finish();
      overridePendingTransition(0, 0);
      break;
    default:
      break;
    }
  }

  private void updateLocale() {
    String languageToLoad = null;
    if (Controller.language.equals("English"))
      languageToLoad = "en";
    else if (Controller.language.equals("Hebrew"))
      languageToLoad = "iw";
    else if (Controller.language.equals("Arabic"))
      languageToLoad = "ar";
    else if (Controller.language.equals("Russian"))
      languageToLoad = "ru";
    Locale locale = new Locale(languageToLoad);
    Locale.setDefault(locale);

    Configuration config = new Configuration();
    config.locale = locale;
    getBaseContext().getResources().updateConfiguration(config,
        getBaseContext().getResources().getDisplayMetrics());
  }

  private void refreshActivity() {

    setTitle(R.string.language);

  }
}
