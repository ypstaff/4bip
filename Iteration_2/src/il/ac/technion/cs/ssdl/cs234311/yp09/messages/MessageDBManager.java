package il.ac.technion.cs.ssdl.cs234311.yp09.messages;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * @author daniel The database management class
 */
public class MessageDBManager {
  private String databaseName;
  private MessageSQLiteHelper messageHelper;
  private SQLiteDatabase database;
  /**
   * 
   */
  public static final String TABLE_MESSAGES = "Messages";
  /**
   * 
   */
  public static final String COLUMN_TYPE = "type";
  /**
   * 
   */
  public static final String COLUMN_CONTACT = "contact";
  /**
   * 
   */
  public static final String COLUMN_BODY = "body";
  /**
   * 
   */
  public static final String COLUMN_DATE = "date";

  /**
   * @param context
   *          - The parent activity of the database
   * @param databaseName
   *          - The database name (usename + service)
   */
  public MessageDBManager(Context context, String databaseName) {
    this.databaseName = "Base" + databaseName;
    this.messageHelper = new MessageSQLiteHelper(context, this.databaseName);
    database = messageHelper.getWritableDatabase();

  }

  /**
   * @param contact
   *          - Type String. The other party of the message
   * @param body
   *          - Type String. The message content
   * @param time
   *          - Type long. The message creation/receipt time in UNIX format
   * @param type
   *          - Type int. Message type - 1 for inbox, 2 for outbox
   * @return - The database operation status
   */
  public long insertMessage(String contact, String body, long time, int type) {
    ContentValues values = new ContentValues();
    values.put(COLUMN_CONTACT, contact);
    values.put(COLUMN_BODY, body);
    values.put(COLUMN_DATE, Long.toString(time));
    values.put(COLUMN_TYPE, type);
    return database.insert(databaseName + TABLE_MESSAGES, null, values);
  }

  /**
   * @param contact
   *          - Type String. The contact whose messages we wish to check
   * @return - Type ArrayList. An ArrayList of communications with the contact
   */
  public ArrayList<SMSMessageItem> getMessageList(String contact) {
    ArrayList<SMSMessageItem> messagesList = new ArrayList<SMSMessageItem>();
    Cursor mCursor = database.query(databaseName + TABLE_MESSAGES, null,
        COLUMN_CONTACT + "=" + "'" + contact + "'", null, null, null,
        COLUMN_DATE + " DESC");
    if (mCursor.getCount() > 0)
      while (mCursor.moveToNext()) {
        int type = Integer.parseInt(mCursor.getString(mCursor
            .getColumnIndexOrThrow(COLUMN_TYPE)));
        String body = mCursor.getString(mCursor
            .getColumnIndexOrThrow(COLUMN_BODY));
        long time = Long.parseLong(mCursor.getString(mCursor
            .getColumnIndexOrThrow(COLUMN_DATE)));

        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        calendar.setTimeInMillis(time);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd h:mma");
        String date = sdf.format(calendar.getTime());
        SMSMessageItem newMessage = new SMSMessageItem(type, body, date);
        messagesList.add(newMessage);

      }
    mCursor.close();
    for (SMSMessageItem item : messagesList)
      System.out.println(item.messageBody + " " + item.messageType + " "
          + item.messageDate);
    return messagesList;
  }
}
