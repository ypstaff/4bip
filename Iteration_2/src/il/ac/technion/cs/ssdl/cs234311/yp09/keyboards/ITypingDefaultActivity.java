package il.ac.technion.cs.ssdl.cs234311.yp09.keyboards;

import java.util.List;

/**
 * The reason this interface was declared is to support auto complete
 * 
 * @date 24/3/2014
 * @email owais.musa@gmail.com
 * @author Owais Musa
 * 
 */
public interface ITypingDefaultActivity {

  /**
   * @param str
   *          the string to be auto completed
   * @return List with auto complete result
   */
  public List<String> autoComplete(String str);
}
