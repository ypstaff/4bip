package il.ac.technion.cs.ssdl.cs234311.yp09.contacts;

import java.util.ArrayList;

import android.app.Activity;
import android.database.Cursor;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;

/**
 * @author Daniel Eidel
 * @desc This class is used to provide the contact data in a comprehensive
 *       format
 */
public class ContactsModule {

  /**
   * Parses the phone's internal contacts database, and constructs a displayable
   * list of phone numbers
   * 
   * @param currAct
   *          - Type Activity. The main activity, in which the contacts are
   *          parsed.
   * @return - Type ArrayList<SMSItem>. An ArrayList of SMSItem objects, in
   *         which holds the contact's data.
   */
  static public ArrayList<SMSItem> contactParser(Activity currAct) {
    ArrayList<SMSItem> contactsList = new ArrayList<SMSItem>();
    Cursor cursor = currAct.getContentResolver().query(
        ContactsContract.Contacts.CONTENT_URI, null, null, null,
        Phone.DISPLAY_NAME + " ASC");

    while (cursor.moveToNext()) {
      String contactId = cursor.getString(cursor
          .getColumnIndex(BaseColumns._ID));
      String name = cursor.getString(cursor
          .getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
      Cursor phones = currAct.getContentResolver().query(Phone.CONTENT_URI,
          null, Phone.CONTACT_ID + "=" + contactId, null, null);
      while (phones.moveToNext()) {
        String number = phones.getString(phones.getColumnIndex(Phone.NUMBER));
        int type = phones.getInt(phones.getColumnIndex(Phone.TYPE));
        int customType = R.drawable.other;
        switch (type) {
        case Phone.TYPE_HOME:
          customType = R.drawable.home;
          break;
        case Phone.TYPE_WORK:
          customType = R.drawable.work;
          break;
        case Phone.TYPE_MOBILE:
          customType = R.drawable.mobile;
          break;
        default:
          customType = R.drawable.other;
        }
        SMSItem newContact = new SMSItem(name, number, customType);
        contactsList.add(newContact);
      }
      phones.close();

    }
    cursor.close();
    return contactsList;
  }
}
