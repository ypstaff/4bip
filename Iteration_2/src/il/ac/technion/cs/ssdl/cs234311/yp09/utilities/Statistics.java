package il.ac.technion.cs.ssdl.cs234311.yp09.utilities;

import java.io.Serializable;

/**
 * @author husam
 * 
 */
public class Statistics implements Serializable {
  private static final long serialVersionUID = 7766L;
  /**
   * the total time using the application in milliseconds
   */
  public Double timeUsingTheApp;
  /**
   * the average time for typing sentence in milliseconds
   */
  public Double timePerSentenceAvg;
  /**
   * number of sentences that this average was calculated on
   */
  public Double numberOfSentence;
  /**
   * the average time for typing word in milliseconds
   */
  public Double timePerWordAvg;
  /**
   * number of words that this average was calculated on
   */
  public Double numberOfWords;
  /**
   * the average time for typing char in milliseconds
   */
  public Double timePerCharAvg;
  /**
   * number of chars that this average was calculated on
   */
  public Double numberOfChars;
  /**
   * number of deleted chars
   */
  public Double numberOfDeletedChars;
  /**
   * number of auto complete uses
   */
  public Double numberOfAutoComplete;
  /**
   * number of facebook messages
   */
  public Double numberOfFaceBookMessages;
  /**
   * number of sms messages
   */
  public Double numberOfSMS;
  /**
   * number of google messages
   */
  public Double numberOfGoogleMessages;
  /**
   * number of green clicks
   */
  public Double numberOfGreenClicks;
  /**
   * number of long green clicks
   */
  public Double numberOfLongGreenClicks;
  /**
   * number of orange clicks
   */
  public Double numberOfOrangeClicks;
  /**
   * number of long orange clicks
   */
  public Double numberOfLongOrangeClicks;
  /**
   * number of red clicks
   */
  public Double numberOfRedClicks;
  /**
   * number of long red clicks
   */
  public Double numberOfLongRedClicks;
  /**
   * number of blue clicks
   */
  public Double numberOfBlueClicks;
  /**
   * number of long blue clicks
   */
  public Double numberOfLongBlueClicks;
  /**
   * number of blue orange clicks
   */
  public Double numberOfBlueOrangeClicks;
  /**
   * number of orange green clicks
   */
  public Double numberOfOrangeGreenClicks;
  /**
   * number of green red clicks
   */
  public Double numberOfGreenRedClicks;
  /**
   * number of blue red clicks
   */
  public Double numberOfBlueRedClicks;

  /**
   * the constructor of the class
   */
  public Statistics() {
    timeUsingTheApp = Double.valueOf(0.0);
    timePerSentenceAvg = Double.valueOf(0.0);
    numberOfSentence = Double.valueOf(0.0);
    timePerWordAvg = Double.valueOf(0.0);
    numberOfWords = Double.valueOf(0.0);
    timePerCharAvg = Double.valueOf(0.0);
    numberOfChars = Double.valueOf(0.0);
    numberOfDeletedChars = Double.valueOf(0.0);
    numberOfAutoComplete = Double.valueOf(0.0);
    numberOfFaceBookMessages = Double.valueOf(0.0);
    numberOfSMS = Double.valueOf(0.0);
    numberOfGoogleMessages = Double.valueOf(0.0);
    numberOfGreenClicks = Double.valueOf(0.0);
    numberOfOrangeClicks = Double.valueOf(0.0);
    numberOfRedClicks = Double.valueOf(0.0);
    numberOfBlueClicks = Double.valueOf(0.0);
    numberOfBlueOrangeClicks = Double.valueOf(0.0);
    numberOfOrangeGreenClicks = Double.valueOf(0.0);
    numberOfGreenRedClicks = Double.valueOf(0.0);
    numberOfBlueRedClicks = Double.valueOf(0.0);
    numberOfLongBlueClicks = Double.valueOf(0.0);
    numberOfLongGreenClicks = Double.valueOf(0.0);
    numberOfLongOrangeClicks = Double.valueOf(0.0);
    numberOfLongRedClicks = Double.valueOf(0.0);
  }

  @Override
  public int hashCode() {
    return (int) (timeUsingTheApp.doubleValue()
        + timePerSentenceAvg.doubleValue() + numberOfSentence.doubleValue()
        + timePerWordAvg.doubleValue() + numberOfWords.doubleValue()
        + timePerCharAvg.doubleValue() + numberOfChars.doubleValue()
        + numberOfDeletedChars.doubleValue()
        + numberOfAutoComplete.doubleValue()
        + numberOfFaceBookMessages.doubleValue() + numberOfSMS.doubleValue()
        + numberOfGoogleMessages.doubleValue()
        + numberOfGreenClicks.doubleValue()
        + numberOfOrangeClicks.doubleValue() + numberOfRedClicks.doubleValue()
        + numberOfBlueClicks.doubleValue()
        + numberOfBlueOrangeClicks.doubleValue()
        + numberOfOrangeGreenClicks.doubleValue()
        + numberOfGreenRedClicks.doubleValue() + numberOfBlueRedClicks
          .doubleValue());
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof Statistics))
      return false;
    Statistics other = (Statistics) o;
    return timeUsingTheApp.equals(other.timeUsingTheApp)
        && timePerSentenceAvg.equals(other.timePerSentenceAvg)
        && numberOfSentence.equals(other.numberOfSentence)
        && timePerWordAvg.equals(other.timePerWordAvg)
        && numberOfWords.equals(other.numberOfWords)
        && timePerCharAvg.equals(other.timePerCharAvg)
        && numberOfChars.equals(other.numberOfChars)
        && numberOfDeletedChars.equals(other.numberOfDeletedChars)
        && numberOfAutoComplete.equals(other.numberOfAutoComplete)
        && numberOfFaceBookMessages.equals(other.numberOfFaceBookMessages)
        && numberOfSMS.equals(other.numberOfSMS)
        && numberOfGoogleMessages.equals(other.numberOfGoogleMessages)
        && numberOfGreenClicks.equals(other.numberOfGreenClicks)
        && numberOfOrangeClicks.equals(other.numberOfOrangeClicks)
        && numberOfRedClicks.equals(other.numberOfRedClicks)
        && numberOfBlueClicks.equals(other.numberOfBlueClicks)
        && numberOfBlueOrangeClicks.equals(other.numberOfBlueOrangeClicks)
        && numberOfOrangeGreenClicks.equals(other.numberOfOrangeGreenClicks)
        && numberOfGreenRedClicks.equals(other.numberOfGreenRedClicks)
        && numberOfBlueRedClicks.equals(other.numberOfBlueRedClicks);
  }

  @Override
  public Statistics clone() {
    Statistics newStatistics = new Statistics();

    newStatistics.timeUsingTheApp = Double.valueOf(timeUsingTheApp
        .doubleValue());
    newStatistics.timePerSentenceAvg = Double.valueOf(timePerSentenceAvg
        .doubleValue());
    newStatistics.numberOfSentence = Double.valueOf(numberOfSentence
        .doubleValue());
    newStatistics.timePerWordAvg = Double.valueOf(timePerWordAvg.doubleValue());
    newStatistics.numberOfWords = Double.valueOf(numberOfWords.doubleValue());
    newStatistics.timePerCharAvg = Double.valueOf(timePerCharAvg.doubleValue());
    newStatistics.numberOfChars = Double.valueOf(numberOfChars.doubleValue());
    newStatistics.numberOfDeletedChars = Double.valueOf(numberOfDeletedChars
        .doubleValue());
    newStatistics.numberOfAutoComplete = Double.valueOf(numberOfAutoComplete
        .doubleValue());
    newStatistics.numberOfFaceBookMessages = Double
        .valueOf(numberOfFaceBookMessages.doubleValue());
    newStatistics.numberOfSMS = Double.valueOf(numberOfSMS.doubleValue());
    newStatistics.numberOfGoogleMessages = Double
        .valueOf(numberOfGoogleMessages.doubleValue());
    newStatistics.numberOfGreenClicks = Double.valueOf(numberOfGreenClicks
        .doubleValue());
    newStatistics.numberOfLongGreenClicks = Double
        .valueOf(numberOfLongGreenClicks.doubleValue());
    newStatistics.numberOfOrangeClicks = Double.valueOf(numberOfOrangeClicks
        .doubleValue());
    newStatistics.numberOfLongOrangeClicks = Double
        .valueOf(numberOfLongOrangeClicks.doubleValue());
    newStatistics.numberOfRedClicks = Double.valueOf(numberOfRedClicks
        .doubleValue());
    newStatistics.numberOfLongRedClicks = Double.valueOf(numberOfLongRedClicks
        .doubleValue());
    newStatistics.numberOfBlueClicks = Double.valueOf(numberOfBlueClicks
        .doubleValue());
    newStatistics.numberOfLongBlueClicks = Double
        .valueOf(numberOfLongBlueClicks.doubleValue());
    newStatistics.numberOfBlueOrangeClicks = Double
        .valueOf(numberOfBlueOrangeClicks.doubleValue());
    newStatistics.numberOfOrangeGreenClicks = Double
        .valueOf(numberOfOrangeGreenClicks.doubleValue());
    newStatistics.numberOfGreenRedClicks = Double
        .valueOf(numberOfGreenRedClicks.doubleValue());
    newStatistics.numberOfBlueRedClicks = Double.valueOf(numberOfBlueRedClicks
        .doubleValue());
    return newStatistics;
  }

  @Override
  public String toString() {
    String retString = "";
    retString = retString
        .concat("************* Statistics print start*****************" + "\n");
    if (0.0 != timeUsingTheApp.doubleValue())
      retString = retString.concat("timeUsingTheApp : " + timeUsingTheApp
          + "\n");
    if (0.0 != timePerSentenceAvg.doubleValue())
      retString = retString.concat("timePerSentenceAvg : " + timePerSentenceAvg
          + "\n");
    if (0.0 != numberOfSentence.doubleValue())
      retString = retString.concat("numberOfSentence : " + numberOfSentence
          + "\n");
    if (0.0 != timePerWordAvg.doubleValue())
      retString = retString.concat("timePerWordAvg : " + timePerWordAvg + "\n");
    if (0.0 != numberOfWords.doubleValue())
      retString = retString.concat("numberOfWords : " + numberOfWords + "\n");
    if (0.0 != timePerCharAvg.doubleValue())
      retString = retString.concat("timePerCharAvg : " + timePerCharAvg + "\n");
    if (0.0 != numberOfChars.doubleValue())
      retString = retString.concat("numberOfChars : " + numberOfChars + "\n");
    if (0.0 != numberOfDeletedChars.doubleValue())
      retString = retString.concat("numberOfDeletedChars : "
          + numberOfDeletedChars + "\n");
    if (0.0 != numberOfAutoComplete.doubleValue())
      retString = retString.concat("numberOfAutoComplete : "
          + numberOfAutoComplete + "\n");
    if (0.0 != numberOfFaceBookMessages.doubleValue())
      retString = retString.concat("numberOfFaceBookMessages : "
          + numberOfFaceBookMessages + "\n");
    if (0.0 != numberOfSMS.doubleValue())
      retString = retString.concat("numberOfSMS : " + numberOfSMS + "\n");
    if (0.0 != numberOfGoogleMessages.doubleValue())
      retString = retString.concat("numberOfGoogleMessages : "
          + numberOfGoogleMessages + "\n");
    if (0.0 != numberOfGreenClicks.doubleValue())
      retString = retString.concat("numberOfGreenClicks : "
          + numberOfGreenClicks + "\n");
    if (0.0 != numberOfLongGreenClicks.doubleValue())
      System.out.println("numberOfLongGreenClicks : " + numberOfLongGreenClicks
          + "\n");
    if (0.0 != numberOfOrangeClicks.doubleValue())
      retString = retString.concat("numberOfOrangeClicks : "
          + numberOfOrangeClicks + "\n");
    if (0.0 != numberOfLongOrangeClicks.doubleValue())
      retString = retString.concat("numberOfLongOrangeClicks : "
          + numberOfLongOrangeClicks + "\n");
    if (0.0 != numberOfRedClicks.doubleValue())
      retString = retString.concat("numberOfRedClicks : " + numberOfRedClicks
          + "\n");
    if (0.0 != numberOfLongRedClicks.doubleValue())
      retString = retString.concat("numberOfLongRedClicks : "
          + numberOfLongRedClicks + "\n");
    if (0.0 != numberOfBlueClicks.doubleValue())
      retString = retString.concat("numberOfBlueClicks : " + numberOfBlueClicks
          + "\n");
    if (0.0 != numberOfLongBlueClicks.doubleValue())
      retString = retString.concat("numberOfLongBlueClicks : "
          + numberOfLongBlueClicks + "\n");
    if (0.0 != numberOfBlueOrangeClicks.doubleValue())
      retString = retString.concat("numberOfBlueOrangeClicks : "
          + numberOfBlueOrangeClicks + "\n");
    if (0.0 != numberOfOrangeGreenClicks.doubleValue())
      retString = retString.concat("numberOfOrangeGreenClicks : "
          + numberOfOrangeGreenClicks + "\n");
    if (0.0 != numberOfGreenRedClicks.doubleValue())
      retString = retString.concat("numberOfGreenRedClicks : "
          + numberOfGreenRedClicks + "\n");
    if (0.0 != numberOfBlueRedClicks.doubleValue())
      retString = retString.concat("numberOfBlueRedClicks : "
          + numberOfBlueRedClicks + "\n");
    retString = retString
        .concat("************* Statistics print end ******************" + "\n");
    return retString;
  }
}
