package il.ac.technion.cs.ssdl.cs234311.yp09.messages;

public class ConversationItem extends SMSMessageItem {

  public String from_to;

  public ConversationItem(int messageType, String messageBody,
      String messageDate, String from_to) {
    super(messageType, messageBody, messageDate);
    this.from_to = from_to;
  }

}
