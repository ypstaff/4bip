package il.ac.technion.cs.ssdl.cs234311.yp09.facebookFeed;

import il.ac.technion.cs.ssdl.cs234311.yp09.facebookFeedGui.PostsActivity;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.facebook.Session;
import com.restfb.Connection;
import com.restfb.DefaultFacebookClient;
import com.restfb.Facebook;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.json.JsonObject;
import com.restfb.types.FacebookType;
import com.restfb.types.NamedFacebookType;
import com.restfb.types.Post;
import com.restfb.types.Post.Likes;
import com.restfb.types.User;

/**
 * The main class for Facebook component
 * 
 * @author Owais Musa
 * 
 */
public class FacebookManager {

  private static final int INIT_POSTS_NUM = 10;

  static Connection<Post> mFeedConnection;

  /**
   * Use this function when you want to refetch posts, else, it will get more
   * posts (continue fetching after the last time)
   */
  public static void resetFeedConnection() {
    mFeedConnection = null;
  }

  // Default access (not private) to improve performance
  // "see warnings when it is private"
  static List<Post> mCurrentPosts = new ArrayList<Post>();

  /**
   * @param activity
   *          activity
   */
  // Suppress warning to use Mokito!
  @SuppressWarnings({ "synthetic-access" })
  public static void getNewsFeed(Activity activity) {
    if (FacebookManagerStub.debugMode) {
      FacebookManagerStub.getNewsFeed(activity);
      return;
    }

    try {
      new NewsFeedGetterAsyncTask().execute(activity);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * @param activity
   *          activity
   * @param postId
   *          post id
   */
  @SuppressWarnings("synthetic-access")
  public static void addLike(Activity activity, String postId) {
    if (FacebookManagerStub.debugMode) {
      FacebookManagerStub.addLike(activity, postId);
      return;
    }

    Log.d("owais like", "start");
    new DoLikeAsyncTask().execute(postId);
    Log.d("owais like", "finish");
    Toast.makeText(activity, "liked successfully!", Toast.LENGTH_LONG).show();
  }

  /**
   * @param activity
   *          activity
   * @param postId
   *          post id
   */
  @SuppressWarnings("synthetic-access")
  public static void unLike(Activity activity, String postId) {
    if (FacebookManagerStub.debugMode) {
      FacebookManagerStub.unLike(activity, postId);
      return;
    }

    Log.d("facebook - unlike", "start");
    new DoUnlikeAsyncTask().execute(postId);
    Log.d("facebook - unlike", "finish");
    Toast.makeText(activity, "unliked successfully!", Toast.LENGTH_LONG).show();
  }

  /**
   * @param activity
   *          activity
   * @param postId
   *          post id
   */
  @SuppressWarnings("synthetic-access")
  public static void share(Activity activity, String postId) {
    if (FacebookManagerStub.debugMode) {
      FacebookManagerStub.share(activity, postId);
      return;
    }

    Log.d("facebook - share", "start");
    new DoShareAsyncTask().execute(postId);
    Log.d("facebook - share", "finish");
    Toast.makeText(activity, "shared successfully!", Toast.LENGTH_LONG).show();
  }

  /**
   * @param activity
   *          activity
   * @param postId
   *          post id
   * @param comment
   *          comment to be shared
   */
  @SuppressWarnings("synthetic-access")
  public static void addComment(Activity activity, String postId, String comment) {
    if (FacebookManagerStub.debugMode) {
      FacebookManagerStub.addComment(activity, postId, comment);
      return;
    }

    Log.d("facebook - comment", "start");
    new DoAddCommentAsyncTask().execute(postId, comment);
    Log.d("facebook - comment", "finish");
    Toast.makeText(activity, "comment has been added successfully!",
        Toast.LENGTH_LONG).show();
  }

  /**
   * @param activity
   *          activity
   * @param postMessage
   *          message to be shared as post
   */
  @SuppressWarnings("synthetic-access")
  public static void addPost(Activity activity, String postMessage) {
    if (FacebookManagerStub.debugMode) {
      FacebookManagerStub.addPost(activity, postMessage);
      return;
    }

    Log.d("facebook - post", "start");
    new DoAddPostAsyncTask().execute(postMessage);
    Log.d("facebook - post", "finish");
    Toast.makeText(activity, "post has been shared successfully!",
        Toast.LENGTH_LONG).show();
  }

  private static class NewsFeedGetterAsyncTask extends
      AsyncTask<Activity, Void, List<FacebookPost>> {

    private FacebookClient mFacebookClient;

    private String getUserProfilePictureUrl(String userId) {
      String query = "SELECT pic_big_with_logo FROM user WHERE uid=" + userId;

      List<UserPicture> userList = mFacebookClient.executeFqlQuery(query,
          UserPicture.class);

      if (!(null == userList || userList.size() == 0 || null == userList.get(0)))
        return userList.get(0).pic_big_with_logo;

      query = "SELECT pic_big FROM page WHERE page_id=" + userId;
      List<PagePicture> pageList = mFacebookClient.executeFqlQuery(query,
          PagePicture.class);

      if (!(null == pageList || pageList.size() == 0 || null == pageList.get(0)))
        return pageList.get(0).pic_big;

      return "";
    }

    public static class PagePicture {
      @SuppressWarnings("unused")
      public PagePicture() {
      }

      @Facebook
      String pic_big;
    }

    public static class UserPicture {
      @SuppressWarnings("unused")
      public UserPicture() {
      }

      @Facebook
      String pic_big_with_logo;
    }

    private long getLikes(Post post) {
      long $;

      try {
        JsonObject jsonObject = mFacebookClient.fetchObject(post.getId()
            + "/likes", JsonObject.class,
            Parameter.with("summary", Boolean.valueOf(true)),
            Parameter.with("limit", Integer.valueOf(1)));
        $ = jsonObject.getJsonObject("summary").getLong("total_count");
      } catch (Exception e) {
        $ = 0;
      }

      return $;
    }

    private long getComments(Post post) {
      long $;

      try {
        JsonObject jsonObject = mFacebookClient.fetchObject(post.getId()
            + "/comments", JsonObject.class,
            Parameter.with("summary", Boolean.valueOf(true)),
            Parameter.with("limit", Integer.valueOf(1)));
        $ = jsonObject.getJsonObject("summary").getLong("total_count");
      } catch (Exception e) {
        $ = 0;
      }

      return $;
    }

    private static long getShares(Post post) {
      long $;

      try {
        $ = post.getSharesCount().longValue();
      } catch (Exception e) {
        $ = 0;
      }

      return $;
    }

    private static boolean likedByMe(Post post, String id) {
      Likes facebookLikes = post.getLikes();
      if (null == facebookLikes)
        return false;

      List<NamedFacebookType> likes = facebookLikes.getData();

      if (null == likes)
        return false;

      for (NamedFacebookType like : likes)
        if (like.getId().equals(id))
          return true;

      return false;
    }

    private Activity mActivity;

    @Override
    protected List<FacebookPost> doInBackground(Activity... params) {
      mActivity = params[0];
      try {
        Session session = Session.getActiveSession();
        mFacebookClient = new DefaultFacebookClient(session.getAccessToken());

        User user = mFacebookClient.fetchObject("me", User.class);
        Log.d("facebook - news feed", "before query");

        List<FacebookPost> $ = new ArrayList<FacebookPost>();

        if (null == mFeedConnection)
          mFeedConnection = mFacebookClient.fetchConnection("me/home",
              Post.class,
              Parameter.with("limit", Integer.valueOf(INIT_POSTS_NUM)));
        else
          mFeedConnection = mFacebookClient.fetchConnectionPage(
              mFeedConnection.getNextPageUrl(), Post.class);

        Log.d("facebook - news feed", "after query");

        int i = 0;
        mCurrentPosts = new ArrayList<Post>();

        for (List<Post> myFeedConnectionPage : mFeedConnection) {
          for (Post post : myFeedConnectionPage) {
            i++;
            Log.d("facebook - news feed", "post: " + i);

            if (post.getMessage() == null)
              continue;

            String imgUrl = getUserProfilePictureUrl(post.getFrom().getId());
            $.add(new FacebookPost(post.getId(), imgUrl, post.getFrom()
                .getName(), post.getMessage(),
                likedByMe(post, user.getId()) ? -getLikes(post)
                    : getLikes(post), getComments(post), getShares(post), post
                    .getCreatedTime()));

            mCurrentPosts.add(post);

            if (i >= INIT_POSTS_NUM)
              break;
          }

          if (i >= INIT_POSTS_NUM)
            break;
        }

        return $;

      } catch (Exception e) {
        e.printStackTrace();
        return null;
      }
    }

    @Override
    protected void onPostExecute(List<FacebookPost> result) {
      ((PostsActivity) mActivity).postFeedFetch(result);
    }
  }

  private static class DoLikeAsyncTask extends AsyncTask<String, Void, Void> {

    private FacebookClient mFacebookClient;

    @Override
    protected Void doInBackground(String... params) {
      try {
        if (null == params || null == params[0])
          return null;

        Session session = Session.getActiveSession();
        mFacebookClient = new DefaultFacebookClient(session.getAccessToken());

        mFacebookClient.publish(params[0] + "/likes", Boolean.class);
      } catch (Exception e) {
        e.printStackTrace();
        return null;
      }

      return null;
    }
  }

  private static class DoUnlikeAsyncTask extends AsyncTask<String, Void, Void> {

    private FacebookClient mFacebookClient;

    @Override
    protected Void doInBackground(String... params) {
      try {
        if (null == params || null == params[0])
          return null;

        Session session = Session.getActiveSession();
        mFacebookClient = new DefaultFacebookClient(session.getAccessToken());
        String postId = params[0];
        Post post = null;

        for (Post p : mCurrentPosts)
          if (postId.equals(p.getId()))
            post = p;

        if (post == null)
          return null;

        mFacebookClient.deleteObject(post.getId() + "/likes");

      } catch (Exception e) {
        e.printStackTrace();
        return null;
      }

      return null;
    }
  }

  private static class DoShareAsyncTask extends AsyncTask<String, Void, Void> {

    private FacebookClient mFacebookClient;

    @Override
    protected Void doInBackground(String... params) {
      try {
        if (null == params || null == params[0])
          return null;

        Session session = Session.getActiveSession();
        mFacebookClient = new DefaultFacebookClient(session.getAccessToken());
        String postId = params[0];
        Post post = null;

        for (Post p : mCurrentPosts)
          if (postId.equals(p.getId()))
            post = p;

        if (post == null)
          return null;

        String message = post.getMessage();
        if (message == null)
          message = "";

        mFacebookClient.publish("me/feed", FacebookType.class,
            Parameter.with("message", message));

      } catch (Exception e) {
        e.printStackTrace();
        return null;
      }

      return null;
    }
  }

  private static class DoAddPostAsyncTask extends AsyncTask<String, Void, Void> {

    private FacebookClient mFacebookClient;

    @Override
    protected Void doInBackground(String... params) {
      try {
        if (null == params || null == params[0])
          return null;

        Session session = Session.getActiveSession();
        mFacebookClient = new DefaultFacebookClient(session.getAccessToken());

        mFacebookClient.publish("me/feed", FacebookType.class,
            Parameter.with("privacy", "{'value':'EVERYONE'}"),
            Parameter.with("message", params[0]));

      } catch (Exception e) {
        e.printStackTrace();
        return null;
      }

      return null;
    }
  }

  private static class DoAddCommentAsyncTask extends
      AsyncTask<String, Void, Void> {

    private FacebookClient mFacebookClient;

    @Override
    protected Void doInBackground(String... params) {
      try {
        if (null == params || null == params[0] || null == params[1])
          return null;

        Session session = Session.getActiveSession();
        mFacebookClient = new DefaultFacebookClient(session.getAccessToken());

        mFacebookClient.publish(params[0] + "/comments", String.class,
            Parameter.with("message", params[1]));
      } catch (Exception e) {
        e.printStackTrace();
        return null;
      }

      return null;
    }
  }

}
