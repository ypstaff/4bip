package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.widget.FrameLayout;

/**
 * @author Muhammad
 * 
 */
public class MainMenuActivity extends GeneralActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main_menu);

    getFragmentManager().beginTransaction()
        .add(R.id.buttons_frame, mFBFragment).commit();

    final FrameLayout box1 = (FrameLayout) findViewById(R.id.stay_connectd_box_frame);
    final DrawView rec1 = new DrawView(this, DrawView.Color.Blue,
        DrawView.Position.Center);
    box1.addView(rec1);

    final FrameLayout box2 = (FrameLayout) findViewById(R.id.snake_box_frame);
    final DrawView rec2 = new DrawView(this, DrawView.Color.Orange,
        DrawView.Position.Center);
    box2.addView(rec2);

    final FrameLayout box3 = (FrameLayout) findViewById(R.id.exit_box_frame);
    final DrawView rec3 = new DrawView(this, DrawView.Color.Green,
        DrawView.Position.Center);
    box3.addView(rec3);

    refreshActivity();
  }

  @Override
  public void onResume() {
    super.onResume(); // Always call the superclass method first
    refreshActivity();
  }

  private void refreshActivity() {
    setTitle(R.string.main_menu);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    return true;
  }

  @Override
  public boolean onPrepareOptionsMenu(Menu menu) {
    return false;
  }

  @Override
  public void onOperation(final int c) {
    super.onOperation(c);
    final Intent intent;
    switch (OpCodeInterpreter.getOp(c)) {
    case BLUE:
      makeSound(0);
      intent = new Intent(this, TypingActivity.class);
      startActivity(intent);
      break;
    case BLUE_ORANGE:
      break;
    case BLUE_RED:
      break;
    case GREEN:
      makeSound(0);
      finishAffinity();
      break;
    case GREEN_RED:
      break;
    case INVALID:
      break;
    case LONG_BLUE:
      break;
    case LONG_GREEN:
      break;
    case LONG_ORANGE:
      break;
    case LONG_RED:
      break;
    case ORANGE:
      makeSound(0);
      intent = getPackageManager().getLaunchIntentForPackage(
          "il.ac.technion.cs.ssdl.cs234311.yp09.snake");
      startActivity(intent);
      break;
    case ORANGE_GREEN:
      break;
    case RED:
      break;
    default:
      break;
    }
  }

  @Override
  public synchronized void onPress(int c) {
    // do nothing
  }

  @Override
  public synchronized void onRelease(int c) {
    // do nothing
  }

  @Override
  public void onHeld(int c) {
    // do nothing
  }

}
