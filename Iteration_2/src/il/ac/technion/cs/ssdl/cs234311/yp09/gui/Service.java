package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 15/1/2014
 * 
 */
public class Service {
  private int serviceIcon;
  private final String serviceText;

  /**
   * @param serviceIcon
   *          icon of the service
   * @param serviceText
   *          text of the service
   */
  public Service(final int serviceIcon, final String serviceText) {
    this.serviceIcon = serviceIcon;
    this.serviceText = serviceText;
  }

  /**
   * @return the icon of the service
   */
  public int getServiceIcon() {
    return serviceIcon;
  }

  /**
   * @param iconId
   *          the icon of the service
   */
  public void setServiceIcon(final int iconId) {
    this.serviceIcon = iconId;
  }

  /**
   * @return the text of the service
   */
  public String getServiceText() {
    return serviceText;
  }

  @Override
  public String toString() {
    return serviceIcon + "\n" + serviceText;
  }
}