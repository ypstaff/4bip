package il.ac.technion.cs.ssdl.cs234311.yp09.messages;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * @author daniel Database helper class
 */
public class MessageSQLiteHelper extends SQLiteOpenHelper {

  static final String TABLE_MESSAGES = "Messages";
  static final String COLUMN_TYPE = "type";
  static final String COLUMN_CONTACT = "contact";
  static final String COLUMN_BODY = "body";
  static final String COLUMN_DATE = "date";

  private static final String DATABASE_NAME = "Commments.db";

  private String tableName;

  /**
   * @param context
   *          - The database parent activity
   * @param name
   *          - The database name
   */
  public MessageSQLiteHelper(Context context, String name) {

    super(context, name + DATABASE_NAME, null, 1);
    this.tableName = name;
  }

  @Override
  public void onCreate(SQLiteDatabase db) {
    db.execSQL("create table " + tableName + TABLE_MESSAGES + " ("
        + COLUMN_TYPE + " integer not null," + COLUMN_BODY + " text not null, "
        + COLUMN_CONTACT + " text not null, " + COLUMN_DATE + " text not null"
        + ");");

  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    db.execSQL("DROP TABLE IF EXISTS " + tableName + TABLE_MESSAGES);
    onCreate(db);

  }

}
