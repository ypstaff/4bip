package il.ac.technion.cs.ssdl.cs234311.yp09.facebookFeedGui;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.Controller;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.DrawView;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.GeneralActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.LanguageActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.LongFragment;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.OpCodeInterpreter;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.ProgressBarFragment;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.SettingsActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.TypingActivity;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.LoggingBehavior;
import com.facebook.Session;
import com.facebook.Session.OpenRequest;
import com.facebook.SessionLoginBehavior;
import com.facebook.SessionState;
import com.facebook.Settings;

/**
 * Main activity for managing sessions in Facebook
 * 
 * @author Muhammad and Owais
 * 
 */
public class FeedOptionsActivity extends GeneralActivity {

  private TextView loginOutView;
  private TextView showPostsView;

  @SuppressWarnings("synthetic-access")
  private Session.StatusCallback statusCallback = new SessionStatusCallback();

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_feed_options);

    final LongFragment longFragment = LongFragment
        .newInstance(LongFragment.Screen.FeedOptions);
    getFragmentManager().beginTransaction()
        .add(R.id.long_press_info_frame, longFragment).commit();

    final ProgressBarFragment progressBarFragment = ProgressBarFragment
        .newInstance(ProgressBarFragment.Screen.FeedOptions);
    getFragmentManager().beginTransaction()
        .add(R.id.progress_bar_frame, progressBarFragment).commit();

    getFragmentManager().beginTransaction()
        .add(R.id.buttons_frame, mFBFragment).commit();

    final FrameLayout box1 = (FrameLayout) findViewById(R.id.login_logout_box_frame);
    final DrawView rec1 = new DrawView(this, DrawView.Color.Blue,
        DrawView.Position.Center);
    box1.addView(rec1);

    final FrameLayout box2 = (FrameLayout) findViewById(R.id.show_posts_box_frame);
    final DrawView rec2 = new DrawView(this, DrawView.Color.Orange,
        DrawView.Position.Center);
    box2.addView(rec2);

    loginOutView = (TextView) findViewById(R.id.login_logout_option);
    showPostsView = (TextView) findViewById(R.id.show_posts_option);

    Settings.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);

    Session session = Session.getActiveSession();
    if (session == null) {
      if (savedInstanceState != null)
        session = Session.restoreSession(this, null, statusCallback,
            savedInstanceState);

      if (session == null)
        session = new Session(this);
      Session.setActiveSession(session);
      if (session.getState().equals(SessionState.CREATED_TOKEN_LOADED))
        session.openForRead(new Session.OpenRequest(this)
            .setCallback(statusCallback));
    }

    updateView();

    refreshActivity();

  }

  private void updateView() {
    Session session = Session.getActiveSession();

    if (session.isOpened())
      loginOutView.setText(R.string.logout_feed);
    else
      loginOutView.setText(R.string.login_feed);
  }

  @Override
  public void onStart() {
    super.onStart();
    Session.getActiveSession().addCallback(statusCallback);
  }

  @Override
  public void onStop() {
    super.onStop();
    Session.getActiveSession().removeCallback(statusCallback);
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    Session.getActiveSession().onActivityResult(this, requestCode, resultCode,
        data);
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    Session session = Session.getActiveSession();
    Session.saveSession(session, outState);
  }

  private void refreshActivity() {

    setTitle(R.string.news_feed);

    updateView();
    showPostsView.setText(R.string.show_posts);

  }

  @Override
  public void onResume() {
    super.onResume(); // Always call the superclass method first
    refreshActivity();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    return true;
  }

  @Override
  public boolean onPrepareOptionsMenu(Menu menu) {
    return false;
  }

  @Override
  public void onOperation(final int c) {
    Session session;
    super.onOperation(c);
    final Intent intent;
    switch (OpCodeInterpreter.getOp(c)) {
    case BLUE:
      makeSound(0);
      session = Session.getActiveSession();
      if (session.isOpened())
        onLogoutSelect();
      else
        onLoginSelect();
      break;
    case BLUE_ORANGE:
      break;
    case BLUE_RED:
      break;
    case GREEN:
      break;
    case GREEN_RED:
      break;
    case INVALID:
      break;
    case LONG_BLUE:
      makeSound(1);
      intent = new Intent(this, SettingsActivity.class);
      intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
      intent.putExtra(LanguageActivity.languageKey, Controller.language);
      startActivity(intent);
      break;
    case LONG_GREEN:
      break;
    case LONG_ORANGE:
      makeSound(1);
      intent = new Intent(this, TypingActivity.class);
      intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
      startActivity(intent);
      break;
    case LONG_RED:
      makeSound(1);
      finish();
      overridePendingTransition(0, 0);
      break;
    case ORANGE:
      makeSound(0);
      session = Session.getActiveSession();
      if (session.isOpened())
        onShowPostSelect();
      else
        Toast.makeText(this, "Please login first!", Toast.LENGTH_SHORT).show();
      break;
    case ORANGE_GREEN:
      break;
    case RED:
      break;
    default:
      break;
    }
  }

  private void onLogoutSelect() {
    Session session = Session.getActiveSession();
    if (!session.isClosed())
      session.closeAndClearTokenInformation();

    updateView();
  }

  private void onShowPostSelect() {
    Intent intent = new Intent(this, PostsActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
    startActivity(intent);
  }

  private void onLoginSelect() {
    Session session = Session.getActiveSession();
    if (!session.isOpened() && !session.isClosed()) {
      OpenRequest op = new Session.OpenRequest(this);

      op.setLoginBehavior(SessionLoginBehavior.SUPPRESS_SSO);
      op.setCallback(null);

      List<String> permissions = new ArrayList<String>();
      permissions.add("user_status");
      permissions.add("read_stream");
      permissions.add("friends_status");
      permissions.add("publish_stream");
      permissions.add("user_likes");
      permissions.add("friends_likes");
      permissions.add("email");
      permissions.add("user_birthday");
      op.setPermissions(permissions);

      Session newSession = new Session.Builder(FeedOptionsActivity.this)
          .build();
      Session.setActiveSession(newSession);
      newSession.openForPublish(op);
    } else
      Session.openActiveSession(this, true, statusCallback);

    updateView();
  }

  private class SessionStatusCallback implements Session.StatusCallback {
    @SuppressWarnings("synthetic-access")
    @Override
    public void call(Session session, SessionState state, Exception exception) {
      updateView();
    }
  }

}
