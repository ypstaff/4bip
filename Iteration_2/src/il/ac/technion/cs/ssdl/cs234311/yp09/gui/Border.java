package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import il.ac.technion.cs.ssdl.cs234311.yp09.gui.BorderedTextView.Style;

/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 3/1/2014
 * 
 */
public class Border {
  private final int orientation;
  private final int width;
  private final int color;
  private final Style style;

  /**
   * @param orientation
   *          border's orientation
   * @param width
   *          border's width
   * @param color
   *          border's color
   * @param style
   *          border's style
   */
  public Border(final int orientation, final int width, final int color,
      final Style style) {
    this.orientation = orientation;
    this.width = width;
    this.color = color;
    this.style = style;
  }

  /**
   * @return width of the border
   */
  public int getWidth() {
    return width;
  }

  /**
   * @return color of the border
   */
  public int getColor() {
    return color;
  }

  /**
   * @return style of the border
   */
  public Style getStyle() {
    return style;
  }

  /**
   * @return orientation of the border
   */
  public int getOrientation() {
    return orientation;
  }

}
