package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;

/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 17/1/2014
 * 
 */
public class ServiceListViewAdapter extends ArrayAdapter<Service> {

  Context context;

  /**
   * @param context
   *          current context
   * @param resourceId
   *          identifier of the resource
   * @param items
   *          list of services
   */
  public ServiceListViewAdapter(final Context context, final int resourceId,
      final List<Service> items) {
    super(context, resourceId, items);
    this.context = context;
  }

  /* private view holder class */
  private class ViewHolder {
    public ViewHolder() {
      // TODO Auto-generated constructor stub
    }

    ImageView serviceIcon;
    TextView serviceText;
  }

  @Override
  public View getView(final int position, View convertView,
      final ViewGroup parent) {

    ViewHolder holder = null;
    View $ = convertView;
    final Service serviceRow = getItem(position);

    final LayoutInflater mInflater = (LayoutInflater) context
        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    if ($ == null) {
      $ = mInflater.inflate(R.layout.listview_layout, null);
      holder = new ViewHolder();
      holder.serviceText = (TextView) $.findViewById(R.id.service_text);
      holder.serviceIcon = (ImageView) $.findViewById(R.id.service_icon);
      $.setTag(holder);
    } else
      holder = (ViewHolder) $.getTag();

    holder.serviceText.setText(serviceRow.getServiceText());
    holder.serviceIcon.setImageResource(serviceRow.getServiceIcon());

    return $;

  }

}
