package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import android.app.Fragment;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 4/1/2014
 * 
 */
public class AutoCompleteFragment extends Fragment {

  private FontFitTextView borderedView;

  @Override
  public View onCreateView(final LayoutInflater inflater,
      final ViewGroup container, final Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    final View $ = inflater.inflate(R.layout.auto_complete, container, false);

    borderedView = (FontFitTextView) $.findViewById(R.id.auto_complete_view);
    borderedView
        .setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);

    final FrameLayout box1 = (FrameLayout) $
        .findViewById(R.id.auto_complete_box1_frame);
    final DrawView rec1 = new DrawView(getActivity(), DrawView.Color.Blue,
        DrawView.Position.Right);
    box1.addView(rec1);

    final FrameLayout box2 = (FrameLayout) $
        .findViewById(R.id.auto_complete_box2_frame);
    final DrawView rec2 = new DrawView(getActivity(), DrawView.Color.Red,
        DrawView.Position.Left);
    box2.addView(rec2);

    final FrameLayout icon = (FrameLayout) $
        .findViewById(R.id.switch_icon_frame);
    final ImageView img = new ImageView(getActivity());
    img.setImageResource(R.drawable.up_down);
    icon.addView(img);

    return $;
  }

  /**
   * @param options
   *          A String composed by concatenating the 3 auto-completed options
   */
  public void setOptionsText(String options) {
    borderedView.setText(options);
    borderedView.resizeText(borderedView.getWidth(), borderedView.getHeight());
  }
}
