package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;

/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 17/1/2014
 * 
 */
public class LanguageListViewAdapter extends ArrayAdapter<Language> {

  Context context;

  /**
   * @param context
   *          current context
   * @param resourceId
   *          resource identifier
   * @param items
   *          list of the supported languages
   */
  public LanguageListViewAdapter(final Context context, final int resourceId,
      final List<Language> items) {
    super(context, resourceId, items);
    this.context = context;
  }

  /* private view holder class */
  private class ViewHolder {
    public ViewHolder() {
      // TODO Auto-generated constructor stub
    }

    ImageView languageIcon;
    TextView language;
  }

  @Override
  public View getView(final int position, View convertView,
      final ViewGroup parent) {

    ViewHolder holder = null;
    View $ = convertView;
    final Language languageRow = getItem(position);

    final LayoutInflater mInflater = (LayoutInflater) context
        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    if ($ == null) {
      $ = mInflater.inflate(R.layout.language_listview, null);
      holder = new ViewHolder();
      holder.language = (TextView) $.findViewById(R.id.language_id);
      holder.languageIcon = (ImageView) $.findViewById(R.id.language_icon);
      $.setTag(holder);
    } else
      holder = (ViewHolder) $.getTag();

    holder.language.setText(languageRow.getLanguage());
    holder.languageIcon.setImageResource(languageRow.getIconId());

    return $;

  }

}
