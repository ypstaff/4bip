package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import android.os.Bundle;
import android.view.Menu;
import android.widget.FrameLayout;
import android.widget.TextView;

/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 16/1/2014
 * 
 */
public class BluetoothActivity extends GeneralActivity {

  private TextView bluetoothOn;
  private TextView bluetoothOff;
  private TextView enableConnection;
  private TextView closeConnection;

  @Override
  protected void onCreate(final Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_bluetooth);

    final LongFragment longFragment = LongFragment
        .newInstance(LongFragment.Screen.Bluetooth);
    getFragmentManager().beginTransaction()
        .add(R.id.long_press_info_frame, longFragment).commit();

    final ProgressBarFragment progressBarFragment = ProgressBarFragment
        .newInstance(ProgressBarFragment.Screen.Bluetooth);
    getFragmentManager().beginTransaction()
        .add(R.id.progress_bar_frame, progressBarFragment).commit();

    getFragmentManager().beginTransaction()
        .add(R.id.buttons_frame, mFBFragment).commit();

    final FrameLayout box1 = (FrameLayout) findViewById(R.id.turn_on_frame);
    final DrawView rec1 = new DrawView(this, DrawView.Color.Blue,
        DrawView.Position.Center);
    box1.addView(rec1);

    final FrameLayout box2 = (FrameLayout) findViewById(R.id.turn_off_frame);
    final DrawView rec2 = new DrawView(this, DrawView.Color.Orange,
        DrawView.Position.Center);
    box2.addView(rec2);

    final FrameLayout box3 = (FrameLayout) findViewById(R.id.enable_connection_frame);
    final DrawView rec3 = new DrawView(this, DrawView.Color.Green,
        DrawView.Position.Center);
    box3.addView(rec3);

    final FrameLayout box4 = (FrameLayout) findViewById(R.id.close_connection_frame);
    final DrawView rec4 = new DrawView(this, DrawView.Color.Red,
        DrawView.Position.Center);
    box4.addView(rec4);

    bluetoothOn = (TextView) findViewById(R.id.turn_bluetooth_on);
    bluetoothOff = (TextView) findViewById(R.id.turn_bluetooth_off);
    enableConnection = (TextView) findViewById(R.id.enable_connection);
    closeConnection = (TextView) findViewById(R.id.close_connection);

    refreshActivity();
  }

  @Override
  public boolean onCreateOptionsMenu(final Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.bluetooth, menu);
    return true;
  }

  @Override
  public void onResume() {
    super.onResume(); // Always call the superclass method first

    refreshActivity();

  }

  @Override
  public boolean onPrepareOptionsMenu(Menu menu) {
    return false;
  }

  @Override
  public void onOperation(final int c) {
    super.onOperation(c);
    switch (OpCodeInterpreter.getOp(c)) {
    case BLUE:
      makeSound(0);
      break;
    case BLUE_ORANGE:
      break;
    case BLUE_RED:
      break;
    case GREEN:
      makeSound(0);
      break;
    case GREEN_RED:
      break;
    case INVALID:
      break;
    case LONG_BLUE:
      break;
    case LONG_GREEN:
      break;
    case LONG_ORANGE:
      break;
    case LONG_RED:
      makeSound(1);
      finish();
      overridePendingTransition(0, 0);
      break;
    case ORANGE:
      makeSound(0);
      break;
    case ORANGE_GREEN:
      break;
    case RED:
      makeSound(0);
      break;
    default:
      break;
    }
  }

  private void refreshActivity() {

    setTitle(R.string.title_activity_bluetooth);

    bluetoothOn.setText(R.string.turn_bluetooth_on);
    bluetoothOff.setText(R.string.turn_bluetooth_off);
    enableConnection.setText(R.string.enable_connection);
    closeConnection.setText(R.string.close_connection);

  }
}
