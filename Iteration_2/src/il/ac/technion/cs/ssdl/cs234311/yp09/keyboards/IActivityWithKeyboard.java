package il.ac.technion.cs.ssdl.cs234311.yp09.keyboards;

/**
 * An interface which all the activities that intend to use KeyboardFragment
 * must implement
 * 
 * @date 4/1/2013
 * @email owais.musa@gmail.com
 * @author Owais Musa
 * 
 */
public interface IActivityWithKeyboard {

  /**
   * 
   * @return the keyboard listener
   */
  public IKeyboardListener getKeyboardListener();

}
