package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import android.app.Fragment;
import android.graphics.PorterDuff.Mode;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 3/1/2014
 * 
 */
public class ProgressBarFragment extends Fragment {

  private static final String SCREEN_KEY = "screen_key";

  /**
   * @author Muhammad
   * 
   */
  public enum Screen {
    /**
     * Typing screen
     */
    Typing, /**
     * Login screen
     */
    Login, /**
     * Contact Screen
     */
    Contact, /**
     * Settings Screen
     */
    Settings, /**
     * Language Screen
     */
    Language, /**
     * Service Screen
     */
    Service, /**
     * Bluetooth screen
     */
    Bluetooth, /**
     * Contact History screen
     */
    ContactHistory, /**
     * Conversation screen
     */
    Conversation, /**
     * Volume screen
     */
    Volume, /**
     * Posts screen
     */
    Posts, /**
     * Feed Options screen
     */
    FeedOptions
  }

  /**
   * @param type
   *          type of the current screen
   * @return a fragment of the progress bars
   */
  public static ProgressBarFragment newInstance(final Screen type) {
    final ProgressBarFragment progressBarFragment = new ProgressBarFragment();
    final Bundle bundle = new Bundle();
    bundle.putSerializable(SCREEN_KEY, type);
    progressBarFragment.setArguments(bundle);

    return progressBarFragment;
  }

  @Override
  public View onCreateView(final LayoutInflater inflater,
      final ViewGroup container, final Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    return inflater.inflate(R.layout.progressbars, container, false);
  }

  @Override
  public void onStart() {
    super.onStart();
    final Screen type = (Screen) getArguments().getSerializable(SCREEN_KEY);
    if (type == Screen.Settings || type == Screen.Conversation) {
      ((ProgressBar) getActivity().findViewById(R.id.progressBar2))
          .setVisibility(View.INVISIBLE);
      ((ProgressBar) getActivity().findViewById(R.id.progressBar3))
          .setVisibility(View.INVISIBLE);
    }
    if (type == Screen.Service || type == Screen.FeedOptions)
      ((ProgressBar) getActivity().findViewById(R.id.progressBar3))
          .setVisibility(View.INVISIBLE);

    if (type == Screen.Bluetooth || type == Screen.Language
        || type == Screen.Volume) {
      ((ProgressBar) getActivity().findViewById(R.id.progressBar1))
          .setVisibility(View.INVISIBLE);
      ((ProgressBar) getActivity().findViewById(R.id.progressBar2))
          .setVisibility(View.INVISIBLE);
      ((ProgressBar) getActivity().findViewById(R.id.progressBar3))
          .setVisibility(View.INVISIBLE);
    }

    ((ProgressBar) getActivity().findViewById(R.id.progressBar1))
        .getProgressDrawable().setColorFilter(
            getResources().getColor(R.color.blue), Mode.SRC_IN);
    ((ProgressBar) getActivity().findViewById(R.id.progressBar2))
        .getProgressDrawable().setColorFilter(
            getResources().getColor(R.color.orange), Mode.SRC_IN);
    ((ProgressBar) getActivity().findViewById(R.id.progressBar3))
        .getProgressDrawable().setColorFilter(
            getResources().getColor(R.color.green), Mode.SRC_IN);
    ((ProgressBar) getActivity().findViewById(R.id.progressBar4))
        .getProgressDrawable().setColorFilter(
            getResources().getColor(R.color.red), Mode.SRC_IN);

  }

}
