package il.ac.technion.cs.ssdl.cs234311.yp09.utilities;

/**
 * @author Tal class for understanding the server- a common language
 */
public class CommonLang {

  /**
   * code for Illegal access to a field while building xml
   */
  public static final int ILLEGAL_EXCESS = 507;
  /**
   * code for bad parser configuration while building xml
   */
  public static final int PARSER_CONF = 508;
  /**
   * code for bad transformer configuration while building xml
   */
  public static final int TRANSFORMER_CONF = 509;
  /**
   * code for transformer Exception while building xml
   */
  public static final int TRANSFORMER_EXCEP = 510;
  /**
   * code for - there is no data on the server
   */
  public static final int NO_DATA = 444;
  /**
   * code for Database had an error
   */
  public static final int DATABASE_ERROR = 511;
  /**
   * code for - the int that was sent to server (to describe the kind of data
   * posted) was Illegel
   */
  public static final int BAD_INT_PARAM = 512;

}
