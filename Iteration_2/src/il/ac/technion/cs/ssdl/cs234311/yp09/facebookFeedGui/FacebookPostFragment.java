package il.ac.technion.cs.ssdl.cs234311.yp09.facebookFeedGui;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import il.ac.technion.cs.ssdl.cs234311.yp09.facebookFeed.FacebookManager;
import il.ac.technion.cs.ssdl.cs234311.yp09.facebookFeed.FacebookPost;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

/**
 * Represents a facebook post in GUI
 * 
 * @author Owais Musa
 * 
 */
public class FacebookPostFragment extends Fragment {

  private final static String POST_ID = "__POST_ID";
  private final static String POST_AUTHOR_IMG_URL_KEY = "__POST_AUTHOR_IMG_URL_KEY";
  private final static String POST_AUTHOR_NAME_KEY = "__POST_AUTHOR_NAME_KEY";
  private final static String POST_TIME_KEY = "__POST_TIME_KEY";
  private final static String POST_TEXT_KEY = "__POST_TEXT_KEY";
  private final static String POST_LIKES_NUM_KEY = "__POST_LIKES_NUM_KEY";
  private final static String POST_COMMENTS_NUM_KEY = "__POST_COMMENTS_NUM_KEY";
  private final static String POST_SHARES_NUM_KEY = "__POST_SHARES_NUM_KEY";

  /**
   * @param post
   *          the post to be shown
   * @return new instance of FacebookPostFragment
   */
  public static FacebookPostFragment getInstance(FacebookPost post) {
    FacebookPostFragment $ = new FacebookPostFragment();

    Bundle args = new Bundle();
    args.putString(POST_ID, post.getPOstId());
    args.putString(POST_AUTHOR_IMG_URL_KEY, post.getAuthorImageUrl());
    args.putString(POST_AUTHOR_NAME_KEY, post.getAuthorName());
    args.putString(POST_TIME_KEY, post.getCreatingTime().toString());
    args.putString(POST_TEXT_KEY, post.getText());
    args.putLong(POST_LIKES_NUM_KEY, post.getLikesNum());
    args.putLong(POST_COMMENTS_NUM_KEY, post.getCommentsNum());
    args.putLong(POST_SHARES_NUM_KEY, post.getSharesNum());

    $.setArguments(args);
    return $;
  }

  // It is not private for better performance in mLikeButton.setOnClickListener
  String mPostId;

  private ImageView mImg;
  private TextView mAuthorNameTextView;
  private TextView mPostText;
  private TextView mTimeTextView;
  private TextView mlikesTextView;
  private TextView mCommentsTextView;
  private TextView mSharesTextView;
  private Button mLikeButton;
  private Button mCommentButton;
  private Button mShareButton;
  private TableRow mMainTableRow;
  private TableRow mPostTextTableRow;
  private TableRow mPostDetailsTableRow;
  private TableRow mPostActionsTableRow;
  private TableRow mEmptyTableRow;

  private int blueColor;
  private int orangeColor;
  private int greenColor;
  private int redColor;
  private int blackColor;

  private boolean turnedOn = false;

  @Override
  public void onResume() {
    super.onResume(); // Always call the superclass method first
    if (turnedOn)
      turnOn();

    turnedOn = false;
  }

  /**
   * Call this method if you want to turn on this fragment when it still not
   * attached!
   */
  public void toBeTurnedOn() {
    turnedOn = true;
  }

  /**
   * Convert buttons background color to green
   */
  public void turnOn() {
    mLikeButton.setBackgroundColor(greenColor);
    mCommentButton.setBackgroundColor(greenColor);
    mShareButton.setBackgroundColor(greenColor);
  }

  /**
   * Convert buttons background color to black
   */
  public void turnOff() {
    mLikeButton.setBackgroundColor(blackColor);
    mCommentButton.setBackgroundColor(blackColor);
    mShareButton.setBackgroundColor(blackColor);
    turnedOn = false;
  }

  private void loadColors() {
    blueColor = getResources().getColor(R.color.blue);
    orangeColor = getResources().getColor(R.color.orange);
    greenColor = getResources().getColor(R.color.green);
    redColor = getResources().getColor(R.color.red);
    blackColor = getResources().getColor(R.color.black);
  }

  /**
   * @return fragment hegith
   */
  public int calcHeight() {
    int $ = mMainTableRow.getHeight();
    $ += mPostTextTableRow.getHeight();
    $ += mPostDetailsTableRow.getHeight();
    $ += mPostActionsTableRow.getHeight();
    $ += mEmptyTableRow.getHeight();

    return $;
  }

  /**
   * @param switched
   *          if true then show enabled buttons colors!
   */
  public void updateButtonsColor(boolean switched) {
    if (!switched) {
      turnOn();
      return;
    }

    mLikeButton.setBackgroundColor(blueColor);
    mCommentButton.setBackgroundColor(orangeColor);
    mShareButton.setBackgroundColor(redColor);
  }

  /**
   * This is relevant when we switch to facebook operations, this method asks
   * from facebook SDK to add like to this post
   */
  public void likeCommand() {
    int likesNum = Integer.valueOf(mlikesTextView.getText().toString())
        .intValue();

    if (mLikeButton.getText().toString().equals("Like")) {
      FacebookManager.addLike(getActivity(), mPostId);
      mLikeButton.setText("Unlike");
      likesNum++;
      mlikesTextView.setText(likesNum + "");
    } else {
      FacebookManager.unLike(getActivity(), mPostId);
      mLikeButton.setText("Like");
      likesNum--;
      mlikesTextView.setText(likesNum + "");
    }
  }

  /**
   * This is relevant when we switch to facebook operations, this method asks
   * from facebook SDK to share this post
   */
  public void shareCommand() {
    FacebookManager.share(getActivity(), mPostId);
  }

  private void updateTextViewsContent() {
    mAuthorNameTextView.setText(getArguments().getString(POST_AUTHOR_NAME_KEY));
    mPostText.setText(getArguments().getString(POST_TEXT_KEY));
    mTimeTextView.setText(getArguments().getString(POST_TIME_KEY));
    mCommentsTextView.setText(""
        + getArguments().getLong(POST_COMMENTS_NUM_KEY));
    mSharesTextView.setText("" + getArguments().getLong(POST_SHARES_NUM_KEY));

    mPostId = getArguments().getString(POST_ID);

    long likes = getArguments().getLong(POST_LIKES_NUM_KEY);
    mlikesTextView.setText("" + Math.abs(likes));
    if (likes < 0)
      mLikeButton.setText("Unlike");
  }

  @Override
  public View onCreateView(final LayoutInflater inflater,
      final ViewGroup container, final Bundle savedInstanceState) {
    final View $ = inflater.inflate(R.layout.fragment_facebook_post, container,
        false);

    loadColors();

    mImg = (ImageView) $.findViewById(R.id.authorProfileImage);
    ImageGetter imageGetter = new ImageGetter();
    mImg.setImageDrawable(imageGetter.getDrawable(getArguments().getString(
        POST_AUTHOR_IMG_URL_KEY)));

    mAuthorNameTextView = (TextView) $
        .findViewById(R.id.postAuthorNameTextView);

    mPostText = (TextView) $.findViewById(R.id.postTextTextView);
    mTimeTextView = (TextView) $.findViewById(R.id.postTimeTextView);
    mlikesTextView = (TextView) $.findViewById(R.id.postLikesNumTextView);
    mCommentsTextView = (TextView) $.findViewById(R.id.postCommentsNumTextView);
    mSharesTextView = (TextView) $.findViewById(R.id.postSharesNumTextView);

    mLikeButton = (Button) $.findViewById(R.id.likeButton);
    mCommentButton = (Button) $.findViewById(R.id.commentButton);
    mShareButton = (Button) $.findViewById(R.id.shareButton);

    mMainTableRow = (TableRow) $.findViewById(R.id.mainTableRow);
    mPostTextTableRow = (TableRow) $.findViewById(R.id.postTextTableRow);
    mPostDetailsTableRow = (TableRow) $.findViewById(R.id.postDetailsTableRow);
    mPostActionsTableRow = (TableRow) $.findViewById(R.id.postActionsTableRow);
    mEmptyTableRow = (TableRow) $.findViewById(R.id.emptyTableRow);

    updateTextViewsContent();
    return $;
  }
}
