package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

/**
 * @author Muhammad
 * 
 */
public class VolumeFragment extends Fragment {

  private static final String VOLUME = "il.ac.technion.cs.ssdl.cs234311.yp09.volume";

  /**
   * @param mute
   *          the sound's state (mute/not)
   * @return the fragment being built
   */
  public static VolumeFragment newInstance(boolean mute) {
    final VolumeFragment volumeFragment = new VolumeFragment();
    final Bundle bundle = new Bundle();
    bundle.putSerializable(VOLUME, Boolean.valueOf(mute));
    volumeFragment.setArguments(bundle);

    return volumeFragment;
  }

  @Override
  public View onCreateView(final LayoutInflater inflater,
      final ViewGroup container, final Bundle savedInstanceState) {

    final View $ = inflater.inflate(R.layout.volume_fragment, container, false);

    final FrameLayout plus_box_frame = (FrameLayout) $
        .findViewById(R.id.plus_box_frame);
    final DrawView rec_select = new DrawView(getActivity(), DrawView.Color.Red,
        DrawView.Position.Center);
    plus_box_frame.addView(rec_select);

    final FrameLayout minus_box_frame = (FrameLayout) $
        .findViewById(R.id.minus_box_frame);
    final DrawView rec_done = new DrawView(getActivity(), DrawView.Color.Blue,
        DrawView.Position.Center);
    minus_box_frame.addView(rec_done);

    final FrameLayout mute_box_frame_1 = (FrameLayout) $
        .findViewById(R.id.mute_box_frame_1);
    final DrawView rec_mute_1 = new DrawView(getActivity(),
        DrawView.Color.Orange, DrawView.Position.Right);
    mute_box_frame_1.addView(rec_mute_1);

    final FrameLayout mute_box_frame_2 = (FrameLayout) $
        .findViewById(R.id.mute_box_frame_2);
    final DrawView rec_mute_2 = new DrawView(getActivity(),
        DrawView.Color.Green, DrawView.Position.Left);
    mute_box_frame_2.addView(rec_mute_2);

    final ImageView prog_view = (ImageView) $
        .findViewById(R.id.mute_icon_frame);

    Boolean type = (Boolean) getArguments().getSerializable(VOLUME);
    if (type.booleanValue())
      prog_view.setImageResource(R.drawable.mute);
    else
      prog_view.setImageResource(R.drawable.unmute);

    return $;

  }
}
