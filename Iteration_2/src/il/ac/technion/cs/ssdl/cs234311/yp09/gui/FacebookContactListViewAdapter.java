package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import il.ac.technion.cs.ssdl.cs234311.yp09.commsModule.FacebookItem;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 17/1/2014
 * 
 */
public class FacebookContactListViewAdapter extends ArrayAdapter<FacebookItem> {

  Context context;

  /**
   * @param context
   *          current context
   * @param resourceId
   *          resource identifier
   * @param items
   *          list of sms items
   */
  public FacebookContactListViewAdapter(final Context context,
      final int resourceId, final List<FacebookItem> items) {
    super(context, resourceId, items);
    this.context = context;
  }

  /* private view holder class */
  private class ViewHolder {
    public ViewHolder() {
      // TODO Auto-generated constructor stub
    }

    ImageView facebookImage;
    TextView contactName;
    TextView contactID;
  }

  @Override
  public View getView(final int position, View convertView,
      final ViewGroup parent) {

    ViewHolder holder = null;
    View $ = convertView;
    final FacebookItem facebookItem = getItem(position);

    final LayoutInflater mInflater = (LayoutInflater) context
        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    if ($ == null) {
      $ = mInflater.inflate(R.layout.contact_listview, null);
      holder = new ViewHolder();
      holder.contactName = (TextView) $.findViewById(R.id.name);
      holder.contactID = (TextView) $.findViewById(R.id.number);
      holder.facebookImage = (ImageView) $.findViewById(R.id.type);
      $.setTag(holder);
    } else
      holder = (ViewHolder) $.getTag();

    holder.contactName.setText(facebookItem.contactName);

    if (facebookItem.contactPresence.equals("available")) {
      holder.facebookImage.setImageDrawable(context.getResources().getDrawable(
          R.drawable.available));
      holder.contactID.setText("available");
    } else if (facebookItem.contactPresence.equals("unavailable")) {
      holder.facebookImage.setImageDrawable(context.getResources().getDrawable(
          R.drawable.signout));
      holder.contactID.setText("unavailable");
    } else if (facebookItem.contactPresence.equals("dnd")) {
      holder.facebookImage.setImageDrawable(context.getResources().getDrawable(
          R.drawable.busy));
      holder.contactID.setText("busy");
    } else if (facebookItem.contactPresence.equals("chat")) {
      holder.facebookImage.setImageDrawable(context.getResources().getDrawable(
          R.drawable.available));
      holder.contactID.setText("available");
    } else if (facebookItem.contactPresence.equals("xa")) {
      holder.facebookImage.setImageDrawable(context.getResources().getDrawable(
          R.drawable.away));
      holder.contactID.setText("away");
    } else if (facebookItem.contactPresence.equals("away")) {
      holder.facebookImage.setImageDrawable(context.getResources().getDrawable(
          R.drawable.away));
      holder.contactID.setText("away");
    } else {
      holder.facebookImage.setImageDrawable(context.getResources().getDrawable(
          R.drawable.signout));
      holder.contactID.setText("unavailable");
    }

    return $;

  }

}
