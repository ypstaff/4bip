package il.ac.technion.cs.ssdl.cs234311.yp09.facebookFeedGui;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.DrawView;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

/**
 * @author Muhammad
 * 
 */
public class FacebookLikeFragment extends Fragment {

  @Override
  public View onCreateView(final LayoutInflater inflater,
      final ViewGroup container, final Bundle savedInstanceState) {

    final View $ = inflater.inflate(R.layout.like_fragment, container, false);

    final FrameLayout box1 = (FrameLayout) $.findViewById(R.id.like_box1_frame);
    final DrawView rec1 = new DrawView(getActivity(), DrawView.Color.Orange,
        DrawView.Position.Right);
    box1.addView(rec1);

    final FrameLayout box2 = (FrameLayout) $.findViewById(R.id.like_box2_frame);
    final DrawView rec2 = new DrawView(getActivity(), DrawView.Color.Green,
        DrawView.Position.Left);
    box2.addView(rec2);

    return $;

  }

}
