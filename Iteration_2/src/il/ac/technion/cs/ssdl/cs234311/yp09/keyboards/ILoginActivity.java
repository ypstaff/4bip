package il.ac.technion.cs.ssdl.cs234311.yp09.keyboards;

/**
 * @author owais
 * 
 */
public interface ILoginActivity {

  /**
   * 
   */
  public void onLoginClick();

  /**
   * 
   */
  public void onSetFocusOnPassword();

  /**
   * 
   */
  public void onSetFocusOnUsername();
}
