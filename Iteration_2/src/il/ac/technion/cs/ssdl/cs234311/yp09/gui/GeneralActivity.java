package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import il.ac.technion.cs.ssdl.cs234311.yp09.bluetooth.BluetoothBroadcastReceiver;
import il.ac.technion.cs.ssdl.cs234311.yp09.commsModule.CommsModule;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.OpCodeInterpreter.Op;
import il.ac.technion.cs.ssdl.cs234311.yp09.utilities.SaveStatistics;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.ProgressBar;

/**
 * The activity that all other activities extend that includes common fields and
 * implementations.
 * 
 * @author Itamar
 * 
 */
public class GeneralActivity extends Activity implements OperationListener,
    PressListener {

  /**
   * The communication module that handles sending the message.
   */
  public static CommsModule commsModule;
  /**
   * Flag - assign true to save statistics.
   */
  public static boolean statisticsMode = true;
  private static final long VIBRATE_LENGTH = 100;
  Handler mHandler;
  protected FourButtonsFragment mFBFragment;
  static Timer timer;
  protected BluetoothBroadcastReceiver mReceiver;

  static boolean pressed = false;

  @Override
  protected void onCreate(final Bundle b) {
    super.onCreate(b);

    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    this.getWindow().setSoftInputMode(
        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

    mHandler = new Handler();

    mFBFragment = new FourButtonsFragment();
    mFBFragment.setHandler(mHandler);
    mFBFragment.setOpListener(this);
    mFBFragment.setPressListener(this);

    // keep screen alive - no dimming
    getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    mReceiver = new BluetoothBroadcastReceiver(this);
    registerToBluetooth();

  }

  @Override
  protected void onResume() {
    super.onResume();
    cleanAllProgressBars();
    registerToBluetooth();
    SaveStatistics.resumeOrStartApp();
  }

  @Override
  public void onBackPressed() {
    // do nothing
  }

  @Override
  public synchronized void onPress(int c) {
    final ProgressBar p;
    switch (c) {
    case 2:
      p = (ProgressBar) findViewById(R.id.progressBar1);
      break;
    case 4:
      p = (ProgressBar) findViewById(R.id.progressBar2);
      break;
    case 8:
      p = (ProgressBar) findViewById(R.id.progressBar3);
      break;
    case 16:
      p = (ProgressBar) findViewById(R.id.progressBar4);
      break;
    default:
      return;
    }
    p.setMax(2000);
    timer = new Timer();

    timer.schedule(new TimerTask() {
      @Override
      public void run() {
        if (p.getProgress() < p.getMax() - 10 && pressed)
          p.incrementProgressBy(10);
        else {
          if (!pressed)
            p.setProgress(0);
          super.cancel();
        }

      }
    }, 0, 10);
    pressed = true;

  }

  @Override
  public synchronized void onRelease(int c) {
    if (timer != null) {
      timer.cancel();
      timer.purge();
      timer = null;
    }
    pressed = false;
    cleanAllProgressBars();
  }

  @Override
  public void onHeld(int c) {
    ((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(VIBRATE_LENGTH);
  }

  @Override
  public void onOperation(int c) {
    if (!statisticsMode)
      return;
    switch (OpCodeInterpreter.getOp(c)) {
    case BLUE:
      SaveStatistics.clickOn(Op.BLUE);
      break;
    case BLUE_ORANGE:
      SaveStatistics.clickOn(Op.BLUE_ORANGE);
      break;
    case BLUE_RED:
      SaveStatistics.clickOn(Op.BLUE_RED);
      break;
    case GREEN:
      SaveStatistics.clickOn(Op.GREEN);
      break;
    case GREEN_RED:
      SaveStatistics.clickOn(Op.GREEN_RED);
      break;
    case INVALID:
      break;
    case LONG_BLUE:
      SaveStatistics.clickOn(Op.LONG_BLUE);
      break;
    case LONG_GREEN:
      SaveStatistics.clickOn(Op.LONG_GREEN);
      break;
    case LONG_ORANGE:
      SaveStatistics.clickOn(Op.LONG_ORANGE);
      break;
    case LONG_RED:
      SaveStatistics.clickOn(Op.LONG_RED);
      break;
    case ORANGE:
      SaveStatistics.clickOn(Op.ORANGE);
      break;
    case ORANGE_GREEN:
      SaveStatistics.clickOn(Op.ORANGE_GREEN);
      break;
    case RED:
      SaveStatistics.clickOn(Op.RED);
      break;
    default:
      break;
    }

  }

  /**
   * An <code>OnTouchListener</code> that ignores touches.
   * 
   * @author Itamar
   * 
   */
  public class NoTouchListener implements OnTouchListener {
    @Override
    public boolean onTouch(View v, MotionEvent e) {
      return true;
    }
  }

  protected void makeSound(int i) {
    if (SplashActivity.mSoundPool == null)
      return;

    if (i == 0)
      SplashActivity.mSoundId = SplashActivity.mSoundPool.load(this,
          R.raw.short_sound, 2);
    else
      SplashActivity.mSoundId = SplashActivity.mSoundPool.load(this,
          R.raw.long_sound, 1);
    SplashActivity.mSoundPool
        .setOnLoadCompleteListener(new OnLoadCompleteListener() {
          @Override
          public void onLoadComplete(SoundPool soundPool, int sampleId,
              int status) {
            if (0 == status) {
              int streamID = -1;
              do
                SplashActivity.mSoundPool.play(SplashActivity.mSoundId,
                    VolumeActivity.volume, VolumeActivity.volume, 1, 0, 1.0f);
              while (streamID == 0);
              SplashActivity.mSoundPool.unload(SplashActivity.mSoundId);
            }
          }
        });
  }

  private static void clearProgressBar(final ProgressBar progressBar) {
    new Timer().schedule(new TimerTask() {

      @Override
      public void run() {

        progressBar.setProgress(0);

      }
    }, 0);
  }

  @Override
  protected void onPause() {
    super.onPause();
    unregisterReceiver(mReceiver);

    SaveStatistics.pauseAndSendStatistcs(this);
  }

  // Muhammad Watad
  protected void cleanAllProgressBars() {
    ProgressBar p;
    int bars[] = { R.id.progressBar1, R.id.progressBar2, R.id.progressBar3,
        R.id.progressBar4 };

    for (int i = 0; i < 4; i++) {
      p = (ProgressBar) findViewById(bars[i]);
      if (p != null)
        clearProgressBar(p);

    }
  }

  protected void registerToBluetooth() {
    registerReceiver(mReceiver, new IntentFilter("fourBIP"));
  }
}
